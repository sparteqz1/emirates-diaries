<?php

class ModelCommonHeader extends Model {
    
    public function getMenuCategories() {
        $query = $this->db->query("SELECT ncd.name, nc.category_id FROM " . DB_PREFIX . "newscategory_description ncd "
                . "LEFT JOIN " . DB_PREFIX . "newscategory nc ON (ncd.category_id = nc.category_id) "
                . "WHERE ncd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND nc.status = '1' ORDER BY nc.sort_order ASC");

        return $query->rows;
    }
    
     
    
}