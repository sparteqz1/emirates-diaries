<?php

class ModelMagazinesMagazines extends Model {
    
    public function getMagazines() {
        $query = $this->db->query("SELECT nid.name, ni.sort_order, nid.description, ni.cover, ni.issue_id "
                . "FROM " . DB_PREFIX . "newsissue_description nid "
                . "LEFT JOIN " . DB_PREFIX . "newsissue ni ON (nid.issue_id = ni.issue_id) "
                . "WHERE nid.language_id = '" . (int) $this->config->get('config_language_id') . "' "
                . "AND ni.status = '1' ORDER BY ni.sort_order DESC");

        return $query->rows;
    }
    
}