<?php

class ModelNewsNews extends Model {

    public function updateViewed($news_id) {
        $this->db->query("UPDATE " . DB_PREFIX . "news SET viewed = (viewed + 1) WHERE news_id = '" . (int) $news_id . "'");
    }

    public function getNews($news_id) {
        $query = $this->db->query("SELECT DISTINCT *, nd.name AS name, n.image, a.name AS author, a.image AS author_image,n.sort_order FROM " . DB_PREFIX . "news n"
                . " LEFT JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id) "
                . " LEFT JOIN " . DB_PREFIX . "author a ON (n.author_id = a.author_id) WHERE n.news_id = '" . (int) $news_id . "' AND nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND n.status = '1' ");

        if ($query->num_rows) {
            return array(
                'news_id' => $query->row['news_id'],
                'issue_id' => $query->row['issue_id'],
                'name' => $query->row['name'],
                'arrangement' => $query->row['arrangement'],
                'description' => $query->row['description'],
                'meta_title' => $query->row['meta_title'],
                'meta_description' => $query->row['meta_description'],
                'meta_keyword' => $query->row['meta_keyword'],
                'image' => $query->row['image'],
                'tag' => $query->row['tag'],
                'author_id' => $query->row['author_id'],
                'author_image' => $query->row['author_image'],
                'author' => $query->row['author'],
                'sort_order' => $query->row['sort_order'],
                'status' => $query->row['status'],
                'date_added' => $query->row['date_added'],
                'date_modified' => $query->row['date_modified'],
                'ago' => $this->time_elapsed_string($query->row['date_added']),
                'badge' => $this->getTag($query->row['tag']),
                'viewed' => $query->row['viewed']
            );
        } else {
            return false;
        }
    }

    public function getAllNews($data = array()) {
        $sql = "SELECT n.news_id";

        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_sub_category'])) {
                $sql .= " FROM " . DB_PREFIX . "newscategory_path cp LEFT JOIN " . DB_PREFIX . "news_to_newscategory n2c ON (cp.category_id = n2c.category_id)";
            } else {
                $sql .= " FROM " . DB_PREFIX . "news_to_newscategory n2c";
            }
            $sql .= " LEFT JOIN " . DB_PREFIX . "news n ON (n2c.news_id = n.news_id)";
        } else {
            $sql .= " FROM " . DB_PREFIX . "news n";
        }

        $sql .= " LEFT JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id)  WHERE nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND n.status = '1'";

        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_sub_category'])) {
                $sql .= " AND cp.path_id = '" . (int) $data['filter_category_id'] . "'";
            } else {
                $sql .= " AND n2c.category_id = '" . (int) $data['filter_category_id'] . "'";
            }
        }
        if (!empty($data['filter_issue_id'])) {
            
                $sql .= " AND n.issue_id = '" . (int) $data['filter_issue_id'] . "'";
        }

        if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
            $sql .= " AND (";

            if (!empty($data['filter_name'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

                foreach ($words as $word) {
                    $implode[] = "nd.name LIKE '%" . $this->db->escape($word) . "%'";
                }

                if ($implode) {
                    $sql .= " " . implode(" AND ", $implode) . "";
                }

                if (!empty($data['filter_description'])) {
                    $sql .= " OR nd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
                }
            }

            if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                $sql .= " OR ";
            }

            if (!empty($data['filter_tag'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_tag'])));

                foreach ($words as $word) {
                    $implode[] = "nd.tag LIKE '%" . $this->db->escape($word) . "%'";
                }

                if ($implode) {
                    $sql .= " " . implode(" AND ", $implode) . "";
                }
            }

            $sql .= ")";
        }

        if (!empty($data['filter_author_id'])) {
            $sql .= " AND n.author_id = '" . (int) $data['filter_author_id'] . "'";
        }

        $sql .= " GROUP BY n.news_id";

        $sort_data = array(
            'nd.name',
            'n.sort_order',
            'n.date_added'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            if ($data['sort'] == 'nd.name') {
                $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
            } else {
                $sql .= " ORDER BY " . $data['sort'];
            }
        } else {
            $sql .= " ORDER BY n.sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC, LCASE(nd.name) DESC";
        } else {
            $sql .= " ASC, LCASE(nd.name) ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $news_data = array();
//echo $sql;
        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $news_data[$result['news_id']] = $this->getNews($result['news_id']);
        }

        return $news_data;
    }

    public function getLatestNews($limit) {
        $news_data = $this->cache->get('news.latest.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int) $limit);

        if (!$news_data) {
            $sql = "SELECT n.news_id FROM " . DB_PREFIX . "news n WHERE n.status = '1' ORDER BY n.date_added DESC";

            if(isset($limit)) {
                $sql .= " LIMIT " . (int) $limit;
            }
            
            $query = $this->db->query($sql);

            foreach ($query->rows as $result) {
                $news_data[$result['news_id']] = $this->getNews($result['news_id']);
            }

            $this->cache->set('news.latest.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int) $limit, $news_data);
        }

        return $news_data;
    }

    public function getPopularNews($limit) {
        $news_data = $this->cache->get('news.popular.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int) $limit);

        if (!$news_data) {
            $sql = "SELECT n.news_id FROM " . DB_PREFIX . "news n WHERE n.status = '1'  ORDER BY n.viewed DESC, n.date_added DESC";

            if(isset($limit)) {
                $sql .= " LIMIT " . (int) $limit;
            }
            
            $query = $this->db->query($sql);

            foreach ($query->rows as $result) {
                $news_data[$result['news_id']] = $this->getNews($result['news_id']);
            }

            $this->cache->set('news.popular.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int) $limit, $news_data);
        }

        return $news_data;
    }

    public function getNewsAttributes($news_id) {
        $news_attribute_group_data = array();

        $news_attribute_group_query = $this->db->query("SELECT ag.attribute_group_id, agd.name FROM " . DB_PREFIX . "news_attribute na LEFT JOIN " . DB_PREFIX . "attribute a ON (na.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_group ag ON (a.attribute_group_id = ag.attribute_group_id) LEFT JOIN " . DB_PREFIX . "attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE na.news_id = '" . (int) $news_id . "' AND agd.language_id = '" . (int) $this->config->get('config_language_id') . "' GROUP BY ag.attribute_group_id ORDER BY ag.sort_order, agd.name");

        foreach ($news_attribute_group_query->rows as $news_attribute_group) {
            $news_attribute_data = array();

            $news_attribute_query = $this->db->query("SELECT a.attribute_id, ad.name, na.text FROM " . DB_PREFIX . "news_attribute na LEFT JOIN " . DB_PREFIX . "attribute a ON (na.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE na.news_id = '" . (int) $news_id . "' AND a.attribute_group_id = '" . (int) $news_attribute_group['attribute_group_id'] . "' AND ad.language_id = '" . (int) $this->config->get('config_language_id') . "' AND na.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY a.sort_order, ad.name");

            foreach ($news_attribute_query->rows as $news_attribute) {
                $news_attribute_data[] = array(
                    'attribute_id' => $news_attribute['attribute_id'],
                    'name' => $news_attribute['name'],
                    'text' => $news_attribute['text']
                );
            }

            $news_attribute_group_data[] = array(
                'attribute_group_id' => $news_attribute_group['attribute_group_id'],
                'name' => $news_attribute_group['name'],
                'attribute' => $news_attribute_data
            );
        }

        return $news_attribute_group_data;
    }

    public function getNewsImages($news_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_image WHERE news_id = '" . (int) $news_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getNewsRelated($news_id) {
        $news_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_related nr LEFT JOIN " . DB_PREFIX . "news n ON (nr.related_id = n.news_id)  WHERE nr.news_id = '" . (int) $news_id . "' AND n.status = '1' ORDER BY n.news_id DESC LIMIT 3");

        foreach ($query->rows as $result) {
            $news_data[$result['related_id']] = $this->getNews($result['related_id']);
        }

        return $news_data;
    }

    public function getNewsFeatured() {
        $news_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_featured nf LEFT JOIN " . DB_PREFIX . "news n ON (nf.news_id = n.news_id)  WHERE n.status = '1'  ORDER BY nf.featured_id ASC LIMIT 6");

        foreach ($query->rows as $result) {
            $news_data[$result['news_id']] = $this->getNews($result['news_id']);
        }

        return $news_data;
    }

    public function getCategories($news_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_to_newscategory WHERE news_id = '" . (int) $news_id . "'");

        return $query->rows;
    }

    public function getTotalNews($data = array()) {
        $sql = "SELECT COUNT(DISTINCT n.news_id) AS total";

        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_sub_category'])) {
                $sql .= " FROM " . DB_PREFIX . "newscategory_path cp LEFT JOIN " . DB_PREFIX . "news_to_newscategory n2c ON (cp.category_id = n2c.category_id)";
            } else {
                $sql .= " FROM " . DB_PREFIX . "news_to_newscategory n2c";
            }

            $sql .= " LEFT JOIN " . DB_PREFIX . "news n ON (n2c.news_id = n.news_id)";
        } else {
            $sql .= " FROM " . DB_PREFIX . "news n";
        }

        $sql .= " LEFT JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id) WHERE nd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND n.status = '1' ";

        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_sub_category'])) {
                $sql .= " AND cp.path_id = '" . (int) $data['filter_category_id'] . "'";
            } else {
                $sql .= " AND n2c.category_id = '" . (int) $data['filter_category_id'] . "'";
            }
        }

        if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
            $sql .= " AND (";

            if (!empty($data['filter_name'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

                foreach ($words as $word) {
                    $implode[] = "nd.name LIKE '%" . $this->db->escape($word) . "%'";
                }

                if ($implode) {
                    $sql .= " " . implode(" AND ", $implode) . "";
                }

                if (!empty($data['filter_description'])) {
                    $sql .= " OR nd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
                }
            }

            if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                $sql .= " OR ";
            }

            if (!empty($data['filter_tag'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_tag'])));

                foreach ($words as $word) {
                    $implode[] = "nd.tag LIKE '%" . $this->db->escape($word) . "%'";
                }

                if ($implode) {
                    $sql .= " " . implode(" AND ", $implode) . "";
                }
            }



            $sql .= ")";
        }

        if (!empty($data['filter_author_id'])) {
            $sql .= " AND n.author_id = '" . (int) $data['filter_author_id'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function getTag($tags) {
        $words = preg_split("/[\s,]+/", $tags);

        $tag = "";
        if (isset($words[0])) {
            $tag = $words[0];
        }
        return $tag;
    }

}
