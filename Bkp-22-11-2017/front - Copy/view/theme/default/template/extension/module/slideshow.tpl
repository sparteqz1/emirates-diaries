<div class="clearfix"></div>
<!-- Banner slider -->
<div class="banner-slider">


    <div id="slideshow<?php echo $module; ?>" class="owl-carousel" style="opacity: 1;">
        <?php foreach ($banners as $banner) { ?>
        <div class="item">
            <?php if ($banner['link']) { ?>
            <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
            <?php } else { ?>
            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
            <?php } ?>
        </div>
        <?php } ?>
    </div>


</div>
<!-- Banner slider -->


<script type="text/javascript">
    $('#slideshow<?php echo $module; ?>').owlCarousel({
        items: 1,
        autoplay: 3000,
        loop: true,
        dots: false,
        autoplayTimeout: 5000,
        transitionStyle: "fade",
        nav: true     
    });
</script>

<div class="clearfix"></div>