<div class="clearfix"></div>
<div class="widget">
    <h3 class="secondry-heading"><?php echo $heading_title; ?></h3>
    <ul class="categories-widget">
        <?php foreach ($categories as $category) { ?>
        <li><a href="<?php echo $category['href']; ?>"><em><?php echo $category['name']; ?></em><span class="bg-newjo"><?php echo $category['total']; ?></span></a></li>
        <?php } ?>


    </ul>
</div>
<div class="clearfix"></div>
