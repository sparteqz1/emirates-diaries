<?php echo $header; ?>

<!-- Inner Bnner -->
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-12">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>

                    <!-- contact map -->
                    <div class="post-widget">

                        <!-- Heading -->
                        <div class="primary-heading">
                            <h2><?php echo $heading_title; ?></h2>
                        </div>
                        <!-- Heading -->

                        <div class="clearfix"></div>
                        <?php if ($success) { ?>
                        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <div  class="row">
                            <div  class="col-sm-12">
                                <h2><?php echo $text_my_account; ?></h2>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
                                    <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
                                    <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>

                                </ul>

                                <h2><?php echo $text_my_newsletter; ?></h2>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->


            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>

<?php echo $footer; ?>