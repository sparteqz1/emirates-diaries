<?php echo $header; ?>

<!-- Inner Bnner -->
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-12">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>

                    <!-- contact map -->
                    <div class="post-widget">

                        <!-- Heading -->
                        <div class="primary-heading">
                            <h2><?php echo $heading_title; ?></h2>
                        </div>
                        <!-- Heading -->

                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="col-sm-12">
                                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <fieldset>
                                        <legend><?php echo $text_password; ?></legend>

                                        <?php if ($showoldpassword) { ?>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-oldpassword"><?php echo $entry_oldpassword; ?></label>
                                            <div class="col-sm-10">

                                                <input type="password" name="oldpassword" value="<?php echo $oldpassword; ?>" placeholder="<?php echo $entry_oldpassword; ?>" id="input-oldpassword" class="form-control" />
                                                <?php if ($error_oldpassword) { ?>
                                                <div class="text-danger"><?php echo $error_oldpassword; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
                                            <div class="col-sm-10">
                                                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                                                <?php if ($error_password) { ?>
                                                <div class="text-danger"><?php echo $error_password; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                                            <div class="col-sm-10">
                                                <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
                                                <?php if ($error_confirm) { ?>
                                                <div class="text-danger"><?php echo $error_confirm; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="buttons clearfix">
                                        <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                                        <div class="pull-right">
                                            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->


            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>

<?php echo $footer; ?>