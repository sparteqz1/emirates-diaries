
<?php echo $header; ?>


<!-- Inner Bnner -->
<section class="banner-parallax overlay-dark" data-image-src="<?php echo $brand_image; ?>" data-parallax="scroll"> 
    <div class="inner-banner">
        <h3><?php echo $heading_title; ?></h3>
        <ul class="tm-breadcrum">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
            <?php } ?>
        </ul>
    </div>
</section>
<!-- Inner Bnner -->
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-9 col-sm-8">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>



                    <!-- latest list posts -->
                    <div class="content products-page">
                        <div class="row">
                            <?php if ($products) { ?>
                            <!-- Post List -->


                            <?php foreach ($products as $product) { ?>
                            <!-- product -->
                            <div class="col-md-4 col-sm-6 col-xs-4">

                                <!-- product holder -->
                                <div class="product-holder">
                                    <!-- product img -->
                                    <div class="product-thumb">
                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                                        <!-- product hover -->
                                        <div class="product-hover">
                                            <ul>
                                                <li><a href="javascript:;" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="fa fa-exchange"></a></li>
                                                <li><a href="<?php echo $product['href']; ?>" class="fa fa-eye" ></a></li>

                                            </ul>
                                        </div>
                                        <!-- product hover -->
                                    </div>
                                    <!-- product img -->

                                    <!-- product detail -->
                                    <div class="product-detail white-bg">
                                        <h5><?php echo $product['name']; ?></h5>
                                        <div class="price-review">

                                            <?php if ($product['price']) { ?>
                                            <span><?php echo $product['price']; ?></span>
                                            <?php } ?>

                                            <?php if ($product['rating']) { ?>
                                            <!-- reviews -->
                                            <ul class="reviews">
                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                <?php if ($product['rating'] < $i) { ?>
                                                <li><i class="fa fa-star"></i></li>
                                                <?php } else { ?>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <?php } ?>
                                                <?php } ?>
                                            </ul>
                                            <!-- reviews -->
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <!-- product detail -->

                                </div>
                                <!-- product holder -->

                            </div>
                            <!-- product -->


                            <?php } ?>

                            <!-- Post List -->

                            <?php echo $pagination; ?>

                            <?php } else { ?>

                            <div class="p-30 light-shadow white-bg">
                                <p><?php echo $text_empty; ?></p>
                                <div class="buttons">
                                    <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->

                <!-- Sidebar -->
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <aside class="side-bar">
                        <?php echo $column_right; ?>



                        <!-- facebook widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">find us on facebook</h3>
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/newjoofficial/&tabs=timeline&width=300&height=220&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId=568059436732228" width="300" height="220" style="border:none;overflow:hidden"></iframe>
                            </div>
                        </div>
                        <!-- facebook widget -->

                        <?php echo $column_left; ?>


                    </aside>

                </div>
                <!-- Sidebar -->

            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>

<?php echo $footer; ?>
