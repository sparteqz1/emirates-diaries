<?php echo $header; ?>


<!-- Inner Bnner -->
<section class="banner-parallax overlay-dark" data-image-src="<?php echo $popup; ?>" data-parallax="scroll"> 
    <div class="inner-banner">
        <h3><?php echo $heading_title; ?></h3>
        <ul class="tm-breadcrum">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
            <?php } ?>
        </ul>
    </div>
</section>
<!-- Inner Bnner -->
<div class="clearfix"></div>

<div class="clearfix"></div>

<!-- Main Content -->
<main class="main-wrap"> 
    <div class="theme-padding">
        <div class="container">
            <div class="row">

                <div class="col-md-9 col-sm-8">

                    <div class="content">
                        <div class="clearfix"></div>
                        <?php echo $content_top; ?>
                        <div class="clearfix"></div>


                        <div class="post-widget p-30 light-shadow white-bg">

                            <div class="row">
                                <div class="col-md-6">

                                    <?php if ($thumb || $images) { ?>
                                    <!-- product info -->
                                    <div class="product-slider" id="product-slider">

                                        <!-- Slides -->
                                        <ul id="product-slides" class="product-slides">


                                            <?php if ($thumb) { ?>
                                            <li><img src="<?php echo $popup; ?>" alt="<?php echo $heading_title; ?>"/></li>
                                            <?php } ?>


                                            <?php if ($images) { ?>
                                            <?php foreach ($images as $image) { ?>
                                            <li><img src="<?php echo $image['popup']; ?>" alt="<?php echo $heading_title; ?>"/></li>
                                            <?php } ?>
                                            <?php } ?>

                                        </ul>   
                                        <!-- Slides -->

                                        <!-- thumbanil navigation -->
                                        <div id="product-thumbnail" class="product-thumbnail">
                                            <?php if ($thumb) { ?>
                                            <a data-slide-index="0" href="#"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></a>
                                            <?php } ?>


                                            <?php if ($images) { ?>
                                            <?php foreach ($images as $image) { ?>
                                            <a data-slide-index="<?php echo $image['index']; ?>" href="#"><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>" /></a>
                                            <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <!-- thumbanil navigation -->

                                    </div>
                                    <!-- product info -->

                                    <?php } ?>
                                </div>

                                <div class="col-md-6">
                                    <!-- product information -->
                                    <div class="product-info-holder">
                                        <h3><?php echo $heading_title; ?></h3>

                                        <?php if ($review_status) { ?>
                                        <!-- reviews -->
                                        <ul class="reviews">

                                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                            <?php if ($rating < $i) { ?>
                                            <li><i class="fa fa-star-o"></i></li>
                                            <?php } else { ?>
                                            <li><i class="fa fa-star"></i></li>
                                            <?php }  ?>
                                            <?php }  ?>
                                            <li><span><?php echo $reviews; ?></span></li>


                                        </ul>
                                        <!-- reviews -->

                                        <?php } ?>

                                        <?php if ($manufacturer) { ?>
                                        <div class="availbility"><strong><?php echo $text_manufacturer; ?></strong> <a href="<?php echo $manufacturers; ?>" <span> <?php echo $manufacturer; ?> </span> </a></div>

                                        <?php } ?>


                                        <?php if ($price) { ?>
                                        <div class="price style-2"><?php echo $price; ?></div>
                                        <?php } ?>
                                        <?php echo $summery; ?>
                                        <div class="quantity-addwish">

                                            <a class="btn red" href="javascript:;" onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i></a>
                                        </div>

                                    </div>
                                    <!-- product information -->

                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <!-- tags and social icons -->
                            <div class="row mb-20">

                                <?php if ($tags) { ?>
                                <!-- populer tags --> 
                                <div class="col-md-6">
                                    <div class="blog-tags font-roboto">
                                        <ul>
                                            <?php for ($i = 0; $i < count($tags); $i++) { ?>
                                            <li><a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a></li>
                                            <?php } ?>

                                        </ul>
                                    </div>
                                </div>
                                <!-- populer tags --> 
                                <?php } ?>

                                <!-- social icons --> 
                                <div class="col-md-6 pull-right">
                                    <div class="blog-social">
                                        <span class="share-icon btn-social-icon btn-adn"  data-toggle="tooltip" data-placement="top" title="Sharing is Caring">
                                            <span class="fa fa-share-alt"></span>
                                        </span>
                                        <ul>
                                            <li>
                                                <a class="btn-social-icon btn-facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $share; ?>"  data-toggle="tooltip" data-placement="top" title="Share of Facebook">
                                                    <span class="fa fa-facebook"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn-social-icon btn-twitter" target="_blank" href="https://twitter.com/home?status=<?php echo $share; ?>"  data-toggle="tooltip" data-placement="top" title="Post on Twitter">
                                                    <span class="fa fa-twitter"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn-social-icon btn-pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo $share; ?>&media=<?php echo $popup; ?>&description=<?php echo $heading_title; ?>"  data-toggle="tooltip" data-placement="top" title="Pin IT">
                                                    <span class="fa fa-pinterest"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn-social-icon btn-linkedin" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $share; ?>&title=<?php echo $heading_title; ?>&summary=<?php echo $heading_title; ?>&source=web"  data-toggle="tooltip" data-placement="top" title="Post on Linked In">
                                                    <span class="fa fa-linkedin"></span>
                                                </a>
                                            </li>

                                        </ul>                                                   
                                    </div>
                                </div>
                                <!-- social icons --> 

                            </div>
                            <!-- tags and social icons -->
                            <div class="clearfix"></div>


                        </div>

                        <div class="clearfix"></div>


                        <!-- review tabs -->
                        <div class="review-tabs-holder post-widget light-shadow p-30 white-bg" >
                            <div class="horizontal-tabs-widget reviews-tabs">

                                <!-- tabs navs -->
                                <ul class="theme-tab-navs">
                                    <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>

                                    <?php if ($review_status) { ?>
                                    <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
                                    <?php } ?>

                                    <?php if ($attribute_groups) { ?>
                                    <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                                    <?php } ?>

                                </ul>


                                <!-- tabs navs -->

                                <!-- Tab panes -->
                                <div class="horizontal-tab-content tab-content">
                                    <div class="tab-pane fade active in" id="tab-description">
                                        <?php echo $description; ?>

                                    </div>


                                    <?php if ($review_status) { ?>
                                    <div class="tab-pane fade" id="tab-review">
                                        <div class="reviews-product">
                                            <form  id="form-review">
                                                <div id="review"></div>
                                                <h3><?php echo $text_write; ?></h3>
                                                <?php if ($review_guest) { ?>

                                                <div class="form-group">
                                                    <input type="text" name="name" class="form-control" placeholder="<?php echo $entry_name; ?>" />
                                                </div>

                                                <div class="form-group">
                                                    <input id="input-name" name="email" type="text" class="form-control" placeholder="Your Email address" />
                                                </div>

                                                <div class="form-group">
                                                    <textarea rows="4" name="text" id="input-review"  placeholder="<?php echo $entry_review; ?>" class="form-control"></textarea>
                                                    <div class="help-block"><?php echo $text_note; ?></div>

                                                </div>

                                                <div class="form-group required">
                                                    <div class="col-sm-12">
                                                        <label class="control-label"><?php echo $entry_rating; ?></label>
                                                        &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                                                        <input type="radio" name="rating" value="1" />
                                                        &nbsp;
                                                        <input type="radio" name="rating" value="2" />
                                                        &nbsp;
                                                        <input type="radio" name="rating" value="3" />
                                                        &nbsp;
                                                        <input type="radio" name="rating" value="4" />
                                                        &nbsp;
                                                        <input type="radio" name="rating" value="5" />
                                                        &nbsp;<?php echo $entry_good; ?></div>
                                                </div>


                                                <?php echo $captcha; ?>
                                                <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn green"><?php echo $button_continue; ?></button>

                                                <?php } else { ?>
                                                <span><?php echo $text_login; ?></span>
                                                <?php } ?>
                                            </form>
                                        </div>
                                    </div>
                                    <?php } ?>

                                    <?php if ($attribute_groups) { ?>
                                    <div class="tab-pane fade" id="tab-specification">
                                        <table class="table table-bordered">
                                            <?php foreach ($attribute_groups as $attribute_group) { ?>
                                            <thead>
                                                <tr>
                                                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                <tr>
                                                    <td><?php echo $attribute['name']; ?></td>
                                                    <td><?php echo $attribute['text']; ?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                            <?php } ?>
                                        </table>
                                    </div>
                                    <?php } ?>
                                </div>
                                <!-- Tab panes -->
                            </div>
                        </div>
                        <!-- review tabs -->


                        <div class="clearfix"></div>
                        <?php echo $content_bottom; ?>
                        <div class="clearfix"></div>


                        <div class="clearfix"></div>


                        <?php if ($products) { ?>
                        <!-- Slider Widget -->
                        <div class="post-widget">

                            <!-- Heading -->
                            <div class="primary-heading">
                                <h2><?php echo $text_related; ?></h2>
                            </div>
                            <!-- Heading -->

                            <!-- post slider -->
                            <div class="light-shadow gray-bg p-30"> 


                                <div class="row">

                                    <?php foreach ($products as $product) { ?>
                                    <!-- product -->
                                    <div class="col-md-4 col-sm-6 col-xs-4">

                                        <!-- product holder -->
                                        <div class="product-holder">
                                            <!-- product img -->
                                            <div class="product-thumb">
                                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                                                <!-- product hover -->
                                                <div class="product-hover">
                                                    <ul>
                                                        <li><a href="javascript:;" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="fa fa-exchange"></a></li>
                                                        <li><a href="<?php echo $product['href']; ?>" class="fa fa-eye" ></a></li>

                                                    </ul>
                                                </div>
                                                <!-- product hover -->
                                            </div>
                                            <!-- product img -->

                                            <!-- product detail -->
                                            <div class="product-detail white-bg">
                                                <h5><?php echo $product['name']; ?></h5>
                                                <div class="price-review">

                                                    <?php if ($product['price']) { ?>
                                                    <span><?php echo $product['price']; ?></span>
                                                    <?php } ?>

                                                    <?php if ($product['rating']) { ?>
                                                    <!-- reviews -->
                                                    <ul class="reviews">
                                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                        <?php if ($product['rating'] < $i) { ?>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <?php } else { ?>
                                                        <li><i class="fa fa-star-o"></i></li>
                                                        <?php } ?>
                                                        <?php } ?>
                                                    </ul>
                                                    <!-- reviews -->
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <!-- product detail -->

                                        </div>
                                        <!-- product holder -->

                                    </div>
                                    <!-- product -->


                                    <?php } ?>


                                </div>

                            </div>
                            <!-- post slider -->

                        </div>
                        <!-- Slider Widget -->

                        <?php } ?>
                    </div>

                </div>
                <!-- Content -->

                <!-- Sidebar -->
                <div class="col-md-3 col-sm-4">
                    <aside class="side-bar">
                        <?php echo $column_right; ?>


                        <div class="clearfix"></div>

                        <div class="widget">
                            <h3 class="secondry-heading">find us on facebook</h3>
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/newjoofficial/&tabs=timeline&width=300&height=220&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId=568059436732228" width="300" height="220" style="border:none;overflow:hidden"></iframe>
                        </div>

                        <div class="clearfix"></div>
                        <?php echo $column_left; ?>


                    </aside>

                </div>
                <!-- Sidebar -->

            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                $('#button-review').button('loading');
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });


//--></script>
<?php echo $footer; ?>