<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333; ">
    <div class="container">
        <div class="intro-text">


            <h2><?php echo $banner_title; ?></h2>


        </div>
    </div>
</header>

<!-- Featured Grid -->
<section class="" >
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading"><?php echo $text_featured; ?></h2> 
            </div>
        </div> 
        <div class="row featured">
            <?php foreach($galleries as $gallery) { ?>
            <div class="item col-lg-4">
                <a data-toggle="modal" href="#portfolioModal<?php echo $gallery['gallery_id']; ?>" class="img-hieght" style="background: url(<?php echo $gallery['image'];  ?>); background-size: cover; background-position: center center;">
                    <div class="title">
                        <?php echo '<h4>'.$gallery['issue'].'</h4>'; ?>
                        <h2><?php echo $gallery['name'];  ?></h2>
                    </div>
                </a>                        
            </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php echo $footer; ?>

<!-- Modal 1 -->
<?php foreach($galleries as $gallery) { ?>
<div class="portfolio-modal modal fade" id="portfolioModal<?php echo $gallery['gallery_id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2><?php echo $gallery['name'];  ?></h2>

                            <img class="img-fluid d-block mx-auto" src="<?php echo $gallery['image']; ?>" alt="">
                            <p class="item-intro text-muted"><?php echo $gallery['description'];  ?></p>
                            <ul class="list-inline">
                                <li>Date: <?php echo $gallery['day']; ?></li>
                                <li>By: <?php echo $gallery['author']; ?></li>

                            </ul>
                             <?php foreach ($gallery['popup'] as $popup) { ?>
                            <img class="img-fluid d-block mx-auto" src="<?php echo $popup['popup']; ?>" alt="">
                            <?php } ?>


                            <button class="btn btn-primary more" data-dismiss="modal" type="button">                                        
                                <a href="<?php echo $galry; ?>" class="modal-link"><?php echo $text_goto; ?></a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
</body>

</html>
