<?php
            $count = 1;
            foreach($magazines as $data) { ?>
                <div class="col-lg-3 col-sm-6">
                    <div class="card h-100">
                        <a class="card-img" href="<?php echo $data['href']; ?>">
                        <img class="card-img-top" src="<?php echo $data['magazineimage']; ?>" alt="<?php echo $data['name']; ?>">

                    </a>
                        <a href="<?php echo $data['href']; ?>" class="card-body">
                            <h4>
                            Issue #<?php echo $data['sort']; ?>
                        </h4>
                            <h3 class="card-title">
                            <?php echo $data['name']; ?>
                        </h3>
                        </a>
                    </div>
                </div>
                <?php
            $count = $count+1;
            } ?>