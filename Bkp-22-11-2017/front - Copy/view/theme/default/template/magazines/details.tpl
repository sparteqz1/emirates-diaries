<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333;">
    <div class="container">
        <div class="intro-text">
            <h2><?php echo $heading_title; ?></h2>
        </div>
    </div>
</header>
<!-- Featured Grid -->
<?php if(count($issue_results) > 0 ) { 
foreach ($categories as $categorykey => $category) {
if(count($category['news']) > 0) { ?>
<section class="" >
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading"><?php echo $category['name']; ?></h2>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="row">
                <?php foreach ($category['news'] as $articlekey => $article) { ?>
                <div class="col-lg-3 col-sm-6 articles">
                    <div class="card h-100">
                        <a class="card-img" href="<?php echo $article['href']; ?>">
                            <img class="card-img-top" src="<?php echo $article['thumb']; ?>">
                            
                        </a>
                        <a href="<?php echo $article['href']; ?>" class="card-body">
                            <h4 class="card-title">
                                <?php echo $article['name']; ?>
                            </h4>
                            <h5 class="date"><?php echo $article['date']; ?></h5>
                            <span class="author">
                                <?php echo $article['author']; ?>
                            </span>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <!-- /.row -->
    </div>
</section>
<?php } } } else { ?>
<section class="newsletter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">                
                        <p>
                            NO ARTICLES FOUND WITH THIS ISSUE!
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <div class="clearfix"></div>
<?php } ?>

<!--<section class="" >
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading">Profiles</h2>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-3 col-sm-6 articles">
                    <div class="card h-100">
                        <a class="card-img" href="http://localhost/bitbucket/emirates-diaries/mobile-aid">
                            <img class="card-img-top" src="http://localhost/bitbucket/emirates-diaries/image/cache/catalog/news/mobile-aid/mobile-aid-900x600.jpg" alt="Mobile Aid">
                            <span>Innovation</span>
                        </a>
                        <a href="http://localhost/bitbucket/emirates-diaries/mobile-aid" class="card-body">
                            <h4 class="card-title">
                                Mobile Aid
                            </h4>
                            <h5 class="date">6 Nov, 2015</h5>
                            <span class="author">
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 articles">
                    <div class="card h-100">
                        <a class="card-img" href="http://localhost/bitbucket/emirates-diaries/outlines-to-ornaments">
                            <img class="card-img-top" src="http://localhost/bitbucket/emirates-diaries/image/cache/catalog/news/outlines-to-or%20naments/interview-question-900x600.jpg" alt="Outlines to Ornaments">
                            <span>
                                Innovation
                            </span>
                        </a>
                        <a href="http://localhost/bitbucket/emirates-diaries/outlines-to-ornaments" class="card-body">
                            <h4 class="card-title">
                                Outlines to Ornaments
                            </h4>
                            <h5 class="date">31 Oct, 2015</h5>
                            <span class="author">
                                Sherina
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 articles">
                    <div class="card h-100">
                        <a class="card-img" href="http://localhost/bitbucket/emirates-diaries/tawlat">
                            <img class="card-img-top" src="http://localhost/bitbucket/emirates-diaries/image/cache/catalog/news/tawlat/tawlat-900x600.jpg" alt="Tawlat">
                            <span>
                                Innovation
                            </span>
                        </a>
                        <a href="http://localhost/bitbucket/emirates-diaries/tawlat" class="card-body">
                            <h4 class="card-title">
                                Tawlat
                            </h4>
                            <h5 class="date">11 Aug, 2015</h5>
                            <span class="author"></span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 articles">
                    <div class="card h-100">
                        <a class="card-img" href="http://localhost/bitbucket/emirates-diaries/gafla-the-arabic-jeweler">
                            <img class="card-img-top" src="http://localhost/bitbucket/emirates-diaries/image/cache/catalog/news/gafla/gaf-900x600.jpg" alt="Gafla: The Arabic Jeweler">
                            <span>
                                Innovation
                            </span>
                        </a>
                        <a href="http://localhost/bitbucket/emirates-diaries/gafla-the-arabic-jeweler" class="card-body">
                            <h4 class="card-title">
                                Gafla: The Arabic Jeweler
                            </h4>
                            <h5 class="date">8 Nov, 2015</h5>
                            <span class="author">
                                Maryam Al Mansoori
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row ->
    </div>
</section>-->

<?php echo $footer; ?>

</body>

</html>
