
<?php echo $header; ?>

<div class="clearfix"></div>


<!-- Main Content -->
<main class="main-wrap"> 
    <div class="theme-padding">
        <div class="clearfix"></div>

        <!-- error 404 -->
        <div class="error-holder font-roboto">
            <div class="error-detail error-2">
                <h2 class="font-roboto"><img src="front/view/theme/default/images/newjo-logo.png" alt=""></h2>
                <p><?php echo $message; ?></p>
            </div>
        </div>

    </div>       
</main>
<!-- main content -->  
<div class="clearfix"></div>

<?php echo $footer; ?>