<!-- subscribe -->
<section class="subscribe-instagram">
    <div class="container">

        <div class="col-lg-12">
            <div class="row">


                <div class="col-lg-6 col-sm-12 subscribe">
                    <div class="card h-100">
                        <div class="card-body">
                            <div class="col-sm-6 subscribe-text">
                                <h4>
                                    GET THE                                            
                                </h4>
                                <h2>
                                    <?php echo $text_magazine; ?>
                                </h2>
                                <p>
                                    <?php echo $text_subscribe_desc; ?>
                                </p>
                                <a href="">
                                    <?php echo $text_subscribe; ?>
                                    <span>
                                        →
                                    </span>
                                </a>
                            </div>
                            <div class="col-sm-6 subscribe-img">
                                <img class="img-fluid"  src="front/view/theme/default/images/get-magazine.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 subscribe">
                    <div class="card h-100">

                        <div class="card-body">
                            <div class="col-sm-6 subscribe-text">
                                <h4>
                                    <?php echo $text_follow_us_on; ?>                                            
                                </h4>
                                <h2>
                                    <?php echo $text_instagram; ?>
                                </h2>
                                <p>
                                    <?php echo $text_follow_us_desc; ?>
                                </p>
                                <a href="https://www.instagram.com/emiratesdiaries">
                                    <?php echo $text_follow_us; ?>
                                    <span>
                                        →
                                    </span>
                                </a>
                            </div>
                            <div class="col-sm-6 subscribe-img">
                                <a href="https://www.instagram.com/emiratesdiaries">
                                <img class="img-fluid"  src="front/view/theme/default/images/instagram.png" alt="">
                            </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>



        </div>
        <!-- /.row -->            

    </div>
</section>


<!-- Footer -->
<footer>
    <div class="container">

        <div class="row contact">

            <div class="col-lg-4 col-md-12 col-sm-12">
                <p>
                    <?php echo $text_call_us; ?>
                </p>
                <h4>
                    <?php echo $text_office_tele; ?> : +971 000 000
                </h4>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <p>
                    <?php echo $text_reach_us; ?>
                </p>
                <h4>
                    Office Address, Street, Dubai
                </h4>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <p>
                    <?php echo $text_send_us; ?>
                </p>
                <h4>
                    <?php echo $text_email; ?>: info@emiratediaries.com
                </h4>
            </div>

        </div>

        <div class="row contact">
            <div class="col-md-4 about">
                <p>
                    Eu solet iudico suavitate sit. Eam eu dicant epicuri volutpat. Illud decore eam ea, ad vim solum urbanitas. Eos feugait
                </p>                        
            </div>
            <div class="col-md-4 links">                        
                <a href="<?php echo $about; ?>"><?php echo $menu_about_us; ?></a>
                <a href=""><?php echo $menu_advertise_us; ?></a>
                <a href="<?php echo $contact; ?>"><?php echo $menu_contact_us; ?></a>
                <a href="<?php echo $terms; ?>"><?php echo $menu_privacy_policy; ?></a>
                <a href=""><?php echo $menu_media_kit; ?></a>
                <a href=""><?php echo $menu_career; ?></a>

            </div>
            <div class="col-md-4 logo">
                <img class="img-fluid" src="front/view/theme/default/images/footer-logo.png" alt="">
            </div>

        </div>

        <div class="row copyright">
            <div class="col-md-4">

            </div>
            <div class="col-md-4">                        

                <p>
                    © <?php echo $text_copy_rights; ?>
                </p>
            </div>
            <div class="col-md-4">

            </div>

        </div>


    </div>
</footer>   




<!-- Bootstrap core JavaScript -->

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="front/view/javascript/vendor/slick/slick.js"></script>
<script src="front/view/javascript/vendor/popper/popper.min.js"></script>
<script src="front/view/javascript/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="front/view/javascript/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for this template -->        
<script src="front/view/javascript/ed.js"></script>
<script src="front/view/javascript/common.js" type="text/javascript"></script>  
<script>

    //**hero slider**//
    $(document).ready(function () {
    $(".slider").slick({
    dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            rtl: <?php echo $dire; ?>,
            adaptiveHeight: false,
            slidesToScroll: 1,
            arrows: true,
            fade: false,
    });
    //remove active class from all thumbnail slides
    $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');
    //set active class to first thumbnail slides
    $('.slider-nav-thumbnails .slick-slide').eq(0).addClass('slick-active');
    // On before slide change match active thumbnail to current slide
    $('.slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    var mySlideNumber = nextSlide;
    $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');
    $('.slider-nav-thumbnails .slick-slide').eq(mySlideNumber).addClass('slick-active');
    $('.slick-active .intro-lead-in').removeClass('animated fadeInDown');
    $('.slick-active .intro-heading').removeClass('animated fadeInUp');
    $('.slick-active .intro-lead-in').hide();
    $('.slick-active .intro-heading').hide();
    });
    //UPDATED 
    $('.slider').on('afterChange', function (event, slick, currentSlide) {
    $('.content').hide();
    $('.content[data-id=' + (currentSlide + 1) + ']').show();
    $('.slick-active .intro-lead-in').show();
    $('.slick-active .intro-lead-in').addClass('animated fadeInDown');
    $('.slick-active .intro-heading').show();
    $('.slick-active .intro-heading').addClass('animated fadeInUp');
    });
    });
    //**magazine slider**//


    $(".magazine-slider").slick({
    dots: false,
            infinite: false,
            speed: 300,
            arrows: true,
            rtl: <?php echo $dire; ?>,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
            {
            breakpoint: 1024,
                    settings: {
                    slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                    }
            },
            {
            breakpoint: 600,
                    settings: {
                    slidesToShow: 2,
                            slidesToScroll: 2
                    }
            },
            {
            breakpoint: 480,
                    settings: {
                    slidesToShow: 1,
                            slidesToScroll: 1
                    }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
            ]
    });
    $(window).resize(function () {
    <?php if ($current_root == '' || $current_root == 'not_found') {
    echo "$('header.masthead').height($(window).height());";
    } else {
    echo "$('header.masthead').height(300);";
    } ?>
    }).resize();
    $('.menu-check').change(function () {
    if ($(this).is(':checked')) {
    $('.sidemenu').addClass('right-0');
    $(".sidemenu-check").attr("checked", true);
    $("body").addClass("sidemenu-open");
    } else {
    $('.sidemenu').removeClass('right-0');
    $(".sidemenu-check").attr("checked", false);
    $("body").removeClass("sidemenu-open");
    if ($('.navbar-nav').hasClass('show')) {
    $('.navbar-nav').removeClass('show');
    }
    }
    });
    $('.sidemenu-check').change(function () {
    if ($(this).is(':checked')) {
    $('.sidemenu').addClass('right-0');
    $("body").addClass("sidemenu-open");
    $(".menu-check").attr("checked", true);
    } else {
    $('.sidemenu').removeClass('right-0');
    $("body").removeClass("sidemenu-open");
    $(".menu-check").attr("checked", false);
    if ($('.navbar-nav').hasClass('show')) {
    $('.navbar-nav').removeClass('show');
    }
    }
    });
    $('.categories-button').on('click', function () {
    $('.navbar-nav').toggleClass('show');
    });
    $('#mainNav .navbar-nav .nav-item .nav-link.back').on('click', function () {
    $('.navbar-nav').removeClass('show');
    });
</script>

<!-- Modal 1 -->
<div class="subscribe-modal modal fade" id="subscribeModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2 class="subscriberesult"></h2>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).delegate('#subscribe-btn', 'click', function () {
        $.ajax({
            url: 'index.php?route=common/footer/enquiry',
            type: 'post',
            data: $("#subscribeform").serialize(),
            dataType: 'json',
            beforeSend: function () {
                $('.subscriberesult').html('');
            },
            complete: function () {
                $('#subscribe-btn').button('reset');
            },
            success: function (result) {
                $('#subscribeModal').modal('show');

                if (result['success']) {
                    $(".subscriberesult").html(result['success']);
                    $('#subscribeform').trigger("reset");
                } else if (result['error'])
                {
                    $(".subscriberesult").html(result['error']);
                }
            },
            error: function () {
                $('#subscribeModal').modal('show');
                $(".subscriberesult").html('Sorry, it seems that server is not responding. Please try again later!');
               
            },
        });

    });
</script>
<script>
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".ed-loader").fadeOut("slow");;
    });
</script>