
<!-- serach modal -->
<div class="md-modal md-effect-7 search-modal font-oswald" id="search-popup">
    <div class="md-content" id="search">
        <!-- serach form -->

        <div  class="search-form">
            <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control" />

        </div>
        <a class="md-close pull-right fa fa-times" href="#"></a>
        <!-- serach form -->
    </div>
</div>
<!-- serach modal -->