<?php echo $header; ?>

<header class="masthead inner-page" style="background: url('front/view/theme/default/images/about/about.jpg'); background-size: cover; background-position: center center; background-repeat: no-repeat;">
    <div class="container">
        <div class="intro-text">
            <h2></h2>                  

        </div>
    </div>
</header>



<!-- Welcome -->
<section class="welcome">
    <div class="container">
        <div class="row">
            <div class="col-lg-1"> </div>
            <div class="col-lg-10">
                <h5>About</h5>
                <h2>Emirates Diaries</h2>
                <hr>
                <p><?php echo $text_welcome_desc; ?></p>
            </div>
        </div>               

    </div>
</section>

<section class="single-article chairman" style="background: #eee;">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <img class="img-fluid" src="front/view/theme/default/images/about/about3.jpg" alt="">
            </div>
            <div class="col-md-6">
                <h2>Chairman's Message</h2>
                <hr>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>

            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>
</section>



<!-- Most Read -->
<section class="board">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading">Board of Trustees</h2> 
            </div>
        </div> 
        <div class="col-lg-12">
            <div class="row">

                <?php 
                $rowCount = 0; 
                foreach($trustees as $trustee) { ?>
                <div class="col-lg-3 col-sm-6 articles">
                    <div class="card h-100 member">
                        <a class="card-img" href="#">
                            <img class="card-img-top" src="<?php echo $trustee['image']; ?>" alt="">
                        </a>                      

                        <a href="" class="card-body">
                            <h4 class="card-title">
                                <?php echo $trustee['name']; ?>
                            </h4>
                            <p>Details</p>
                        </a>
                    </div>
                </div>
                <?php
                $rowCount++;
                if($rowCount % 4 == 0) echo '</div><div class="row">';
                }
                ?>
            </div>

        </div>
        <!-- /.row -->            

    </div>
</section>

<?php echo $footer; ?>

</body>

</html>
