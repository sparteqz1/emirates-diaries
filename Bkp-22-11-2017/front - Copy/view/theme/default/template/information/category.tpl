<?php echo $header; ?>

<header class="masthead inner-page" style="background: url('front/view/theme/default/images/article/health.jpeg'); background-size: cover; background-position: center center; background-repeat: no-repeat;">
            <div class="container">
                <div class="intro-text">


                    <h2><?php echo $menu_category; ?></h2>


                </div>
            </div>
        </header>

        <!-- Category-article -->
        <?php if(isset($articles) && count($articles) > 0 ) { ?>
        <section class="category-article">
            <div class="container"> 

            <?php 
            $count = 0;
            foreach ($articles as $articlekey => $article) { ?>

                <div class="row item">
                    <div class="col-md-6 category-article-img">
                        <a href="<?php echo $article['href']; ?>">
                            <img class="img-fluid" src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>">
                        </a>
                    </div>
                    <div class="col-md-6 category-article-content">
                        <h4><?php echo $article['issue']; ?></h4>
                        <h3><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a> 
                        </h3>
                        <p><?php echo $article['description']; ?></p>
                        <h5 class="author"><?php echo $article['author']; ?> <span><?php echo $article['ago']; ?></span></h5>
                        <a class="btn btn-primary more" href="<?php echo $article['href']; ?>">Read More</a>
                    </div>
                </div>

            <?php 
            $count = $count+1;
                if($count==4){
                        break;
                } 
            } ?>
                
            </div>
        </section>
        <!-- Most Read -->
        <?php if(count($articles)>3){ ?>
        <section class="most-read">
            <div class="container">

                <div class="col-lg-12">
                    <div class="row">

                        <?php 
                        $rowcount = 0;
                        for($i=4;$i<count($articles);$i++) { ?>
                        <div class="col-lg-3 col-sm-6 articles">
                            <div class="card h-100">
                                <a class="card-img" href="<?php echo $articles[$i]['href']; ?>">
                                    <img class="card-img-top" src="<?php echo $articles[$i]['thumb']; ?>" alt="<?php echo $articles[$i]['name']; ?>">
                                    <span>
                                        <?php echo $articles[$i]['issue']; ?>
                                    </span>
                                </a>

                                <a href="<?php echo $articles[$i]['href']; ?>" class="card-body">
                                    <h4 class="card-title">
                                        <?php echo $articles[$i]['name']; ?> 
                                    </h4>
                                    <h5 class="date"><?php echo $articles[$i]['date']; ?></h5>
                                    <span class="author">
                                        <?php echo $articles[$i]['author']; ?>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <?php 
                        $rowcount = $rowcount+1;
                        if($rowcount % 4 == 0) {
                echo '</div><div class="row">';
                }
                    } ?>

                    </div>
                    
                </div>
                <!-- /.row -->            

            </div>
        </section>

        <?php } } else { ?>

        <section class="newsletter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">                
                        <p>
                            NO ARTICLES FOUND WITH THIS CATEGORY!
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <div class="clearfix"></div>

        <?php } ?>



        <!-- Latest Magazine -->
        

                    <section class="magazines">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading"><?php echo $text_latest_magazines; ?></h2> 
            </div>
        </div> 

        <div class="row magazine-slider">

           <?php echo $ed_magazines; ?>

        </div>


        <!-- /.row -->            

    </div>
</section>

<?php echo $footer; ?>

</body>

</html>
