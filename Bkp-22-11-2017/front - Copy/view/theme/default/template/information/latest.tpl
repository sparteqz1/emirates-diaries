<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333;">
            <div class="container">
                <div class="intro-text">
                    <h2><?php echo $banner_title; ?></h2>                  

                </div>
            </div>
        </header>

        <!-- Latest Grid -->
        <section class="latest">
            <div class="container">

                <div class="col-lg-12">
                    <div class="row">
                    <?php
                        $count = 0;
                        foreach ($latestnews as $latest_key => $latest) { ?>                        
                        <div class="<?php if($latest_key == '0' || $latest_key == '5') { echo 'col-lg-6'; } else { echo 'col-lg-3'; } ?> col-sm-6 articles">
                            <div class="card h-100">
                                <a class="card-img" href="<?php echo $latest['href']; ?>">
                                    <img class="card-img-top" src="<?php echo $latest['thumb']; ?>" alt="<?php echo $latest['name']; ?>">
                                    <span>
                                        <?php echo $latest['latest_issue']; ?>
                                    </span>
                                </a>

                                <a href="<?php echo $latest['href']; ?>" class="card-body">
                                    <h4 class="card-title">
                                        <?php echo $latest['name']; ?>
                                    </h4>
                                    <h5 class="date"><?php echo $latest['date']; ?></h5>
                                    <span class="author">
                                        <?php echo $latest['author']; ?>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <?php
                $count = $count+1;
                if($count==6){
                        break;
                }   
                if($count % 3 == 0) {
                echo '</div><div class="row">';
                } 
               
                   
            } ?>   

                    </div>




                </div>
                <!-- /.row -->            

            </div>
        </section>


        <!-- Category-article -->
        <section class="category-article">
            <div class="container"> 
                <?php 
                    if(count($latestnews)>5){
                    for($i=6;$i<count($latestnews);$i++) { ?>
                <div class="row item">
                    <div class="col-md-6 category-article-img">
                        <a href="<?php echo $latestnews[$i]['href']; ?>">
                            <img class="img-fluid" src="<?php echo $latestnews[$i]['thumb']; ?>" alt="<?php echo $latestnews[$i]['name']; ?>">
                        </a>
                    </div>
                    <div class="col-md-6 category-article-content">
                        <h4><?php echo $latestnews[$i]['latest_issue']; ?></h4>
                        <h3><a href="<?php echo $latestnews[$i]['href']; ?>"><?php echo $latestnews[$i]['name']; ?></a>
                        </h3>
                        <p><?php echo $latestnews[$i]['description']; ?></p>
                        <h5 class="author"><?php echo $latestnews[$i]['author']; ?> <span><?php echo $latestnews[$i]['days']; ?></span></h5>
                        <a class="btn btn-primary more" href="<?php echo $latestnews[$i]['href']; ?>">Read More</a>
                    </div>
                </div>
                <?php } } ?>
            </div>
        </section>
        
        <!-- newsletter -->

<?php echo $newsletter; ?>

<?php echo $footer; ?>

</body>

</html>
