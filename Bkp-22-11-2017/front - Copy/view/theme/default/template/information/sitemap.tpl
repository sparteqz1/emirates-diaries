
<?php echo $header; ?>
<!-- Inner Bnner -->
<section class="banner-parallax overlay-dark"  data-image-src="<?php echo $banner_logo; ?>" data-parallax="scroll"> 
    <div class="inner-banner">
        <h3><?php echo $heading_title; ?></h3>
        <ul class="tm-breadcrum">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
            <?php } ?>
        </ul>
    </div>
</section>
<!-- Inner Bnner -->
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-12 col-sm-12">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>

                    <!-- latest list posts -->
                    <div class="post-widget m-0">
                        <div class="p-30 light-shadow white-bg">
                            <div class="row">
                                <div class="col-sm-6">
                                    <ul>
                                        <?php foreach ($categories as $category_1) { ?>
                                        <li><a href="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?></a>
                                            <?php if ($category_1['children']) { ?>
                                            <ul>
                                                <?php foreach ($category_1['children'] as $category_2) { ?>
                                                <li><a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
                                                    <?php if ($category_2['children']) { ?>
                                                    <ul>
                                                        <?php foreach ($category_2['children'] as $category_3) { ?>
                                                        <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
                                                        <?php } ?>
                                                    </ul>
                                                    <?php } ?>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                            <?php } ?>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <div class="col-sm-6">
                                    <ul>

                                        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
                                            <ul>
                                                <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
                                                <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>

                                            </ul>
                                        </li>

                                        <li><a href="<?php echo $search; ?>"><?php echo $text_search; ?></a></li>
                                        <li><?php echo $text_information; ?>
                                            <ul>
                                                <?php foreach ($informations as $information) { ?>
                                                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                                                <?php } ?>
                                                <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->


            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>

<?php echo $footer; ?>