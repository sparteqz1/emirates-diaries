<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333;">
            <div class="container">
                <div class="intro-text">
                    <h2><?php echo $banner_title; ?></h2>                  

                </div>
            </div>
        </header>

        <!-- Featured Grid -->
        <section class="" >
            <div class="container">               
                <div class="row featured">
            <?php
            $count = 0;

            foreach($popularnews as $popularkey => $popular) { ?>
                <div class="item <?php if($popularkey == '0' || $popularkey == '4') { echo 'col-lg-6'; } else { echo 'col-lg-3'; } ?>">
                    <a href="<?php echo $popular['href']; ?>" class="img-hieght" style="background: url('<?php echo $popular["thumb"]; ?>'); background-size: cover;">
                        <div class="title">
                            <h4><?php echo $popular['popular_issue']; ?></h4>
                            <h2><?php echo $popular['name']; ?></h2>
                        </div>
                    </a>                        
                </div>
                <?php
                $count = $count+1;
                if($count==6){
                        break;
                }   
                if($count % 3 == 0) {
                echo '</div><div class="row featured">';
                } 
               
                   
            } ?>            
        </div>
        
            </div>
        </section>



        <!-- Category-article -->
        <section class="category-article">
            <div class="container">              

                <?php 
                    if(count($popularnews)>5){
                    for($i=6;$i<count($popularnews);$i++) { ?>
                        <div class="row item">
                            <div class="col-md-6 category-article-img">
                                <a href="<?php echo $popularnews[$i]['href']; ?>">
                                    <img class="img-fluid" src="<?php echo $popularnews[$i]['thumb']; ?>" alt="<?php echo $popularnews[$i]['name']; ?>">
                                </a>
                            </div>
                            <div class="col-md-6 category-article-content">
                                <h4><?php echo $popularnews[$i]['popular_issue']; ?></h4>
                                <h3><a href="<?php echo $popularnews[$i]['href']; ?>"><?php echo $popularnews[$i]['name']; ?></a> 
                                </h3>
                                <p><?php echo $popularnews[$i]['description']; ?></p>
                                <h5 class="author"><?php echo $popularnews[$i]['author']; ?> <span><?php echo $popularnews[$i]['date']; ?></span></h5>
                                <a class="btn btn-primary more" href="<?php echo $popularnews[$i]['href']; ?>">Read More</a>
                            </div>
                        </div>
                <?php } } ?>
                
                
                





            </div>
        </section>

<!-- newsletter -->

<?php echo $newsletter; ?>

<?php echo $footer; ?>

</body>

</html>
