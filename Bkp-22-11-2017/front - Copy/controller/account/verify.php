<?php

class ControllerAccountVerify extends Controller {

    private $error = array();

    public function index() {
        if ($this->customer->isLogged()) {
            $this->response->redirect($this->url->link('account/account', '', true));
        }

        if (isset($this->request->get['code'])) {
            $code = $this->request->get['code'];
        } else {
            $code = '';
        }

        $this->load->model('account/customer');
        $this->load->language('account/verify');

        $customer_info = $this->model_account_customer->getCustomerByCode($code);

        if ($customer_info) {
            $this->model_account_customer->verifyEmail($customer_info['email']);

            if ($this->config->get('config_customer_activity')) {
                $this->load->model('account/activity');

                $activity_data = array(
                    'customer_id' => $customer_info['customer_id'],
                    'name' => $customer_info['firstname'] . ' ' . $customer_info['lastname']
                );

                $this->model_account_activity->addActivity('verify', $activity_data);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('account/login', '', true));
        } else {


            $this->session->data['error'] = $this->language->get('error_code');

            return new Action('account/login');
        }
    }

}
