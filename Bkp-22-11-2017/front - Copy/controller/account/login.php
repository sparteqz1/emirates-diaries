<?php

class ControllerAccountLogin extends Controller {

    private $error = array();
    private $redirect = '';
    private $setting = array();

    public function index() {
        $this->load->model('account/customer');


        // Login override for admin users
        if (!empty($this->request->get['token'])) {
            $this->customer->logout();

            unset($this->session->data['comment']);

            $customer_info = $this->model_account_customer->getCustomerByToken($this->request->get['token']);

            if ($customer_info && $this->customer->login($customer_info['email'], '', true)) {

                $this->response->redirect($this->url->link('account/account', '', true));
            }
        }

        if ($this->customer->isLogged()) {
            $this->response->redirect($this->url->link('account/account', '', true));
        }

        $this->load->language('account/login');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            // Unset guest
            unset($this->session->data['guest']);


            // Add to activity log
            if ($this->config->get('config_customer_activity')) {
                $this->load->model('account/activity');

                $activity_data = array(
                    'customer_id' => $this->customer->getId(),
                    'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
                );

                $this->model_account_activity->addActivity('login', $activity_data);
            }

            // Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
            if (isset($this->request->post['redirect']) && $this->request->post['redirect'] != $this->url->link('account/logout', '', true) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
                $this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
            } else {
                $this->response->redirect($this->url->link('account/account', '', true));
            }
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_login'),
            'href' => $this->url->link('account/login', '', true)
        );

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_new_customer'] = $this->language->get('text_new_customer');
        $data['text_register'] = $this->language->get('text_register');
        $data['text_register_account'] = $this->language->get('text_register_account');
        $data['text_returning_customer'] = $this->language->get('text_returning_customer');
        $data['text_i_am_returning_customer'] = $this->language->get('text_i_am_returning_customer');
        $data['text_forgotten'] = $this->language->get('text_forgotten');

        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_password'] = $this->language->get('entry_password');

        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_login'] = $this->language->get('button_login');

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } elseif (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['action'] = $this->url->link('account/login', '', true);
        $data['register'] = $this->url->link('account/register', '', true);
        $data['forgotten'] = $this->url->link('account/forgotten', '', true);
        $data['facebook_link'] = $this->url->link('account/login/sociallogin&provider=Facebook', '', true);


        // Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
        if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
            $data['redirect'] = $this->request->post['redirect'];
        } elseif (isset($this->session->data['redirect'])) {
            $data['redirect'] = $this->session->data['redirect'];

            unset($this->session->data['redirect']);
        } else {
            $data['redirect'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['password'])) {
            $data['password'] = $this->request->post['password'];
        } else {
            $data['password'] = '';
        }

        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('account/login', $data));
    }

    protected function validate() {
        // Check how many login attempts have been made.
        $login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

        if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
            $this->error['warning'] = $this->language->get('error_attempts');
        }

        // Check if customer has been approved.
        $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

        if ($customer_info && !$customer_info['approved']) {
            $this->error['warning'] = $this->language->get('error_approved');
        }
        
        if ($customer_info && !$customer_info['email_verified'] && $this->config->get('config_customer_email_verification')) {
            $this->error['warning'] = $this->language->get('error_email_verified');
        }

        if (!$this->error) {
            if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
                $this->error['warning'] = $this->language->get('error_login');

                $this->model_account_customer->addLoginAttempt($this->request->post['email']);
            } else {
                $this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
            }
        }

        return !$this->error;
    }

    public static function getCurrentUrl($request_uri = true) {
        if (
                isset($_SERVER['HTTPS']) && ( $_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1 ) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
        ) {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }

        $url = $protocol . $_SERVER['HTTP_HOST'];

        if (isset($_SERVER['SERVER_PORT']) && strpos($url, ':' . $_SERVER['SERVER_PORT']) === FALSE) {
            $url .= ($protocol === 'http://' && $_SERVER['SERVER_PORT'] != 80 && !isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) || ($protocol === 'https://' && $_SERVER['SERVER_PORT'] != 443 && !isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) ? ':' . $_SERVER['SERVER_PORT'] : '';
        }

        if ($request_uri) {
            $url .= $_SERVER['REQUEST_URI'];
        } else {
            $url .= $_SERVER['PHP_SELF'];
        }

        // return current url
        return $url;
    }

    public function sociallogin() {

        $this->setup();

        require_once(DIR_SYSTEM . 'library/Hybrid/Auth.php');
        $this->language->load('account/login');
        $this->load->model('account/loginsocial');

        $this->setting = $this->config->get('config_social_login_setting');
        $this->setting['base_url'] = $this->config->get('config_secure') ? HTTPS_SERVER . 'social_login.php' : HTTP_SERVER . 'social_login.php';

        if (isset($this->request->get['provider'])) {
            $this->setting['provider'] = $this->request->get['provider'];
        } else {

            // Save error to the System Log
            $this->log->write('Missing application provider.');

            // Set Message
            $this->session->data['error'] = sprintf("An error occurred, please <a href=\"%s\">notify</a> the administrator.", $this->url->link('information/contact'));

            // Redirect to the Login Page
            $this->response->redirect($this->redirect);
        }

        try {

            $hybridauth = new Hybrid_Auth($this->setting);
            $hybridauth::$logger->info('config_social_login: Start authantication.');
            $adapter = $hybridauth->authenticate($this->setting['provider']);
            $hybridauth::$logger->info('config_social_login: Start getUserProfile.');
            //get the user profile 
            $profile = $adapter->getUserProfile();
            $this->setting['profile'] = (array) $profile;

            $hybridauth::$logger->info('config_social_login: got UserProfile.' . serialize($this->setting['profile']));
            $authentication_data = array(
                'provider' => $this->setting['provider'],
                'identifier' => $this->setting['profile']['identifier'],
                'web_site_url' => $this->setting['profile']['webSiteURL'],
                'profile_url' => $this->setting['profile']['profileURL'],
                'photo_url' => $this->setting['profile']['photoURL'],
                'display_name' => $this->setting['profile']['displayName'],
                'description' => $this->setting['profile']['description'],
                'first_name' => $this->setting['profile']['firstName'],
                'last_name' => $this->setting['profile']['lastName'],
                'gender' => $this->setting['profile']['gender'],
                'language' => $this->setting['profile']['language'],
                'age' => $this->setting['profile']['age'],
                'birth_day' => $this->setting['profile']['birthDay'],
                'birth_month' => $this->setting['profile']['birthMonth'],
                'birth_year' => $this->setting['profile']['birthYear'],
                'email' => $this->setting['profile']['email'],
                'email_verified' => $this->setting['profile']['emailVerified'],
                'phone' => $this->setting['profile']['phone'],
                'address' => $this->setting['profile']['address'],
                'country' => $this->setting['profile']['country'],
                'region' => $this->setting['profile']['region'],
                'city' => $this->setting['profile']['city'],
                'zip' => $this->setting['profile']['zip']
            );

            $hybridauth::$logger->info('config_social_login: set authentication_data ' . serialize($authentication_data));

            //check by identifier
            $customer_id = $this->model_account_loginsocial->getCustomerByIdentifier($this->setting['provider'], $this->setting['profile']['identifier']);

            if ($customer_id) {
                $hybridauth::$logger->info('config_social_login: getCustomerByIdentifier success.');
                //login
                $this->model_account_loginsocial->login($customer_id);

                //redirect
                $this->response->redirect($this->redirect);
            }

            //check by email
            if ($this->setting['profile']['email']) {

                $customer_id = $this->model_account_loginsocial->getCustomerByEmail($this->setting['profile']['email']);
                if ($customer_id) {
                    $hybridauth::$logger->info('config_social_login: getCustomerByEmail success.');
                }
            }


            if (!$customer_id) {
                $hybridauth::$logger->info('config_social_login: no customer_id. creating customer_data');
                //prepare customer data
                $address = array();


                $customer_data = array(
                    'email' => $this->setting['profile']['email'],
                    'firstname' => $this->setting['profile']['firstName'],
                    'lastname' => $this->setting['profile']['lastName'],
                    'telephone' => $this->setting['profile']['phone'],
                    'newsletter' => 1, //$this->setting['newsletter'],
                    'customer_group_id' => (isset($this->setting['customer_group'])) ? $this->setting['customer_group'] : '1',
                    'company' => false,
                    'address_1' => '',
                    'city' => '',
                    'postcode' => '',
                    'country_id' => '',
                    'zone_id' => '',
                    'password' => '',
                    'social_image' => $this->setting['profile']['photoURL']
                );

                $hybridauth::$logger->info('config_social_login: set customer_data ' . serialize($customer_data));


                $customer_id = $this->model_account_loginsocial->addCustomer($customer_data);

                $authentication_data['customer_id'] = (int) $customer_id;
                $this->model_account_loginsocial->addAuthentication($authentication_data);

                //login
                $this->customer->loginsocial($customer_id);
                // Unset guest
                unset($this->session->data['guest']);


                // Add to activity log
                if ($this->config->get('config_customer_activity')) {
                    $this->load->model('account/activity');

                    $activity_data = array(
                        'customer_id' => $this->customer->getId(),
                        'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
                    );

                    $this->model_account_activity->addActivity('login', $activity_data);
                }

                $this->response->redirect($this->redirect);
            }

            if ($customer_id) {
                $hybridauth::$logger->info('config_social_login: customer_id found');
                $authentication_data['customer_id'] = (int) $customer_id;

                $this->model_account_loginsocial->addAuthentication($authentication_data);
                $hybridauth::$logger->info('config_social_login: addAuthentication');
                //login
                $this->model_account_loginsocial->login($customer_id);

                //redirect
                $this->response->redirect($this->redirect);
            }
        } catch (Exception $e) {

            switch ($e->getCode()) {
                case 0 : $error = "Unspecified error.";
                    break;
                case 1 : $error = "Hybriauth configuration error.";
                    break;
                case 2 : $error = "Provider not properly configured.";
                    break;
                case 3 : $error = "Unknown or disabled provider.";
                    break;
                case 4 : $error = "Missing provider application credentials.";
                    break;
                case 5 : $error = "Authentication failed. The user has canceled the authentication or the provider refused the connection.";
                    break;
                case 6 : $error = "User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.";
                    if (isset($adapter)) {
                        $adapter->logout();
                    }
                    break;
                case 7 : $error = "User not connected to the provider.";
                    break;
                case 8 : $error = "Provider does not support this feature.";
                    break;
            }
            if (isset($adapter)) {
                $adapter->logout();
            }

            $this->session->data['social_login_error'] = $error;

            $error .= "\n\nHybridAuth Error: " . $e->getMessage();
            $error .= "\n\nTrace:\n " . $e->getTraceAsString();

            $this->log->write($error);

            $this->response->redirect($this->redirect);
        }
    }

    private function setup() {
        // correct &amp; in url
        if (isset($this->request->get) && isset($_GET)) {

            foreach ($this->request->get as $key => $value) {
                $this->request->get[str_replace('amp;', '', $key)] = $value;
            }

            foreach ($_GET as $key => $value) {
                $_GET[str_replace('amp;', '', $key)] = $value;
            }
        }

        // set redirect address
        if (isset($this->session->data['redirect'])) {
            $this->redirect = $this->session->data['redirect'];
        } else {
            $this->redirect = $this->url->link('account/account', '', true);
        }
    }

}
