<?php

class ControllerExtensionModulePopularNews extends Controller {

    public function index($setting) {
        $this->load->language('extension/module/popular_news');

        $data['heading_title'] = $this->language->get('heading_title');


        $this->load->model('news/news');

        $this->load->model('tool/image');

        $data['news'] = array();

        $results = $this->model_news_news->getPopularNews($setting['limit']);

        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                }


                $data['news'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_news_description_length')) . '..',
                    'href' => $this->url->link('news/news/news_id', 'news_id=' . $result['news_id'])
                );
            }

            return $this->load->view('extension/module/popular_news', $data);
        }
    }

}
