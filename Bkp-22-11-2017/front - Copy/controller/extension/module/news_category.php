<?php

class ControllerExtensionModuleNewsCategory extends Controller {

    public function index() {
        $this->load->language('extension/module/news_category');

        $data['heading_title'] = $this->language->get('heading_title');

        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string) $this->request->get['path']);
        } else {
            $parts = array();
        }

        if (isset($parts[0])) {
            $data['category_id'] = $parts[0];
        } else {
            $data['category_id'] = 0;
        }

        if (isset($parts[1])) {
            $data['child_id'] = $parts[1];
        } else {
            $data['child_id'] = 0;
        }

        $this->load->model('news/category');

        $this->load->model('news/news');

        $data['categories'] = array();

        $categories = $this->model_news_category->getCategories(0);

        foreach ($categories as $category) {
            $filter_data = array(
                'filter_category_id' => $category['category_id'],
                'filter_sub_category' => true
            );

            $data['categories'][] = array(
                'category_id' => $category['category_id'],
                'name' => $category['name'],
                'total' => $this->model_news_news->getTotalNews($filter_data),
                'href' => $this->url->link('news/news', 'news_path=' . $category['category_id'])
            );
            if ($category['category_id'] == $data['category_id']) {
                $children = $this->model_news_category->getCategories($category['category_id']);

                foreach ($children as $child) {
                    $filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

                    $data['categories'][] = array(
                        'category_id' => $child['category_id'],
                        'name' => $child['name'],
                        'total' => $this->model_news_news->getTotalNews($filter_data),
                        'href' => $this->url->link('news/news', 'news_path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }
            }
        }

        return $this->load->view('extension/module/news_category', $data);
    }

}
