<?php

class ControllerStartupSeoUrl extends Controller {

    private $url_list = array(
        'common/home' => '',
        'account/account' => 'account',
        'account/edit' => 'account/edit',
        'account/password' => 'account/password',
        'account/address' => 'account/address',
        'account/address/edit' => 'account/address/edit',
        'account/login' => 'account/login',
        'account/logout' => 'account/logout',
        'account/newsletter' => 'eaccount/newsletter',
        'account/forgotten' => 'account/recover',
        'account/register' => 'account/register',
        'information/contact' => 'contact',
        'information/about' => 'about',
        'information/terms' => 'terms',
        //'information/category' => 'category',
        'information/latest' => 'latest-articles',
        'information/read' => 'most-read',
        'information/top' => 'top-articles',
        'information/details' => 'details',
        'gallery/gallery' => 'photo-library',
        'magazines/magazines' => 'magazines',
        'information/sitemap' => 'sitemap',
        'product/manufacturer' => 'brands',
        'product/compare' => 'compare',
        'product/search' => 'search',
        'news/author' => 'authors',
    );

    public function index() {
// Add rewrite to url class
        if ($this->config->get('config_seo_url')) {
            $this->url->addRewrite($this);
        }

// Decode URL
        if (isset($this->request->get['_route_'])) {
            $parts = explode('/', $this->request->get['_route_']);

// remove any empty arrays from trailing
            if (utf8_strlen(end($parts)) == 0) {
                array_pop($parts);
            }

            foreach ($parts as $part) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "'");

                if ($query->num_rows) {
                    $url = explode('=', $query->row['query']);


                    if ($url[0] == 'product_id') {
                        $this->request->get['product_id'] = $url[1];
                    }

                    if ($url[0] == 'category_id') {
                        if (!isset($this->request->get['path'])) {
                            $this->request->get['path'] = $url[1];
                        } else {
                            $this->request->get['path'] .= '_' . $url[1];
                        }
                    }

                    if ($url[0] == 'newscategory_id') {
                        if (!isset($this->request->get['news_path'])) {
                            $this->request->get['news_path'] = $url[1];
                        } else {
                            $this->request->get['news_path'] .= '_' . $url[1];
                        }
                    }
                    if ($url[0] == 'manufacturer_id') {
                        $this->request->get['manufacturer_id'] = $url[1];
                    }

                    if ($url[0] == 'information_id') {
                        $this->request->get['information_id'] = $url[1];
                    }
                    if ($url[0] == 'news_id') {
                        $this->request->get['news_id'] = $url[1];
                    }
                    if ($url[0] == 'author_id') {
                        $this->request->get['author_id'] = $url[1];
                    }

                    if ($query->row['query'] && $url[0] != 'information_id' && $url[0] != 'manufacturer_id' && $url[0] != 'category_id' && $url[0] != 'product_id' && $url[0] != 'newscategory_id' && $url[0] != 'news_id' && $url[0] != 'author_id') {
                        $this->request->get['route'] = $query->row['query'];
                    }
                } else {


                    if ($part != "author" && $part != "news") {
                        $this->request->get['route'] = 'error/not_found';

                        if ($_s = $this->setURL($this->request->get['_route_'])) {
                            $this->request->get['route'] = $_s;
                        }
                        break;
                    }
                }
            }

            if (!isset($this->request->get['route'])) {
                if (isset($this->request->get['product_id'])) {
                    $this->request->get['route'] = 'product/product';
                } elseif (isset($this->request->get['path'])) {
                    $this->request->get['route'] = 'product/category';
                } elseif (isset($this->request->get['manufacturer_id'])) {
                    $this->request->get['route'] = 'product/manufacturer/info';
                } elseif (isset($this->request->get['information_id'])) {
                    $this->request->get['route'] = 'information/information';
                } elseif (isset($this->request->get['news_id'])) {
                    $this->request->get['route'] = 'news/news/details';
                } elseif (isset($this->request->get['news_path'])) {
                    $this->request->get['route'] = 'news/news';
                } elseif (isset($this->request->get['author_id'])) {
                    $this->request->get['route'] = 'news/author/info';
                }
            }

//            if (isset($this->request->get['route'])) {
//               return new Action($this->request->get['route']);
//            }
        }
    }

    public function rewrite($link) {
        $url_info = parse_url(str_replace('&amp;', '&', $link));

        $url = '';

        $data = array();

        parse_str($url_info['query'], $data);

        foreach ($data as $key => $value) {
            if (isset($data['route'])) {
                if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') ||  ($data['route'] == 'information/information' && $key == 'information_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } else if ($data['route'] == 'news/author/info' && $key == 'author_id') {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/author/' . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } else if ($data['route'] == 'news/news/details' && $key == 'news_id') {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif ($key == 'path') {
                    $categories = explode('_', $value);

                    foreach ($categories as $category) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int) $category . "'");

                        if ($query->num_rows && $query->row['keyword']) {
                            $url .= '/' . $query->row['keyword'];
                        } else {
                            $url = '';

                            break;
                        }
                    }

                    unset($data[$key]);
                } elseif ($key == 'news_path') {
                    $categories = explode('_', $value);

                    foreach ($categories as $category) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'newscategory_id=" . (int) $category . "'");

                        if ($query->num_rows && $query->row['keyword']) {
                            $url .= '/news/' . $query->row['keyword'];
                        } else {
                            $url = '';

                            break;
                        }
                    }

                    unset($data[$key]);
                }
            }
        }

        if ($_u = $this->getURL($data['route'])) {


            $url .= $_u;

            if (isset($data['address_id'])) {
                $url .='?address_id=' . $data['address_id'];
            }
            if (isset($data['page'])) {
                $url .='?page=' . $data['page'];
            }
            unset($data[$key]);
        }


        if ($url) {
            unset($data['route']);

            $query = '';

            if ($data) {
                foreach ($data as $key => $value) {
                    $query .= '&' . rawurlencode((string) $key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string) $value));
                }

                if ($query) {
                    $query = '?' . str_replace('&', '&amp;', trim($query, '&'));
                }
            }

            return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
        } else {
            return $link;
        }
    }

    public function getURL($route) {

        if (count($this->url_list) > 0) {
            foreach ($this->url_list as $key => $value) {

                if ($route == $key) {
                    return '/' . $value;
                }
            }
        }
        return false;
    }

    public function setURL($_route) {
//echo $_route;
        if (count($this->url_list) > 0) {
            foreach ($this->url_list as $key => $value) {
                if ($_route == $value) {
//echo $_route;
                    return $key;
                }
            }
        }
        return false;
    }

}
