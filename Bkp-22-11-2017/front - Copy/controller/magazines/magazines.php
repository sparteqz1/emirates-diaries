<?php

class ControllerMagazinesMagazines extends Controller {

    private $error = array();

    public function index() {

        $this->load->model('news/news');
        $this->load->model('news/issue');

        $this->load->language('magazines/magazines');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');
        $data['banner_title'] = $this->language->get('banner_title');

        $this->load->model('tool/image');

        if ($this->config->get('config_image')) {
            $data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
        } else {
            $data['image'] = false;
        }

        //Magazines
        
        $data['ed_magazines'] = $this->load->controller('magazines/ed_magazines');

        //Featured News

        $data['featured'] = $this->load->controller('news/featured');   

        // Newsletter
        $data['newsletter'] = $this->load->controller('common/newsletter');    
        
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('magazines/magazines', $data));
    }
    
    public function details() {
        $this->load->language('magazines/magazines');

        $this->load->model('news/news');
        $this->load->model('news/category');
        $this->load->model('news/issue');
        
        $data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('tool/image');

        if ($this->config->get('config_image')) {
            $data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
        } else {
            $data['image'] = false;
        }

        if (isset($this->request->get['magazine'])) {
            $magazine_id = (int) $this->request->get['magazine'];
        } else {
            $magazine_id = 0;
        }        

        $magazine_info = $this->model_news_issue->getIssue($magazine_id);

        if ($magazine_info) {

            $this->document->setTitle($magazine_info['name']);

            $data['heading_title'] = $magazine_info['name'];            

        $data['categories'] = array();

        $categories = $this->model_news_category->getCategories(0);

        
        foreach ($categories as $category) {
                $category_id = $category['category_id'];
                $category_info = $this->model_news_category->getCategory($category_id);

                if ($category_info) {
                    $news = array();

                    $filter_data = array(
                        'filter_category_id' => $category_id,
                        'filter_issue_id' => $magazine_id
                    );


                    $results = $this->model_news_news->getAllNews($filter_data);


                    foreach ($results as $result) {
                        if ($result) {
                            if ($result['image']) {
                                $image = $this->model_tool_image->resize($result['image'], 900, 600);
                            } else {
                                $image = $this->model_tool_image->resize('placeholder.png', 900, 600);
                            }

                            $news[] = array(
                                'news_id' => $result['news_id'],
                                'thumb' => $image,
                                'name' => $result['name'],
                                'date' => date("j M, Y", strtotime($result['date_added'])),
                                'author' => $result['author'],
                                'href' => $this->url->link('news/news/details', 'news_id=' . $result['news_id'])
                            );
                        }
                    }
                    
                    $data['categories'][] = array(
                        'category_id' => $category_info['category_id'],
                        'news' => $news,
                        'name' => $category_info['name']
                    );
                }
        }

        $filter_issue = array(
                        'filter_issue_id' => $magazine_id
                    );
        $data['issue_results'] = $this->model_news_news->getAllNews($filter_issue);

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            
            $data['single'] = $this->url->link('information/details');
            $data['artcle'] = $this->url->link('magazines/magazines/details');

            $this->response->setOutput($this->load->view('magazines/details', $data));
        } else {

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }        
        
    }

}
