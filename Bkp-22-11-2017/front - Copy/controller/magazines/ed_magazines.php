<?php

class ControllerMagazinesEdMagazines extends Controller {

    public function index() {

        $this->load->language('information/ed_magazines');
        
        $data['magazines'] = array();
        $this->load->model('magazines/magazines');
        $this->load->model('tool/image');
        $all_magazines = $this->model_magazines_magazines->getMagazines();

        foreach ($all_magazines as $magazines_data) {                      

            if ($magazines_data['cover']) {
                $magazineimage = $this->model_tool_image->resize($magazines_data['cover'], 379, 425);
            } else {
                $magazineimage = $this->model_tool_image->resize('placeholder.png', 379, 425);
            }

            $data['magazines'][] = array(
                'issue_id' => $magazines_data['issue_id'],
                'sort' => $magazines_data['sort_order'],
                'magazineimage' => $magazineimage,
                'name' => $magazines_data['name'],
                'href' => $this->url->link('magazines/magazines/details', 'magazine=' . $magazines_data['issue_id'])
            );
        }

        $data['text_latest_magazines'] = $this->language->get('text_latest_magazines');

        return $this->load->view('magazines/ed_magazines', $data);
    }

}
