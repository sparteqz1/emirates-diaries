<?php

class ControllerNewsAuthor extends Controller {

    public function index() {
        $this->load->language('news/author');

        $this->load->model('news/author');

        $this->load->model('tool/image');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_index'] = $this->language->get('text_index');
        $data['text_empty'] = $this->language->get('text_empty');

        $data['button_continue'] = $this->language->get('button_continue');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_brand'),
            'href' => $this->url->link('news/author')
        );

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['baner_logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['baner_logo'] = '';
        }


        $data['categories'] = array();

        $results = $this->model_news_author->getAuthors();

        foreach ($results as $result) {
            if (is_numeric(utf8_substr($result['name'], 0, 1))) {
                $key = '0 - 9';
            } else {
                $key = utf8_substr(utf8_strtoupper($result['name']), 0, 1);
            }

            if (!isset($data['categories'][$key])) {
                $data['categories'][$key]['name'] = $key;
            }

            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
            }

            $data['categories'][$key]['authors'][] = array(
                'name' => $result['name'],
                'thumb' => $image,
                'href' => $this->url->link('news/author/info', 'author_id=' . $result['author_id'])
            );
        }



        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');


        $data['flash'] = $this->load->controller('common/flash');


        $this->response->setOutput($this->load->view('news/author_list', $data));
    }

    public function info() {
        $this->load->language('news/author');

        $this->load->model('news/author');

        $this->load->model('news/news');

        $this->load->model('tool/image');

        if (isset($this->request->get['author_id'])) {
            $author_id = (int) $this->request->get['author_id'];
        } else {
            $author_id = 0;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'n.sort_order';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int) $this->request->get['limit'];
        } else {
            $limit = (int) $this->config->get($this->config->get('config_theme') . '_news_limit');
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_brand'),
            'href' => $this->url->link('news/author')
        );

        $author_info = $this->model_news_author->getAuthor($author_id);

        if ($author_info) {
            $this->document->setTitle($author_info['name']);

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $author_info['name'],
                'href' => $this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'] . $url)
            );

            $data['heading_title'] = $author_info['name'];

            if ($author_info['image']) {
                $author_image = $this->model_tool_image->resize($author_info['image'], $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
            } else {
                $author_image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
            }

            $data['author_image'] = $author_image;

            $data['text_empty'] = $this->language->get('text_empty');
            $data['text_author'] = $this->language->get('text_author');
            $data['text_sort'] = $this->language->get('text_sort');
            $data['text_limit'] = $this->language->get('text_limit');

            $data['button_continue'] = $this->language->get('button_continue');


            $data['news'] = array();

            $filter_data = array(
                'filter_author_id' => $author_id,
                'sort' => $sort,
                'order' => $order,
                'start' => ($page - 1) * $limit,
                'limit' => $limit
            );

            $news_total = $this->model_news_news->getTotalNews($filter_data);

            $results = $this->model_news_news->getAllNews($filter_data);

            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
                }


                $data['news'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'date' => $result['ago'],
                    'badge' => $result['badge'],
                    'author' => $result['author'],
                    'viewed' => $result['viewed'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_news_description_length')) . '..',
                    'href' => $this->url->link('news/news/details', 'author_id=' . $result['author_id'] . '&news_id=' . $result['news_id'] . $url)
                );
            }

            $url = '';

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['sorts'] = array();

            $data['sorts'][] = array(
                'text' => $this->language->get('text_default'),
                'value' => 'n.sort_order-ASC',
                'href' => $this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'] . '&sort=n.sort_order&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_name_asc'),
                'value' => 'nd.name-ASC',
                'href' => $this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'] . '&sort=nd.name&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_name_desc'),
                'value' => 'nd.name-DESC',
                'href' => $this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'] . '&sort=nd.name&order=DESC' . $url)
            );



            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            $data['limits'] = array();

            $limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_news_limit'), 25, 50, 75, 100));

            sort($limits);

            foreach ($limits as $value) {
                $data['limits'][] = array(
                    'text' => $value,
                    'value' => $value,
                    'href' => $this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'] . $url . '&limit=' . $value)
                );
            }

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $pagination = new Pagination();
            $pagination->total = $news_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'] . $url . '&page={page}');

            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($news_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($news_total - $limit)) ? $news_total : ((($page - 1) * $limit) + $limit), $news_total, ceil($news_total / $limit));

            if ($page == 1) {
                $this->document->addLink($this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'], true), 'canonical');
            } elseif ($page == 2) {
                $this->document->addLink($this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'], true), 'prev');
            } else {
                $this->document->addLink($this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'] . $url . '&page=' . ($page - 1), true), 'prev');
            }

            if ($limit && ceil($news_total / $limit) > $page) {
                $this->document->addLink($this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'] . $url . '&page=' . ($page + 1), true), 'next');
            }

            $data['sort'] = $sort;
            $data['order'] = $order;
            $data['limit'] = $limit;



            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $data['flash'] = $this->load->controller('common/flash');

            $this->response->setOutput($this->load->view('news/author_info', $data));
        } else {
            $url = '';

            if (isset($this->request->get['author_id'])) {
                $url .= '&author_id=' . $this->request->get['author_id'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('news/author/info', $url)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['header'] = $this->load->controller('common/header');
            $data['footer'] = $this->load->controller('common/footer');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

}
