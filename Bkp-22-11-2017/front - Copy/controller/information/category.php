<?php

class ControllerInformationCategory extends Controller {

    private $error = array();

    public function index() {

        $this->load->model('news/news');

        $this->load->model('tool/image');
        $this->load->model('news/category');
        $this->load->model('news/issue');

        $this->load->language('information/category');

        if (isset($this->request->get['category'])) {
            $category_id = (int) $this->request->get['category'];
        } else {
            $category_id = 0;
        }

        $category_info = $this->model_news_category->getCategory($category_id);

        if ($category_info) {

            $data['menu_category'] = $category_info['name'];

            $articles = array();

                $filter_data = array(
                    'filter_category_id' => $category_id
                );

                $results = $this->model_news_news->getAllNews($filter_data);

                foreach ($results as $result) {
                        if ($result) {
                            if ($result['image']) {
                                $image = $this->model_tool_image->resize($result['image'], 900, 600);
                            } else {
                                $image = $this->model_tool_image->resize('placeholder.png', 900, 600);
                            }

                            $issue = $this->model_news_issue->getIssue($result['issue_id']);


                            $data['articles'][] = array(
                                'news_id' => $result['news_id'],
                                'thumb' => $image,
                                'issue' => $issue['name'],
                                'name' => $result['name'],
                                'date' => date("j M, Y", strtotime($result['date_added'])),
                                'ago' => $result['ago'],
                                'author' => $result['author'],
                                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 284) . '..',
                                'href' => $this->url->link('news/news/details', 'news_id=' . $result['news_id'])
                            );
                        }
                    }

            $data['ed_magazines'] = $this->load->controller('magazines/ed_magazines');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['heading_title'] = $this->language->get('heading_title');
            $data['text_latest_magazines'] = $this->language->get('text_latest_magazines');

            $this->load->model('tool/image');

            if ($this->config->get('config_image')) {
                $data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
            } else {
                $data['image'] = false;
            }

        
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            
            $data['artcle'] = $this->url->link('magazines/magazines/details');

            $this->response->setOutput($this->load->view('information/category', $data));

        } else {
            $url = '';

            if (isset($this->request->get['category'])) {
                $url .= '&category=' . $this->request->get['category'];
            }
            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }


        
    }

}
