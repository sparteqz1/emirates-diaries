<?php

class ControllerInformationLatest extends Controller {

    private $error = array();

    public function index() {

        $this->load->model('news/news');
        $this->load->model('news/issue');

        $this->load->language('information/latest');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');
        $data['banner_title'] = $this->language->get('banner_title');

        $this->load->model('tool/image');

        if ($this->config->get('config_image')) {
            $data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
        } else {
            $data['image'] = false;
        }

        $data['latestnews'] = array();

        $results = $this->model_news_news->getLatestNews(NULL);

        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], 900, 600);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', 900, 600);
                }
                $issue_latest = $this->model_news_issue->getIssue($result['issue_id']);


                $data['latestnews'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'latest_issue' => $issue_latest['name'],
                    'date' => date("j M, Y", strtotime($result['date_added'])),
                    'days' => $result['ago'],
                    'author' => $result['author'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 284) . '..',
                    'href' => $this->url->link('news/news/details', 'news_id=' . $result['news_id'])
                );
            }
        }

        // Newsletter
        $data['newsletter'] = $this->load->controller('common/newsletter');

        
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('information/latest', $data));
    }

}
