<?php
// Heading
$_['heading_title'] = 'Your Account Has Been Created!';

// Text
$_['text_message']  = '<p>Congratulations! Your new account has been successfully created! You can now take advantage of member privileges to enhance your  experience with us.</p> <p>If you have ANY questions about this website please <a href="%s">contact us</a>.</p>';
$_['text_approval'] = '<p>Thank you for registering with %s!</p><p>You will be notified by e-mail once your account has been activated by adnin team</p><p>If you have ANY questions about the operation of this website, please <a href="%s">contact us</a>.</p>';
$_['text_account']  = 'Account';
$_['text_success']  = 'Success';
