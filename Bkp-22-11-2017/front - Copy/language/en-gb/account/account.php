<?php
// Heading
$_['heading_title']      = 'My Account';

// Text
$_['text_account']       = 'Account';
$_['text_my_account']    = 'My Account';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit your account information';
$_['text_password']      = 'Change your password';
$_['text_address']       = 'Modify your address book entries';
$_['text_newsletter']    = 'Subscribe / unsubscribe to newsletter';