<?php
// Text
$_['text_home']          = 'Home';
$_['text_category']      = 'Categories';
$_['text_all']           = 'Show All';
$_['menu_all_magazines'] = 'All Magazines';
$_['menu_latest_articles'] = 'Latest Articles';
$_['menu_top_articles']    = 'Top Articles';
$_['menu_most_read']       = 'Most Read';
$_['menu_photo_library']   = 'Photo Library';

$_['menu_about']           = 'About Us';
$_['menu_advertise'] = 'Advertise with us ';
$_['menu_privacy']           = 'Privacy Policy';
$_['menu_career']          = 'Career';
$_['menu_contact']         = 'Contact Us';