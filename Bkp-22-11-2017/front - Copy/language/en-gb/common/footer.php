<?php
// Text
$_['text_information']  = 'Information';
$_['text_contact']      = 'Contact Us';
$_['text_newsletter']   = 'Newsletter';

$_['menu_media_kit']   = 'Media Kit';
$_['menu_about_us']   = 'About Us';
$_['menu_contact_us']   = 'Contact Us';
$_['menu_career']   = 'Career';
$_['menu_privacy_policy']   = 'Privacy Policy';
$_['menu_advertise_us']   = 'Advertise With Us';
$_['text_copy_rights']   = '2017 Emirates Diaries. All Rights Reserved.';

$_['text_follow_us_on']      = 'FOLLOW US ON ';
$_['text_instagram']      = 'Instagram';
$_['text_follow_us']      = 'Follow Us';
$_['text_subscribe']      = 'Subscribe';
$_['text_subscribe_desc']      = 'Subscribe now and get a FREE new edition.';
$_['text_follow_us_desc']      = 'Follow @emiratesdiaries for new updates';
$_['text_call_us']      = 'Call Us Now ';
$_['text_send_us']      = 'Send Us A Message';
$_['text_office_tele']      = 'Office Telephone';
$_['text_reach_us']      = 'Reach To Us';
$_['text_email']      = 'Email';
$_['text_magazine']      = 'Magazine';
