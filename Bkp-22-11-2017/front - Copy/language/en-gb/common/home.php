<?php


// Text
$_['text_welcome_desc']       = 'We are a team of Emirati editors, writers, designers and photographers. Together, we make it our mission to follow the trails of Emirati talents to wherever they lead us and showcase their endless achievements as well as offer Emirati writers a chance to share their work with a larger audience. Every issue we make revolves around a unique theme that combines articles from various fields such as art, sports, science and technology, education, initiatives, people and more. Here at Emirates Diaries, we continuously strive to paint our homegrown issues with red, green, white and black.';

$_['text_latest_articles'] = 'LATEST ARTICLES';
$_['text_most_read'] = 'MOST READ';
$_['text_latest_magazines'] = 'Latest Magazines';