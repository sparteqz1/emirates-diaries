<?php
// Heading
$_['heading_title']    = 'Site Map';

$_['text_account']     = 'My Account';
$_['text_edit']        = 'Account Information';
$_['text_password']    = 'Password';
$_['text_search']      = 'Search';
$_['text_information'] = 'Information';
$_['text_contact']     = 'Contact Us';