<?php
// Heading
$_['heading_title']  = 'Contact Us';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Telephone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';

// Entry
$_['entry_name']     = 'Your Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Enquiry';

// Email
$_['email_subject']  = 'Enquiry %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';
$_['error_null_email']  = 'Enter Your Email';

$_['success_subscription']   = "Your are successfully subscribed to our newsletter services!!";
$_['error_exist'] = "Email already exists!!!";

$_['text_reach_us']  = 'Reach To Us.';
$_['text_get_in_touch']  = 'Get in Touch.';
$_['text_write_story']  = 'Want to write a story about ED';
$_['text_general_enquiries']  = 'For General Enquiries';
$_['text_advertise']  = 'Want to advertise on ED';
$_['text_want_write']  = 'Want ED to write about you?';