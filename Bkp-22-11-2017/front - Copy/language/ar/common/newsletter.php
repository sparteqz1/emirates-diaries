<?php


// Text
$_['text_subscribe']    = 'اشترك الآن';
$_['text_newsletter']    = 'النشرة الدورية';
$_['text_signup']    = 'اشترك في نشرتنا الدورية لتبقى على إطلاع بآخر الأخبار';
$_['text_newsletter_placeholder']          = 'البريد الإلكتروني';