<?php

// Text
$_['text_home']          = 'الصفحة الرئيسية 	';
$_['text_category']      = 'أقسام الموقع';
$_['text_all']           = 'اذهب الى قسم';
$_['menu_all_magazines'] = 'جميع المجلات';
$_['menu_latest_articles'] = 'أحدث المقالات';
$_['menu_top_articles']    = 'أبرز المقالات';
$_['menu_most_read']       = 'الأكثر قراءة ';
$_['menu_photo_library'] = 'مكتبة الصور';

// Menu
$_['menu_about']           = 'عن المجلة ';
$_['menu_advertise'] = ' أعلن معنا';
$_['menu_privacy']           = 'سياسة الخصوصية ';
$_['menu_career']          = 'الوظائف';
$_['menu_contact']         = 'تواصل معنا';