<?php
// Heading
$_['heading_title']     = 'News';
$_['text_news']         = 'News';
$_['text_error']        = 'Category not found!';
$_['text_empty']        = 'There are no news to list in this category.';
$_['text_author']       = 'Author:';
$_['tab_description']          = 'Description';
$_['tab_attribute']            = 'Specification';
$_['text_related']             = 'Related News';
$_['text_tags']                = 'Tags:';
$_['text_further_read']                = 'اقرأ أكثر';
$_['text_latest_magazines']                = 'أحدث الأعداد';