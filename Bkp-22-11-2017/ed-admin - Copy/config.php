<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/bitbucket/emirates-diaries/ed-admin/');
define('HTTP_CATALOG', 'http://localhost/bitbucket/emirates-diaries/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/bitbucket/emirates-diaries/ed-admin/');
define('HTTPS_CATALOG', 'http://localhost/bitbucket/emirates-diaries/');

// DIR
define('DIR_APPLICATION', 'D:/xampp/htdocs/bitbucket/emirates-diaries/ed-admin/');
define('DIR_SYSTEM', 'D:/xampp/htdocs/bitbucket/emirates-diaries/system/');
define('DIR_IMAGE', 'D:/xampp/htdocs/bitbucket/emirates-diaries/image/');
define('DIR_LANGUAGE', 'D:/xampp/htdocs/bitbucket/emirates-diaries/ed-admin/language/');
define('DIR_TEMPLATE', 'D:/xampp/htdocs/bitbucket/emirates-diaries/ed-admin/view/template/');
define('DIR_CONFIG', 'D:/xampp/htdocs/bitbucket/emirates-diaries/system/config/');
define('DIR_CACHE', 'D:/xampp/htdocs/bitbucket/emirates-diaries/system/storage/cache/');
define('DIR_DOWNLOAD', 'D:/xampp/htdocs/bitbucket/emirates-diaries/system/storage/download/');
define('DIR_LOGS', 'D:/xampp/htdocs/bitbucket/emirates-diaries/system/storage/logs/');
define('DIR_MODIFICATION', 'D:/xampp/htdocs/bitbucket/emirates-diaries/system/storage/modification/');
define('DIR_UPLOAD', 'D:/xampp/htdocs/bitbucket/emirates-diaries/system/storage/upload/');
define('DIR_CATALOG', 'D:/xampp/htdocs/bitbucket/emirates-diaries/front/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'emirates-diaries');
define('DB_PORT', '3306');
define('DB_PREFIX', 'sp_');
