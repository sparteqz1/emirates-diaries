<?php

class ModelContactContact extends Model {
    
    public function deleteSubscriber($subscribe_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "subscription WHERE subscribe_id = '" . (int) $subscribe_id . "'");
       
        $this->cache->delete('subscription');
    }
    
    public function getTotalSubscribers($data = array()) {
        $sql = "SELECT COUNT(DISTINCT subscribe_id) AS total FROM " . DB_PREFIX . "subscription ORDER BY subscribe_id DESC"; 
        
        $query = $this->db->query($sql);

        return $query->row['total'];
    }
    
    public function getSubscribers($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "subscription ";


        $sort_data = array(
            'email',
            'contact_text',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY email";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }
}
