<?php

class ModelProjectClient extends Model {

    public function addClient($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "project_clients SET name = '" . $this->db->escape($data['name']) . "', client_remarks = '" . $this->db->escape($data['client_remarks']) . "', client_address = '" . $this->db->escape($data['client_address']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', website = '" . $this->db->escape($data['website']) . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $project_client_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "project_clients SET image = '" . $this->db->escape($data['image']) . "' WHERE project_client_id = '" . (int) $project_client_id . "'");
        }

       

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'project_client_id=" . (int) $project_client_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        return $project_client_id;
    }

    public function editClient($project_client_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "project_clients SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int) $data['sort_order'] . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', website = '" . $this->db->escape($data['website']) . "', client_remarks = '" . $this->db->escape($data['client_remarks']) . "', client_address = '" . $this->db->escape($data['client_address']) . "', status = '" . (int) $data['status'] . "', date_modified = NOW() WHERE project_client_id = '" . (int) $project_client_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "project_clients SET image = '" . $this->db->escape($data['image']) . "' WHERE project_client_id = '" . (int) $project_client_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'project_client_id=" . (int) $project_client_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'project_client_id=" . (int) $project_client_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

    }

    public function deleteClient($project_client_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "project_clients WHERE project_client_id = '" . (int) $project_client_id . "'");
       
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'project_client_id=" . (int) $project_client_id . "'");

        
    }

    public function getClient($project_client_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'project_client_id=" . (int) $project_client_id . "') AS keyword FROM " . DB_PREFIX . "project_clients WHERE project_client_id = '" . (int) $project_client_id . "'");

        return $query->row;
    }

    public function getClients($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "project_clients";

        if (!empty($data['filter_name'])) {
            $sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    

    public function getTotalClients() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "project_clients");

        return $query->row['total'];
    }
    
    public function getTotalProjectsByClientId($project_client_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "projects WHERE project_client_id = '" . (int) $project_client_id . "'");

        return $query->row['total'];
    }

}
