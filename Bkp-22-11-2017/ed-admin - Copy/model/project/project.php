<?php

class ModelProjectProject extends Model {

    public function addProject($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "projects SET name = '" . $this->db->escape($data['name']) . "', subtitle = '" . $this->db->escape($data['project_sub_title']) . "', project_description = '" . $this->db->escape($data['project_description']) . "', project_client_id = '" . $this->db->escape($data['project_client_id']) . "', price = '" . $this->db->escape($data['price']) . "', project_category_id = '" . $this->db->escape($data['project_category_id']) . "', link = '" . $this->db->escape($data['link']) . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $project_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "projects SET image = '" . $this->db->escape($data['image']) . "' WHERE project_id = '" . (int) $project_id . "'");
        }

       if (isset($data['project_image'])) {
            foreach ($data['project_image'] as $project_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "project_image SET project_id = '" . (int) $project_id . "', image = '" . $this->db->escape($project_image['image']) . "', sort_order = '" . (int) $project_image['sort_order'] . "'");
            }
        }

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'project_id=" . (int) $project_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        return $project_id;
    }

    public function editProject($project_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "projects SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int) $data['sort_order'] . "', project_client_id = '" . $this->db->escape($data['project_client_id']) . "', price = '" . $this->db->escape($data['price']) . "', project_category_id = '" . $this->db->escape($data['project_category_id']) . "', link = '" . $this->db->escape($data['link']) . "', subtitle = '" . $this->db->escape($data['project_sub_title']) . "', project_description = '" . $this->db->escape($data['project_description']) . "', status = '" . (int) $data['status'] . "', date_modified = NOW() WHERE project_id = '" . (int) $project_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "projects SET image = '" . $this->db->escape($data['image']) . "' WHERE project_id = '" . (int) $project_id . "'");
        }        
        
        if (isset($data['project_image'])) {
            foreach ($data['project_image'] as $project_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "project_image SET project_id = '" . (int) $project_id . "', image = '" . $this->db->escape($project_image['image']) . "', sort_order = '" . (int) $project_image['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'project_id=" . (int) $project_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'project_id=" . (int) $project_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

    }

    public function deleteProject($project_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "projects WHERE project_id = '" . (int) $project_id . "'");
       
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'project_id=" . (int) $project_id . "'");

        
    }

    public function getProject($project_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'project_id=" . (int) $project_id . "') AS keyword FROM " . DB_PREFIX . "projects WHERE project_id = '" . (int) $project_id . "'");

        return $query->row;
    }

    public function getProjects($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "projects";

        if (!empty($data['filter_name'])) {
            $sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    

    public function getTotalProjects() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "projects");

        return $query->row['total'];
    }
    
    
    
    public function getProjectImages($project_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project_image WHERE project_id = '" . (int) $project_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

}
