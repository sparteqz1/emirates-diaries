<?php

class ModelNewsIssue extends Model {

    public function addIssue($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "newsissue SET sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $issue_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "newsissue SET image = '" . $this->db->escape($data['image']) . "' WHERE issue_id = '" . (int) $issue_id . "'");
        }
        if (isset($data['cover'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "newsissue SET cover = '" . $this->db->escape($data['cover']) . "' WHERE issue_id = '" . (int) $issue_id . "'");
        }

        foreach ($data['issue_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "newsissue_description SET issue_id = '" . (int) $issue_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'issue_id=" . (int) $issue_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('issue');

        return $issue_id;
    }

    public function editIssue($issue_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "newsissue SET sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW() WHERE issue_id = '" . (int) $issue_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "newsissue SET image = '" . $this->db->escape($data['image']) . "' WHERE issue_id = '" . (int) $issue_id . "'");
        }
        if (isset($data['cover'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "newsissue SET cover = '" . $this->db->escape($data['cover']) . "' WHERE issue_id = '" . (int) $issue_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "newsissue_description WHERE issue_id = '" . (int) $issue_id . "'");

        foreach ($data['issue_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "newsissue_description SET issue_id = '" . (int) $issue_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }

        
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'issue_id=" . (int) $issue_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'issue_id=" . (int) $issue_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('issue');
    }

    public function deleteIssue($issue_id) {
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "newsissue WHERE issue_id = '" . (int) $issue_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "newsissue_description WHERE issue_id = '" . (int) $issue_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'issue_id=" . (int) $issue_id . "'");

        $this->cache->delete('issue');
    }

    
    public function getIssue($issue_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias "
                . "WHERE query = 'issue_id=" . (int) $issue_id . "') AS keyword "
                . "FROM " . DB_PREFIX . "newsissue c LEFT JOIN " . DB_PREFIX . "newsissue_description cd2 "
                . "ON (c.issue_id = cd2.issue_id) WHERE c.issue_id = '" . (int) $issue_id . "' "
                . "AND cd2.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getIssues($data = array()) {
        $sql = "SELECT c.issue_id AS issue_id, "
                . "cd.name, c.sort_order FROM " . DB_PREFIX . "newsissue c "
                . "LEFT JOIN " . DB_PREFIX . "newsissue_description cd ON (c.issue_id = cd.issue_id) "
                . "WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY c.issue_id";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getIssueDescriptions($issue_id) {
        $issue_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "newsissue_description WHERE issue_id = '" . (int) $issue_id . "'");

        foreach ($query->rows as $result) {
            $issue_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description']
            );
        }

        return $issue_description_data;
    }

    

    public function getTotaIssues() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "newsissue");

        return $query->row['total'];
    }

}
