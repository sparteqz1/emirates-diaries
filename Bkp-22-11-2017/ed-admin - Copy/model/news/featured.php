<?php

class ModelNewsFeatured extends Model {

    public function addPost($data) {
        
        if (isset($data['news_featured'])) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "news_featured");
            foreach ($data['news_featured'] as $related_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_featured SET news_id = '" . (int) $related_id . "', date_added = NOW()");
              
                }
        }
    }
    
    public function getPost($news_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "news p LEFT JOIN " . DB_PREFIX . "news_description pd ON (p.news_id = pd.news_id) WHERE p.news_id = '" . (int) $news_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    
    public function getNewsRelated() {
        $news_featured_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_featured ");

        foreach ($query->rows as $result) {
            $news_featured_data[] = $result['news_id'];
        }

        return $news_featured_data;
    }

}
