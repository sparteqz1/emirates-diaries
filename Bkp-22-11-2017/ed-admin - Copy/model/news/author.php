<?php

class ModelNewsAuthor extends Model {

    public function addAuthor($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "author SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int) $data['sort_order'] . "'");

        $author_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "author SET image = '" . $this->db->escape($data['image']) . "' WHERE author_id = '" . (int) $author_id . "'");
        }

       

        if (isset($data['keyword'])) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'author_id=" . (int) $author_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('author');

        return $author_id;
    }

    public function editAuthor($author_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "author SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int) $data['sort_order'] . "' WHERE author_id = '" . (int) $author_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "author SET image = '" . $this->db->escape($data['image']) . "' WHERE author_id = '" . (int) $author_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'author_id=" . (int) $author_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'author_id=" . (int) $author_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('author');
    }

    public function deleteAuthor($author_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "author WHERE author_id = '" . (int) $author_id . "'");
       
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'author_id=" . (int) $author_id . "'");

        $this->cache->delete('author');
    }

    public function getAuthor($author_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'author_id=" . (int) $author_id . "') AS keyword FROM " . DB_PREFIX . "author WHERE author_id = '" . (int) $author_id . "'");

        return $query->row;
    }

    public function getAuthors($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "author";

        if (!empty($data['filter_name'])) {
            $sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    

    public function getTotalAuthors() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "author");

        return $query->row['total'];
    }

}
