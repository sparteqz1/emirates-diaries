<?php

class ControllerProjectClient extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('project/client');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('project/client');

        $this->getList();
    }

    public function add() {
        $this->load->language('project/client');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('project/client');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_project_client->addClient($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('project/client', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit() {
        $this->load->language('project/client');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('project/client');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_project_client->editClient($this->request->get['project_client_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('project/client', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('project/client');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('project/client');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $project_client_id) {
                $this->model_project_client->deleteClient($project_client_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('project/client', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('project/client', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['add'] = $this->url->link('project/client/add', 'token=' . $this->session->data['token'] . $url, true);
        $data['delete'] = $this->url->link('project/client/delete', 'token=' . $this->session->data['token'] . $url, true);

        $data['clients'] = array();

        $filter_data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $client_total = $this->model_project_client->getTotalClients();

        $results = $this->model_project_client->getClients($filter_data);

        foreach ($results as $result) {
            $data['clients'][] = array(
                'project_client_id' => $result['project_client_id'],
                'name' => $result['name'],
                'sort_order' => $result['sort_order'],
                'edit' => $this->url->link('project/client/edit', 'token=' . $this->session->data['token'] . '&project_client_id=' . $result['project_client_id'] . $url, true)
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('project/client', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
        $data['sort_sort_order'] = $this->url->link('project/client', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $client_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('project/client', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($client_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($client_total - $this->config->get('config_limit_admin'))) ? $client_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $client_total, ceil($client_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('project/client_list', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['project_client_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_percent'] = $this->language->get('text_percent');
        $data['text_amount'] = $this->language->get('text_amount');
        
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_website'] = $this->language->get('entry_website');
        $data['entry_telephone'] = $this->language->get('entry_telephone');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_keyword'] = $this->language->get('entry_keyword');
        $data['entry_client_address'] = $this->language->get('entry_client_address');
        $data['entry_client_remarks'] = $this->language->get('entry_client_remarks');
        
        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_customer_group'] = $this->language->get('entry_customer_group');

        $data['help_keyword'] = $this->language->get('help_keyword');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = '';
        }
        if (isset($this->error['client_address'])) {
            $data['error_client_address'] = $this->error['client_address'];
        } else {
            $data['error_client_address'] = '';
        }
        if (isset($this->error['client_remarks'])) {
            $data['error_client_remarks'] = $this->error['client_remarks'];
        } else {
            $data['error_client_remarks'] = '';
        }
        
        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }
        if (isset($this->error['website'])) {
            $data['error_website'] = $this->error['website'];
        } else {
            $data['error_website'] = '';
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('project/client', 'token=' . $this->session->data['token'] . $url, true)
        );

        if (!isset($this->request->get['project_client_id'])) {
            $data['action'] = $this->url->link('project/client/add', 'token=' . $this->session->data['token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('project/client/edit', 'token=' . $this->session->data['token'] . '&project_client_id=' . $this->request->get['project_client_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('project/client', 'token=' . $this->session->data['token'] . $url, true);

        if (isset($this->request->get['project_client_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $client_info = $this->model_project_client->getClient($this->request->get['project_client_id']);
        }

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($client_info)) {
            $data['name'] = $client_info['name'];
        } else {
            $data['name'] = '';
        }


        if (isset($this->request->post['keyword'])) {
            $data['keyword'] = $this->request->post['keyword'];
        } elseif (!empty($client_info)) {
            $data['keyword'] = $client_info['keyword'];
        } else {
            $data['keyword'] = '';
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($client_info)) {
            $data['image'] = $client_info['image'];
        } else {
            $data['image'] = '';
        }
        
        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($client_info)) {
            $data['status'] = $client_info['status'];
        } else {
            $data['status'] = true;
        }
        if (isset($this->request->post['client_address'])) {
            $data['client_address'] = $this->request->post['client_address'];
        } elseif (!empty($client_info)) {
            $data['client_address'] = $client_info['client_address'];
        } else {
            $data['client_address'] = '';
        }
        if (isset($this->request->post['client_remarks'])) {
            $data['client_remarks'] = $this->request->post['client_remarks'];
        } elseif (!empty($client_info)) {
            $data['client_remarks'] = $client_info['client_remarks'];
        } else {
            $data['client_remarks'] = '';
        }
        
        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } elseif (!empty($client_info)) {
            $data['email'] = $client_info['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $data['telephone'] = $this->request->post['telephone'];
        } elseif (!empty($client_info)) {
            $data['telephone'] = $client_info['telephone'];
        } else {
            $data['telephone'] = '';
        }
        
        if (isset($this->request->post['website'])) {
            $data['website'] = $this->request->post['website'];
        } elseif (!empty($client_info)) {
            $data['website'] = $client_info['website'];
        } else {
            $data['website'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($client_info) && is_file(DIR_IMAGE . $client_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($client_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($client_info)) {
            $data['sort_order'] = $client_info['sort_order'];
        } else {
            $data['sort_order'] = '';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('project/client_form', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'project/client')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 64)) {
            $this->error['name'] = $this->language->get('error_name');
        }
        if ((!empty($this->request->post['client_address'])) && (utf8_strlen($this->request->post['client_address']) < 2) || (utf8_strlen($this->request->post['client_address']) > 499)) {
            $this->error['client_address'] = $this->language->get('error_client_address');
        }
        if ((!empty($this->request->post['client_remarks'])) && (utf8_strlen($this->request->post['client_remarks']) < 2) || (utf8_strlen($this->request->post['client_remarks']) > 2000)) {
            $this->error['client_remarks'] = $this->language->get('error_client_remarks');
        }
        if((!empty($this->request->post['email']))) {
            if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = $this->language->get('error_email');
        }
        }
        
        if ((!empty($this->request->post['telephone'])) && (utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }
        
        if ((!empty($this->request->post['website'])) && !filter_var($this->request->post['website'], FILTER_VALIDATE_URL)) {
            $this->error['website'] = $this->language->get('error_website');
        }

        if (utf8_strlen($this->request->post['keyword']) > 0) {
            $this->load->model('catalog/url_alias');

            $url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

            if ($url_alias_info && isset($this->request->get['project_client_id']) && $url_alias_info['query'] != 'project_client_id=' . $this->request->get['project_client_id']) {
                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
            }

            if ($url_alias_info && !isset($this->request->get['project_client_id'])) {
                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
            }
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'project/client')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('project/client');

        /*foreach ($this->request->post['selected'] as $project_client_id) {
            $project_total = $this->model_project_client->getTotalProjectsByClientId($project_client_id);

            if ($project_total) {
                $this->error['warning'] = sprintf($this->language->get('error_project'), $project_total);
            }
        }*/

        return !$this->error;
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('project/client');

            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'start' => 0,
                'limit' => 5
            );

            $results = $this->model_project_client->getClients($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    'project_client_id' => $result['project_client_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
