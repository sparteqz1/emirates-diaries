<?php

class ControllerCommonDashboard extends Controller {

    public function index() {
        $this->load->language('common/dashboard');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        // Check install directory exists
        if (is_dir(dirname(DIR_APPLICATION) . '/install')) {
            $data['error_install'] = $this->language->get('error_install');
        } else {
            $data['error_install'] = '';
        }

        // Dashboard Extensions
        $dashboards = array();

        $this->load->model('extension/extension');

        // Get a list of installed modules
        $extensions = $this->model_extension_extension->getInstalled('dashboard');

        // Add all the modules which have multiple settings for each module
        foreach ($extensions as $code) {
            if ($this->config->get('dashboard_' . $code . '_status') && $this->user->hasPermission('access', 'extension/dashboard/' . $code)) {
                $output = $this->load->controller('extension/dashboard/' . $code . '/dashboard');

                if ($output) {
                    $dashboards[] = array(
                        'code' => $code,
                        'width' => $this->config->get('dashboard_' . $code . '_width'),
                        'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
                        'output' => $output
                    );
                }
            }
        }

        $sort_order = array();

        foreach ($dashboards as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $dashboards);

        // Split the array so the columns width is not more than 12 on each row.
        $width = 0;
        $column = array();
        $data['rows'] = array();

        foreach ($dashboards as $dashboard) {
            $column[] = $dashboard;

            $width = ($width + $dashboard['width']);

            if ($width >= 12) {
                $data['rows'][] = $column;

                $width = 0;
                $column = array();
            }
        }


        $this->load->model('trustees/trustees');
        $data['trustees_total'] = $this->model_trustees_trustees->getTotalTrustees();
        $data['trustees_link'] = $this->url->link('trustees/trustees', 'token=' . $this->session->data['token'], true);

        $this->load->model('gallery/gallery');
        $data['gallery_total'] = $this->model_gallery_gallery->getTotalGallerys();
        $data['gallery_link'] = $this->url->link('gallery/gallery', 'token=' . $this->session->data['token'], true);

        $this->load->model('contact/contact');
        $data['subscriber_total'] = $this->model_contact_contact->getTotalSubscribers();
        $data['subscriber_link'] = $this->url->link('contact/subscribers', 'token=' . $this->session->data['token'], true);

        $this->load->model('news/news');
        $data['news_total'] = $this->model_news_news->getTotalPosts();
        $data['news_link'] = $this->url->link('news/news', 'token=' . $this->session->data['token'], true);

        $this->load->model('news/issue');
        $data['issues_total'] = $this->model_news_issue->getTotaIssues();
        $data['issues_link'] = $this->url->link('news/issue', 'token=' . $this->session->data['token'], true);

        $this->load->model('news/category');
        $data['category_total'] = $this->model_news_category->getTotalCategories();
        $data['category_link'] = $this->url->link('news/category', 'token=' . $this->session->data['token'], true);

        $this->load->model('news/author');
        $data['author_total'] = $this->model_news_author->getTotalAuthors();
        $data['author_link'] = $this->url->link('news/author', 'token=' . $this->session->data['token'], true);
        $this->load->model('news/featured');
        $data['featured_total'] = $this->model_news_featured->getNewsRelated();
        $data['featured_link'] = $this->url->link('news/featured', 'token=' . $this->session->data['token'], true);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // Run currency update
        if ($this->config->get('config_currency_auto')) {
            $this->load->model('localisation/currency');

            $this->model_localisation_currency->refresh();
        }

        $this->response->setOutput($this->load->view('common/dashboard', $data));
    }

}
