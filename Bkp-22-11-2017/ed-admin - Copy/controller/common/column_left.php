<?php

class ControllerCommonColumnLeft extends Controller {

    public function index() {
        if (isset($this->request->get['token']) && isset($this->session->data['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
            $this->load->language('common/column_left');

            $this->load->model('user/user');

            $this->load->model('tool/image');

            $user_info = $this->model_user_user->getUser($this->user->getId());

            if ($user_info) {
                $data['firstname'] = $user_info['firstname'];
                $data['lastname'] = $user_info['lastname'];

                $data['user_group'] = $user_info['user_group'];

                if (is_file(DIR_IMAGE . $user_info['image'])) {
                    $data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
                } else {
                    $data['image'] = '';
                }
            } else {
                $data['firstname'] = '';
                $data['lastname'] = '';
                $data['user_group'] = '';
                $data['image'] = '';
            }

            // Create a 3 level menu array
            // Level 2 can not have children
            // Menu
            $data['menus'][] = array(
                'id' => 'menu-dashboard',
                'icon' => 'fa-dashboard',
                'name' => $this->language->get('text_dashboard'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
                'children' => array()
            );

            // Catalog
            $catalog = array();

            if ($this->user->hasPermission('access', 'catalog/category')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_category'),
                    'href' => $this->url->link('catalog/category', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/product')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_product'),
                    'href' => $this->url->link('catalog/product', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }



            if ($this->user->hasPermission('access', 'catalog/filter')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_filter'),
                    'href' => $this->url->link('catalog/filter', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            // Attributes
            $attribute = array();

            if ($this->user->hasPermission('access', 'catalog/attribute')) {
                $attribute[] = array(
                    'name' => $this->language->get('text_attribute'),
                    'href' => $this->url->link('catalog/attribute', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/attribute_group')) {
                $attribute[] = array(
                    'name' => $this->language->get('text_attribute_group'),
                    'href' => $this->url->link('catalog/attribute_group', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($attribute) {
                $catalog[] = array(
                    'name' => $this->language->get('text_attribute'),
                    'href' => '',
                    'children' => $attribute
                );
            }



            if ($this->user->hasPermission('access', 'catalog/manufacturer')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_manufacturer'),
                    'href' => $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/download')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_download'),
                    'href' => $this->url->link('catalog/download', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/review')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_review'),
                    'href' => $this->url->link('catalog/review', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/information')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_information'),
                    'href' => $this->url->link('catalog/information', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($catalog) {
                $data['menus'][] = array(
                    'id' => 'menu-catalog',
                    'icon' => 'fa-tags',
                    'name' => $this->language->get('text_catalog'),
                    'href' => '',
                    'children' => $catalog
                );
            }
            
            // Trustees
            $trustees = array();

            if ($this->user->hasPermission('access', 'trustees/trustees')) {
                $trustees[] = array(
                    'name' => $this->language->get('text_trustees'),
                    'href' => $this->url->link('trustees/trustees', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            
            if ($trustees) {
                $data['menus'][] = array(
                    'id' => 'menu-trustees',
                    'icon' => 'fa-black-tie',
                    'name' => $this->language->get('text_trustees'),
                    'href' => '',
                    'children' => $trustees
                );
            }
            
            //Gallery
            
            $gallery = array();
            
            if ($this->user->hasPermission('access', 'gallery/gallery')) {
                $gallery[] = array(
                    'name' => $this->language->get('text_photo_gallery'),
                    'href' => $this->url->link('gallery/gallery', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($gallery) {
                $data['menus'][] = array(
                    'id' => 'menu-gallery',
                    'icon' => 'fa-picture-o',
                    'name' => $this->language->get('text_gallery'),
                    'href' => '',
                    'children' => $gallery
                );
            }
            
            
            $news = array();
            
            if ($this->user->hasPermission('access', 'news/issue')) {
                $news[] = array(
                    'name' => $this->language->get('text_issue'),
                    'href' => $this->url->link('news/issue', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'news/category')) {
                $news[] = array(
                    'name' => $this->language->get('text_category'),
                    'href' => $this->url->link('news/category', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            
             if ($this->user->hasPermission('access', 'news/author')) {
                $news[] = array(
                    'name' => $this->language->get('text_author'),
                    'href' => $this->url->link('news/author', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'news/news')) {
                $news[] = array(
                    'name' => $this->language->get('text_articles'),
                    'href' => $this->url->link('news/news', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'news/featured')) {
                $news[] = array(
                    'name' => $this->language->get('text_featured'),
                    'href' => $this->url->link('news/featured', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($news) {
                $data['menus'][] = array(
                    'id' => 'menu-news',
                    'icon' => 'fa-book',
                    'name' => $this->language->get('text_articles'),
                    'href' => '',
                    'children' => $news
                );
            }
            
            // Contacts
            $contacts = array();
            
            if ($this->user->hasPermission('access', 'contact/subscribers')) {
                $contacts[] = array(
                    'name' => $this->language->get('text_subscribers'),
                    'href' => $this->url->link('contact/subscribers', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }  

            if ($contacts) {
                $data['menus'][] = array(
                    'id' => 'menu-contacts',
                    'icon' => 'fa-envelope',
                    'name' => $this->language->get('text_contacts'),
                    'href' => '',
                    'children' => $contacts
                );
            }
            
            
            $project = array();

            if ($this->user->hasPermission('access', 'project/category')) {
                $project[] = array(
                    'name' => $this->language->get('text_project_category'),
                    'href' => $this->url->link('project/category', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            
             if ($this->user->hasPermission('access', 'project/client')) {
                $project[] = array(
                    'name' => $this->language->get('text_client'),
                    'href' => $this->url->link('project/client', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'project/project')) {
                $project[] = array(
                    'name' => $this->language->get('text_projects'),
                    'href' => $this->url->link('project/project', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($project) {
                $data['menus'][] = array(
                    'id' => 'menu-project',
                    'icon' => 'fa-tasks',
                    'name' => $this->language->get('text_projects'),
                    'href' => '',
                    'children' => $project
                );
            }

            // Extension
            $extension = array();



            if ($this->user->hasPermission('access', 'extension/extension')) {
                $extension[] = array(
                    'name' => $this->language->get('text_extension'),
                    'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }



            if ($this->user->hasPermission('access', 'extension/event')) {
                $extension[] = array(
                    'name' => $this->language->get('text_event'),
                    'href' => $this->url->link('extension/event', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($extension) {
                $data['menus'][] = array(
                    'id' => 'menu-extension',
                    'icon' => 'fa-puzzle-piece',
                    'name' => $this->language->get('text_extension'),
                    'href' => '',
                    'children' => $extension
                );
            }

            // Design
            $design = array();

            if ($this->user->hasPermission('access', 'design/layout')) {
                $design[] = array(
                    'name' => $this->language->get('text_layout'),
                    'href' => $this->url->link('design/layout', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'design/banner')) {
                $design[] = array(
                    'name' => $this->language->get('text_banner'),
                    'href' => $this->url->link('design/banner', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($design) {
                $data['menus'][] = array(
                    'id' => 'menu-design',
                    'icon' => 'fa-television',
                    'name' => $this->language->get('text_design'),
                    'href' => '',
                    'children' => $design
                );
            }


            // Customer
            $customer = array();

            if ($this->user->hasPermission('access', 'customer/customer')) {
                $customer[] = array(
                    'name' => $this->language->get('text_customer'),
                    'href' => $this->url->link('customer/customer', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'customer/customer_group')) {
                $customer[] = array(
                    'name' => $this->language->get('text_customer_group'),
                    'href' => $this->url->link('customer/customer_group', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }


            if ($customer) {
                $data['menus'][] = array(
                    'id' => 'menu-customer',
                    'icon' => 'fa-user',
                    'name' => $this->language->get('text_customer'),
                    'href' => '',
                    'children' => $customer
                );
            }

            // Marketing
            $marketing = array();

            if ($this->user->hasPermission('access', 'marketing/contact')) {
                $marketing[] = array(
                    'name' => $this->language->get('text_contact'),
                    'href' => $this->url->link('marketing/contact', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($marketing) {
                $data['menus'][] = array(
                    'id' => 'menu-marketing',
                    'icon' => 'fa-share-alt',
                    'name' => $this->language->get('text_marketing'),
                    'href' => '',
                    'children' => $marketing
                );
            }

            // System
            $system = array();

            if ($this->user->hasPermission('access', 'setting/setting')) {
                $system[] = array(
                    'name' => $this->language->get('text_setting'),
                    'href' => $this->url->link('setting/store', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            // Users
            $user = array();

            if ($this->user->hasPermission('access', 'user/user')) {
                $user[] = array(
                    'name' => $this->language->get('text_users'),
                    'href' => $this->url->link('user/user', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'user/user_permission')) {
                $user[] = array(
                    'name' => $this->language->get('text_user_group'),
                    'href' => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'user/api')) {
                $user[] = array(
                    'name' => $this->language->get('text_api'),
                    'href' => $this->url->link('user/api', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($user) {
                $system[] = array(
                    'name' => $this->language->get('text_users'),
                    'href' => '',
                    'children' => $user
                );
            }

            // Localisation
            $localisation = array();

            if ($this->user->hasPermission('access', 'localisation/location')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_location'),
                    'href' => $this->url->link('localisation/location', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/language')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_language'),
                    'href' => $this->url->link('localisation/language', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/currency')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_currency'),
                    'href' => $this->url->link('localisation/currency', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }




            if ($this->user->hasPermission('access', 'localisation/country')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_country'),
                    'href' => $this->url->link('localisation/country', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/zone')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_zone'),
                    'href' => $this->url->link('localisation/zone', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/geo_zone')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_geo_zone'),
                    'href' => $this->url->link('localisation/geo_zone', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }



            if ($localisation) {
                $system[] = array(
                    'name' => $this->language->get('text_localisation'),
                    'href' => '',
                    'children' => $localisation
                );
            }

            // Tools	
            $tool = array();

            if ($this->user->hasPermission('access', 'tool/upload')) {
                $tool[] = array(
                    'name' => $this->language->get('text_upload'),
                    'href' => $this->url->link('tool/upload', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'tool/backup')) {
                $tool[] = array(
                    'name' => $this->language->get('text_backup'),
                    'href' => $this->url->link('tool/backup', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'tool/log')) {
                $tool[] = array(
                    'name' => $this->language->get('text_log'),
                    'href' => $this->url->link('tool/log', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($tool) {
                $system[] = array(
                    'name' => $this->language->get('text_tools'),
                    'href' => '',
                    'children' => $tool
                );
            }

            if ($system) {
                $data['menus'][] = array(
                    'id' => 'menu-system',
                    'icon' => 'fa-cog',
                    'name' => $this->language->get('text_system'),
                    'href' => '',
                    'children' => $system
                );
            }

            // Report
            $report = array();


            // Report Products			
            $report_product = array();

            if ($this->user->hasPermission('access', 'report/product_viewed')) {
                $report_product[] = array(
                    'name' => $this->language->get('text_report_product_viewed'),
                    'href' => $this->url->link('report/product_viewed', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }


            if ($report_product) {
                $report[] = array(
                    'name' => $this->language->get('text_report_product'),
                    'href' => '',
                    'children' => $report_product
                );
            }

            // Report Customers				
            $report_customer = array();

            if ($this->user->hasPermission('access', 'report/customer_online')) {
                $report_customer[] = array(
                    'name' => $this->language->get('text_report_customer_online'),
                    'href' => $this->url->link('report/customer_online', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/customer_activity')) {
                $report_customer[] = array(
                    'name' => $this->language->get('text_report_customer_activity'),
                    'href' => $this->url->link('report/customer_activity', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/customer_search')) {
                $report_customer[] = array(
                    'name' => $this->language->get('text_report_customer_search'),
                    'href' => $this->url->link('report/customer_search', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }


            if ($report_customer) {
                $report[] = array(
                    'name' => $this->language->get('text_report_customer'),
                    'href' => '',
                    'children' => $report_customer
                );
            }



            if ($report) {
                $data['menus'][] = array(
                    'id' => 'menu-report',
                    'icon' => 'fa-bar-chart-o',
                    'name' => $this->language->get('text_reports'),
                    'href' => '',
                    'children' => $report
                );
            }


            return $this->load->view('common/column_left', $data);
        }
    }

}
