<?php

class ControllerCommonHeader extends Controller {

    public function index() {
        $data['title'] = $this->document->getTitle();

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts();
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $this->load->language('common/header');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_return'] = $this->language->get('text_return');
        $data['text_customer'] = $this->language->get('text_customer');
        $data['text_online'] = $this->language->get('text_online');
        $data['text_approval'] = $this->language->get('text_approval');
        $data['text_product'] = $this->language->get('text_product');
        $data['text_stock'] = $this->language->get('text_stock');
        $data['text_review'] = $this->language->get('text_review');
        $data['text_store'] = $this->language->get('text_store');
        $data['text_front'] = $this->language->get('text_front');
        $data['text_help'] = $this->language->get('text_help');
        $data['text_support'] = $this->language->get('text_support');
        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->user->getUserName());
        $data['text_logout'] = $this->language->get('text_logout');

        if (!isset($this->request->get['token']) || !isset($this->session->data['token']) || ($this->request->get['token'] != $this->session->data['token'])) {
            $data['logged'] = '';

            $data['home'] = $this->url->link('common/dashboard', '', true);
        } else {
            $data['logged'] = true;

            $data['home'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true);
            $data['logout'] = $this->url->link('common/logout', 'token=' . $this->session->data['token'], true);


            // Customers
            $this->load->model('report/customer');

            $data['online_total'] = $this->model_report_customer->getTotalCustomersOnline();

            $data['online'] = $this->url->link('report/customer_online', 'token=' . $this->session->data['token'], true);

            $this->load->model('customer/customer');

            $customer_total = $this->model_customer_customer->getTotalCustomers(array('filter_approved' => false));

            $data['customer_total'] = $customer_total;
            $data['customer_approval'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&filter_approved=0', true);

            

            // Reviews
            $this->load->model('catalog/review');

            $review_total = $this->model_catalog_review->getTotalReviews(array('filter_status' => 0));

            $data['review_total'] = $review_total;

            $data['review'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . '&filter_status=0', true);


            $data['alerts'] = $customer_total + $review_total;

        }

        return $this->load->view('common/header', $data);
    }

}
