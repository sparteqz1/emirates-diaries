<?php

class ControllerExtensionModuleSeoKeywordGenerator extends Controller {

    private $error = array();

    public function index() {

        $this->load->language('extension/module/seo_keyword_generator');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product');
        $this->load->model('catalog/information');
        $this->load->model('catalog/manufacturer');
        $this->load->model('news/news');
        $this->load->model('news/author');
        $this->load->model('catalog/seo_keyword_generator');


        // This Block returns the warning if any
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }



        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_catalog_seo_keyword_generator->SaveSEOKeyword($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'));
        }


        $data['products'] = array();
        $results = $this->model_catalog_product->getProducts(array());

        foreach ($results as $result) {
            $data['products'][] = array(
                'product_id' => $result['product_id'],
                'name' => $result['name'],
                'model' => $result['model'],
                'status' => $result['status'],
                'keyword' => $this->model_catalog_seo_keyword_generator->getKeyword("product_id=" . $result['product_id'])
            );
        }

        $data['categorys'] = array();
        $results = $this->model_catalog_seo_keyword_generator->getAllCategorys(array());

        foreach ($results as $result) {
            $data['categorys'][] = array(
                'category_id' => $result['category_id'],
                'name' => $result['name'],
                'keyword' => $this->model_catalog_seo_keyword_generator->getKeyword("category_id=" . $result['category_id'])
            );
        }


        $data['informations'] = array();
        $results = $this->model_catalog_information->getInformations(array());
        foreach ($results as $result) {
            $data['informations'][] = array(
                'information_id' => $result['information_id'],
                'name' => $result['title'],
                'keyword' => $this->model_catalog_seo_keyword_generator->getKeyword("information_id=" . $result['information_id'])
            );
        }

        $data['manufacturers'] = array();
        $results = $this->model_catalog_manufacturer->getManufacturers(array());

        foreach ($results as $result) {
            $data['manufacturers'][] = array(
                'manufacturer_id' => $result['manufacturer_id'],
                'name' => $result['name'],
                'keyword' => $this->model_catalog_seo_keyword_generator->getKeyword("manufacturer_id=" . $result['manufacturer_id'])
            );
        }

        
        $data['news'] = array();
        $results = $this->model_news_news->getPosts(array());

        foreach ($results as $result) {
            $data['news'][] = array(
                'news_id' => $result['news_id'],
                'name' => $result['name'],
                'status' => $result['status'],
                'keyword' => $this->model_catalog_seo_keyword_generator->getKeyword("news_id=" . $result['news_id'])
            );
        }
        $data['newscategorys'] = array();
        $results = $this->model_catalog_seo_keyword_generator->getAllNewsCategorys(array());

        foreach ($results as $result) {
            $data['newscategorys'][] = array(
                'category_id' => $result['category_id'],
                'name' => $result['name'],
                'keyword' => $this->model_catalog_seo_keyword_generator->getKeyword("newscategory_id=" . $result['category_id'])
            );
        }
        $data['authors'] = array();
        $results = $this->model_news_author->getAuthors(array());

        foreach ($results as $result) {
            $data['authors'][] = array(
                'author_id' => $result['author_id'],
                'name' => $result['name'],
                'keyword' => $this->model_catalog_seo_keyword_generator->getKeyword("author_id=" . $result['author_id'])
            );
        }




        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_edit'] = $this->language->get('text_edit');


        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_check_doubles'] = $this->language->get('button_check_doubles');
        $data['button_generate_all'] = $this->language->get('button_generate_all');
        $data['button_add_html_all'] = $this->language->get('button_add_html_all');
        $data['button_generate'] = $this->language->get('button_generate');
        $data['button_add_html'] = $this->language->get('button_add_html');



        $data['entry_products'] = $this->language->get('entry_products');
        $data['entry_categories'] = $this->language->get('entry_categories');
        $data['entry_informations'] = $this->language->get('entry_informations');
        $data['entry_manufacturers'] = $this->language->get('entry_manufacturers');
        $data['entry_news'] = $this->language->get('entry_news');
        $data['entry_author'] = $this->language->get('entry_author');
        $data['entry_news_categories'] = $this->language->get('entry_news_categories');








        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/seo_keyword_generator', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('extension/module/seo_keyword_generator', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL');

        $data['modules'] = array();


        $this->load->model('design/layout');

        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/seo_keyword_generator.tpl', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/seo_keyword_generator')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

}

?>