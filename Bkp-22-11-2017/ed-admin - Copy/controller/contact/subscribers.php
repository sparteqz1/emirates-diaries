<?php

class ControllerContactSubscribers extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('contact/contact');

        $this->document->setTitle($this->language->get('subscribe_title'));

        $this->load->model('contact/contact');

        $this->getList();
    }

    public function delete() {
        $this->load->language('contact/contact');

        $this->document->setTitle($this->language->get('subscribe_title'));

        $this->load->model('contact/contact');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $subscribe_id) {
                $this->model_contact_contact->deleteSubscriber($subscribe_id);
            }

            $this->session->data['success'] = $this->language->get('subscribe_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('contact/subscribers', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList() {
        
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('subscribe_title'),
            'href' => $this->url->link('contact/subscribers', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['delete'] = $this->url->link('contact/subscribers/delete', 'token=' . $this->session->data['token'] . $url, true);

        $data['subscribers'] = array();

        $filter_data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $total_subscribers = $this->model_contact_contact->getTotalSubscribers($filter_data);

        $results = $this->model_contact_contact->getSubscribers($filter_data);

        foreach ($results as $result) {
            
            $data['subscribers'][] = array(
                'subscribe_id' => $result['subscribe_id'],
                'email' => $result['email']
            );
        }

        $data['subscribe_title'] = $this->language->get('subscribe_title');

        $data['text_subscriber'] = $this->language->get('text_subscriber');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_email'] = $this->language->get('column_email');
        $data['column_subject'] = $this->language->get('column_subject');
        $data['column_enquiry'] = $this->language->get('column_enquiry');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_action'] = $this->language->get('column_action');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_image'] = $this->language->get('entry_image');

        $data['button_delete'] = $this->language->get('button_delete');

        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('contact/subscribers', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
        $data['sort_subject'] = $this->url->link('contact/subscribers', 'token=' . $this->session->data['token'] . '&sort=subject' . $url, true);
        $data['sort_email'] = $this->url->link('contact/subscribers', 'token=' . $this->session->data['token'] . '&sort=email' . $url, true);
        $data['sort_enquiry'] = $this->url->link('contact/subscribers', 'token=' . $this->session->data['token'] . '&sort=enquiry' . $url, true);
        $data['sort_order'] = $this->url->link('contact/subscribers', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $total_subscribers;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('contact/subscribers', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($total_subscribers) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total_subscribers - $this->config->get('config_limit_admin'))) ? $total_subscribers : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total_subscribers, ceil($total_subscribers / $this->config->get('config_limit_admin')));

        

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('contact/contact_list', $data));
    }    

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'contact/subscribers')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

}
