<?php
// Heading
$_['heading_title']          = 'Featured';

// Text
$_['text_success']           = 'Success: You have modified news!';
$_['text_add']               = 'Add Featured';
$_['text_default']           = 'Default';

// Entry
$_['entry_text']             = 'Text';
$_['entry_required']         = 'Required';
$_['entry_related']          = 'Featured News';

// Help
$_['help_related']           = '(Autocomplete)';
$_['help_tag']               = 'Comma separated';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify news!';