<?php
// Heading
$_['heading_title']          = 'News';

// Text
$_['text_success']           = 'Success: You have modified news!';
$_['text_list']              = 'News List';
$_['text_add']               = 'Add News';
$_['text_edit']              = 'Edit News';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Title';
$_['column_image']           = 'Image';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Title';
$_['entry_description']      = 'Description';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword']     = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_image']            = 'Image';
$_['entry_additional_image'] = 'Additional Images';
$_['entry_attribute']        = 'Attribute';
$_['entry_attribute_group']  = 'Attribute Group';
$_['entry_text']             = 'Text';
$_['entry_required']         = 'Required';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_category']         = 'Categories';
$_['entry_related']          = 'Related News';
$_['entry_tag']              = ' Tags';
$_['entry_author']           = 'Author';


// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_category']          = '(Autocomplete)';
$_['help_related']           = '(Autocomplete)';
$_['help_author']            = '(Autocomplete)';
$_['help_issue']             = 'Select Issue';
$_['help_arrangement']       = 'Determine how to show Articles in front end';
$_['help_tag']               = 'Comma separated';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify news!';
$_['error_name']             = 'Title must be greater than 3 and less than 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 1 and less than 64 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
