<?php
// Heading
$_['heading_title']       = 'SEO Keyword Generator';

// Button
$_['button_check_doubles']         = 'Check Doubles';
$_['button_generate_all']         = 'Generate All';
$_['button_add_html_all']         = 'Add .html All';
$_['button_generate']         = 'Generate';
$_['button_add_html']         = 'Add .html';
$_['text_module']         = 'Modules';
$_['text_edit']           = 'Edit SEO Keyword Generator Module';


// Entry
$_['entry_products']		= 'Products';
$_['entry_categories']		= 'Categories';
$_['entry_informations']	= 'Informations';
$_['entry_manufacturers']	= 'Manufacturers';
$_['entry_blogs']               = 'Blogs';
$_['entry_news']               = 'News';
$_['entry_author']               = 'Author';
$_['entry_news_categories']               = 'News Categories';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module SEO Keyword Generator!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module information!';
?>