<?php
// Heading
$_['heading_title']                = 'Stores';

// Text
$_['text_settings']                = 'Settings';
$_['text_success']                 = 'Success: You have modified Stores!';
$_['text_list']                    = 'Store List';
$_['text_items']                   = 'Items';
$_['text_tax']                     = 'Taxes';
$_['text_account']                 = 'Account';
$_['text_checkout']                = 'Checkout';

// Column
$_['column_name']                  = 'Store Name';
$_['column_url']	          = 'Store URL';
$_['column_action']                = 'Action';
