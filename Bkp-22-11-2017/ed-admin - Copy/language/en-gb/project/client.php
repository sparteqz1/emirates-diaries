<?php
// Heading
$_['heading_title']     = 'Clients';

// Text
$_['text_success']      = 'Success: You have modified Client!';
$_['text_list']         = 'Client List';
$_['text_add']          = 'Add Client';
$_['text_edit']         = 'Edit Client';
$_['text_default']      = 'Default';
$_['text_percent']      = 'Percentage';
$_['text_amount']       = 'Fixed Amount';


// Column
$_['column_name']       = 'Client Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']           = 'Client Name';
$_['entry_keyword']        = 'SEO URL';
$_['entry_image']          = 'Image';
$_['entry_sort_order']     = 'Sort Order';
$_['entry_type']           = 'Type';
$_['entry_status']         = 'Status';
$_['entry_client_address'] = 'Adress';
$_['entry_client_remarks'] = 'Remarks';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Telephone';
$_['entry_website']        = 'website';

// Help
$_['help_keyword']      = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Client!';
$_['error_name']        = 'Client Name must be between 2 and 64 characters!';
$_['error_keyword']     = 'SEO URL already in use!';
$_['error_product']     = 'Warning: This manufacturer cannot be deleted as it is currently assigned to %s products!';
$_['error_email']       = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']   = 'Telephone must be between 3 and 32 characters!';
$_['error_website']     = 'Website does not appear to be valid!';

$_['error_client_address']    = 'Client Adress must be between 2 and 500 characters!';
$_['error_client_remarks']    = 'Client Remarks must be between 2 and 1000 characters!';
