-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 20, 2018 at 03:07 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emiartes_diaries`
--

-- --------------------------------------------------------

--
-- Table structure for table `sp_address`
--

CREATE TABLE `sp_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `telephone` varchar(12) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '173',
  `zone_id` int(11) NOT NULL DEFAULT '2669'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_api`
--

CREATE TABLE `sp_api` (
  `api_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_api`
--

INSERT INTO `sp_api` (`api_id`, `name`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'XHlQ3MNylutxMDNSuZ4KaS18AKGSH0RP3COtn8V9czHrG0fC74xLTVtTLAJZB3kGLg1Q0Jl4ypLqakC6xh1jg1CpbzBWCVOVjJu3jMWstXPWuHIXBgTngfAgygq1KkpQ2hDGHubc12FNIG1gtZsRyVRgOveBp2IjjaE4OSIGaYO5HDnzuFmCDrUbWykwS2ndZ9oEx5xe9ZL9LWoEsHvD70R2LUjB3l7nv5r2bLu2ztCMO0pwt771eny9IbPK1CNp', 1, '2016-11-07 10:23:22', '2016-12-07 13:52:16');

-- --------------------------------------------------------

--
-- Table structure for table `sp_api_ip`
--

CREATE TABLE `sp_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_api_session`
--

CREATE TABLE `sp_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `session_name` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_attribute`
--

CREATE TABLE `sp_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_attribute_description`
--

CREATE TABLE `sp_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_attribute_group`
--

CREATE TABLE `sp_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_attribute_group_description`
--

CREATE TABLE `sp_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_author`
--

CREATE TABLE `sp_author` (
  `author_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_author`
--

INSERT INTO `sp_author` (`author_id`, `name`, `image`, `sort_order`) VALUES
(1, 'Maryam Al Mansoori', '', 1),
(2, 'Sherina', '', 2),
(3, 'Mohammed Suhail', '', 3),
(4, 'Buthaina Bin Karam', '', 4),
(5, 'Rowdha Al Sayegh', '', 5),
(6, 'The Untitled Chapters team', '', 6),
(7, 'Fatma Al Hashemi', '', 7),
(8, 'Abeer Al Haddad', '', 8),
(9, 'Hessa Al Eassa', '', 9),
(10, 'Bahar Al Awadhi', '', 10),
(11, 'Juma Al Haj', '', 11),
(12, 'Alanood Burhaima', '', 12),
(13, 'Hamda Al Falasi', '', 13),
(14, 'Moaza Obaid', '', 14),
(15, 'Sara Al Qassimi', '', 15),
(17, 'Ali Alyousuf', '', 16),
(18, 'Fatima Abdulrahman', '', 17),
(19, 'Nuha Al Mokbily', '', 18),
(20, 'Saeed Al Gergawi', '', 19),
(21, 'Hessa Al Khalifa', '', 20),
(22, 'حنان الحمادي', '', 21),
(23, 'ريم الحمادي', '', 22),
(24, ' مريم ابراهيم', '', 23),
(26, 'لطيفة الحمادي', '', 24),
(27, 'هند سعيد', '', 25),
(28, 'سعيد القرقاوي', '', 27),
(29, 'إيمان الحمادي', '', 26),
(30, 'حمدة حسن الحمادي', '', 28),
(31, 'ألبيرتو مانغويل', '', 29),
(32, 'نعمة الشحي', '', 30),
(33, 'سناء المرزوقي', '', 31),
(34, 'باسمة يونس', '', 32),
(35, 'سلمى النوَّحي', '', 33),
(36, 'شيخة سالم ناصر', '', 34),
(37, 'أسماء النقبي', '', 35),
(38, 'فاطمة علي المعمري', '', 36),
(39, 'حصة العبدالله', '', 37),
(40, 'مريم الرميثي', '', 38),
(41, 'ريتشارد تمبلر', '', 39),
(42, 'بدر الشمري', '', 40),
(43, 'Nazim', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sp_banner`
--

CREATE TABLE `sp_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_banner`
--

INSERT INTO `sp_banner` (`banner_id`, `name`, `status`) VALUES
(7, 'Home Page Slideshow', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_banner_image`
--

CREATE TABLE `sp_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_banner_image`
--

INSERT INTO `sp_banner_image` (`banner_image_id`, `banner_id`, `language_id`, `title`, `link`, `image`, `sort_order`) VALUES
(112, 7, 1, 'newjo', '', 'catalog/banner/3.jpg', 3),
(111, 7, 1, 'newjo', '', 'catalog/banner/4.jpg', 1),
(110, 7, 1, 'newjo', '', 'catalog/banner/2.jpg', 2),
(109, 7, 1, 'newjo', '', 'catalog/banner/newjo1.jpg', 0),
(113, 7, 2, 'newjo', '', 'catalog/banner/3.jpg', 3),
(114, 7, 2, 'newjo', '', 'catalog/banner/4.jpg', 1),
(115, 7, 2, 'newjo', '', 'catalog/banner/2.jpg', 2),
(116, 7, 2, 'newjo', '', 'catalog/banner/newjo1.jpg', 0),
(117, 7, 2, 'newjo', '', 'catalog/banner/3.jpg', 3),
(118, 7, 2, 'newjo', '', 'catalog/banner/4.jpg', 1),
(119, 7, 2, 'newjo', '', 'catalog/banner/2.jpg', 2),
(120, 7, 2, 'newjo', '', 'catalog/banner/newjo1.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sp_category`
--

CREATE TABLE `sp_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_category_description`
--

CREATE TABLE `sp_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_category_filter`
--

CREATE TABLE `sp_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_category_path`
--

CREATE TABLE `sp_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_country`
--

CREATE TABLE `sp_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_country`
--

INSERT INTO `sp_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_currency`
--

CREATE TABLE `sp_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_currency`
--

INSERT INTO `sp_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 0.75889999, 1, '2017-10-17 15:56:21'),
(2, 'US Dollar', 'USD', '$', '', '2', 1.00000000, 1, '2018-10-20 06:44:18'),
(3, 'Euro', 'EUR', '', '€', '2', 0.85119998, 1, '2017-10-17 15:56:21');

-- --------------------------------------------------------

--
-- Table structure for table `sp_customer`
--

CREATE TABLE `sp_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `uniqueid` varchar(100) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `social_image` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `address_id` int(11) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `email_verified` tinyint(4) NOT NULL DEFAULT '0',
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_customer_activity`
--

CREATE TABLE `sp_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_customer_authentication`
--

CREATE TABLE `sp_customer_authentication` (
  `customer_authentication_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `provider` varchar(55) NOT NULL,
  `identifier` varchar(200) NOT NULL,
  `web_site_url` varchar(255) NOT NULL,
  `profile_url` varchar(255) NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `birth_day` varchar(255) NOT NULL,
  `birth_month` varchar(255) NOT NULL,
  `birth_year` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_customer_group`
--

CREATE TABLE `sp_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_customer_group`
--

INSERT INTO `sp_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_customer_group_description`
--

CREATE TABLE `sp_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_customer_group_description`
--

INSERT INTO `sp_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Default', 'test'),
(1, 2, 'Default', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `sp_customer_history`
--

CREATE TABLE `sp_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_customer_ip`
--

CREATE TABLE `sp_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_customer_login`
--

CREATE TABLE `sp_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_customer_online`
--

CREATE TABLE `sp_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_customer_online`
--

INSERT INTO `sp_customer_online` (`ip`, `customer_id`, `url`, `referer`, `date_added`) VALUES
('110.227.237.45', 0, 'http://www.emiratesdiaries.com/video-gallery', 'http://www.emiratesdiaries.com/', '2018-10-20 07:04:55');

-- --------------------------------------------------------

--
-- Table structure for table `sp_customer_search`
--

CREATE TABLE `sp_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_download`
--

CREATE TABLE `sp_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_download_description`
--

CREATE TABLE `sp_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_event`
--

CREATE TABLE `sp_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_event`
--

INSERT INTO `sp_event` (`event_id`, `code`, `trigger`, `action`, `status`, `date_added`) VALUES
(1, 'voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sp_extension`
--

CREATE TABLE `sp_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_extension`
--

INSERT INTO `sp_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(19, 'module', 'slideshow'),
(20, 'theme', 'theme_default'),
(21, 'dashboard', 'activity'),
(31, 'module', 'seo_keyword_generator'),
(25, 'dashboard', 'online'),
(27, 'dashboard', 'customer'),
(40, 'analytics', 'google_analytics'),
(39, 'module', 'news_category');

-- --------------------------------------------------------

--
-- Table structure for table `sp_filter`
--

CREATE TABLE `sp_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_filter_description`
--

CREATE TABLE `sp_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_filter_group`
--

CREATE TABLE `sp_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_filter_group_description`
--

CREATE TABLE `sp_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_gallery`
--

CREATE TABLE `sp_gallery` (
  `gallery_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_gallery`
--

INSERT INTO `sp_gallery` (`gallery_id`, `image`, `author_id`, `issue_id`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES
(1, 'catalog/photo_gallery/provedore.jpg', 1, 2, 1, 1, 2, '2017-05-02 17:32:03', '2018-10-15 06:35:08'),
(2, 'catalog/photo_gallery/grand-mosq.jpg', 2, 6, 2, 1, 1, '2017-06-02 17:49:51', '2018-10-15 06:35:21'),
(3, 'catalog/photo_gallery/pearla.jpg', 1, 3, 1, 1, 1, '2017-09-02 17:56:07', '2018-10-15 06:34:28'),
(4, 'catalog/photo_gallery/hk.jpg', 1, 4, 1, 1, 4, '2017-08-02 18:05:24', '2018-10-15 06:35:31'),
(5, 'catalog/photo_gallery/nayif.jpg', 2, 6, 3, 1, 13, '2017-07-02 18:14:09', '2018-10-15 06:33:12'),
(6, 'catalog/photo_gallery/mobile-aid.jpg', 1, 6, 1, 1, 0, '2017-09-26 17:12:58', '2017-12-16 04:10:56');

-- --------------------------------------------------------

--
-- Table structure for table `sp_gallery_description`
--

CREATE TABLE `sp_gallery_description` (
  `gallery_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_gallery_description`
--

INSERT INTO `sp_gallery_description` (`gallery_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 2, 'Provedore: Your Local Artisan Market &amp; Restaurant', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(6, 1, 'Mobile Aid', '&lt;p&gt;A certified Mobile Aid technician will arrive at your designated location to carry out the repair.&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(5, 1, 'Nayif 1', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(5, 2, 'Nayif 1', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(6, 2, 'Mobile Aid', '&lt;p&gt;A certified Mobile Aid technician will arrive at your designated location to carry out the repair.&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(3, 1, 'Pearla', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(3, 2, 'Pearla', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(1, 1, 'Provedore: Your Local Artisan Market &amp; Restaurant', '&lt;p&gt;The Polygraph Hall in J.P. Nagar sprung to life with vibrant art works. Articature, an exhibition conducted by Tamaala comprised paintings printed on fine fabric and frames using wood from Channapatna, transfer printing of famous artworks on tote and sling bags and pillows, terracotta lanterns of artists from Rajasthan and Gujarat, handmade jewellery, traditional masks of Karnataka, a counter for on-the-spot caricature drawn by Balraj K.N., and T-shirts and mugs with caricatures.&lt;/p&gt;', '', '', '', ''),
(2, 1, 'Sheikh Zayed Grand Mosque', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(2, 2, 'Sheikh Zayed Grand Mosque', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(4, 1, 'When HK Hunts For Food', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(4, 2, 'When HK Hunts For Food', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sp_gallery_image`
--

CREATE TABLE `sp_gallery_image` (
  `gallery_image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_gallery_image`
--

INSERT INTO `sp_gallery_image` (`gallery_image_id`, `gallery_id`, `image`, `sort_order`) VALUES
(34, 3, 'catalog/photo_gallery/perla.jpg', 2),
(33, 3, 'catalog/photo_gallery/pearla2.jpg', 1),
(36, 4, 'catalog/photo_gallery/hk2.jpg', 0),
(32, 5, 'catalog/photo_gallery/pearla.jpg', 0),
(31, 5, 'catalog/photo_gallery/perla.jpg', 0),
(35, 2, 'catalog/photo_gallery/nayif.jpg', 0),
(30, 6, 'catalog/photo_gallery/latest-3.jpg', 3),
(29, 6, 'catalog/photo_gallery/aljalila.jpg', 2),
(28, 6, 'catalog/photo_gallery/latest-2.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_geo_zone`
--

CREATE TABLE `sp_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_geo_zone`
--

INSERT INTO `sp_geo_zone` (`geo_zone_id`, `name`, `description`, `date_modified`, `date_added`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2010-02-26 22:33:24', '2009-01-06 23:26:25'),
(4, 'UK Shipping', 'UK Shipping Zones', '2016-12-07 13:51:47', '2009-06-23 01:14:53');

-- --------------------------------------------------------

--
-- Table structure for table `sp_information`
--

CREATE TABLE `sp_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_information`
--

INSERT INTO `sp_information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(3, 1, 3, 1),
(4, 1, 1, 1),
(5, 1, 4, 1),
(6, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_information_description`
--

CREATE TABLE `sp_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_information_description`
--

INSERT INTO `sp_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(4, 1, 'About Us', '&lt;p&gt;\r\n	About Us&lt;/p&gt;\r\n', 'About Us', '', ''),
(5, 1, 'Terms &amp; Conditions', '&lt;p&gt;\r\n	Terms &amp;amp; Conditions&lt;/p&gt;\r\n', 'Terms &amp; Conditions', '', ''),
(3, 1, 'Privacy Policy', '&lt;p&gt;\r\n	Privacy Policy&lt;/p&gt;\r\n', 'Privacy Policy', '', ''),
(6, 1, 'Delivery Information', '&lt;p&gt;\r\n	Delivery Information&lt;/p&gt;\r\n', 'Delivery Information', '', ''),
(4, 2, 'About Us', '&lt;p&gt;\r\n	About Us&lt;/p&gt;\r\n', 'About Us', '', ''),
(5, 2, 'Terms &amp; Conditions', '&lt;p&gt;\r\n	Terms &amp;amp; Conditions&lt;/p&gt;\r\n', 'Terms &amp; Conditions', '', ''),
(3, 2, 'Privacy Policy', '&lt;p&gt;\r\n	Privacy Policy&lt;/p&gt;\r\n', 'Privacy Policy', '', ''),
(6, 2, 'Delivery Information', '&lt;p&gt;\r\n	Delivery Information&lt;/p&gt;\r\n', 'Delivery Information', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sp_language`
--

CREATE TABLE `sp_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_language`
--

INSERT INTO `sp_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 1, 1),
(2, 'العربية', 'ar', 'ar-bh,ar-kw,ar-qa,ar-sa,ar-ae,arabic', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_layout`
--

CREATE TABLE `sp_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_layout`
--

INSERT INTO `sp_layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search'),
(14, 'News');

-- --------------------------------------------------------

--
-- Table structure for table `sp_layout_module`
--

CREATE TABLE `sp_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_layout_module`
--

INSERT INTO `sp_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(69, 10, 'affiliate', 'column_right', 1),
(106, 1, 'news_category', 'column_right', 0),
(105, 1, 'slideshow.27', 'content_top', 0),
(104, 1, 'slideshow.27', 'column_left', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sp_layout_route`
--

CREATE TABLE `sp_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_layout_route`
--

INSERT INTO `sp_layout_route` (`layout_route_id`, `layout_id`, `route`) VALUES
(56, 6, 'account/%'),
(17, 10, 'affiliate/%'),
(44, 3, 'product/category'),
(73, 1, 'common/home'),
(20, 2, 'product/product'),
(24, 11, 'information/information'),
(23, 7, 'checkout/%'),
(31, 8, 'information/contact'),
(55, 9, 'information/sitemap'),
(34, 4, ''),
(45, 5, 'product/manufacturer'),
(52, 12, 'product/compare'),
(53, 13, 'product/search'),
(70, 14, 'news/news');

-- --------------------------------------------------------

--
-- Table structure for table `sp_location`
--

CREATE TABLE `sp_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_location`
--

INSERT INTO `sp_location` (`location_id`, `name`, `address`, `telephone`, `fax`, `geocode`, `image`, `open`, `comment`) VALUES
(1, 'Location 1', 'Location addres', '432535', '532523', '3124,535', 'catalog/cart.png', '12:65', 'dfdsfsd dgdsgfsd');

-- --------------------------------------------------------

--
-- Table structure for table `sp_magazine_subscription`
--

CREATE TABLE `sp_magazine_subscription` (
  `subscribe_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `issue` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_manufacturer`
--

CREATE TABLE `sp_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_menu`
--

CREATE TABLE `sp_menu` (
  `menu_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `type` varchar(6) NOT NULL,
  `link` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_menu_description`
--

CREATE TABLE `sp_menu_description` (
  `menu_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_menu_module`
--

CREATE TABLE `sp_menu_module` (
  `menu_module_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_modification`
--

CREATE TABLE `sp_modification` (
  `modification_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_module`
--

CREATE TABLE `sp_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_module`
--

INSERT INTO `sp_module` (`module_id`, `name`, `code`, `setting`) VALUES
(27, 'Home Page Slider', 'slideshow', '{\"name\":\"Home Page Slider\",\"banner_id\":\"7\",\"width\":\"1140\",\"height\":\"380\",\"status\":\"1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `sp_news`
--

CREATE TABLE `sp_news` (
  `news_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `arrangement` varchar(15) NOT NULL,
  `published_date` date NOT NULL DEFAULT '0000-00-00',
  `langage` tinyint(4) NOT NULL DEFAULT '0',
  `author_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_news`
--

INSERT INTO `sp_news` (`news_id`, `image`, `arrangement`, `published_date`, `langage`, `author_id`, `issue_id`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES
(1, 'catalog/news/osyami/osaymi.jpg', 'right', '2017-07-27', 1, 3, 6, 1, 1, 47, '2015-10-31 17:32:03', '2017-11-29 16:33:15'),
(2, 'catalog/news/mobile-aid/mobile-aid.jpg', 'right', '2017-08-09', 1, 3, 6, 1, 1, 221, '2015-11-06 17:49:51', '2017-11-23 16:24:32'),
(3, 'catalog/news/gafla/gaf.jpg', 'right', '2017-08-09', 1, 1, 6, 1, 1, 116, '2015-11-08 17:56:07', '2017-11-23 16:39:33'),
(4, 'catalog/news/tawlat/tawlat.jpg', 'right', '2017-08-09', 1, 1, 6, 1, 1, 77, '2015-08-11 18:05:24', '2017-11-23 16:41:21'),
(5, 'catalog/news/mbr/mbr.jpg', 'right', '2017-08-09', 1, 3, 6, 1, 1, 51, '2015-11-08 18:14:09', '2017-11-23 16:39:53'),
(10, 'catalog/news/provedore/provedore.jpg', 'left', '2017-08-09', 1, 1, 6, 3, 1, 115, '2015-10-19 11:09:47', '2018-10-13 07:40:32'),
(7, 'catalog/news/outlines-to-ornaments/interview-question.jpg', 'right', '2017-08-09', 1, 2, 6, 1, 1, 50, '2015-10-31 12:34:44', '2017-11-23 16:40:47'),
(8, 'catalog/news/hk-hunts/bad-brownies.jpg', 'right', '2017-08-09', 1, 21, 6, 1, 1, 58, '2015-10-10 12:58:49', '2018-02-28 00:30:00'),
(9, 'catalog/news/nayif/CubeSat.jpg', 'right', '2017-08-09', 1, 20, 6, 1, 1, 42, '2015-10-12 19:03:00', '2017-11-23 16:40:39'),
(11, 'catalog/news/Alezan/s_profile_01.jpeg', 'left', '2017-08-09', 1, 3, 6, 4, 1, 50, '2015-10-12 11:31:10', '2017-11-23 16:38:31'),
(12, 'catalog/news/sheikh-zayed-grand-mosque/Grand-Mosque.jpg', 'left', '2017-08-09', 1, 4, 6, 5, 1, 90, '2015-08-27 12:03:13', '2018-02-28 00:36:31'),
(13, 'catalog/news/HIPA/tajmahal.jpg', 'right', '2017-08-09', 1, 5, 7, 1, 1, 99, '2017-10-16 14:39:57', '2017-11-23 16:39:38'),
(14, 'catalog/news/untitled-chapters/untitled-chapters.jpg', 'right', '2017-08-09', 1, 6, 7, 1, 1, 90, '2017-10-16 17:32:14', '2017-11-23 16:41:35'),
(15, 'catalog/news/wanna-read/5TH-SKMC.jpg', 'right', '2017-08-09', 1, 1, 7, 1, 1, 89, '2017-10-17 11:47:39', '2018-10-13 07:50:26'),
(16, 'catalog/news/world-without-barriers/DSC-6286.jpg', 'right', '2017-08-09', 1, 7, 7, 1, 1, 93, '2017-10-17 12:00:54', '2017-11-23 16:38:26'),
(17, 'catalog/news/speed-reading/DSC_6387.jpg', 'right', '2017-08-09', 1, 4, 7, 1, 1, 35, '2017-10-18 12:33:26', '2017-11-23 16:41:17'),
(18, 'catalog/news/fairytales/IMG-20151218-WA0020.jpg', 'right', '2017-08-09', 1, 1, 7, 1, 1, 107, '2017-10-18 12:52:01', '2017-11-23 16:39:21'),
(19, 'catalog/news/read-your-stress-away/DSC_7138.jpg', 'right', '2017-08-09', 1, 8, 7, 1, 1, 34, '2017-10-18 13:29:41', '2017-11-23 16:41:07'),
(20, 'catalog/news/the-actual-adventure-within-us/MG_4000.jpg', 'right', '2017-08-09', 1, 9, 7, 1, 1, 67, '2017-10-18 13:49:18', '2017-11-23 16:41:26'),
(21, 'catalog/news/for-the-lovelof-reading/DSC_2343.jpg', 'right', '2017-08-09', 1, 10, 7, 1, 1, 38, '2017-10-18 13:58:29', '2017-11-23 16:39:29'),
(22, 'catalog/news/mira-thani/living.jpg', 'right', '2017-08-09', 1, 1, 8, 1, 1, 51, '2017-11-20 12:10:13', '2017-11-23 16:39:58'),
(23, 'catalog/news/design_talk/design.jpg', 'right', '2017-08-09', 1, 11, 8, 2, 1, 54, '2017-11-20 13:09:50', '2018-02-28 00:36:56'),
(24, 'catalog/news/qafiya/founders.jpg', 'left', '2017-08-09', 1, 1, 8, 4, 1, 33, '2017-11-21 11:19:51', '2017-11-23 16:41:02'),
(25, 'catalog/news/mohammed-almusabi/mosque.jpg', 'left', '2017-08-09', 1, 12, 8, 5, 1, 50, '2017-11-21 11:45:52', '2018-02-28 00:39:13'),
(26, 'catalog/news/fashion-spotlight/fashion.jpg', 'left', '2017-08-09', 1, 3, 8, 1, 1, 45, '2017-11-21 12:16:23', '2017-11-23 16:39:25'),
(27, 'catalog/news/i-have-this-thing-with-tiles/floorl.jpg', 'right', '2017-08-09', 1, 11, 8, 2, 1, 35, '2017-11-21 12:50:50', '2018-02-28 00:36:04'),
(28, 'catalog/news/louvre-abu-dhabi/ship_structure.jpg', 'right', '2017-08-09', 1, 14, 8, 1, 1, 45, '2017-11-21 13:08:21', '2017-11-23 16:39:49'),
(29, 'catalog/news/dubai-opera/ship_structure.jpg', 'right', '2017-08-09', 1, 15, 8, 1, 1, 85, '2017-11-21 13:13:26', '2018-02-28 00:35:06'),
(30, 'catalog/news/dubai-water-canal/dubai-water-canal.jpg', 'left', '2017-08-09', 1, 15, 8, 1, 1, 53, '2017-11-21 13:27:54', '2018-02-28 00:35:31'),
(31, 'catalog/news/ana-gow-running/ana_gow.jpg', 'left', '2017-08-09', 1, 12, 8, 1, 1, 95, '2017-11-21 13:41:11', '2018-02-28 00:34:37'),
(32, 'catalog/news/the-parkour-enthusiast/parkour.jpg', 'left', '2017-08-09', 1, 3, 8, 2, 1, 35, '2017-11-21 14:00:03', '2017-11-23 16:41:31'),
(33, 'catalog/news/are-computers-smarter-than-humans/system.jpg', 'right', '2017-08-09', 1, 17, 8, 1, 1, 89, '2017-11-21 14:54:27', '2018-02-28 00:34:08'),
(34, 'catalog/news/alia-bin-omair/alia-bin-omair.jpg', 'right', '2017-08-09', 1, 18, 6, 1, 1, 83, '2017-11-21 15:35:35', '2018-02-28 00:39:47'),
(35, 'catalog/news/pearla/design.jpg', 'right', '2017-08-09', 1, 19, 6, 1, 1, 82, '2017-11-21 16:06:35', '2018-02-28 00:40:38'),
(36, 'catalog/news/bait-al-kandora/showroom.jpg', 'left', '2017-08-09', 1, 18, 6, 1, 1, 106, '2017-11-21 17:49:14', '2018-02-28 00:40:14'),
(37, 'catalog/news/last-edited/last-exit.jpg', 'right', '2017-08-09', 2, 0, 8, 1, 1, 13, '2017-11-23 10:20:55', '2017-11-23 17:32:49'),
(38, 'catalog/news/the-green-planet/green-planet.jpg', 'right', '2017-08-09', 2, 0, 8, 1, 1, 12, '2017-11-23 12:12:14', '2018-02-28 00:50:14'),
(39, 'catalog/news/dubai-opera-house/opera_house.jpg', 'right', '2017-08-09', 2, 0, 8, 1, 1, 13, '2017-11-23 12:25:12', '2017-11-23 16:54:07'),
(40, 'catalog/news/dubai-marathon/medal.jpg', 'left', '2017-08-09', 2, 0, 8, 1, 1, 2, '2017-11-23 17:25:09', '2017-11-24 11:39:30'),
(41, 'catalog/news/when-the-dome-is-pronounced/dome-is-pronounced.jpg', 'left', '2017-08-09', 2, 0, 8, 1, 1, 2, '2017-11-23 17:27:41', '2018-02-28 00:49:24'),
(42, 'catalog/news/the-handsome-boy/handsome-boy.png', 'left', '2017-08-09', 2, 23, 8, 2, 1, 5, '2017-11-24 11:44:16', '2018-02-28 00:49:07'),
(43, 'catalog/news/brussels-is-a-battleground-to-a-tourist-city/brussels.jpg', 'left', '2017-08-09', 2, 23, 8, 1, 1, 6, '2017-11-24 12:25:14', '2018-02-28 00:56:24'),
(44, 'catalog/news/dusseldorf-city/city.jpg', 'right', '2017-08-09', 2, 23, 8, 1, 1, 5, '2017-11-24 12:44:15', '2018-02-28 00:48:44'),
(45, 'catalog/news/captain-challenges/ship.jpg', 'right', '2017-08-09', 2, 0, 8, 1, 1, 3, '2017-11-24 14:10:16', '2017-11-24 14:10:52'),
(46, 'catalog/news/exciting-and-attractive-classes/school.jpg', 'left', '2017-08-09', 2, 24, 8, 1, 1, 8, '2017-11-24 14:21:23', '2017-11-24 15:02:27'),
(47, 'catalog/news/throw-the-newspaper/newspaper.jpg', 'left', '2017-08-09', 2, 0, 8, 1, 1, 6, '2017-11-24 15:01:52', '0000-00-00 00:00:00'),
(48, 'catalog/news/safety-boat/sea.jpg', 'right', '2017-08-09', 2, 24, 8, 1, 1, 2, '2017-11-24 15:30:22', '0000-00-00 00:00:00'),
(49, 'catalog/news/al-bastakiya/bastakiya.jpg', 'left', '2017-08-09', 2, 26, 8, 1, 1, 1, '2017-11-24 15:41:51', '0000-00-00 00:00:00'),
(50, 'catalog/news/writings-are-not-valid-for-publication/writings.jpg', 'right', '2017-08-09', 2, 27, 8, 1, 1, 1, '2017-11-24 15:54:54', '0000-00-00 00:00:00'),
(51, 'catalog/news/ibrahim-al-hashimi/ibrahim.jpg', 'right', '2017-08-09', 2, 28, 7, 1, 1, 7, '2017-11-27 11:29:58', '2018-02-28 00:54:04'),
(52, 'catalog/news/ahmed-bin-rakad-al-ameri/ahmed-bin.jpg', 'right', '2017-08-09', 2, 29, 7, 1, 1, 2, '2017-11-27 12:05:40', '2018-02-28 00:53:32'),
(53, 'catalog/news/biography/biography.jpg', 'left', '2017-08-09', 2, 30, 7, 1, 1, 2, '2017-11-27 13:28:38', '0000-00-00 00:00:00'),
(54, 'catalog/news/dr-hamad-al-hammadi/books.jpg', 'right', '2017-08-09', 2, 23, 7, 1, 1, 1, '2017-11-27 13:52:31', '2017-11-27 14:16:42'),
(55, 'catalog/news/library-in-every-house/library-in-house.jpg', 'right', '2017-08-09', 2, 23, 7, 1, 1, 2, '2017-11-27 14:26:31', '0000-00-00 00:00:00'),
(56, 'catalog/news/book-cafe/book-cafe.jpg', 'right', '2017-08-09', 2, 0, 7, 1, 1, 1, '2017-11-27 15:19:55', '0000-00-00 00:00:00'),
(57, 'catalog/news/s-b-10/sb.jpg', 'right', '2017-08-09', 2, 23, 7, 1, 1, 2, '2017-11-27 15:30:34', '0000-00-00 00:00:00'),
(58, 'catalog/news/date-of-reading/date-of-reading.jpg', 'right', '2017-08-09', 2, 23, 7, 1, 1, 2, '2017-11-27 16:19:27', '0000-00-00 00:00:00'),
(59, 'catalog/news/five-reasons-to-visit-tokyo/tokyo.jpg', 'right', '2017-08-09', 2, 32, 7, 1, 1, 5, '2017-11-27 16:58:04', '2018-02-28 00:47:07'),
(60, 'catalog/news/reading-life/life.jpg', 'right', '2017-08-09', 2, 26, 7, 1, 1, 2, '2017-11-27 17:14:40', '0000-00-00 00:00:00'),
(61, 'catalog/news/my-story-with-reading/my-story.jpg', 'right', '2017-08-09', 2, 33, 7, 1, 1, 3, '2017-11-27 17:21:44', '0000-00-00 00:00:00'),
(62, 'catalog/news/reading-for-the-successful/reading-successful.jpg', 'right', '2017-08-09', 2, 34, 7, 1, 1, 1, '2017-11-27 17:37:42', '0000-00-00 00:00:00'),
(63, 'catalog/news/try-to-read/try-to-read.jpg', 'right', '2017-08-09', 2, 35, 7, 1, 1, 2, '2017-11-27 17:45:58', '0000-00-00 00:00:00'),
(64, 'catalog/news/yousef-al-mutawa/rules.jpg', 'left', '2017-08-09', 2, 36, 6, 1, 1, 5, '2017-11-28 12:27:55', '2018-02-28 00:46:13'),
(65, 'catalog/news/adeeb-al-balushi/us-space.jpg', 'left', '2017-08-09', 2, 37, 6, 1, 1, 3, '2017-11-28 13:08:06', '0000-00-00 00:00:00'),
(66, 'catalog/news/o-flag/relay.jpg', 'right', '2017-08-09', 2, 38, 6, 1, 1, 6, '2017-11-28 14:17:50', '2018-02-28 00:52:19'),
(67, 'catalog/news/stop-your-absence/damaged-paper.jpg', 'right', '2017-08-09', 2, 39, 6, 1, 1, 3, '2017-11-28 14:29:37', '0000-00-00 00:00:00'),
(68, 'catalog/news/i-am-a-pilot/pilot.jpg', 'right', '2017-08-09', 2, 40, 6, 1, 1, 2, '2017-11-28 14:50:41', '0000-00-00 00:00:00'),
(69, 'catalog/news/when-innovation-is-born/light.jpg', 'right', '2017-08-09', 2, 34, 6, 1, 1, 2, '2017-11-28 15:15:25', '0000-00-00 00:00:00'),
(70, 'catalog/news/mbr/mohd-bin-rashid-centre-for-gov-innovation.jpg', 'right', '2017-08-09', 2, 26, 6, 1, 1, 5, '2017-11-28 15:23:29', '0000-00-00 00:00:00'),
(71, 'catalog/news/rules-of-life/rules-of-Life.jpg', 'right', '2017-08-09', 2, 41, 6, 1, 1, 2, '2017-11-28 15:43:22', '0000-00-00 00:00:00'),
(72, 'catalog/news/why-write/why-write.jpg', 'left', '2017-08-09', 2, 23, 6, 1, 1, 2, '2017-11-28 16:10:08', '0000-00-00 00:00:00'),
(73, 'catalog/news/center-of-the-majestic/creativity.jpg', 'right', '2017-08-09', 2, 23, 6, 1, 1, 3, '2017-11-28 16:46:23', '2018-02-28 00:51:22'),
(74, 'catalog/news/nuray/nuray-home.jpg', 'right', '2017-08-09', 2, 30, 6, 1, 1, 2, '2017-11-28 17:24:51', '2018-02-28 00:51:57'),
(75, 'catalog/news/sheikh-zayed-grand-mosque/Grand-Mosque.jpg', 'right', '2017-08-09', 2, 42, 6, 1, 1, 3, '2017-11-28 17:40:05', '2018-02-28 00:51:00'),
(76, 'catalog/news/mbr/mbr.jpg', 'left', '2017-08-09', 2, 0, 6, 1, 1, 2, '2017-11-28 17:55:37', '0000-00-00 00:00:00'),
(77, 'catalog/news/youssef-al-zaabi/wiewer.jpg', 'right', '2017-08-09', 2, 0, 6, 1, 1, 2, '2017-11-28 18:09:18', '2018-02-28 00:49:49');

-- --------------------------------------------------------

--
-- Table structure for table `sp_newscategory`
--

CREATE TABLE `sp_newscategory` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `langage` tinyint(4) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_newscategory`
--

INSERT INTO `sp_newscategory` (`category_id`, `image`, `parent_id`, `langage`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(1, '', 0, 1, 1, 1, '2017-01-02 17:27:13', '2017-11-23 10:25:15'),
(2, '', 0, 1, 5, 1, '2017-01-02 17:53:01', '2017-11-22 19:18:27'),
(3, '', 0, 1, 2, 1, '2017-01-02 18:03:02', '2017-11-23 10:25:05'),
(4, '', 0, 1, 3, 1, '2017-01-02 18:08:57', '2017-11-23 10:24:53'),
(5, '', 0, 1, 4, 1, '2017-01-02 18:09:20', '2017-11-23 10:25:10'),
(6, '', 0, 1, 6, 1, '2017-09-27 12:16:30', '2017-11-23 10:25:19'),
(7, '', 0, 1, 7, 1, '2017-09-27 12:16:42', '2017-12-16 04:12:02'),
(8, '', 0, 1, 8, 1, '2017-09-27 12:16:58', '2017-11-23 10:24:33'),
(9, '', 0, 2, 9, 1, '2017-11-22 17:31:16', '2017-11-23 11:02:22'),
(10, '', 0, 2, 10, 1, '2017-11-23 13:11:18', '2017-11-23 13:11:18'),
(11, '', 0, 2, 11, 1, '2017-11-24 10:53:34', '2017-11-24 10:53:34'),
(12, '', 0, 2, 12, 1, '2017-11-24 12:12:29', '2017-11-24 12:12:29'),
(13, '', 0, 2, 13, 1, '2017-11-24 12:46:48', '2017-11-24 12:46:48'),
(14, '', 0, 2, 14, 1, '2017-11-27 10:59:07', '2017-11-27 10:59:07'),
(15, '', 0, 2, 15, 1, '2017-11-27 15:28:27', '2017-11-27 15:28:27'),
(16, '', 0, 2, 16, 1, '2017-11-28 14:10:08', '2017-11-28 14:10:08');

-- --------------------------------------------------------

--
-- Table structure for table `sp_newscategory_description`
--

CREATE TABLE `sp_newscategory_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_newscategory_description`
--

INSERT INTO `sp_newscategory_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 2, 'الشخصيات', '', 'الشخصيات', '', ''),
(3, 1, 'Initiatives', '', 'Initiatives', '', ''),
(4, 1, 'Fashion &amp; Beauty', '', 'Fashion &amp; Beauty', '', ''),
(5, 1, 'Out &amp; About', '', 'Out &amp; About', '', ''),
(2, 1, 'Fitness &amp; Health', '', 'Fitness &amp; Health', '', ''),
(6, 1, 'Science &amp; Technology', '', 'Science &amp; Technology', '', ''),
(7, 2, 'طعام', '', '', '', ''),
(8, 1, 'Entertainment', '', 'Entertainment', '', ''),
(3, 2, 'المبادرات', '', 'المبادرات', '', ''),
(4, 2, 'الموضة والجمال', '', 'الموضة والجمال', '', ''),
(5, 2, 'سياحة وسفر ', '', 'سياحة وسفر ', '', ''),
(2, 2, 'Fitness &amp; Health', '', 'صحة ولياقة', '', ''),
(6, 2, 'علوم وتكنولوجيا ', '', 'علوم وتكنولوجيا ', '', ''),
(7, 1, 'Food', '', '', '', ''),
(1, 1, 'Profiles', '', 'Profiles', '', ''),
(9, 1, 'محطات إماراتية', '', 'محطات إماراتية', '', ''),
(8, 2, 'ترفيه', '', 'ترفيه', '', ''),
(9, 2, 'محطات إماراتية', '', 'محطات إماراتية', '', ''),
(10, 1, 'روح هندسية', '', '', '', ''),
(10, 2, 'روح هندسية', '', '', '', ''),
(11, 1, 'ملخصات كتب', '', '', '', ''),
(11, 2, 'ملخصات كتب', '', '', '', ''),
(12, 1, 'سياحة وسفر', '', '', '', ''),
(12, 2, 'سياحة وسفر', '', '', '', ''),
(13, 1, 'منوعات', '', '', '', ''),
(13, 2, 'منوعات', '', '', '', ''),
(14, 1, 'شخصيات', '', '', '', ''),
(14, 2, 'شخصيات', '', '', '', ''),
(15, 1, 'كُتُب', '', '', '', ''),
(15, 2, 'كُتُب', '', '', '', ''),
(16, 1, 'خواطر وأشعار', '', '', '', ''),
(16, 2, 'خواطر وأشعار', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sp_newscategory_path`
--

CREATE TABLE `sp_newscategory_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_newscategory_path`
--

INSERT INTO `sp_newscategory_path` (`category_id`, `path_id`, `level`) VALUES
(1, 1, 0),
(2, 2, 0),
(3, 3, 0),
(4, 4, 0),
(5, 5, 0),
(6, 6, 0),
(7, 7, 0),
(8, 8, 0),
(9, 9, 0),
(10, 10, 0),
(11, 11, 0),
(12, 12, 0),
(13, 13, 0),
(14, 14, 0),
(15, 15, 0),
(16, 16, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sp_newsissue`
--

CREATE TABLE `sp_newsissue` (
  `issue_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `cover` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_newsissue`
--

INSERT INTO `sp_newsissue` (`issue_id`, `image`, `cover`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(1, 'catalog/news/issue/slider1.jpg', 'catalog/news/issue/cover/1.jpg', 1, 1, '2017-01-02 17:27:13', '2017-10-05 19:39:39'),
(2, '', 'catalog/news/issue/cover/5.jpg', 5, 1, '2017-01-02 17:53:01', '2017-10-05 18:29:31'),
(3, 'catalog/news/issue/issue2.jpg', 'catalog/news/issue/cover/2.jpg', 2, 1, '2017-01-02 18:03:02', '2017-10-05 17:38:53'),
(4, 'catalog/news/issue/issue3.jpg', 'catalog/news/issue/cover/3.jpg', 3, 1, '2017-01-02 18:08:57', '2017-10-05 18:28:45'),
(5, 'catalog/news/issue/issue4.jpg', 'catalog/news/issue/cover/4.jpg', 4, 1, '2017-01-02 18:09:20', '2017-10-05 18:29:15'),
(6, 'catalog/news/issue/innovation.jpg', 'catalog/news/issue/cover/6.jpg', 6, 1, '2017-09-22 18:02:32', '2017-12-16 04:13:59'),
(7, 'catalog/news/issue/reading.jpg', 'catalog/news/issue/cover/7.jpg', 7, 1, '2017-09-26 18:25:18', '2017-12-16 04:13:45'),
(8, 'catalog/news/issue/Pride.JPG', 'catalog/news/issue/cover/8.jpg', 8, 1, '2017-09-26 18:25:34', '2018-03-26 01:33:19');

-- --------------------------------------------------------

--
-- Table structure for table `sp_newsissue_description`
--

CREATE TABLE `sp_newsissue_description` (
  `issue_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_newsissue_description`
--

INSERT INTO `sp_newsissue_description` (`issue_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'The culture', ''),
(2, 1, 'My Dubai', ''),
(7, 2, 'القراءة', ''),
(5, 1, 'Avant Garde', ''),
(3, 1, 'New Beginnings', ''),
(4, 1, 'Leadership', ''),
(6, 2, 'الابتكار', ''),
(1, 2, 'The culture', ''),
(3, 2, 'New Beginnings', ''),
(4, 2, 'Leadership', ''),
(5, 2, 'Avant Garde', ''),
(2, 2, 'My Dubai', ''),
(8, 2, 'الهندسة المعمارية والتصميم', ''),
(6, 1, 'Innovation', ''),
(7, 1, 'Reading', ''),
(8, 1, 'Architecture and design', '');

-- --------------------------------------------------------

--
-- Table structure for table `sp_news_attribute`
--

CREATE TABLE `sp_news_attribute` (
  `news_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_news_description`
--

CREATE TABLE `sp_news_description` (
  `news_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_news_description`
--

INSERT INTO `sp_news_description` (`news_id`, `language_id`, `name`, `description`, `tag`) VALUES
(2, 2, 'Mobile Aid', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', ''),
(3, 2, 'Gafla: The Arabic Jeweler', '', ''),
(4, 1, 'Tawlat', '&lt;p&gt;Following 18 months of research and development, a UAE-based team of internet-savvy entrepreneurs officially launched Tawlat - the region’s biggest online restaurants loyalty platform that allows diners to search and book restaurants, find latest promotions and collect valuable reward points. Tawlat acts as a search engine for restaurants in the UAE and enables diners to make dining arrangements with few clicks.&lt;br&gt;With the growing number of Internet users in the region standing at more than 80 million and the constantly expanding food and beverage industry, the team founders –an Emirati entrepreneur, Hamad AlMarri and CEO, Tariq Abdelhaq, identifiedthe need for a real-time booking andsearch engine that provides up-to-dateinformation about restaurants.&lt;br&gt;“It can be a real pain trying to search for a good restaurant that suits a diner’s budget, and making a reservation can be so frustrating, especially when you find out your restaurant of choice is all booked up” – said Hamad Almarri. He added: “By introducing Tawlat, the free innovative app allows diners to search and book restaurants easily and quickly from the comfort of your home or when dashing across town with friends.”&lt;/p&gt;&lt;p&gt;With Tawlat you can browse hundreds of UAE restaurants by cuisine, location or feature to help you choose the best one for you, find new exciting places to eat, read customer reviews, check opening times, and see when tables are available. Then, you can secure a reservation with just one click!&lt;/p&gt;&lt;p&gt;Tawlat’s unique functionality also provides you with driving directions, parking availability and allows you to leave instant feedback about your eat-out experience before you depart.&lt;/p&gt;&lt;p&gt;“On the restaurant’s end, using the cutting-edge cloud technology to manage reservations, reviews, feedback, and guest information is as simple as using social media platforms. Restaurants are able to track the past reservations and fully manage future bookings from the comfort of their office or mobile phones.” –Mentioned Thani AlQasem, Co-Founderand Managing Director.&lt;/p&gt;&lt;p&gt;“Tawlat’s special loyalty program allows you to collect reward points with every booking, which can be redeemed as a discount on your next reservation or accumulated for an even bigger discount another time. Updated 24/7, Tawlat keeps you informed about upcoming events, special discounts and exclusive promotions, which can all be booked directly through your device.” – Says Tariq Abdelhaq.&lt;/p&gt;&lt;p&gt;The company is currently targeting restaurants throughout the UAE and aiming to expand the outreach of Tawlat to the entire Middle East region. Al-Marri is looking to collaborate with Dubai Tourism Authority and to cater to visiting tourists who are not familiar with the country and can easily access rich information and the best services the F&amp;amp;B industry has to offer..&lt;/p&gt;&lt;p&gt;Tawlat is now available on the World Wide Web, and is soon launching its iOS and Android mobile applications, which will be available to the public by the end of October 2015.&lt;/p&gt;', ''),
(2, 1, 'Mobile Aid', '&lt;h6&gt;&lt;b&gt;How was the idea behind Mobile Aid born?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Founded by Khalid Sharaf and Essa Sharaf, the idea for Mobile Aid was born when the two cousins spotted a gap in the market for fast, simple, and safe mobile phone repair. Khalid, a qualified mechanical engineer, had always been fascinated by technology and decided to take this knowledge and combine it with great cus-tomer service.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What is the concept behind Mobile Aid?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The concept is simple: fast, efficient mobile repair, at your desk or home. Mobile Aid comes directly to you and fixes your phone on the spot, saving you time, money and giving you a peace of mind: your phone or tablet never leaves your side whilst it is being repaired.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;As founders are you both interested in gadgets and technology, or was this purely a business venture?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We are both fascinated by technology and always have been. Khalid is a qualified mechanical engineer meaning that his love of gadgets began at a young age.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What motivated you to launch this start-up?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;In a region where mobile and smartphone penetration is amongst the highest in the world, we know that consumers depend heavily on their devices for both work and leisure and it is extremely inconvenient when something goes wrong and the device breaks.&lt;/p&gt;&lt;p&gt;A mobile phone is more than just a piece of technology, it is an emotive item, of-ten containing milestone images and videos: a baby’s first steps, family holidays, school or university reunions. The prospect of finding a way to fix the phone, with the risk of losing these memories can be very daunting. That’s why we launched Mobile Aid: we offer a fast, safe and reliable service. The device never leaves your sight, so you can be confident that your memories are safe.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;If you were to point a few aspects that make Mobile Aid unique, what would these aspects be? What would you say makes Mobile Aid an innovative business?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;With a team of fully trained expert professional technicians on standby to travel to a home or office within minutes, Mobile Aid delivers great service in a timely man-ner. We also carry a huge range of parts and products meaning they are reliable and dependable. %90 of all phones and tablets are fixed on the spot, right in front of you, within an hour.&lt;br&gt;For the %10 of phones that can’t be fixed immediately, Mobile Aid has a state-of-the-art Laboratory staffed by highly qualified technicians. These experts admit the phones (usually water-damaged) into the lab where stay until repaired - typically 3 days.&lt;br&gt;We are also the first in the region to launch cost-effective Mobile phone insurance.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;One challenging situation you faced with Mobile Aid and what you learnt from that?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;In order to ensure the highest level of quality, Mobile Aid employs skilled and con-scientious staff with proven repair experience and first-class communication skills. The team is what makes us different and stand out, and finding the right talent to fit our standards is always a challenge.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Are there any plans you have to expand in the future?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;At the moment we operate mainly in Dubai and Sharjah, we plan to expand into the entire UAE. We are also working on an exciting new accessories range.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Please share your contact information so the readers can get in touch:&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Visit us online: &lt;a href=&quot;http://www.mobileaid.ae&quot;&gt;www.mobileaid.ae&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Facebook: &lt;a href=&quot;http://www.facebook.com/Mobile-Aid-LLC&quot;&gt;www.facebook.com/Mobile-Aid-LLC&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Twitter: www.twitter.com/MobileAidLLC&lt;/p&gt;&lt;p&gt;Instagram: www.instagram.com/mobileaid&lt;/p&gt;&lt;h6&gt;&lt;/h6&gt;', ''),
(7, 2, 'Outlines to Ornaments', '', ''),
(7, 1, 'Outlines to Ornaments', '&lt;h6&gt;&lt;b&gt;Can you tell us more about yourself?&amp;nbsp;&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Sherina Abdulla, currently studying Multimedia Design at Zayed University with an emphasis on Graphic Design. Throughout my years at the university, I’ve been interested in different fields that are and aren’t related to my major and this is how I got the chance to explore the field of 3D printing and jewelry design.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;How did you come up with the concept of 3D jewelry?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The concept of 3D printed jewelry existed for years around the world and it has only recently grown in the region. I was introduced to 3D printing in a course I enrolled in back in 2014 called 3D Printing &amp;amp; Jewelry Design at Zayed University. At the time, the university had bought a new 3D printer and our class was the first to use it and explore the field of 3D modelling and printing. Our professor taught us how to use the printer and the different stages that your design goes through during the process. The first part of the course was all about getting familiar with the modelling software, 3dsMax, then it was all about knowing how models look like when printed. Of course, a lot of trial and error came along with the testing phase and that is what you learn the most from. Once we got more familiar with the process, we started working on small assignments that eventually led to the final project, which was to design and print a jewelry set of three pieces. Designing and modeling the jewelry pieces depended on how familiar each student was with the modeling software and where we get the inspiration from. This is how we ended up with more than 10 unique jewelry sets for the final project at the end of the course.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What makes 3D jewelry different &amp;amp; innovative?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;3D modeling and printing in general is an innovative and powerful method of creating and designing. This method is used in many fields such as mechanics, medicine, fashion and many others. What makes it so is that you can design, model, and print items that experts and craftsmen cannot create with their own hands because of the technical precision that the technology provides. This is where you can come up with jewelry pieces that are unique and detailed in a way that is unfamiliar to the norm making them stand out between typical jewelry designs. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;Would you turn this idea into a business one day?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;It would be lovely to do so and I believe that now is a good time for anyone who is interested in doing so - the technique is fairly new in the country and the region and can stand out in the market. Just like any other kind of business, it would need the skill, time, effort, people, motivation, planning and of course a sufficient amount of money to start with. I have to say that this method of designing and producing goods can be ridiculously expensive but I’m positive that this will change in the near future when 3D printers become more accessible and available to the public.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Can you briefly explain the process of making 3D jewelry?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The first step is to have an idea of what you want to make and be able to use a 3D modeling program; some people go directly to the program and design from there. I personally prefer sketching ideas roughly and then move onto the program. Once you have an idea, you model it in the modeling program using the different tools and modifiers available and then save it in a specific format that the 3D printer can read. As for printing, you’ve go to use another software that connects you directly to the printer. So it isn’t like printing paper where you can send your file to the printer directly from your computer. Once you setup your design in the second software (the one connected to the printer), and figure the printing options, you simply click on print and when you do, the printer warms up and that can take several minutes. The printer that we have at the university prints models in ABS plastic and it starts by printing something called the support structure first. Basically this structure is an integral part of the printing process where it acts like a scaffolding for the model to be built and is removable once the model is printed. The time it takes to print a model depends on the size and complexity of the design; it can take an hour or up to 10 hours and sometimes even more. When it’s done printing and you take out your model from the printer, you sometimes can remove the support structure physically or if not, you place it in a special bath that contains a chemical that dissolves the support structure without affecting the model. Once you have the model without the support structure, you can sand it or paint it to give it a better look as the ABS plastic is plain off white that you might like or want to change its colour.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What are other innovative things you can create using the 3D printing technique?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;What you can make and produce using this technique is endless. It depends on how creative you can be with your imagination. I have recently seen a video about producing 3D printed cars where this technique helped in putting cars together in a very short amount of time and this can be something revolutionary in the world of cars. Another article I read recently was about how a man got some of his ribs replaced, that he lost due to cancer, by placing 3D printed titanium rib-like structures in his chest. We don’t yet know how 3D printing will impact our lives in the future but from what exists right now, it is sure to be revolutionary!&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What are your plans for the future?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We don’t always have a clear image of our future nor have detailed plans that work perfectly, however I would like to gain more knowledge and experience at using the modeling programs and be able to explore the different methods of 3D printing. I am interested in design for social change and would like to further experiment with this technique to see how it can be used to enhance the lives of many people in developing countries that face difficulties in everyday life.&lt;br&gt;&lt;/p&gt;', ''),
(36, 1, 'Bait Al Kandora', '&lt;h6&gt;&lt;b&gt;Tell us a little about yourself...&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The two founders of Bait Al Kandora are Khaled Al Huraimel and Saif Al Midfa, two UAE nationals from Sharjah. We actually met approximately 15 years ago when we started our career at ENOC together in Dubai overlooking international sales and business development. Currently Khaled is the Group CEO of Beeah and Saif is the CEO of Expo Sharjah. Saif grew up in Sharjah while Khaled spent over 20 years of his life outside the UAE, as his father was an ambassador of the UAE in several countries. Both are MBA graduates and also serial entrepreneurs who have started several businesses in the UAE in different sectors.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Why was your brand named as Bait Al Kandora?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We wanted a name that reflects our brand and offerings, and so we chose Bait Al Kandora or the House of Kandora. Our vision is to be the leading local and gulf menswear brand in the UAE and the name also reflects its confidence. We paid a lot of attention in every detail from the brand name to the logo - our logo and brand symbolizes our proud origins with the Emirati falcon and the shape of the Arabic number seven, for the seven United Emirates as well as the sign for peace and victory.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;How did Bait Al Kandora happen?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The concept of Bait Al Kandora was envisioned many years ago by us when we saw a business opportunity to create the first Emirati and Gulf menswear brand. We are a very proud nation where we have excelled in many sectors from airlines, to real estate and finance, but our national dress which we are very proud to wear has been the same for decades. We wanted to elevate and create a local menswear brand that takes Emirati menswear to the next level, offering the same brand experience as you would get from an international fashion brand.&lt;/p&gt;&lt;p&gt;So in 2013, we started working on this project and spent many long hours and days to study the market as well as study how international fashion brands have developed and put a strategy and business plan together. We wanted to not just offer kandoras, but also offer a full range of local menswear products from sandals, ghutras, wallets, and even underwear. So we designed our full range of menswear and we also worked with international fashion consultants to ensure our store experience and design is the same level of international fashion brands. We also wanted to offer the best and most convenient experience and delivery of our products by offering a visiting tailor to take orders, as well as the first to offer an online store where clients can design and customize their own kandoras online.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What motivated you to start your Kandora line &amp;amp; what makes it special?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The whole brand experience makes it special, and we are proud to say we are the only local menswear brand that rivals international fashion brands from store experience, to products and online store. We are very lucky to have leaders in our country that have provided us with the best level of living and taught us always to excel, and we wanted to do that with this business and elevate the national dress to the next level.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Who’s your main target?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Our main target market are male Emirati and GCC nationals from the age of 16 to 55 who want the best level of service and the best quality product. We are also looking to attract females who are also our target market. Many females would love to buy gifts, tailor a kandora or buy a sandal for their male family member, and until today most local menswear store are not female friendly. However our stores have females sales assistance, and our online store and visiting tailor services make it very easy for someone to order for their family.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What are the challenges that you\'ve faced so far?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The fashion industry was new to us, we found ourselves struggling to balance learning from others and managing the start up while also having full time jobs leading large organizations. So we were working many late nights to ensure we create a brand that we aspired to achieve, all the while managing our time. The other challenge we faced was ensuring we get the right location for our brand positioning which took time until we secured our first boutique in downtown Dubai at Souk Al Bahar. Also, like any other new brand, it requires a lot of brand awareness so we have been continuously investing in building our brand using marketing and advertising.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Tell us about your next step.What do you want to achieve professionally?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;For Bait Al Kandora, our vision and aim is to be the leading menswear brand in the UAE and to open boutiques across the Emirates. We already have plans to open at least two more locations next year. Currently we have two boutiques, in Dubai and Sharjah, which we opened earlier this year. We have also received a lot of interest from other GCC countries and are studying to expand our brand in neighboring countries soon.&lt;/p&gt;&lt;p&gt;We are in the final stages of opening a new production facitility which will open by early next year, and that will be one of the largest and most modern when it comes to kandoras.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;You have your unique sense of style when it comes to the material used in your designs, what inspires you?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;While our brand elevates local menswear to the next level, we have maintained a conservative design direction when it comes to our full product range. Our selection of fabrics have been hand picked from the best available in the market and we have classified them into three categories: Classic, Gold and Black Edition. In addition to that, we are very proud of our sandal range which are hand made and hand painted in Italy, and we have designed a collection of 50 different designs with our partners in Florence, Pakerson, who have been producing luxury shoes and leather products since 1923.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;When it comes to the latest trends, do you tend to set your own or search latest trends?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We actually do both, we study the market, and demand, and thus design and produce products to meet that demand, as well as have a range that we design to set new trends. We are working on interesting new products to add to our range next year which we are sure will set new trends too.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Anything special about your clothing line, services and products?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Everything! The store experience, the first to offer a full branded range of products, the visiting tailor service, the online experience and the quality.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Which piece is your favorite?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;That\'s a very hard question as I like all the products and don’t just wear one piece. Our most popular kandora has been the Arabi Kandora, which is also one of my favorites, and I love the solid silver falcon cuff-link which we made in the UAE.&lt;/p&gt;&lt;h6&gt;&lt;b&gt; An advice you send out to people who are inspired by you..&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;If you believe and are passionate about making a business successful, then you need to spend a lot of time and hours and go into details, and not always depend and delegate to others, especially at the beginning of a business. Also, managing the finances is extremely important for any start up to ensure you have a plan and budget, and remain focused on your goals and objectives.&lt;br&gt;&lt;/p&gt;', ''),
(35, 1, 'Pearla', '&lt;p&gt;Pearla, established by Hessa and Fatma Ali Al Joker, is an extravagant Dubaibased fashion brand, that creates conceptual ready-to-wear garments. The garments designed by Pearla vary in terms of their uses; there are pieces that are wearable in everyday life, made out of the finest fabrics to assure carelessness when worn, and there are pieces that are designed to capture the attention of the crowd due to how abstract and exceptional they are.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What is the main concept behind your brand?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The main concept of our brand is to introduce abstract and contemporary pieces that merge art, storytelling and wide imagination with fashion. We like to think that each piece we create withholds a story, that the woman who chooses to wear a garment from Pearla, feels like the protagonist of a never-ending fairytale. We try to create pieces that are memorable and uncommon in the fashion industry. We also make sure that the garments we create are out of the ordinary that they would end up creating the \'theatrical crowd head-turning\' impact. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;How did you develop your interest in fashion design?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We developed our interest in fashion design at such a young age, but we didn’t pursue it until 2008. We think it all had to do with growing up in a family that supported the whole ideology of creating something out of what you have. Since we were artists, long before we were graphic designers, we saw fashion design as just another medium and a tool to bring our ideas and imagination to life. When we were kids, we had a fixation with fairytales and we were so caught up with our imagination, that when we grew older, we thought why not create a brand that could help us introduce these stories to the future generation, but also add our own twist to them.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Did your skills as graphic designers contribute to the success of your brand?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;They definitely have, considering the fact that the fabrics we use in our designs are all designed by us. We think this makes our brand phenomenal and separates us from all of the emerging brands in the fashion industry. Without graphic design, I don’t think our pieces would’ve had the same impact that we’ve envisioned for every garment we create. It also makes it so much easier to change the packaging for every collection we launch, in order for it to fit the storyline of the collection. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;The textiles and prints used in your clothing line are very unique, how do you manage to produce them?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We work on the designs of our fabrics and textiles from scratch. It often involves lots of brainstorming, getting out of our comfort zone, researching more about the subject we are targeting for each outfit, as well as following the color scheme we’ve set for each piece. However, we don’t necessary limit ourselves to that, because it’s acceptable to break rules sometimes. We think it’s the most time-consuming process when it comes to designing, because it involves so many procedures and steps that if one of them was left out, we’ll have to start again.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What inspired your latest collection?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The main inspiration behind creating our latest collection was the wonders that are hidden in nature and in the pages of books. We try to look at things as artists, rather than ordinary people. The main reason we chose to use kids in this photo-shoot, is because the imagination of a child is limitless, unlike an adult who is too caught up with reality. If you take a closer look at the designs used, you’d see a lot of unusual patterns and imagery that you’d expect a kid or an artist to see. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;Since people are more likely to purchase trendy and typical designs, was it a challenge for you to sell pieces that are unusual?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;When it comes to our designs, we tend to target a certain group of people... those who are daring in what they wear, those who don’t settle or depend on trends but are rather independent in their style, and are not afraid to stand out in the crowd. We don’t think Pearla was created for everyone, but it was created for those who have a vivid appreciation for art. Everyone has their own views on artworks, so if we were, for instance, to put a dress from Pearla in a museum, some would stand there in awe for a while, and others would walk away with distaste. Thankfully, we’ve been lucky enough to attract an abundant number of people who would fit the category that we were hoping to target.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;In your opinion, what skills are important for a successful career in fashion design?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The most important skills for a successful career in fashion designing would be to withhold extensive creativity and imagination. A designer should also be hardworking and willing to stand in the way of failure and not give up from the first time, because the fashion industry is a competitive industry and as a designer, a lot of obstacles will come your way and a lot of doors will be slammed in your face, but that doesn’t necessarily mean that you should give up. In the fashion industry, your glass of expectation shouldn’t be filled to the very top, otherwise you’d end up being disappointed and lose all of your motivation.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Did you expect the success that you have achieved so far?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Not at all, we are so thankful that the brand has reached this far and for all of those who keep it going. We plan to see Pearla grow further and be more widely known internationally - we are gradually working on that.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What advice do you have for other aspiring fashion designers?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Our advice is to not be afraid to experiment and create something the designers would love to wear as well, not just because it’s a trend. As well as to keep on working hard and not let the opinion of others stand in their way, and the same thing could be said about obstacles that could be thrown at them.&lt;br&gt;&lt;/p&gt;', ''),
(9, 2, 'Nayif-1 To Earth', '', ''),
(9, 1, 'Nayif-1 To Earth', '&lt;h6&gt;&lt;b&gt;Can you begin with explaining what a CubeSat is? &lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Firstly I believe I should highlight what a nanosatellite is – we always hear about satellites but there are many different classes, one of them are nanosatellites. Nanosatellites are very small satellites that usually weigh from 1 to 10kg; a CubeSat is a type of a nanosatellite. What differentiates CubeSats from other nanosatellites is that CubeSats come in a standardized design, meaning that the length and width of a CubeSat is always 10x10cm. Usually the height is more flexible and increases by increments of 10cms. You usually have CubeSats that are called 1U (1 unit) which means their dimensions are 10x10x10cm, and there are 2U that are dimensions 10x10x20cm… so on and so forth.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Give us an introduction to the Nayif-1 project?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Nayif-1 is a 1U CubeSat with an Amateur Radio payload and is the first nanosatellite from the UAE. Nayif-1 is a collaboration of efforts between the Mohammed Bin Rashid Space Centre and the American University of Sharjah, where seven Emirati students from different engineering schools such as electrical, computer and mechanical engineering worked on the project from design to assembly, integration, testing and operating the satellite.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;How did CubeSats become popular?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Because of CubeSats standardized design, you can have access to off shelf systems and components. Based on that, CubeSats’ missions are both time and cost effective, allowing space to be more accessible to a wider range of nations.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What’s a CubeSat’s mission?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The uses of CubeSats are endless – they all depend on the scientific question the design team would like to answer. Depending on the question asked, the team could identify a payload or instrument to achieve an answer. CubeSats today are used for communication purposes, image capturing and environmental monitoring. Our Nayif-1 CubeSat’s payload is an Amateur Radio with additional sensors for different studies. The data we will receive on the ground station will be made available to the academic community for research. Adding on that, we will be testing and studying a new control system on Nayif-1.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What are the processes of Nayif-1, from design, to manufacturing and launch?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Nayif-1’s mission started with a preliminary design, where the mission and system requirements were defined, as well as the preliminary necessary budgets such as power and mass. The preliminary design was then presented to the team of engineers we have at the Mohammed Bin Rashid Space Centre, and feedback was received. The second stage was moving to the critical design, where the design of the satellite was finalized. The critical design was also presented to our engineers, since feedback is always vital. Finalizing the design, the team trained on mechanically assembling and integrating an engineering model of a CubeSat to prepare for Nayif-1’s final assembly and integration. In September 2015, we initiated the assembly, integration and testing phase of Nayif-1’s flight and final model in the centre’s facilities. The final environmental testing of the whole satellite will be led by our team, but will take place in our knowledge transfer partners in Netherlands. Launch management is being carried out by myself, in respect of launch documentations and contacting both the local and international entities to prepare for our licensing and launch. In addition, our mechanical engineering lead on the team is in charge of studying the effect of the rocket on the satellite itself, and will be in attendance the day of the launch.&amp;nbsp;&lt;/p&gt;&lt;h6&gt;&lt;b&gt;On what rocket will Nayif-1 launch in &amp;amp; why this particular launch vehicle?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Nayif-1 will be launched with a group of other CubeSats as secondary payloads on a Falcon 9 rocket, by early 2016. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;Where does Nayif-1 fit in UAE’s space plan?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The UAE currently is focusing on building strong capabilities in the space sector, to be able to do that you need to have strong bridges with the academic sector. Nayif-1 fulfills that by being the first UAE university satellite.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;How does this mission affect the education sector in preparing future UAE STEM professionals? Why?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Space is a new field of interest to the UAE; some universities today have not yet built the required infrastructure to sustain programs to support this field. Because of the accessibility and simplicity of CubeSats, they can give an introduction to both students and educators to the field of space sciences and space systems engineering. I find&amp;nbsp; space missions to be the mother of STEM fields, because to run sustainable and effective space missions you need experts and professionals from all STEM fields. In addition to that, because of how challenging space missions are, they tend to push individuals from STEM fields to innovate in ways never thought of before.&amp;nbsp;&lt;/p&gt;&lt;p&gt; &lt;/p&gt;', ''),
(4, 2, '', '', ''),
(5, 1, 'MBR Centre for Govt Innovation', '&lt;h6&gt;&lt;b&gt;What do you do at the Mohammed&amp;nbsp;&lt;/b&gt;&lt;b&gt;Bin Rashid Centre for Government&amp;nbsp;&lt;/b&gt;&lt;b&gt;Innovation?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Through the centre, the UAE seeks to develop a comprehensive system of modern tools to help government entities innovate in areas such as policies and services provided to the public. Through these tools, the centre aims at transforming government innovation into an organized institutional work system that serves as a key pillar of the UAE Government strategy.&lt;/p&gt;&lt;p&gt;Our work relies on creating a work culture across government and having them incorporate the concept of innovation in their entities. In order to do that, we work on providing tools, developing workshops, hosting talks and we also have a Public Sector Innovation Diploma program that is tailor made for government employees. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;What are some of the key areas the centre focuses on?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;At the Mohammed Bin Rashid Centre for Government Innovation we focus on three pillars: Experiment, Enable, and Enrich – we call them the three E’s.&lt;/p&gt;&lt;p&gt;Our Experiment pillar focuses on providing a world-class space for testing, creating, and encouraging innovation. While Enable focuses more on building innovative capabilities by providing them with the essential knowledge and practice, in order to develop a highly skilled and well connected community of innovators. Our last pillar, Enrich mainly aspires to promote an innovation culture and make innovation an everyday practice in UAE government.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What is the goal the centre is trying to achieve?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;At the centre, we aim to become a global hub that stimulates innovation within the government sector, and to be amongst the top innovative governments around the world.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Who does the centre target?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We mainly target government sector employees as most of our programs revolve around innovation in the government. However, we often have individuals from all federal and local government sectors attend our workshops and programs.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Can you tell us about the programs and workshops offered by the centre?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;There are many programs and workshops that are offered through the centre and we tend to have something on a monthly basis. Some of our initiatives include:&lt;/p&gt;&lt;p&gt;1.Government Innovation Lab, whichconsists of specialized, interactivesessions and workshops that useinnovative methods and tools to inspireinnovative ideas and find realistic solutionsto the challenges that face governmententities.&lt;/p&gt;&lt;p&gt;2.Ibtikar Talks (Innovation Talks), which isa series of inspirational seminars that areconducted throughout the year.&lt;/p&gt;&lt;p&gt;3.Government Innovation MasterclassSeries, which is a series of specializedGovernment Innovation Workshops incollaboration with INSEAD to spreadawareness and knowledge, promote aculture of innovation, and develop nationalcadre’s innovative skills.&lt;/p&gt;&lt;p&gt;4.Ibtekr, this is more like a roadshowthat spreads awareness amongstgovernment employees and increases theirunderstanding about innovation. We offerthem a box called the Ibtekr Box (InnovateBox) which has the tools required tostimulate innovative thinking.&lt;/p&gt;&lt;p&gt;5.Public Sector Innovation Diploma, ayearlong program that’s specialized andcurrently offered to senior governmentemployees.&lt;/p&gt;&lt;p&gt;We have more offerings and programs which you can find more about through our website.&lt;/p&gt;&lt;p&gt;&lt;i&gt;www.MBRCGI.gov.ae&lt;/i&gt;&lt;/p&gt;&lt;p&gt;&lt;i&gt;+9714 330 4433&lt;/i&gt;&lt;/p&gt;&lt;p&gt;&lt;i&gt;info@mbrcgi.gov.ae&lt;/i&gt;&lt;/p&gt;&lt;h6&gt;&lt;b&gt;We heard a lot about the Public Sector Innovation Diploma, could you tell us more about this unique program?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The Diploma launched by the centre is a high impact and practical professional development program in Public Sector Innovation delivered by senior faculty from the University of Cambridge, Judge Business School. The first cycle, comprising of more than 50 candidates, kicked-off in April 2015. The program’s key strategic objectives are:&lt;/p&gt;&lt;p&gt;•Actively assisting the UAE government’s vision to become among the most innovative governments globally by 2021.&lt;/p&gt;&lt;p&gt;•Developing the innovation culture in governmental departments and ministries of the UAE; all project elements of the program will be linked to UAE Vision 2021 and the UAE’s six priorities.&lt;/p&gt;&lt;p&gt;•Identifying, supporting and developing top government talents who have the potential to become Chief Innovation Officers in each ministry or government entity. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;Are there any plans to get the young adults generation involved in innovation?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;There are many programs where we have the youth involved through the National Innovation strategy. We are constantly developing programs either through the centre or with academic institutions across the UAE, in order to deploy innovative skills and concepts in the country.&lt;/p&gt;&lt;h6&gt;&lt;b&gt; How can you help individuals who have innovative ideas?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We have a platform called Afkari, where individuals from the federal government sectors can submit their innovative ideas and get a chance for it to be funded. For those who are interested, they can find out more about Afkari. At the same time, we have a section on our website where we showcase innovations from the UAE, and anyone who has implemented an innovative idea can submit it through the website and have it showcased.&lt;/p&gt;', ''),
(5, 2, 'MBR Centre for Govt Innovation', '&lt;p&gt;With the declaration of His Highness Sheikh Khalifa bin Zayed Al Nahyan, President of the UAE, 2015 is officially the Year of Innovation in the country. This followed the unveiling of a National Innovation Strategy. To further support this innovation drive, the Mohammed Bin Rashid Centre for Government Innovation, which bears the name of His Highness the Vice President, Prime Minister of the UAE and Ruler of Dubai was also launched in late 2014. The Centres mandate is to make innovation in government an everyday practice, thus helping to realize HHs vision for the UAE to be among the most innovative nations in the world.&lt;/p&gt;&lt;p&gt;‘’Our goal is to create an integrated environment for innovation, which will foster the generation, incubation, and implementation of ideas and will continuously measure their effectiveness. Innovation is the capital of the future’’ – His Highness Sheikh Mohammed Bin Rashid, Vice President, Prime Minister of the UAE, and the Ruler of Dubai.&lt;/p&gt;&lt;p&gt;Emirates Diaries got a special sit down with Huda AlHashimi, Executive Director of Mohammed Bin Rashid Centre for Government Innovation, and Shatha AlHashmi, Director of Mohammed Bin Rashid Centre for Government Innovation, to find out more about this unique centre.&lt;/p&gt;&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;b&gt; -&lt;span style=&quot;white-space: pre;&quot;&gt;	&lt;/span&gt;What do you do at the Mohammed Bin Rashid Centre for Government Innovation?&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; Through the centre, the UAE seeks to develop a comprehensive system of modern tools to help government entities innovate in areas such as policies and services provided to the public. Through these tools, the centre aims at transforming government innovation into an organized institutional work system that serves as a key pillar of the UAE Government strategy.&lt;/p&gt;&lt;p&gt;Our work relies on creating a work culture across government and having them incorporate the concept of innovation in their entities. In order to do that, we work on providing tools, developing workshops, hosting talks and we also have a Public Sector Innovation Diploma program that is tailor made for government employees.&lt;/p&gt;&lt;p&gt;&amp;nbsp; &amp;nbsp; &lt;b&gt;-&lt;span style=&quot;white-space: pre;&quot;&gt;	&lt;/span&gt;What are some of the key areas the centre focuses on?&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; At the Mohammed Bin Rashid Centre for Government Innovation we focus on three pillars: Experiment, Enable, and Enrich – we call them the three E’s.&amp;nbsp;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Our Experiment pillar focuses on providing a world-class space for testing, creating, and encouraging innovation. While Enable focuses more on building innovative capabilities by providing them with the essential knowledge and practice, in order to develop a highly skilled and well connected community of innovators. Our last pillar, Enrich mainly aspires to promote an innovation culture and make innovation an everyday practice in UAE government.&amp;nbsp;&lt;/p&gt;', ''),
(1, 1, 'Osaymi', '&lt;h6&gt;&lt;b&gt;What OSAYMI?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The brand name OSAYMI honors my family name but with a slight twist, it reflects my love for designing with meaning and with a purpose.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What is the concept behind Osaymi?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;I wanted to capture the spirit and sense of this generation by designing clothes that combine luxurious fabrics with sporty looks. Every piece in my collection can be mixed and matched to create a variety of different looks, by simply adding a cape or jacket. A day time sporty look can become a classic elegant look for an evening event.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What attracted you to enter the fashion world?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;For as long as I can remember, I have been passionate about the idea of creating fashion as a means of self-expression. I love designing informal clothing of the highest quality, combining beautiful materials, with patterns and textures that stand out from the crowd and make a statement.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Why did you start with a small collection?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The reason for developing a capsule collection is because I believe in simplicity and I wanted to prove the ability to create a range of looks with as few items as possible, developing three essential items - modern, classic and sporty. It’s a collection developed to get feedback from the industry and consumers. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;What is the inspiration behind your first capsule collection ‘I Am Future’?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;&amp;nbsp;It’s a statement that reflects how we feel and the influence we can play in our society.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Where do you produce your outfits and what makes them unique?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;My journey started with searching the globe looking for the right materials, and a manufacturer who could work with me and help me deliver my vision for Osaymi. I have found great partners in Italy who managed to deliver the looks with a fresh approach that represents my personality. My collection is unique and can be wearable by both genders. Playing with the structure to create a distinctive style and personality to each piece, I have created classic looks with a twist of modernity. I combined different high quality materials with a simple, elegant and vibrant print to make my collection come to life through an explosion of colors.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What challenges have you faced when developing this collection?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;There are a couple of challenges I faced during my journey. In order to produce a collection of the highest quality, you need to find a producer that can accept your quantities as a beginner. I am sure most of the designers face the same challenge when they first started out in the business. Secondly, ordering the fabrics is a challenge since most fabric producers will require a minimum amount of meters per color that usually exceeds the amount the designer requires. Any less than that minimum amount will add a percentage to the total cost of the material, increasing the budget. Moreover, logistics is another challenge as I wanted to use our local postal office for their good rates for shipping but unfortunately could not do so since most distributors claim problems with delivery times, leading to a delay in the production process. Hence, I have been forced to use international courier services, which charge triple the prices of regular local postal services. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;What are some of your future goals?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;To build a full collection that is the first choice for customers looking for high quality clothing and competitive prices, and to be a leading brand in the Middle East headed by an Emirati designer.&lt;/p&gt;', ''),
(1, 2, 'Osaymi', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '');
INSERT INTO `sp_news_description` (`news_id`, `language_id`, `name`, `description`, `tag`) VALUES
(12, 1, 'Sheikh Zayed Grand Mosque', '&lt;p&gt;Sheikh Zayed Grand Mosque was the idea and vision of the late Sheikh Zayed bin Sultan Al Nahyan, first ruler of the United Arab Emirates. He wanted to build a center for Islamic culture and a reference of modern Islamic architecture in Abu Dhabi. When mentioning innovation, it would be unfair not to mention Sheikh Zayed because he was truly an innovative leader. He transformed the country, in just a few years, to a better place for its citizens. He had a clear vision and a strong will; which are two main characteristics of innovators. Sheikh Zayed set the foundation stone of this mosque and was also buried outside its gardens when he died in the holy month of Ramadan; making his mosque a prominent place for Emirati people.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;The Grand Mosque&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The mosque is located at the entrance of Abu Dhabi city; between Mussafah and Maqta bridges. It can be recognized by its pure white colors and large number of domes.&lt;/p&gt;&lt;p&gt;The space the mosque was built on is the size of five football fields; it has four minarets (107 meters high), and 82 domes of seven different sizes, and the largest dome is above the center of the mosque. It took 10 years for the mosque to be built and it was finally completed in 2007. The mosque can accommodate more than 40,000&amp;nbsp; worshipers. It consists of internal and external areas, so 30,000 people can pray in the external area while 10,000 in the internal area. The main prayer room is well-known for its geometric Islamic designs with floral additions. &lt;/p&gt;&lt;p&gt;More than 38 contractors were involved in the structure and decoration of the mosque, in addition to thousands of workers working together to complete this masterpiece. &lt;/p&gt;&lt;p&gt;Materials used in the structure of this mosque were sourced from many countries including India, China, Italy, Germany, Austria, Greece, and New Zealand.&lt;/p&gt;&lt;p&gt;The design of the minarets is a combination of Mamluk, Ottoman, and Fatimid styles; while the overall design is influenced by Moorish and Mughal architecture. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;The&amp;nbsp; Architecture &lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The white color of the mosque comes from the CIVEC marble that was used, and it was brought from Macedonia. This type of marble was used for the reconstruction of Diocletian’s Palace; which is an ancient palace built in the 4th century AD in Croatia. &lt;/p&gt;&lt;p&gt;The marble was also used in Slovenia for Preseren Square, Slovenia. Nowadays it is used in building hotels, palaces, and commercial buildings. &lt;/p&gt;&lt;p&gt;To give the architecture an aesthetic touch; floral designs with natural colors were added to the columns in the mosque for a more magnificent effect. In addition to that, glass-work, mosaic, and different Arabic calligraphy styles were also used in this wonderful structure. &lt;/p&gt;&lt;p&gt;GRG (Glass Fiber Reinforced Gypsum) was used for the inside of the domes to create Moroccan artistic art pieces. GRG has many advantages such as good plasticity, no deformation, lightweight, high intensity, fire proof, and environmental friendly. &lt;/p&gt;&lt;p&gt;This kind of Gypsum has a breathing effect; which means that when the temperature is high and humidity is low indoors, the panel will release moisture; on the other hand, when temperature is low and humidity is high; the panel will absorb the moisture in the air. &lt;/p&gt;&lt;p&gt;Also, inside the domes are some verses of the Holy Qura\'an that were also molded using GRG and were painted in gold. &lt;/p&gt;&lt;p&gt;There are seven crystal chandeliers inside the halls of the mosque. The largest one weighs 12 tons and is located in the main hall. The other two smaller chandeliers, each weighing 8 tons, are located in the same hall. These chandeliers are made of gilded stainless steel, gilded brass, 24 carat gold, and Swarovski crystals. The main hall contains the largest handknotted carpet in the world, which weighs 35 tons. It was designed by the renowned carpet-maker and artist Ali Khaliqi, and hand-crafted by around 1,200 artisans. The creation of the carpet took two years and the design took around 8 months. The size of the carpet is 5,700 square meters, 30% of it is cotton while 70% is wool. The estimated value of the carpet is around $8.5 million.&lt;/p&gt;&lt;p&gt;The lighting system in the mosque was designed to reflect the different phases of the moon. At night, the white marble of the mosque changes to a bluish gray color. The projection of the light differs each day depending on the phases of the moon. When it is a full moon, the lighting effect becomes clearer and more brilliant. Twenty-two light towers create light projection with light projectors that help us achieve those effects. &lt;/p&gt;&lt;p&gt;The \'minbar\', a raised platform where the Imam sits to address the people in the mosque in his \'khotba\', is made of eleven steps and carved cedar wood with floral designs. Pearls and white gold were also added in the design. The \'minbar\' has a dome shaped roof that matches the design of the mosque itself. &lt;/p&gt;&lt;p&gt;Outside the mosque, you can see lovely pools surrounding it, adding to its beauty. &lt;/p&gt;&lt;p&gt;The Sheikh Zayed Grand Mosque attracts people from all over the world coming to see this magnificent piece of art. Islamic rituals such as the \'Friday khotba\' and Eid prayers are also conducted in this place. Offices for staff who are managing the day to day operations are located on the east side of the Mosque; in addition to an Islamic center that is located in the same place. &lt;/p&gt;&lt;p&gt;A library is located in the north eastern side of the mosque. It contains variety of books in different languages such as Arabic, English, French, Italian, and Spanish.&amp;nbsp;&lt;/p&gt;', ''),
(11, 1, 'Alezan by SK', '&lt;h6&gt;&lt;b&gt;Who is Salama Khalfan?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Born in Alain and raised in Abu Dhabi, I slowly became a citizen of the world. I live between Dubai and France, which gives me the opportunity to stay close to my horses and atelier of design in France; and also close to my loved ones at home, and remain on top of my business at the workshop in Dubai.&lt;/p&gt;&lt;p&gt;I have an Executive MBA, and studied at Gemological Institute of America (GIA). Passionate about horse riding and show jumping in specific, I entered both national and international competitions, and have my horses with me in Dubai and France.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Before Alezan by SK launched, what did Salama do?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;As a young girl, I’ve always enjoyed crafting things with my hands. I made hair ornaments and accessories that I gifted to my friends and family. I had no idea that this would develop into a life passion. After graduating from university, I joined an Abu Dhabi-based financial organization where I focused on strategy and building new businesses. It developed my business acumen and financial skills, and exposed me to world-class global platforms of business.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Could you tell us more about Alezan by SK? What is the story behind the name?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The first piece of jewelry I created was a bracelet that I made for myself, and the inspiration was fired up by my show jumping mare, Penelope. She sits at the heart of all of this. It was designed in my small atelier in France. Penelope’s breed is a Selle-Francias, which is the primary breed for show jumping horses originating from France; it is also my favorite show jumping breed. The word “Alezan” originates from the French word “chestnut” which refers to the color of the horse’s coat; a deep shade of reddish burned-brown. My horse Penelope is a classical “Alezan”, or chestnut. It seemed like a natural choice for me.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What made you enter the fashion world and leave the corporate world?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;As I highlighted earlier, it was that urge and need to honor my passion for creation. I see jewelry less of a fashion piece and more as an art piece. The inspiration, the stories, the design process and finally the creation – these steps are driven by emotions, just like art. Jewelry often have memories attached to them, and I love that aspect, the sentiments we re-experience every time we wear or touch a piece of jewelry that we were given by someone we love, or one that we gifted ourselves.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Was it a hard decision when you had to choose between your corporate role and your fashion label?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;I didn’t have to choose either or. I was at a stage where I realized that I am passionate about creating pieces of art, and I had to honor that truth. It was very difficult to steer away from the corporate world, not only because I was in a great organization and amongst a wonderful dream-team, but also because sometimes, the most difficult thing can be taking a step forward into the unfamiliar. It can be a place filled with doubt and uncertainty, but the more I focused on the aspired positive outcome, the faster I saw doubt subside.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Where do you draw your inspiration from?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The love for my horses is the main driver of inspiration. I am especially intrigued by how they are perceived by others across different cultures and times. I also experience inspiration when I travel and explore new places, and when I satisfy my ever-thirsty curiosity for knowledge; be it stories from history, architecture, or the sounds and aromas of places I visit.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What is one memory related to Alezan that you are extremely fond of?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;I remember how it felt to see, touch, and feel the first prototype piece. There is something magical about watching something grow from an idea and manifest into a physical matter. A piece of art.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;How many collections have you showcased so far?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Alezan By SK currently has two collections: &lt;/p&gt;&lt;p&gt;1)Artalana Colleciton and &lt;/p&gt;&lt;p&gt;2) Luck at FirstSight, as well as special pieces.&lt;/p&gt;&lt;p&gt;The Artalana Collection is inspired by the horses of the Ottoman Empire age. During my visit to Istanbul to research the topic, I drew inspiration from the architecture as well as the royal treasury of the Sultan’s jewels. The jewelry in the museum looked elegant and mature – the gold had the effects of graceful aging, and was no longer bright yellow or shiny. I wanted to have the same effect on my jewelry, so I used a base of rose gold and white gold for the basic piece, and a mix of rose and black gold for the accents in order to give them that same effect of demure and beautiful age, as though they have been passed over to you by your great-great-grandmother.&lt;/p&gt;&lt;p&gt;In the history of a large spectrum of cultures across the world, people have associated the shape of a horseshoe with luck. Luck at First Sight connects these cultures together through one shared sentiment and belief. Despite differences in languages and religions, “Luck” is a word that shows itself in history and stories of old and modern times. These bracelets were designed to always remind their wearer to be open to good vibes and positivity.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Who are your biggest supporters?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The clients that write back after they’ve bought their pieces to share with me photos and sentiments, the people who visited our stand at Ataya Exhibition 2015, and Khalid, my best friend and husband… he’s always supportive of what I do.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What are some of the obstacles that you faced? How did you overcome them?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Expectations. They make us see one outcome, and none of the other possibilities. I realized that every obstacle I faced was drawn from expectations I had from myself, which then became hurdles because I was too focused on them and overlooked the million other alternatives out there. Stay focused, but remain open and welcoming of opportunities – never say no to opportunity.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What are some of your future plans for Alezan?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;To build a good foundation, and to take these love creations from the UAE to the world.&lt;/p&gt;', ''),
(13, 1, 'HIPA', '&lt;h3 style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Hamdan International Photography Award&lt;/u&gt;&lt;/b&gt;&lt;/h3&gt;&lt;h4 style=&quot;text-align: center; &quot;&gt;&lt;b&gt;An Interview with Omar AlKendi&lt;/b&gt;&lt;/h4&gt;&lt;h4 style=&quot;text-align: left;&quot;&gt;&lt;b&gt;&lt;br&gt;&lt;/b&gt;&lt;/h4&gt;&lt;p style=&quot;text-align: left;&quot;&gt;&lt;b&gt;What does winning at HIPA mean to you? And how does it feel like to be the only Emirati to win this year?&lt;/b&gt;&lt;/p&gt;&lt;p&gt;To be called out on the stage of His Highness Sheikh Hamdan Photography Competition was a dream I have always aimed for and a dream of every photographer as well.&lt;/p&gt;&lt;p&gt;I am so proud of myself for being the only Emirati photographer to win this session.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;I would like to thank His Highness Sheikh Hamdan Bin Mohammad bin Rashid Al Maktoum for the continuous support he provides to photographers locally and internationally. Moreover, I would also like to thank HIPA’s committee for their noticeable hard work that led to the tremendous success of the award and ceremony.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;From one photographer to another:&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;1. Have photographers as your friends to be able to exchange knowledge and ideas.&lt;/p&gt;&lt;p&gt;2. Follow the work of great photographers and attend galleries. Visual feeding is so important for excelling your level and ideas.&lt;/p&gt;&lt;p&gt;3. Take photos to transmit your emotions and thoughts to the viewers.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;', ''),
(14, 2, 'Untitled Chapters', '', ''),
(14, 1, 'Untitled Chapters', '&lt;p&gt;&lt;b&gt;The Beginning&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Untitled Chapters was founded in December 2011 By Fatma Al Bannai who realized there are many Emirati women with a tremendous amount of talent and no way to find one another to connect. She sought to create a friendly environment that united them and allowed them to grow and hone their craft. Over the last couple of years, Untitled Chapters has grown incredibly and has been lucky to find amazing multilingual Emirati women writers who write in all kinds of genres, as well as versatile forms of poetry.&lt;/p&gt;&lt;p&gt;What is unique about Untitled Chapters is that it offers mentorship by the founding members to younger writers. It is one of the key objectives that sets the group apart from other groups in the field; it is not only a platform that showcases members’ writing talents, but we also offer members a chance to grow with us.&lt;/p&gt;&lt;p&gt;Every year, Untitled Chapters celebrates their members and their work by printing their work on a book to showcase what it feels like for writers to have their words in print. It has produced two versions of the book and is currently working on the third one.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Untitled Chapter 2016 Reading Challenge&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Because H.H Sheikh Mohammed bin Rashid has declared 2016 as the Year of Reading, the Untitled Chapters team has issued a challenge to its members, which is collectively reading 500 books this year.&lt;/p&gt;&lt;p&gt;The challenge is being hosted on the Untitled Chapter Goodreads account, shelves are created for each member who participates to show the amount of books they have contributed to the Untitled Chapter Goodreads challenge.&lt;/p&gt;&lt;p&gt;Untitled Chapter encourages members to read in any genre, any language, but most of all to enjoy falling in love with fictional worlds.&lt;/p&gt;&lt;p&gt;&lt;b&gt;How to become a reader?&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Many people believe they cannot become readers. They claim that they don’t know how to start reading because it is either boring, that the act itself would put them to sleep or have no idea where to start reading.&lt;/p&gt;&lt;p&gt;I have been a reader ever since I was 6 years old and I went through a journey to discover many genres throughout the years and I’ve known reading to be one of my greatest joys of in life. So when people ask me how can I love reading so much or how they can start reading, I always tell them to start with identifying what they like.&lt;/p&gt;&lt;p&gt;A very simple way to identify what they like is to think about what kind of movies they are drawn to. Almost everyone enjoys movies and each individual has a certain idea of what genre they enjoy. Use that knowledge of what you like to see in movies and go to the bookstore and pick out one book from the same genre. For example: if someone enjoys romantic movies then they are most likely to enjoy romance books.&lt;/p&gt;&lt;p&gt;But identifying the genre is not enough because there are so many subgenres within one genre, which drives people to get lost. People tend to be attracted to book covers and that’s okay, but it will not necessarily lead you to a book that you would enjoy – so I would first suggest you look through the book titles and see which one grabs your attention the most.&lt;/p&gt;&lt;p&gt;Once you find a title that draws you in, pick that book up and read the summary written on the back, it will give you a fair idea of what the book is about, so you can know if you would be interested in reading something like that or not. And you find that the summary intrigues you, then go ahead and read at least the first three pages of the book, just so you can get a feel of the writing style.&lt;/p&gt;&lt;p&gt;One crucial thing that I always tell first-time readers is when they are reading, don’t look at the page and only see words but try to imagine the characters and the scene developing in your head as you are reading. Create a movie in your mind as you read along. You will feel entrapped by the story and the characters before you know it.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Bring Your Own Book Club&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Because life is too short to read one specific book, the Untitled Chapter members have created a space where readers can bring books they have fallen in love with to introduce to others.&lt;/p&gt;&lt;p&gt;We love inviting people to share their love of books and reading with us, and one of our aims is to create a strong reading culture within the UAE. We also believe that in order for a writer to learn the necessary elements to create poetry and storytelling, one must read.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Here are a few reasons why you should join us at Bring Your Own Book Club:&lt;/b&gt;&lt;/p&gt;&lt;p&gt;1. You will explore titles you wouldn’t read otherwise.&lt;/p&gt;&lt;p&gt;2. You will connect with passionate readers who pursue different genres.&lt;/p&gt;&lt;p&gt;3. You will exchange book reviews.&lt;/p&gt;&lt;p&gt;4. You will practice socializing with other book lovers and engage in intellectual discussions.&lt;/p&gt;&lt;p&gt;5. You will find a place to feel safe, valued, and passionate.&lt;/p&gt;&lt;p&gt;&lt;b&gt;How do I prepare for book club?&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Try to find the book that resonated with you the most. It’s the one you probably try to convince your friends to read!&lt;/p&gt;&lt;p&gt;1. Try taking brief notes while reading the book.&lt;/p&gt;&lt;p&gt;2. Think about your emotional response to the book.&lt;/p&gt;&lt;p&gt;3. Did you connect to the characters?&lt;/p&gt;&lt;p&gt;4. Strengths and weakness of the plot.&lt;/p&gt;&lt;p&gt;5. What message did you take away from the book?&lt;/p&gt;&lt;p&gt;It doesn’t matter if you don’t know the answers to the questions, but it’s good to keep them in mind because chances are that during the discussion you will find out.&lt;/p&gt;', ''),
(15, 2, '&quot;Wanna Read?&quot;', '', ''),
(16, 2, 'A World Without Barriers', '', ''),
(16, 1, 'A World Without Barriers', '&lt;p&gt;The book was developed during my course “Design for a Social Change” at my last year at Zayed University as a Graphic Designer. The project’s aim was to tackle a certain subject that had not much of an awareness about and then to overcome the challenge by introducing it to the younger generation. I’ve always wanted to do something&amp;nbsp; related to Autism since my brother Ahmed is affected by it and he’s one of the most important people in my life. After conducting some interviews and surveys, I found that&amp;nbsp; &amp;nbsp; Autism is not a subject that a lot of people know enough of, especially children who become easily fearful of what they can’t understand; which is why I decided to focus on it and develop this storybook that introduces children to a world of a child with autism.&lt;/p&gt;&lt;p&gt;The fact that Ahmed was into art made this project much more interesting. I had the opportunity of sitting with him and asking him to tell me how he sees things around him. He started by drawing himself sleeping in his room but instead of a bed, he was sleeping on his favorite car, which was quite amusing that I had to use that image in the story. Ahmed started to share his vision and world with me and in some parts of the story I incorporated his exact drawings because it’s his world and I wanted whoever to read the story to see it.&lt;/p&gt;&lt;p&gt;Finally, I had to combine his world in a context that is found appealing by children aged 5 and above. My target was to grab their attention to the life of a child with Autism, show them that this child, despite all the difficulty he goes through, is still a person with a very interesting world. And instead of fearing him, they should be much more curious to engage and find out more about his life, or even like the imaginary world he lives in!The storybook was done specially as an iBook ( for apple devices) and is now found on the iBook store. Unfortunately it’s not available in the UAE store, just the US store and a few others.&lt;/p&gt;', ''),
(3, 1, 'Gafla: The Arabic Jeweler', '&lt;h6&gt;&lt;b&gt;What’s your story?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Gafla is the Emirati Arabic for ‘caravan’ or ‘qafila’ in classical Arabic, and it’s inspired by the nomads’ journey through the deserts and dunes of Arabia.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Who’s behind it?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The people behind the brand are Hamad Bin Shaiban Al Muhairi – Managing Director; Abdulla Beljafla – Creative Director; and Bushra Bint Darwish – Junior Designer.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What makes Gafla innovative and different?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;At Gafla, we are creating a celebration of timeless jewels inspired by the heritage of the United Arab Emirates, traditions of the Gulf and the rich Arabic culture of the Middle East.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Where can one purchase Gafla pieces? &lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Gafla offers two options for the clients – online ordering or private viewings.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;How do you pick names for your pieces?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Every collection is inspired by an element that tells a story from our Emirati tradition and heritage, hence why we choose Arabic names to represent our collections.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;How would you describe your pieces?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Our jewelry is inspired by the traditions and the heritage of the UAE as well as the Gulf, but we design them in a way to make them look modern and international.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Tell us about the design process…&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Our Creative Director, Abdulla Beljafla designs all the pieces with Bushra Bint Darwish, the Junior Designer. The process starts merely by an idea, and then we go through a research phase from which we move onto prototyping and finalizing the design. It then finds its way to our workshop for production.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;How did your jewelry interest come about?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Each one of us was interested in jewelry in many different ways. Women in the Gulf absolutely love to wear jewelry, so we took the opportunity of offering them unique pieces inspired by the United Arab Emirates. We think it is the perfect representation of our history and culture.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What are some of your goals as jewelry designers?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Our goal is to showcase our Emirati heritage, the traditions of the Gulf and the Arabic culture to the world through our jewels.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What inspires your collections?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We at Gafla are inspired by our surroundings, from the nature around us to the elements we see or experience everyday with our families and friends. For example, our Jumah collection is inspired by Friday being a blessed day in our culture as Arabs and as Muslims – it’s a joyous day where families gather and enjoy their time.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Were there any obstacles you encountered during the establishment of Gafla? &lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Every business goes through bumpy roads, so of course we have been through a few obstacles, but thank God we manage to get through them as a team with hard work and persistence.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What advice would you give to those starting a journey in the jewelry world?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;There are two pieces of advice that we really believe everyone who is trying to create business should know: Education is your key to success, and always be original in your brand and designs.&lt;br&gt;&lt;/p&gt;', ''),
(24, 2, '', '', ''),
(8, 1, 'When HK Hunts For Food', '&lt;p&gt;Anyone who personally knows me knows that I am a frequent flyer to London. I consider this iconic city, my all-time favorite destination. There is always a sense of excitement within this ever changing city and many hidden gems to discover. My love for London’s street food is what brings me back to the city. Many visitors check online food guides, such as Time Out London, to map out their travel plans but are often left confused as where to go. During every visit, I make sure to hunt for new foodies spots and see what the city has to offer more from street food to casual eatery to even fine dining spots, hoping to discover something new which makes me fall more in love with the city. As a foodie my passion for food and my experience in London always ends up becoming a food marathon. To me, London is all about food and I explore the city on foot, walking to burning my consumed calories.&lt;/p&gt;&lt;p&gt;Here, I am sharing my efforts of years of energy, finding the best places to eat. This is a brief plan for all spots worth trying at least once in a lifetime in London.&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Bobs Lobster Food Truck&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;The best lobster rolls in town, so fresh, tangy, and crispy. Love how the lobster mixture is topped with fresh oregano.&lt;/p&gt;&lt;p&gt;Nearest Tube: On the go food truck. Check their twitter account or Facebook profile for their schedules @BOBs_ Lobster&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Food stall in London China town&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best hidden gem in London China Town: Hong Kong famous eggs waffle filled with Nutella. Surprisingly crispy waffles, different fillings are available. Went with Nutella. One word: Yum.&lt;/p&gt;&lt;p&gt;Nearest Tube: Leicester Square&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Granger and co&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best pancakes: The legendary Ricotta hotcakes (pancakes) with honeycomb butter, fresh bananas, and syrup.&lt;/p&gt;&lt;p&gt;If you are a breakfast person and you love pancakes this is the right place. Go early in the morning to avoid queuing for up to 2 hours. Pancakes are served till 5 pm.&lt;/p&gt;&lt;p&gt;Nearest Tube: Bayswater&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Chin Chin Lab&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best Ice cream. London’s first nitrogen ice-cream store, their flavors are just of out this world. I tried their raspberry, chocolate &amp;amp; caramel and as for the toppings you can go for cardamom as an option plus honeycomb &amp;amp; caramelized pretzels. Loved the experience it was so yummy!&lt;/p&gt;&lt;p&gt;Nearest Tube: Camden Town&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Tap Coffee&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best ice cold brew coffee. If you wake up in London with a caffeine craving. This bottle of magic will definitely kick start your day to awaken your senses.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford Circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Attendant Coffee&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best unusual coffee house. This coffee house formerly was Victorian male public toilet. Lovely tourist attraction place with nice coffee. Don’t worry the place has been scrubbed new.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Home Slice + Pizza Pilgrimage&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best pizza: Margarita Pizza. Both spots my all time favorite pizzeria’s. Pizza pilgrimage is considered one of the successful food trucks; they have 3 branches a cross L town! Yes that’s how good their pizza is. Home slice is another other favorite spot, the base and the toppings are all just perfect. The place gets really packed all day, my favorite timing to go is at 12pm when they first open.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford circus for Pizza Pilgrimage and Covent Garden for Home slice&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Coya Restaurant&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best Peruvian fine dining: Hip trendy place and great Peruvian food with a twist. It is packed all the time so better book a table one week ahead especially if you are going in the weekend. Sea bass risotto is to die for, the blend and seasoning is all just WOW.&lt;/p&gt;&lt;p&gt;Nearest Tube: Hyde Park Corner&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Choocy Woccy Doodah&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best hot chocolate. The most sinful place in L town; a chocolatier specialized in artisan cakes and chocolate goodies. My experience with their hot chocolate was delightful and dreamy. A smile was formed with every sip.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford Circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Pix Pintxos&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best tapas (small Spanish dishes): Great tapas place with cozy atmosphere. Accidentally stumbled onto this place while roaming around in Covent Garden. The moment you enter you can easily recognize this as a tapas place, loved the concept where you can pick what you want and then the staff will just count the total number of sticks you grabbed.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford circus or Covent Garden. Been to two of their branches in Soho and in Neal’s St. in Covent Garden&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Pret A Manager&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Healthy &amp;amp; organic spot. Every item in this place is an experience in itself with flavors and freshness of their food. If you are bored of healthy food you can go for their cheese tomato croissant- so addictive. Their ginger ale drink also not to be missed, so refreshing.&lt;/p&gt;&lt;p&gt;Nearest Tube: Marble Arch&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;One Aldwych Hotel&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best afternoon experience – Charlie &amp;amp; the chocolate factory: Best afternoon experience – Charlie &amp;amp; the chocolate factory: An unusual afternoon tea set that will please both adults and children serving cotton candy, chocolate milk, and other chocolate goodies inspired by the movie itself making it a delightful experience with amazing presentation. I was impressed when my cousin requested the gluten free option and they served her a gluten free afternoon tea set.&lt;/p&gt;&lt;p&gt;Nearest Tube: Covent Garden&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Dishoom Resturant&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best Indian breakfast – Dishooms eggs naan roll. Best egg roll ever, the naan bread is so soft and moist and combined with the eggs it becomes irresistible a must try, I believe the secret the sauce and the herbs blended with the eggs. YUM.&lt;/p&gt;&lt;p&gt;Nearest Tube: Covent Garden&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Busaba Eathai&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best modern Thai eatery – Chicken green curry. This is probably one of the best curries out there. It is seasoned with flavorful Thai herbs, creating a perfect combination of creamy and spicy. The tender chicken is perfectly accompanied with the green curry.&lt;/p&gt;&lt;p&gt;Nearest Tube: Bond Street&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Kopapa&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best Turkish eggs. If you are looking to have an amazing breakfast I would suggest this dish, a must try. This winning combination of poached eggs, topped with whipped yogurt and chili butter and a toast that will leave you wanting for more. Recommended for those who want to try something new.&lt;/p&gt;&lt;p&gt;Nearest Tube: Covent Garden&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Flesh &amp;amp; Bun&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best grilled marshmallow experience. If you are up for an experience, you can ask for marshmallows and toast them. A fire pit is brought and you are given skewers with marshmallows and you toast them yourself. The smell and taste are so mouthwatering-ly delicious. Best experience for a group of friends.&lt;/p&gt;&lt;p&gt;Nearest Tube: Covent Garden&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Muriel’s kitchen&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best carrot cake: Officially the best carrot cake in L town amazing texture &amp;amp; scrumptious. My mother fell in love with it as well.&lt;/p&gt;&lt;p&gt;Nearest Tube: Covent Garden&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Gail Bakery&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Creative breakfast pastries. Craving creative pastries and baking goodies; Gail bakery is the right place to go to. Every item in their shelves is unique, from their peach scones to their mini cakes and sandwiches. Their cookies are not to be missed, they bake them freshly upon your order.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford Circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Prinsi&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Mini chocolate croissant &amp;amp; Italian canteen style breakfast. Princi is an Italian bakery and pizzeria. Their mini chocolate croissant is out of this world. Love their open station style where you see all food displayed on site for your view to choose and feast.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Sumosan&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Chocolate Fondue: The most famous oh so yummy blend of milk, white, and dark chocolate fondue. Every bite is pure happiness.&lt;/p&gt;&lt;p&gt;Nearest Tube: Bond Street&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Duck &amp;amp; Waffle&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best view: One of my personal favorite restaurants and it is considered one of the highest restaurants, as it located on the 40th floor of The Heron Tower providing panoramic views of River Thames.&lt;/p&gt;&lt;p&gt;Nearest Tube: Liverpool street&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Stax&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best brunch &amp;amp; halal burgers American diner. My number 1 halal burger-place, their weekend brunch is not to be missed especially their pancakes.&lt;/p&gt;&lt;p&gt;Nearest Tube: Bond Street&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Alounak&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best Iranian cuisine: if you crave Iranian cuisine such as kebab. Alounak is one of the oldest restaurants since the 80s serving authentic Iranian halal dishes in L town.&lt;/p&gt;&lt;p&gt;Nearest Tube: Bayswater&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Bao london&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Horlicks ice cream bun: One of the successful food truck startups that became a restaurant located in the Soho area. I got addicted to their Baos- Steamed Taiwanese buns when they were served from their trucks first. Their latest crazy creation is Horlicks bun and I can’t wait to try it.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Bread ahead&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best donut: Bread Ahead at Borough Market serves the best donuts that are yummy and fluffy. Highly recommended to any adventurous foodie. Their vanilla and crunchy honeycomb donuts will surely blow your mind.&lt;/p&gt;&lt;p&gt;Nearest Tube: London Bridge&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Riding house café&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Breakfast smoothie: Perfect kick start to the day with a selection of smoothies. Also, their breakfast dishes are also recommended such as their pancakes and their baked omelet.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Monocle Cafe&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Amazing coffee house with cozy vibes. Great coffee and their green tea matcha with strawberry cake is scrumptious, offering an amazing blend of Japanese/French taste.&lt;/p&gt;&lt;p&gt;Nearest Tube: Marylebone&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Honest burger&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best French fries: Perfectly seasoned French fries, a perfect crispy addiction that is truly the best French fries I ever tried.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Koshari Street&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best fix for Egyptian koshari: If you are craving koshari this is the place for you that will serve authentic Egyptian koshari dish.&lt;/p&gt;&lt;p&gt;Nearest Tube: Covent Garden&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Khan&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Butter chicken. Khan considered one of the most famous Indian restaurants in L town serving authentic Indian dishes, my favorite is their delicious chicken butter that is not heavy or creamy but seasoned to perfection. Their faloodah not to be missed as well.&lt;/p&gt;&lt;p&gt;Nearest Tube: Bayswater&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Imli&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Indian finger food: Nicely presented and flavorful whether you go for their pakora or their samosa injected with tamarind sauce both tasty.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford Circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Spuntino&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best blend of cheese and truffle oil. Whether you love cheese or truffle oil or are a big fan of both this dish is a must. The only thing I hate about this place is that it is very tiny with bar style seating &amp;amp; not family friendly.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Sketch&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best place for art lovers: Quirky place that is highly recommended to those who appreciate arts &amp;amp; food, divided into 3 halls and in every corner you would see something unusual #Hint the first moment you step into the place; head straight away to their toilets and be prepared to be surprised.&lt;/p&gt;&lt;p&gt;Nearest Tube: Oxford circus&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;L’eto&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best milk cake tres de leches cake. One of my favorite cafes in L town, famous for their wide variety of cakes and pastries including their honey cake. It is also a good spot for breakfast. Their window display, showcasing an array of desserts, is a famous touristic attraction.&lt;/p&gt;&lt;p&gt;Nearest Tube: Knightsbridge&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Waterside cafe&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best chillax spot. One of my favorite spots for morning coffee. This is a floating boat that I get to by taking the river taxis from Little Venice to Camden Town market passing by the Regent Canal. This provides a perfect view for tourists and perfect alternative transportation other than the tube and the taxis.&lt;/p&gt;&lt;p&gt;Nearest Tube: Warwick Avenue&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Comptoir Libanais&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best casual Lebanese eatery. Wanted to experience Lebanese food in London, food is served in a cozy and funky environment especially for breakfast or brunch. Their rose lemonade is so refreshing!&lt;/p&gt;&lt;p&gt;Nearest Tube: Bond Street&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Crispy candy&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best fried sinful chocolates. I stumbled on this place in Camden market, set in a small pink setup that serves all varieties of sinful fried chocolates that will satisfy all chocolate cravings.&lt;/p&gt;&lt;p&gt;Nearest Tube: Camden Market&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;La Pâtisserie des Rêves&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;This is for all Parisian pastries fans, La Pâtisserie des Rêves is a Paris-born spot providing contemporary irresistible Parisian patisserie, founded by Philippe Conticini &amp;amp; Thierry Teyssier. The French pastry, St. Honore, is amazing.&lt;/p&gt;&lt;p&gt;Nearest Tube: Marylebone&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Bird&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best chicken waffle burger. A fast food joint specialized in fried chicken and donuts. If you fancy a burger why don’t you go for their waffle burger? Note their chicken is halal; which is a plus.&lt;/p&gt;&lt;p&gt;Nearest Tube: Shoreditch high street&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;Nata 28&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Best tarts - Pastel de nata: Tiny famous Portuguese tarts sold in a food truck that specializes in Portuguese custard tarts- Nata. A perfect combination to have with coffee.&lt;/p&gt;&lt;p&gt;Nearest Tube: Camden&lt;/p&gt;', ''),
(17, 2, 'Speed Reading', '', ''),
(17, 1, 'Speed Reading', '&lt;p&gt;We live in an age where information is available more than any other time before, but due to our workload and busy lifestyle, we have less time to read. To solve this problem, we need a method that will help us read faster in order to absorb more information. Reading has many advantages, the first one is that it increases our&amp;nbsp; productivity; therefore, reading more will make us produce more.&lt;/p&gt;&lt;p&gt;The second advantage is that wisdom comes from knowledge; to be wise people, we need to read more. The top readers, who are around 1% of the readers, read at a speed of 1,000 wpm; on the other side, the average reader reads at a speed of 200 wpm only.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Speed reading advantages&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Speed-reading will increase your knowledge; by reading more books you will be more knowledgeable; and this will result in an increase in wisdom. You will also be more confident by the knowledge you have; you won’t need to ask people for answers anytime you face an issue. You will have more freedom because by increasing your knowledge, you will have more choices to make. You will feel more secured, and reading a lot will improve your memory. You will be able to focus better and provide more logic answers. You would have better emotions and better solutions because your knowledge would have increased.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Speed reading techniques&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Some of the techniques used in speed-reading:&lt;/p&gt;&lt;ol&gt;&lt;li&gt;Read the table of contents first: Table of contents will tell you in brief what the book is about; therefore, when you start skimming or scanning you would already have known what the book talks about.&lt;/li&gt;&lt;li&gt;Questions you need to ask: After reading each chapter, answer the following questions: What is the main idea of this chapter? What does the author wish to convey? And after finishing the book, answer the questions: What is the book primarily about? Has the author succeeded in conveying his message? How can I benefit from this book?&amp;nbsp;&lt;/li&gt;&lt;li&gt;Meta guiding: Running your finger beneath the text as you read is called meta-guiding. Meta-guiding will train your eyes through motion detection.&amp;nbsp;&lt;/li&gt;&lt;li&gt;Visualization: Making mental images of the text. Mental images help you read faster and will enable you to remember the text for longer.&amp;nbsp;&lt;/li&gt;&lt;li&gt;Keyword search: Will help you to focus on what is important.&amp;nbsp;&lt;/li&gt;&lt;li&gt;Chunk reading: Group words together to form an idea. The important thing is to make the idea clearer with fewer numbers of words.&lt;/li&gt;&lt;li&gt;Skimming: Read the first paragraph, the first lines of the rest of the paragraphs, then read the last paragraph. The main ideas are available mainly in the first and last paragraph.&lt;/li&gt;&lt;li&gt;Scanning: Similar to skimming, but you need to read through the text and discover important points.&amp;nbsp;&lt;br&gt;&lt;/li&gt;&lt;/ol&gt;&lt;p&gt;&lt;b&gt;Speed reading exercises&amp;nbsp;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;There are many exercises you can do that will increase your reading speed up to 300%. One of those exercises is: Eye writing. Focus on a wall in front of you, and magine that you are writing on the wall by moving your eyes. This exercise works out the muscles in your eyes and increases exibility of your eyeball.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Factors that affect reading&lt;/b&gt;&lt;/p&gt;&lt;p&gt;There are many things that affect your reading and the speed of your reading. If you are trying to read in a noisy environment where you can’t concentrate; you wont be able to focus or speed up your reading. Exercise will also help you focus more when you read. The food you eat is another factor that can affect you. Needless to say at this point, eating junk food will decrease your focus. Also, make sure you are having a good night’s sleep.&lt;/p&gt;&lt;p&gt;Finally, read with intention – if you meet the author, what is the one question that you would ask him or her? Think of this question while you read.&lt;/p&gt;', '');
INSERT INTO `sp_news_description` (`news_id`, `language_id`, `name`, `description`, `tag`) VALUES
(18, 1, 'Fairytales', '&lt;p&gt;Emirates Diaries sat with Fatma Al Madani and Basma Al Fahim, founders of Fairytales to know more about the space and how it came about.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;What are three words that inspired you to open fairytales?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Motherhood, learning and imagination.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;Can you tell us more about fairytales and what you’re trying to achieve?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Our focus is on imaginative play. We were very careful in selecting our staff to make sure all of them have educational backgrounds and certain characters to make sure they can work with the kids. We also focus on health and safety in our play area and encourage kids to be involved in community service to make sure we instill this habit in them.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;How do you balance between motherhood and managing&amp;nbsp;&lt;/u&gt;&lt;/b&gt;&lt;b&gt;&lt;u&gt;the space?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;We try to schedule our time in a very clever way. We try to make use of our mornings as much as we can and try to schedule things at night when our children are asleep. You also need to know your priorities because when you do that, everything falls into place.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;Were your kids involved in the setup?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;They were very involved. We used to bring them to the space before it opened and also show them sketches. The first time we brought our kids to see the space that was basically built out of their imagination, it gave us motivation to continue.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;Can people participate or is it only the kids that benefit?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;We accept talents like people who can read stories to the kids and others who can give workshops. We are also thinking of creating an internship for Education studies. We want to support our local community. It’s so important to support one another.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;How is Fairytales encouraging reading?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Our theme itself encourages children to be interested in stories and &quot;fairytales&quot;.&lt;/p&gt;&lt;p&gt;We believe in the importance if reading; we have a cozy reading corner hidden in a tree which children are very attracted to and love sitting inside. Inside the tree we have shelves stacked with children’s favourite stories in both English and Arabic. We also hold a story telling session daily to get children more interested in stories, for this we encourage volunteer readers from the community to participate as well.&lt;/p&gt;&lt;p&gt;Moreover we try to participate in community events to encourage reading both locally and internationally. Our first event was a successful children books donation campaign with UAEBBY in which donated books were given to children in need in the region, we will definitely do this again. Another was an interactive story telling activity for children with special needs in an event organised by Dubai Media’s publishing sector.&lt;/p&gt;&lt;p&gt;We are planning a fairytales themed summer camp which again will encourage the love of stories and imagination. We are always thinking of new ideas to be part of our country›s encouraging reading initiative.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;What are the mothers’ reactions when they enter the space?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;We get many positive first comments about the quality of our structures, as most of them are made from wood and handcrafted for us, the cleanliness of the space and also the brightness due to the big windows we have. One challenge we are facing is that our focus on imagination is new to the market and many parents expect to see lots of slides, trampolines and playground toys. We are training our reception staff to explain and educate parents who come to explore the place about our focus and the importance of imaginative play.&lt;/p&gt;&lt;p&gt;&lt;u&gt;&lt;b&gt;What programs do you offer for the children?&lt;/b&gt;&lt;/u&gt;&lt;/p&gt;&lt;p&gt;We have dance and language classes for the children so they could have fun and get educated at the same time. We also frequently organise themed workshops and hold camps during the holidays.&lt;br&gt;&lt;br&gt;&lt;/p&gt;', ''),
(19, 2, 'Read Your Stress Away', '', ''),
(19, 1, 'Read Your Stress Away', '&lt;p&gt;&lt;b&gt;“A great book provides escapism for me. The artistry and the creativity in a story are better than any drugs.”&lt;/b&gt; –Wentworth Miller&lt;/p&gt;&lt;p&gt;As a person who is highly susceptible to stress, I always find myself endlessly thinking of ways to reduce the mental tension I feel whether it is caused by school, to-do lists, general responsibilities or otherwise. I always find myself stressed even when I am sitting calmly and motionless in my own bed, and it is even worse when midterms or finals are days away. Stress is essentially a normal and temporary physiological reaction to various situations and events in our lives. Yes, reader, you don’t have to worry! Being under pressure is completely normal! In fact, what you should be thinking about is how to deal with the stress.&lt;/p&gt;&lt;p&gt;I would usually give my friends suggestions and methods to deal with stress; even though ironically being prone to mental tension and stress myself. From experience, I can confirm that reading is one of the things that help me cope with pressure. Apart from having a warm bubble bath, petting my cat or making myself a cup of chamomile tea, reading also helps in the reduction of stress. How so? Dear reader, when you are stressed, you are more likely to think of doing something that helps you calm down and forget about the stress; subconsciously trying to completely forget about the stressors.&lt;/p&gt;&lt;p&gt;Luckily, all methods of relaxation primarily distract you from the stress and contribute in the reduction of tension; so it is a form of escapism. Those methods do not help you directly deal with the problem that is causing you to stress, they help you escape temporarily; which is why reading itself is a form of escapism, or at least in my words. Others find escapism in reckless activities and habits that could be potentially dangerous such as smoking, drinking or “emotional” eating.&lt;/p&gt;&lt;p&gt;Reading is as, and more, efficient as other stress-relief methods, and simultaneously works to ones benefit through widening one’s vocabulary range to improve memory skills and mental health. According to University Of Sussex, reading for at least six minutes while being stressed lowers your heart rate and relieves your tensed muscles because your mind is only focusing on one thing; ignoring other stressors. It eases your tension, makes you feel in control and will definitely help you think sharply afterwards.&lt;/p&gt;&lt;p&gt;Whenever I recommend that my friends should grab a book on the go when they feel stressed or pressured, their response is somewhat always similar: “But I do not like reading.”&lt;/p&gt;&lt;p&gt;The problem here is not the disinterest in reading; it is simply not being aware of the fact that the literary world has a wide variety of genres to offer to you and I am sure, at least one, would interest you in this case. I do not perceive reading as a hobby of mine as well and I am not fully indulged in reading or call myself a “book-worm”, in fact, I am the worst reader. I have a pile of unfinished books, but that is okay because I am not obliged to have a burning passion for reading – I simply do it because I enjoy it and it helps me in many ways. You too, reader, do not have to be a “book-worm”; simply read what interests you, like when you thought about getting your hands on Emirates Diaries and started reading this article. Actually, while reading this, your heart rate probably lowered, your breathing pattern changed and you probably are two times more relaxed than you were before started reading.&lt;/p&gt;&lt;p&gt;&lt;b&gt;I can sum up this in two simple steps:&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Finding what interests you first is necessary. If you are not interested in reading books, or you are not easily entertained by how two-hundred pages of lines, you can always turn to online articles or even comics; the key is to find what genres you love and would like to extendedly read. Personally, I read articles more than I read books. I am particularly interested in reading articles from websites like Tiny Buddha and Polygon. I like to read about astronomy and Greek mythology when I have a busy to-do list. It only takes me less than ten minutes to read and those minutes help me more than I expect, every time.&lt;/p&gt;&lt;p&gt;The process of reading should be done in a quite place, away from daily distractions. If you are a student, then reading in the school library is the best option by default. If you are an employee, reading quietly in your office or in the waiting areas will suffice. To put it shortly, being in a quiet atmosphere is a massive stress reliever. You finding the right spot to read away from the stressors and distractions is important in the process of stress reduction.&lt;/p&gt;&lt;p&gt;I try linking the daily stress I experience with the reading I do in my head before actually searching online, and I was surprised to find that reading is actually linked to stress but only in a good way. I hope this article convinced you to appoint reading as one of the relaxation methods in your list and helped you in the slightest way possible; even if you use this as a way to advise a friend who is having a stressful day. Happy reading!&lt;/p&gt;', ''),
(20, 2, 'The Actual Adventure Within Us', '', ''),
(20, 1, 'The Actual Adventure Within Us', '&lt;p&gt;After a long hectic working day, we reach home desperate for a moment of reconnection with ourselves, a moment of relaxation away from all of the distractions of life. “What would be a better idea then grabbing a warm cup of tea and a book filled with adventures?” something us bookworms would say.&lt;/p&gt;&lt;p&gt;We isolate ourselves from our surrounding by moving to the quietest zone in our house in order to enjoy a chapter or two of the book of adventures. Blending in its tale to personalize ourselves to be so-called “heroes of the day”. Have you ever wondered what happens next as we read? Not in the story, but in our bodies. Here is where the real adventure begins. All sorts of emotional, cognitive and physiological changes are created in our brain as we start reading the first page of our books.&lt;/p&gt;&lt;p&gt;Here is a scenario of what happens when we read a book:&lt;/p&gt;&lt;p&gt;“It is midnight, the moon is full and he is running across the forest trying to tackle 300-feet long trees that are hindering his movement. He is desperately trying to distract and escape the assassins following him.” When we start reading, we automatically live the story to make it our reality. Our brain slowly starts living the experience of the character in which it pictures the entire scene in front of us. We are no longer in our room sitting on a couch; we are now in the forest beside the main character. This phenomenon occurs because our brains actually believes that we experience what we read, so the same neurological regions are stimulated whether we are living the thing or reading about it.&lt;/p&gt;&lt;p&gt;“He is gasping now, he lost his breath but he is trying his best to run as fast as possible but there is no way out. ‘It is a maze’ he suddenly realized.” Once we start reading about any physical activity in the story, our neurons, specifically in the left temporal cortex, start registering changes. Our brain now believes that we are running. This actually activates our sensorimotor region in the brain.&lt;/p&gt;&lt;p&gt;“He stopped running now and flashes of memory start crossing his mind while tears of fear run down his cheek. He remembered how he promised his father that he would never give up and how he will always face his fears with the sword of courage. However he is scared of death.” In moments of conflict when the character of a story is going through a situation, we as readers start to analyze his emotions and actions. We become aware of the character’s mental and psychological states in terms of his/her beliefs, intents, desires and knowledge. This deep understanding of the character helps us in our real life when we deal with people around us. We start to empathize with them and this happens because we start having a deeper understanding of our mental/psychological states in comparison to the mental/psychological states of the people we interact with, this is known as the Theory of Mind.&lt;/p&gt;&lt;p&gt;“He finally made a decision, and now he is running but this time towards the assassins with no place for regret in his mind.” Stories in general have a structure of a beginning, middle and end. Structures in stories in general are made to create a dramatic arc and they highlight the climax, or main issue, of the story for us as readers to transport into the character’s world, and this creates changes in the chemistry of our brains. Also, having structure in a story helps in increasing the capacity of attention in a reader’s brain.&lt;/p&gt;&lt;p&gt;Reading stories help improve memory, reduce anxiety and decreases the chances of getting Alzheimer. Let us read for a better mental and psychological health.&lt;/p&gt;', ''),
(21, 2, 'For the Love of Reading', '', ''),
(21, 1, 'For the Love of Reading', '&lt;p&gt;&lt;b&gt;&quot;The human mind is the center of development and the book is the tool used to renew the mind. A nation can never grow without a renewed mind and lively, knowledgeable spirit,&quot;&lt;/b&gt;– H.H Sheikh Mohammad bin Rashid&lt;/p&gt;&lt;p&gt;The first word that brought Islam to us was “read”. This is the act by which the Prophet Muhammad (peach be upon him) was ordained to spread Islam throughout the world. The significance of reading has since transcended through centuries and eras, and has evolved over various platforms. In ancient Egypt, papyrus was used by the royals to preserve their sacred texts. Writings were carved on stone, or clay tablets, in hieroglyphics for others to read. This was used to communicate messages, record great stories, or to scribe down events happening at the time. These are some of the ways that history has been preserved enabling us to understand the past.&lt;/p&gt;&lt;p&gt;The platform for the written word has gone through a number of transformations for it to reach the paperback books we enjoy today. Yet, the metamorphosis did not end there. We have Braille so the blind or visually impaired are not left behind in this vast world of knowledge. We have audio books for people on the go – you no longer have to spend that long hour commute miserable and stressed. This is something that is not only beneficial to people on the go, but also to the dyslexic who face challenges with reading.&lt;/p&gt;&lt;p&gt;Books have also evolved with technological advances of the modern world. E-books are now at the forefront of a readers’ life. Like any other change, e-books may have initially been resisted but it is difficult to abstain from it for too long since it provides convenience and ease in today’s fast-paced and modern lifestyle. I had my Kindle for years before I finally gave in and fell in love. It’s true that it cannot replace the euphoria of walking into a library or bookstore as you absorb the beauty around you and run your fingers across the rows of books, but it allows us to search through book listings, read reviews, and to have that book instantly, with just a click of a button. It allows us to carry one slim item on holiday, but not restricting to just one story.&lt;/p&gt;&lt;p&gt;It is through reading, in whatever shape or form, that we have increased our knowledge and continued our learning beyond comprehension. While books may have initially been a result of ancient societies wanting to preserve their religious and cultural texts, they have now transcended into domains far and wide. Books tell us stories of the unknown, real and unreal, of things beyond our reach. We get to learn about the world around us. Imagine a little girl in a faraway village who gets to learn about a world she may never get to see. Now imagine reading about that same little girl and getting a glimpse into her world and her thoughts.&lt;/p&gt;&lt;p&gt;By reading, we get to experience pleasures and pain that may otherwise be unknown to us. It is through Shakespeare’s sonnets that we have learned about love and loss. It is through the words of Anne Frank that we have learned about the struggles during the holocaust. While she and her family were in hiding, they passed their days and nights by reading and learning. In those dire circumstances, books became their companion. Books are not only there to enhance our learning, but to bring us joy, and to give us life.&lt;/p&gt;&lt;p&gt;That is the vision that our leaders have for the current and new generations to come. That is why H.H Sheikh Khalifa Bin Zayed Al Nahyan, UAE President, and H.H Sheikh Mohammad Bin Rashid Al Maktoum, Vice President, Prime Minister of the UAE, and Ruler of Dubai, have declared 2016 as the “Year of Reading”. For our nation to continue to flourish and prosper, we need readers. We need people with open minds who are willing to look beyond themselves and to embrace the differences of this world and to learn from one another.&lt;/p&gt;&lt;p&gt;The UAE has been developing and leading in many aspects over the last years, but when a study revealed alarming figures of how little time is spent reading in this region, something had to be done. That is where the idea for the “Year of Reading” was born. With this declaration, various initiatives are being launched across the country to re-ignite the value of reading.&lt;/p&gt;&lt;p&gt;One initiative announced by Sheikh Mohammad Bin Rashid Al Maktoum is that the largest library in the Arab world is to be built in Dubai and is expected to open its doors by the end of 2017. This library will be a hub for knowledge and appreciation of the written word, as it aims to bring together readers, writers, thinkers, and researchers. This and other initiatives being driven across the country are in an effort to ensure that our children learn to love books early on in their lives, so that it may develop their imagination and cultivate their creativity. The literary world brings with it endless possibilities and it is up to us to seize this and to spread its magic.&lt;/p&gt;', ''),
(22, 1, 'Mira Thani', '&lt;h4&gt;Tell us more about Mira Thani, her passions and what got her into the field of architecture and design?&lt;/h4&gt;&lt;p&gt;I like to refer to myself as an artist, as that is how the story unravels itself. I grew up in a family of artists and therefore creativity. Color and innovation became my siblings. I studied at a British school that devoted itself to teaching students fine arts, and as a result, I was blessed with having an art family both at home and school. This had initially set the stage for my endless explorations of art materials and so my passion for art grew, as it was the easiest yet the most complex way of expression. Through color and shape, I began to channel and explore endless options as I finally settled for one that I have always dreamed of. My first project began when I was 15 years old, it was based on buildings and structures that skyrocketed the Dubai skies. These sketches reflected hallways, rooftops, windows and I realized that I was inspired by the architecture that flourished around me. My door towards architecture began to crack open and through it I realized that this is the view that I have always been sketching. Today, I am twenty-three years of age, a 2013 graduate from Zayed University specializing in Integrated Strategic Management and currently pursuing my masters in Diplomacy and International Affairs. Many people tell me both fields are a far stretch from architecture, and I tell them it’s a bridge I’ve built between the two. Education and knowledge have always been my favorite pursuit and with passion, it is always easy to find common grounds between all fields.&lt;/p&gt;&lt;h4&gt;What is Mira Thani? What are your goals? Tell us more about your team at Mira Thani Dubai&lt;/h4&gt;&lt;p&gt;Mira Thani is an establishment that was created to address the needs of today’s generation by offering distinctive structural solutions that can be built virtually anywhere, whether its an exhibition, a conference or a pop up store. These structural solutions are extremely convenient in the fast world we live in, as most structures can be installed and removed in one day. Creating efficient and versatile spaces became my new type of art, and with every space I encounter, I strive to capture its purpose, efficiency and dynamism. This has only become a reality with the efforts and passion of an innovative team that I’m extremely proud to call my family. My goal is to become a trusted Emirati company that is regarded as a beacon for aspiring locals. I strive to help people build homes and memories, because what’s better than creating spaces catered to you? In the near future, I would like to divert my focus towards eco-friendly structures; I believe that nothing is more important than insuring the sustainability of Earth for our future generations.&lt;/p&gt;&lt;h4&gt;What kind of clients do you work with?&lt;/h4&gt;&lt;p&gt;The kind of clients I work with are everyone and anyone, because requests vary between mini and large structures. I cater to the needs of my clients individually, whether the request is to set up large structures for events like tournaments and conferences, or smaller structures that are usually requested for private homes or small pop-up stores. My services can be tailor-made to each client that approaches me.&lt;/p&gt;&lt;h4&gt;What are some obstacles you have faced when starting this company and how did you overcome them?&lt;/h4&gt;&lt;p&gt;The first problem that I came across was the fact that I did not study the field of architecture academically; I was an explorer and an admirer of the field and held no certificate to prove it. I did not let that stop me as I decided to go ahead with establishing the company regardless of my credentials, and I dove into books of architecture and interior design. I spent my time conversing with architects and I soon began to speak the language. Being an Emirati living in Dubai is my greatest blessing because I grew up witnessing everything the world said was impossible to do, and I was just following in the footsteps.&lt;/p&gt;&lt;h4&gt;Mention your favorite architectural piece in the UAE (it can be a building, location, etc). What makes you love it in particular?&lt;/h4&gt;&lt;p&gt;My absolute favorite location would have to be Al Bastakiya because it shows a clear transition of what Dubai was and what it has become. It has also honored itself with being a reputable art hub and cultural destination for locals and tourists. When I walk around the narrow passageways -which is more often than you would think- I find myself daydreaming and going back in time. I love the effect it has on me and I love how architecture can stir up so many emotions.&lt;/p&gt;&lt;h4&gt;What advice would you give to people who want to start a business in the field of architecture and design?&lt;/h4&gt;&lt;p&gt;The first thing I would say would be to believe in yourself, never let anyone tell you what you can and can not do. Whether it’s in the field of architecture or any other field, it is never impossible to discover the endless possibilities that may land your way. I graduated as a Media Communications student, and I spent so much time finding where I can fit in the field of media. But I never had to fit in, I saw that it was okay to stand out and be different, and I realized there was so much for me to discover and I enjoyed every minute of the hunt.&lt;br&gt;&lt;/p&gt;', ''),
(22, 2, '', '', ''),
(23, 1, 'Design Talk', '&lt;h4&gt;Maryam Al Suwaidi, Artist, Designer, mother of three and the creative head of “Design Talk Studio” speaks to Emirates Diaries about her experience, beginings and thoughts about today’s shifting design identity.&lt;/h4&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;h5&gt;How were your beginnings?&lt;/h5&gt;&lt;p&gt;I started my studies in the UAE and continued my bachelor’s degree abroad, eventually graduating with a master’s degree from Utah State University (2010).&lt;/p&gt;&lt;p&gt;Upon settling back home, I knew that it was going to be difficult to find the “Right” Job, which resulted in me starting out as a freelancer. Primarily, I took on small commercial projects in order to enter the market and gain experience. With two years of freelance experience under my belt I felt confident to take the next step and open up my own practice, this is when “Design Talk” was born. The studio specializes in commercial design; this is where as a creative head I feel that I could be most creative. I also enjoy designing functional art pieces; luckily I got plenty of practice while working on exhibition stands in Abu Dhabi and Dubai. My passion fro design revolves around the surrounding experience, either through creative furniture, functional art or even architecture.&lt;/p&gt;&lt;h5&gt;How would you describe your style in interior design?&lt;/h5&gt;&lt;p&gt;I don’t really have a certain style per se, but I know that I’m very passionate about the elements of Islamic designs. I tend to use geometric designs as an inspiration for many of my projects. I’m just fascinated by the complexity of geometric shapes, especially when modernized. I also love using raw materials while juxtaposing them in a contemporary setting. I guess I can say my style is a mixture of contemporary and oriental design, but at the end it all depends on the project, as each project has its limitations in design.&lt;/p&gt;&lt;h5&gt;As the creative head of an ongoing design firm, what do you think of the design boom that is currently taking over the region and the UAE in particular?&lt;/h5&gt;&lt;p&gt;I think that design is finally getting the attention that it deserves in our region and especially the UAE. I think that has to do with education and exposure to other cultures that people have these days. In my opinion, the current design boom creates amazing opportunities for young creative minds from all over the globe, giving them the chance to participate in this design revolution that’s taking over the region.&lt;/p&gt;&lt;h5&gt;What do you think are the reasons behind it?&lt;/h5&gt;&lt;p&gt;I believe globalization is one reason, where people are exposed to all kinds of design worldwide. I also think that our leaders’ vision to make this country the best in every aspect is another main reason as they support this movement. The leadership has created different platforms for young designers to get involved through exhibitions, competitions, seminars and scholarships. I believe this gives an opportunity for many your talents to be recognized for their work, while also getting others motivated and excited to get involved in the field.&lt;/p&gt;&lt;h5&gt;Do you think this design boom is taking into consideration the cultural origins of the region?&lt;/h5&gt;&lt;p&gt;At times I think it does, while at others it may not; it honestly varies and that’s fine by me. The UAE is known for its diversity, that’s why we need to have different design approaches for different projects. Simultaneously, when it comes to government entities and community based areas, I do believe that it is very important to have them designed in a culturally aware concept, as they are a representation of our identity.&lt;/p&gt;&lt;h5&gt;How does design talk or Maryam al Suwaidi as a designer face the constantly changing style shifts in design particularly with regards to cultural identity?&lt;/h5&gt;&lt;p&gt;I always keep myself updated changing interior design trends and general trends in the market, as each aspect affects the other. Of Course I always keep in mind the importance of preserving our cultural identity within design, but practically, each project has its own style and requirements. Modernizing the design is one way to of keeping cultural identity preserved, and sometimes mixing the traditional and modern elements aids in cultural preservation as well.&lt;/p&gt;&lt;h5&gt;As a designer, have you ever disagreed with a client over preserving cultural integrity in a project?&lt;/h5&gt;&lt;p&gt;Yes, That happened few times but I won’t say it’s a disagreement but rather advising and educating the client on the importance of preserving cultural integrity. Many of projects have different styles, but at the end I try to use inspiration from cultural and Islamic art elements whenever I can. Some projects have a strong concept style where it would be hard to include aspects of cultural design, but I always make sure it does not misrepresent or offend our identity.&lt;/p&gt;&lt;h5&gt;What, in your opinion is the key to preserving cultural identity in design?&lt;/h5&gt;&lt;p&gt;I think including the importance of cultural identity in our education system is one way of keeping young people interested, it stimulates them and encourages them to create designs that showcase our cultural identity in a creative way. This could be done by the concept of mixing old and new (traditional design with contemporary) or modernizing traditional designs i.e. cultural pop art. I also believe having ongoing events that focus on cultural identity in design will help raise awareness. Furthermore, having ongoing local competitions for design students will also help encourage students to be creative in the aspects of adding a touch of our cultural identity within their designs.&lt;/p&gt;&lt;h5&gt;Do you see a shift in the materials that are used in today’s interior design projects?&lt;/h5&gt;&lt;p&gt;Yes I do. New materials are always introduced in the market especially with evolving technologies. Simultaneously older materials always make it back to the design scene and are applied in new innovative ways. We always try to use new materials and experiment. Sometimes it is a challenge to convince clients to take risks with such materials, as many of the clients like to play it safe. I like to make a statement or take risks with design, as I believe that unusual designs are the ones that are always remembered.&lt;/p&gt;&lt;h5&gt;What message does Maryam al Suwaidi wish to convey when it comes to cultural and design?&lt;/h5&gt;&lt;p&gt;When it comes to design and culture, I always like to bring in elements that represent us in design. I feel that the most beautiful designs are ones that tell the story or history of a particular region. Design is a universal language; to me it’s important to convey culture through the art of design. I’ve done two years of research at grad school about how to shape our cultural and Islamic designs in the modern day. I feel that I have this huge energy and enthusiasm of applying this knowledge and love for our culture and Islamic art into my designs, whether in interiors, architecture, or it’s overall elements.&lt;/p&gt;&lt;h5&gt;What are your future goals for design talk?&lt;/h5&gt;&lt;p&gt;When I started design talk, I had a plan of not only designing spaces for people and events, but also being part of the community where we can share our love of design with people through participating in events, humanitarian work that relates to interiors and architecture. Furthermore we aim to create concepts and produce products. I have offered internship programs and given seminars to design students as a way of participating in the community, and I still intend on working hard in order to reach my goals of making design talk a space for creativity, innovation, experimenting with product design, and lastly, handling bigger projects.&lt;br&gt;&lt;/p&gt;', ''),
(11, 2, 'Alezan by SK', '', ''),
(24, 1, 'Qafiya', '&lt;h5&gt;’Qafiya’ - tell us about the name and the concept.&lt;/h5&gt;&lt;p&gt;The literal translation of the word Qafiya is ‘rhythm’. Poetry plays an important role in both Emirati and regional culture, as it is one of the main ways we document our history. Qafiya in poetry is a rule, or a grammatical guideline, where every line has to end with the same two letters. What that creates, we say in Arabic, is balance and rhythm within the poem - the poem itself is not accepted if it does not have Qafiya. Just imagine yourself reading a novel with grammar mistakes in it - that is how a poem sounds without Qafiya. We wanted to translate that meaning to our brand and concept. Qafiya is designed to add rhythm to your lifestyle, whether you work in media or as a sportsman or a musician. Everyone has their own lifestyle but Qafiya adds rhythm and balance to it.&lt;/p&gt;&lt;h5&gt;What made you think of such a concept? What inspired you to do so?&lt;/h5&gt;&lt;p&gt;The fragrances have ingredients that you can relate to an Emirati’s daily life; coffee, saffron, cardamom, musk and amber. These are all scents you can smell in every Emirati house in the bukhoor and perfumes we use. Both fragrances have a unique mixture that you can recognise but you can’t place anywhere else. We didn’t want Qafiya to be a typical fragrance where one would shout saffron and another would shout cardamom – we wanted to create something that people’s subconscious recognizes, but that is also new to them. When it came to differentiating between the two fragrances, we decided to use Arabic numbers, as they are not commonly used, one for day and one for night. 01 is the lighter fragrance for day use whereas 02 has a stronger scent, which most people would prefer to use in the evening. But it depends on your taste; I know people that use 02 for the day because they prefer it.&lt;/p&gt;&lt;h5&gt;Can you talk us through your love of perfumery? Did you have a passion towards it from the start?&lt;/h5&gt;&lt;p&gt;Perfumes have always been part of our life and culture, we all grew up smelling perfumes and mixing them at home, that’s how my passion started. I have been working with the Ajmal group for the past 4 years and we have since developed a strong relationship, along with a group of my friends. It was through this relationship with the Ajmal group and their management that started it all. Almost 2 years ago, I approached them with a proposal that ended up being the base of the Qafiya concept.&lt;/p&gt;&lt;h5&gt;How did the design of Qafiya come about?&lt;/h5&gt;&lt;p&gt;The design of Qafiya started with developing a whole new modern, yet Emirati-reflected brand identity. The logo of the brand was crafted by blending the first initials of the name in an aesthetic manner. This approach was intentionally employed to blend both the Arabic and the English cultures in one harmonized logo with a fine line that differentiates yet clearly connects both initials. The overall design was meant to bring together the nostalgic beat of ancient Arabic lore and love of perfumes with the English passion for quality and uniquely designed beauty products. It also represents the main concept of the brand, which is the idea of rhythms coming together with each line signifying sheer beauty and harmony.&lt;/p&gt;&lt;p&gt;We then moved from designing the brand identity to designing the perfume’s primary and secondary packaging. In product design, packaging becomes as important as the product itself. The main focus was incorporating raw, rough wood into the design to add a sensory appeal to the product. Different wooden tones were used in order to differentiate between the two scents, day and night. The dark one represents the strong scent, while the light one represents the light scent. Additionally, Arabic numerals were added to each perfume to add a name to both scents. The dhow, an integral symbol of our culture and heritage, inspired the overall shape.&lt;/p&gt;&lt;p&gt;The secondary packaging of the perfume was made using recycled materials to emphasise on the natural content of the perfume. Consumers are currently aware of the state of our environment and are getting smarter by choosing environmentally friendly packaging, this was also considered while designing the product packaging. A fine golden metallic line was added to the packaging to give a sense of luxury and elegance, further shaping it as a premium value item. Last but not least, the most important factor of the brand, the brand concept/story, was added to the backside of the package in order to make it visible and for it to act as a meaningful element of the design.&lt;/p&gt;&lt;h5&gt;When did you know the packaging and design was right? How many other choices did you have?&lt;/h5&gt;&lt;p&gt;Countless sketches and options were designed and discussed during the process. It was the most time consuming stage yet the most interesting part as we got to see results and things coming to life! Different insights and feedback were gathered during the process in almost every stage through meetings with an Emirati panel of friends and supporters. Those discussions were very helpful in the development process of the product as everything was being put into consideration. We knew that everything was right when the production took place; we got to see things coming together and starting to make sense. In the end, when the product was displayed to the public, we received plenty of good feedback and realised that all the effort was worth it.&lt;/p&gt;&lt;h5&gt;Who are Qafiya’s main target audience?&lt;/h5&gt;&lt;p&gt;It is basically for everyone, especially young, modern and passionate Khaleejis who love evolving concepts and can share the journey with us to be part of a movement.&lt;/p&gt;&lt;h5&gt;Where can people get Qafiya? And why should they?&lt;/h5&gt;&lt;p&gt;All Ajmal’s outlets in the UAE, Bahrain, Qatar &amp;amp; Kuwait. The best answer to why is that people should be going with what makes them feel good. The beautiful thing about Qafiya is that you can use 01 or 02 separately or you can use the both of them as a mixture. People love that because both of them have different fragrances so you can layer them to create a fragrance just for you.&lt;br&gt;&lt;/p&gt;', ''),
(25, 1, 'Mohammed Almusabi', '&lt;h5 style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, sans-serif; color: rgb(0, 0, 0);&quot;&gt;An Emirati photographer and a member of the Emirates Falcons Photography Club.&lt;/h5&gt;&lt;p style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, sans-serif; color: rgb(0, 0, 0);&quot;&gt;&lt;br&gt;&lt;/p&gt;&lt;h6 style=&quot;&quot;&gt;&lt;u&gt;What got you into photography?&lt;/u&gt;&lt;/h6&gt;&lt;p style=&quot;&quot;&gt;I started photography in 2011 using a digital camera. I used to capture anything I saw until I gradually started learning how to shoot in manual mode. Later on, I got a professional camera and further developed my skills by attending several workshops and reading photography books.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What do you like taking pictures of?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;Nowadays I mostly enjoy capturing cityscapes and architecture to show the beauty of towers as well as mosques of the UAE.&lt;br&gt;&lt;/p&gt;', ''),
(26, 1, 'Fashion Spotlight', '&lt;p&gt;Many Emirati individuals have been entering the fashion industry and have proved their presence over the years. However, there are a few that truly grab the attention of the public, and make a statement of their own. One of these brands is ‘By Hamda AlFalasi’ which has been founded by Hamda AlFalasi from Dubai just a few months ago.&lt;/p&gt;&lt;p&gt;Hamda is a 23-year-old fashion designer and artist, who enjoys reading, writing, design, and art. Hamda is also a graduate from Dubai Women’s College and has a bachelor’s degree in Applied Communications. She states that her degree helped her in starting her brand, since the whole concept started when she started working on her capstone project.To find out more about this talented designer we had the opportunity to interview her and learn more about her journey in the fashion industry.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;Tell us more about your brand By Hamda AlFalasi:&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;I wanted to create a brand that was easy to wear but still fashionable. I created my brand as an experimental line and started off with seven looks; I wanted to learn the fashion process from start to finish, from an idea to a rough sketch to becoming an actual piece. The brand started as my senior project and I debuted my work with a capsule collection. My latest collection was released this FW16 season (Spring Came Late).&lt;/p&gt;&lt;p&gt;Currently, the brand\'s main focus is building a strong image which depends heavily on modernizing and elevating the traditional concept of &quot;casual chic&quot; with aims to dress and inspire all women, regardless of nationality.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What motivated you to enter the fashion industry?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;I always knew I wanted to create. Also, I always knew it had to be fashion. I\'ve been interested in fashion ever since I was a child. I was especially drawn to the constant evolution of fashion: How it changes, elevates...etc. When I was younger, I would always watch old fashion runway shows with my mother, that also heavily influenced my decision to work in fashion although I was encouraged only recently to enter the industry.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What are some of your fashion goals?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;I would love to present my collection abroad and expand my consumer base and reach a wider audience. I would love to dress women from all nationalities and different cultures, and not just women from my region. I want my pieces to be available in high-end department stores and I would love to see them in major fashion capitals. I also aim to open my own fashion design studio and turn that into my full time job.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What does fashion mean to Hamda AlFalasi?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;For me fashion is everywhere. From every object I see, I can draw or create a look. It is a crucial part of our day-to-day life and it inspires me all the time.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;Who are your designs made for?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;I don\'t design for a specific nationality or age group. I design for every woman because I believe that every woman deserves to be well dressed.&lt;/p&gt;&lt;p&gt;I dream of seeing my label on a global scale; dressing women from every part of the world is my goal.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;Where do you get the inspiration for your designs?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;Truly everything inspires me but I draw most of my inspiration from different art works. Any type of structure inspires me, especially in terms of aesthetics of the piece i\'m creating.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;Do you believe in following fashion trends, or developing your own personal style?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;I believe a designer must create, so personal style is more important than following fashion trends, but I still take what I like from existing trends and make them my own. In every look I create, I like to embed my own personal touch, while keeping things current and trendy.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What are some of the obstacles that you have faced?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;Time; because I have a day job, it becomes difficult to find the right balance between my job and my passion. For the entire process of creating a line, it takes at least three months. From research to sketching to sampling and finally producing.&lt;/p&gt;&lt;p&gt;There are several obstacles in the middle of the creation process. Some fabrics may not work for certain looks and production can take longer than your actual deadline. It\'s all about time management, staying focused and chasing your passion.&lt;/p&gt;&lt;p&gt;You can find out more about this talented designer on her&lt;u&gt;&lt;i&gt; instagram @By.HamdaAlFalasi&lt;/i&gt;&lt;/u&gt;&lt;/p&gt;', ''),
(26, 2, '', '', ''),
(13, 2, 'HIPA', '', ''),
(27, 1, 'I Have This Thing With Tiles', '&lt;p&gt;&quot;I\'m walking on stars&quot;. It all started on a cool December afternoon, as I walked into that hotel lobby. A series of black, white and grey colors that worked together so wonderfully. A perfect harmony of unparalleled synchronicity, a sea of mirrored geometric shapes that blanketed its surroundings and engulfed the beholder into a vast kaleidoscope.&lt;/p&gt;&lt;p&gt;To clear things up, it was the magic of the tile floor that captivated me. I could go and give a whole account of the history of tiles and the ancient civilizations that used them, however that would just turn into another endless article. My purpose here today is to try and understand why these tiles caught my eye and discover why tiles are so popular. I obviously cannot speak for you as a reader, however, I would like to take you through my scattered clouds of thought and share with you some of my conclusions.&lt;/p&gt;&lt;p&gt;Once again, I have often wondered why are tiles so popular? Is it purely for aesthetic reasons or practical ones? Do the patterns on these tiles speak a hidden story that the artist/creator tried to convey? One would think that I have more important things to do than think about tiles but I honestly have to confess that I have this random &quot;thing&quot; with tiles. &lt;/p&gt;&lt;p&gt;One of my many observations is that tiles are a unit of something greater, much like humans are a part of this never ending galaxy. There is a physical reality about tiles that touches upon the internal human instinct of being part of a unit, group, or community. The individual unit depends on the surrounding units in order to create an even bigger unit and so goes the cycle. This cycle does not simply depend on things created by humans, but how humans are created as well!&lt;/p&gt;&lt;p&gt;Now let us look at the popularity of tiles from a slightly different angle. Their popularity may not stem from the actual physical tile but rather the creative freedom that they offer. Tiles come in an array of endless patterns and colors and do not necessarily have to be identical in shape or pattern, as their beauty comes from the chaotic order that they create; order from chaos and chaos from order.&lt;/p&gt;&lt;p&gt;I have noticed that most of the successful and captivating patterns (to me anyhow) are, more often than not, made up of geometric shapes that boast the absence of color (black) and the reflection of light (white). The use of black and white draws attention to the actual geometric form creating a contrast that helps us clearly identify the relationship between the geometric form and the unit. Finally, let us not forget, black and white go well with anything and everything.&lt;/p&gt;&lt;p&gt;One final observation that I feel compelled to share with you is the ease and multi-functionality of tiles. On a very recent trip to London, I came across a soap shop that used hexagonal tiles right alongside hardwood flooring which created an amazing juxtaposition of materials. Running across this shop made me remember how many mosques in our lovely Emirates use tile right alongside an array of brightly colored carpets. I\'m also sure that there are many more examples that you as readers could think of and go &quot;Aha&quot;.&lt;/p&gt;&lt;p&gt;Whether they be black or white, colorful or geometric, tiles are most certainly captivating; an abundance of them is sure to transport our imagination worlds away. Not every one may look at tiles the way I do, however I hope that my thoughts have intrigued you to pay closer attention to such a popular but ironically overlooked design feature in most of our inhabited spaces. So remember this: the next time you walk into a space, keep your head down for just a second and appreciate that you are walking on stars.&lt;/p&gt;', '');
INSERT INTO `sp_news_description` (`news_id`, `language_id`, `name`, `description`, `tag`) VALUES
(28, 1, 'Louvre Abu Dhabi', '&lt;p&gt;Louvre Abu Dhabi will be the first universal museum in the Middle East, where it will act as a bridge between the East and the West. The design signifies the simplicity of a medina and the grandeur of a palace. It is a place where all civilizations melt into one, with the aim of demonstrating exchanges and similarities arising from shared human experiences as well as educating art at a global meaning.&lt;/p&gt;&lt;p&gt;The Louvre Abu Dhabi Student Ambassador Programme targeted university students as a link between the museum and the people. As ambassadors, we represent the museum and will be part of the journey of the opening of the museum. This experience has taught me about the museum, its components, how to manage the collection and the importance of preserving it.&lt;/p&gt;&lt;p&gt;We were trained through scientific and practical workshops that took place in Manarat Al Saadiyat, and our leadership skills were enriched in order to raise awareness about opening a universal museum in the heart of Arabian Gulf region in particular, and the Arab world in general. As a history and archaeology graduate from UAEU, the programme positively impacted my studies and made me realize how much I want to be involved in the museum field.&lt;/p&gt;&lt;p&gt;Construction of the iconic building, designed by Pritzker-Prize winning architect Jean Nouvel, started in January 2013. Nouvel stated ‘I wanted this building to mirror a protected territory that belongs to the Arab world and this geography.’ The design was inspired by Arabic architectural designs, such as the medina-like buildings making up the galleries while the dome’s intricate pattern resembles the overlapping of palm trees. The programme has given me the opportunity to visit Louvre Abu Dhabi’s construction site, located in Saadiyat Island.&lt;/p&gt;&lt;p&gt;Positioned between land and sea, Louvre Abu Dhabi’s architecture is one of a kind where one often stops to marvel at its beauty. The dome is a complex piece of artwork that is made up of 7,850 star-shaped pieces of stainless steel and aluminium; rays of light must penetrate eight layers before disappearing and appearing, which results in a mesmerising ‘Rain of Light’ effect. The total weight of the dome is 7,500 tones weighing almost as much as the Eiffel Tower. It provides shade during the day and an ‘oasis of light under a spangled dome’. Four permanent piers are hidden within the museum’s buildings and support the dome, in order to give an impression that it is floating.&lt;/p&gt;&lt;p&gt;The museum design is a combination of modern construction techniques and traditional culture. There will be an underground service tunnel that will connect Louvre Abu Dhabi, Zayed National Museum and Guggenheim Abu Dhabi in the Saadiyat Cultural District. Once open, the calm museum environment will encourage the museum visitor to enjoy the ever-changing relationship between the sea, dome, sun, buildings and land.&lt;/p&gt;&lt;p&gt;The interior exhibition spaces will be home to the museum’s permanent galleries, temporary exhibitions and the Children’s Museum. The permanent galleries will be the key element of the museum, as they will create the universal narrative through its 12 sequences, ranging from prehistory to our modern times. Currently, the permanent collection is around 600 pieces. In addition, 300 art works will be loaned from 13 French partner museums. The temporary exhibitions are dedicated to rotating exhibitions with new subjects and themes for the regular visitor to explore. The Children’s Museum is a 2-story building that will be adapted to children; with exhibitions linked to the school curriculums and it will include activities and workshops.&lt;/p&gt;&lt;p&gt;During my visit to the construction site earlier this January, the beauty of the structure captivated me. We walked into the museum until we arrived under the dome’s shade and witnessed the ‘Rain of Light’ in person, the shadows twirled on the floor and on the buildings. We arrived in the open plaza and the sea breeze welcomed us, the Arabian Gulf was snaking into the museum only to be stopped by the temporary sea protection walls that have been removed later this year. Thus, achieving Jean Nouvel’s ‘museum city’ floating on the Arabian Gulf. It was a quick visit, yet it marks the solidification of all information regarding the museum in our memories.&lt;/p&gt;', ''),
(28, 2, '', '', ''),
(29, 1, 'Dubai Opera', '&lt;p&gt;On the 31st of August, the heart of The Opera District pulsated with life for the opening night of its newest attraction, the stunning Dubai Opera. While it has merely been a few months since its opening, the venue quickly became a cultural and artistic beacon, joining the ranks of others such as Louvre and Guggenheim.&lt;/p&gt;&lt;p&gt;Designed by Atkins, spearheaded by design director Janus Rostock, the conception of the building began with the notion of creating a vibrant arts complex that not only showcases a vast array of cultural events, but also transmits its energy to the surrounding community, while simultaneously capturing the essence of Arabian heritage in the final design.&lt;/p&gt;&lt;p&gt;The building has been designed with the ability to shape-shift into three different configurations that enables the opera house to host a wide variety of performances and events; theater mode, concert hall mode and into a flat-floor mode. In theater mode, the room is suitable for large-scale drama productions, musicals, ballets and conferences that seat up to 2,000 people. Meanwhile in concert mode, the room transforms into the heart of an acoustic shell made out of several towers and reflectors on stage and overhead to produce an impeccable musical environment. Lastly, flat-floor mode allows the room to host events such as weddings, receptions, art galleries and trade shows.&lt;/p&gt;&lt;p&gt;Dubai Opera is located in The Opera District, on Sheikh Mohammed Bin Rashid Boulevard in Downtown Dubai. Naturally, to successfully transmit the energy and excitement to the whole community, the design of the building was made to complement its surroundings, rather than compete with it, spreading its cultural function from the inner transformable theater to an external multifunctional urban plaza, towards the alleyways and walkways of adjacent neighborhoods in a smooth manner. The addition of the opera house adds a new artistic dimension to the circle of attractions located within its vicinity, such as the Dubai Mall and the Burj Khalifa.&lt;/p&gt;&lt;p&gt;The exterior design of the opera house is an ode to the traditional dhow and Dubai’s maritime history, wherein the ‘bow’ of the building contains the main stage, orchestra and seating, while the ‘hull’ features the waiting area for guests, transportation drop-offs and parking amenities. Its façade comprises of 1,710 individual facades and mullion sections, and 1,270 individually sized glass panels, which are shaded by the roof overhang and mounted shading louvers; the goal of which is to make the building as transparent as possible, while keeping the solar radiation to a minimum. Lighting is integrated in a way that creates the sense of a lantern that offers a warm glow to onlookers during the evenings.&lt;/p&gt;&lt;p&gt;A prominent centerpiece of the interior of Dubai Opera is the central chandelier composed of 2,900 LED lights that weigh 5,000 kg, located in the main atrium and spirals three stories down. This dazzling work of art, titled Symphony, is created by Libor Sostak, a designer of bespoke glass and light sculptures, who took the inspiration for the abstract sculpture from the UAE’s traditional pearl-diving heritage. Elements of the sea and pearls can be seen in the strings of individually crafted glass pearls, each of which have been injected with tiny bubbles of air to enhance their refractive potential as it moves in the sculpture. Symphony itself somewhat takes the shape of a suspended gymnastic ribbon mid-twirl, as though mimicking the dynamic flow of water. The thousands of LED lights that are individually programmed to move only add to the effect of flowing dynamism. This beautiful installation took nearly two years to create.&lt;/p&gt;&lt;p&gt;The opening of this exquisite architectural complex marks a new beginning of molding the artistic landscape of Dubai. It is truly a privilege to witness the upwards growth of the wonder that is the emirate’s cultural transformation.&lt;/p&gt;', ''),
(18, 2, 'Fairytales', '', ''),
(30, 1, 'Dubai Water Canal', '&lt;p&gt;The Dubai Water Canal was a vision birthed from the mind of Sheikh Rashid bin Saeed Al Maktoum in 1959 to facilitate marine navigations and began with the deepening of the Dubai Creek. His vision came full circle when the Dubai Water Canal project was unveiled in 2013. The construction took nearly three years to complete and, on November 9th of this year, was officially inaugurated by Sheikh Mohammed bin Rashid Al Maktoum; a vision carried on from a father to a son and from one generation to another.&lt;/p&gt;&lt;p&gt;It extends the Dubai Creek from the Business Bay district to the Arabian Gulf, cutting through Safa Park and Jumeirah to culminate in a 3-kilometer long waterway. This vast project was split into five phases of construction: the first phase was the partial elevation of Sheikh Zayed Road, which runs eight lanes in each direction and will be a kilometer long, to create a flyover that can carry traffic over the waterway up to 8.5 meters in height. Phenomenally, this phase only took approximately 6 months to complete and officially open; the second phase was the construction of several bridges on Al Wasal Road and Jumeirah Beach roads to facilitate the crossing of 8.5-meter tall yachts through these areas of the canal;&lt;/p&gt;&lt;p&gt;the third phase was the digging of the canal itself and includes the construction of three pedestrian bridges, 10 marine transit stations, and a manmade peninsula along Jumeirah Park; the fourth phase covered the construction of the area around the canal and its infrastructure; the fifth phase saw the completion of the earthworks of the canal and the commencing of facilitating the waterworks.&lt;/p&gt;&lt;p&gt;The Dubai Water Canal is not only a massive undertaking, but it is also an architectural treasure chest. Several examples of distinct and graceful designs are dotted along the canal, one example of this are the pedestrian bridges, each of which present with a singular aesthetic specification; for there is a metal bridge suspended by metal cables to lift its floor, another bridge has a S-shaped curved floor suspended by an oval arch, from which metal cables hang to lift the floor of the bridge, and is supported by great, concrete pillars on both sides of the canal and the third bridge comes in the form of a twisted, steel bracket built on steel columns on both sides of the canal. Feats of engineering and creative lighting also come into play with the waterfall of the canal bridge on Sheikh Zayed Road. Another beautiful example lies in the numerous marine stations located around the canal and Business Bay. Sheikh Mohammed handpicked the designs as they matched the local architecture in the region and show stunning crystalline patterns on the shade atop every station.&lt;/p&gt;&lt;p&gt;If we were to widen our scope to include the surroundings of the Dubai Water Canal, we would see that it is equally vast and incredible. Various property developments will run along the length of the canal with 5,345 residential units planned, as well as 948 hotel rooms. The areas will be called Gate Towers, Jumeirah and Peninsula. There will be four residential towers linked with Safa Park, as well as 211 residential units right on the water.&lt;/p&gt;&lt;p&gt;The closed Jumeirah Beach Park will reopen its sandy gates again and has been extended by one kilometer to run over the canal’s peninsula, which will be home to 60 marinas, 1,817 residential units, 957 five star hotel rooms and 347 retails outlets and restaurants. The Gate Towers bridge at the beginning of the canal will feature a three-storey mall built over the canal and spans 300,000 square meters. It will have 434 retail outlets, restaurants and a rooftop green park.&lt;/p&gt;&lt;p&gt;All in all, the Dubai Water Canal and its surrounding areas conduct a harmonious connectivity to a diverse set of infrastructures. With the connection of the Dubai Creek to the Arabian Gulf, an umbilical link has been reattached; a long awaited adjoining of the creek with its mother gulf. In the words of Sheikh Mohammed, “The Dubai Water Canal is not just an architectural accomplishment, but also a triumph of our heritage, history and heart.”&lt;/p&gt;', ''),
(31, 1, 'Ana Gow Running', '&lt;h6&gt;&lt;u&gt;Tell us more about the Ana Gow Running initiative. How did it come into being?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;We believe running should be a fun social activity shared with others. Whether you’re running to compete, lose weight, relieve stress, or to kill time, you’ll find that Ana Gow Running training sessions provide all that and more. In late 2014, a group of ladies from different athletic backgrounds met through a small running group and decided to establish their own running club. The founders are Salha Al Basti, Amna Al Marri, Maryam Al Mazrooei, Mira Achkar and Abeer Al Khaja&lt;/p&gt;&lt;p&gt;At the time of the initiation, we were enrolled in a running challenge for ladies and were under the impression that it’s a closed area exclusively for women. Surprisingly we arrived wearing t-shirts and abayas only to discover later that men from the media were covering up the event. We spoke to the lady in charge and she asked us to lift our abayas and run; it was a humiliating statement to make so we decided not to run at all and were very furious. Ever since then, Dubai Ladies Club provided us with their facilities to use free of charge for running purposes.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What does the Ana Gow initiative aim to achieve? What do you offer?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;Ana Gow Running aims towards having a fitter and healthier community of runners and athletes amongst the ladies in the UAE. We offer ladies a chance to run freely in a comfortable environment by hosting sessions at Dubai Ladies Club’s private outdoor track. Workouts are programmed in a way that is suitable for beginners and advanced runners alike.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What are some of the club activities?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;We offer a wide range of activities such as challenges, talks, events, weekly running sessions, freestyle run, and ‘mommy &amp;amp; kids’. The Ana Gow Running talks take place prior to a running session covering topics such as competing, nutrition, and motivation.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What are some of the recent activities the Ana Gow initiative has organized?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;The most recent activity organized by Ana Gow Running was the partner Ana Gow Beach Challenge with more than 25 teams participating in the 2 day challenge.&lt;/p&gt;&lt;p&gt;In Aug 2016, Ana Gow Running organized the Run for Khalifa Campaign to show our support for Khalifa, a former athlete who is currently battling cancer. More than 60 participants attended.&lt;/p&gt;&lt;p&gt;Ana Gow Running also participated in the festivities of Lorna Jane’s Active Nation Day event held at Dubai Ladies Club in September. The team celebrated being active with the female residents of the UAE.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;Any future plans for the Ana Gow Running initiative?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;We hope to continue hosting challenges and encourage women to participate and lead a more active lifestyle. We also aspire to become a leader amongst the UAE’s running clubs.&lt;/p&gt;&lt;p&gt;&lt;i&gt;Instagram: Anagowrunning&lt;/i&gt;&lt;/p&gt;&lt;p&gt;&lt;i&gt;Email: Anagowrunning@gmail.com&lt;/i&gt;&lt;/p&gt;', ''),
(32, 1, 'The Parkour Enthusiast: Amal Murad', '&lt;h6&gt;Nowadays, most of the youth go to gyms and have a passion to train, lead a healthy lifestyle, and get the perfect body. However, some are more passionate than others when it comes to exercising, and decide to become an instructor rather than just attend classes offered at the gym.&lt;br&gt;Emirates Diaries got to interview one of their very own previous members who today teaches an all ladies parkour class at Gravity.&lt;/h6&gt;&lt;h6&gt;&lt;u&gt;Please introduce yourself:&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;I am Amal Murad, a 24 year old Emirati woman who has a Bachelors Degree in Multimedia Design from the American University of Sharjah. I graduated three years ago and worked as a freelancer for a year and a half then ended up finding a full-time job. Ironically, I am one of the designers that rebranded Emirates Diaries and helped create their new logo. However, things have changed since then and I am now the first Emirati woman who teaches all-ladies parkour classes. Since childhood and my early teen years, I have always been interested in sports such as basketball, running and volleyball. Once I graduated I realized that especially as a graphic designer, I had less and less time being active in my life. I would spend long hours behind a computer and by the end of the day I was too drained to do anything else.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What is Parkour?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;Parkour, by definition, is going from point A to point B in the quickest and most efficient manner while overcoming obstacles. Some might describe it as “using the city as your playground”. In reality, you are working on overcoming your mental obstacles more than the physical ones in front of you. Parkour includes running, jumping, climbing, swinging, vaulting, rolling and many more movements depending on the situation and environment you are in. It’s about viewing your surroundings in a new way, allowing your body to use your environment’s features to navigate your way across with no equipments; just using your mere imagination and the strength of your body.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;Could you share with us the story on how your journey started with Parkour?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;Like I said, once I began my career as a graphic designer, I did not feel like I was being active enough. And I feel when you are not being active, you automatically start feeling weak and somewhat depressed. I truly believe that you do not have to limit yourself to what you have studied in university. To me, the biggest struggle at first was convincing myself that I did not want to do graphic design for the rest of my life... That who I am and what I do as a living were two different things. Yes, I went to one of the toughest universities in the region but what I studied was not my passion. I did not realize that until I started practicing as a professional after university. Surprisingly, I was actually a great graphic designer and was getting clients at that time, but I did not enjoy it. So I slowly started searching for new sports to try, something that can get my mind off things and allow me to become a healthier and a happier person again. As a child, I have always been known to be the “monkey” of the family. I was that clumsy girl who jumped around a lot, the girl who never stayed put. I always wanted to try parkour but there weren’t any all-ladies classes in the whole region. When I started telling people about the sport, they told me to try gymnastics instead since it was the more “feminine” sport. That didn’t change my mind, I wanted to try this specific sport and convinced my parents that the gym my cousin, Yousif AlGurg, owned provided these classes. My parents agreed as long as I respected myself, stayed conservative, and did not forget my values. After starting the classes, I can see how much I have progressed and enjoyed the sport and I wanted women to have the chance to try something new as well. This is when I made a deal with my cousin to give me a year. A year to get as good as I can to start teaching my very own all-ladies classes. and *spoiler alert*, I did exactly that!&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What encouraged you to become a Parkour instructor?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;As children, we are fearless beings. We jump, we fall, we get up and try again. However, as adults, we are somewhat taught to be afraid. It’s like we are too scared to even try new things because we are too scared that we would fail. In Parkour, I have overcome that fear. To be honest, I have bruises I consider battle scars all over my knees to prove to myself not how many times I have fallen but how many times I was brave enough to try regardless of that risk. I wanted women to know how that felt and to trust themselves to be able to get up again.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;Were there any challenges you faced when you decided to become a coach?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;Definitely. Firstly, it was a hard decision to not pursue the major I have a degree in. Secondly, parkour was not a well-known sport in the region especially to women. I hoped that through my videos on Instagram, women would start seeing how much fun I was having and would be encouraged to try the sport. Also, I felt that people mistook what I was doing for a “hobby” or a “phase”. It was very difficult to explain to people that I wanted this to be my career and my life. It was up to me to show everyone how serious I was and how much I wanted this to become a movement, to make women feel empowered. I can’t say I am there yet, but I am getting there. In general, people are often threatened by what is not familiar to them and what I was doing was very new. I had to understand that not everyone would accept what I was doing but as long as I had the support of my family and friends, nothing else mattered.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;Who motivates you?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;Being surrounded by strong women such as my mother and sister. My mother is an entrepreneur who owns several businesses and has really been my motivation since childhood. She has shown me what being a strong individual is regardless of the circumstances. My sister, on the other hand, was my driving force to enter sports. She was in the national basketball team and I could see just how passionate she was about her sport. Every game she laid her heart on the court and I wanted that feeling, the feeling of being so connected to yourself in that moment. Other than that, the best motivation I have gotten as well is the positive comments and feedback I have been getting from society. I can see people reacting to my videos and children actually wanting to be like me when they grow up. That is the best compliment anyone could ever ask for.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;Who would you encourage to come train Parkour?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;Everyone! It doesn’t matter how old you are or what level of fitness you have. I genuinely believe that we all underestimate just how strong we are and we can never know until we challenge ourselves to try new things.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What is the best part of being an instructor?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;Seeing people start trusting their abilities and watching them grow as individuals not just as athletes.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;Your motto in life:&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;It’s cheesy, I made up a quote when I first started teaching parkour haha! “Falling for the things you love is always scary, except when you learn how to land.”&lt;/p&gt;&lt;h6&gt;&lt;u&gt;A memorable moment you will never forget:&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;I once made my kids parkour class wear their favorite superhero costume for a day. They were so excited. Moments like these are priceless.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What have you learnt from your clients?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;I learned that each person is different and each person grows at a different pace and in a different way. I wouldn’t be the coach I am today if it weren’t for the people I have met and got the privilege of teaching them. I have learned to see how people are strong in different ways and in order to truly understand them, we must not compare our strengths to others but rather understand that we are special in every way.&lt;/p&gt;&lt;h6&gt;&lt;u&gt;What are some of your future plans?&lt;/u&gt;&lt;/h6&gt;&lt;p&gt;A lot of things coming ahead, let’s keep it as a surprise. Keep an eye out!&lt;/p&gt;', ''),
(32, 2, '', '', ''),
(33, 1, 'Are Computers Smarter Than Humans', '&lt;p&gt;Artificial Intelligence might convince us that current technologies are much smarter than we are, since it is a group of programs that enable a computer device to act like humans. A.I. can make a computerized robot act like a nanny for a child, a surgeon for a surgery, or languages teacher in universities. A.I. is also used in vehicles to allow them to solve driving mistakes like re-aligning the car using line indicators. Another example is the handicap assistant that allows handicapped people to move cursors and type words using something as simple as the eye movement and voice commands.&lt;/p&gt;&lt;p&gt;Considering the examples above are just the beginning of a new technology era that might replace humans with robots in the future, BUT! What is more important to consider is what is hiding behind this robot machine, computer. By considering this we can break it down into simpler pieces, we can change our point of view about how computers work. The computer was invented to “help” humans in giant calculations and data storing, but it didn’t and will never replace the human’s brain as it is not applicable to do that, and here is why:&lt;/p&gt;&lt;p&gt;It was discovered by Professor Pyotr Anokhin (a student of Professor Pavlov, the founder of Neurophysiology) that a human brain can receive a number of calls from body cells that exceeds the number of all kind of communications around the earth, and guess what, that happens every second! Then the brain decides whom it should answer and whom to ignore, so it has a priority system just like the computer when it decide between popping-up a virus warning or continue playing a video, unlike the brain which can decide between trillions of tasks.&lt;/p&gt;&lt;p&gt;Besides multitasking, Anokhin also stated that the capacity of the human brain can save up to 3,000,000,000 information notebook without taking it to the IT technician to upgrade it. After knowing that we would say why don’t we remember whatever we saved in our brains? The reason is the huge flow data related to real life, while if our life styles has changed we would be able to recall all those data more compatibly.&lt;/p&gt;&lt;p&gt;Are computers smarter than humans after all those evidences? If the answer to this question was positive, I would not have written this article, the computer would have typed it himself.&lt;/p&gt;', ''),
(34, 1, 'Alia Bin Omair', '&lt;p&gt;Alia Bin Omair is an Emirati jewelery artist born and raised in Dubai. She received a Bachelor’s degree in Applied Communications from Dubai Women’s College, with a focus on Graphic Design. &lt;/p&gt;&lt;p&gt;Alia has tried her hand at photography, product design, illustrations and more currently jewelery design, which has given her plenty of room and experience to play around with her creative capabilities and interests. In 2013, she pursued a Diploma in Jewelery Design and Production at the Damas Jewelery Academy in Dubai, after which she won the Emirati Jewelery Designer of the Year Award in the same year.&lt;/p&gt;&lt;p&gt;Alia is a curious designer who is interested in experimenting with different materials from her country’s heritage and traditional craft - she likes to explore new techniques and methods to combine art and design.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&quot;My life has always been inclined towards art and design.&quot;&lt;/b&gt; - Alia Bin Omair&lt;/p&gt;&lt;p&gt;Studying design as part of her university degree compelled Alia to further explore and break the boundaries of what design can and cannot be, which she has since been experimenting with. The bedrock of Alia’s designs is to retain the raw feel of natural materials. She seeks to use objects, materials and forgotten crafts that appeal to Alia’s style and are not conventionally used, and shape them into pieces that question the limitations of art and design.&lt;/p&gt;&lt;p&gt;The shift from graphic design to jewelery design was triggered by Alia’s interest in working with more tangible objects. She began with illustrating complex jewelery pieces and working out detailed measurements and sketches of the designs. She soon found the confidence to move towards experimenting with different materials and learning new crafts. &lt;/p&gt;&lt;p&gt;Alia aims to work with different materials and objects from the UAE in the future and find different ways to present them, encouraging a dialogue about the many substances found in the UAE that hold design potentials and can substitute for other commonly used elements. &lt;/p&gt;&lt;p&gt;“My life has always been inclined towards art and design. Since I was little, life has found ways to expose me to different types of artistic experiences. My mother used to draw for me when I was little, which perhaps laid the foundation to my interest in illustrations. In 2013, I graduated with a degree in Applied Communication with a focus on Graphic Design. The drawing and painting courses I had taken in college pushed me to try and obtain a second degree, one that was closer to what I enjoyed doing. So I enrolled myself into the Damas Jewelery Academy in 2013. Five months later, I graduated with a diploma in Jewelery Design and Production, and then went onto winning the Emirati Jewelery Designer of the Year Award in the same year. I started illustrating ready-made jewelery designs, imitating them on paper and learning how to perfect illustrations of jewelery work. Eight months later, I attended a workshop given by the Azza Fahmy Studio in Dubai, and that workshop marked the beginning of my career as a jewelery designer. More than anything it showed me the possibilities to combine art with jewelery, so that jewelery did not only exist to decorate, but also became an art-form. I saw that with jewelery design it is possible for me to experiment with different materials and designs at the same time, breaking different boundaries and rules as I go further. Despite so much focus on jewelery design, I have always had more than anything the inclination to experiment. So, while in college, I dabbled in photography, product design, and more importantly fashion design, which I now practice alongside art jewelery.&lt;/p&gt;&lt;p&gt;My plans for the future involve honing my skills as a jewelery artist; that includes learning new techniques, but more importantly experimenting with more local/regional materials. I have already played with Gargoor, palm fronds, Niello, and frankincense, so I’d like to continue to find more materials that I can work with. I seek to find more ways to combine jewelery and art, and to blur the lines between them. I feel that this is a gap in the UAE market and design industry, and with the help of my Emirati cultural background and my interest in design, I want to attempt to fill this gap and hopefully instigate some interesting thoughts and ideas. &lt;/p&gt;&lt;p&gt;Inspiration to me does not come from a specific artist. Although there are people I may look towards for new techniques, my actual inspiration comes from within the materials I use. Whether for my fashion designs, or for my jewelery designs, the material itself presents its potential to me, which I try to work with. The materials’ inherent qualities give me ideas on how to design around their “personalities”, rather than to break and reshape it out of its original state.&lt;/p&gt;', ''),
(37, 1, 'لاست إكزت', '', ''),
(37, 2, 'لاست إكزت', '&lt;p&gt;في الآونة الأخيرة استطاعت مراس القابضة أن تضع بصمتها السياحية على خارطة مدينة دبي للسياحة وذلك عن طريق طرح وتنفيذ مشاريع جديدة وبطريقة مبتكرة حيث أن السياحة كانت تقتصر على المراكز التجارية المكيّفة ولأن مناخ دولة الإمارات العربية المتحدة مناخ صحراوي فمن الصعب المجازفة لإنشاء مراكز تجارية خارجية غير مكيفة إلا أن مراس القابضة استطاعت أن تغير هذا المفهوم السائد لدى المقيم والسائح، إذ أنها ابتكرت بعض الحلول الإبداعية وهي تكييف الممرات في بعض المراكز التجارية الخارجية كسيتي ووك ولم تكتفي بهذا المشروع فقط بل أنشأت مشاريع عدة كبوكس بارك وذا بيتش وغيرها من المشاريع الأخرى..&lt;/p&gt;&lt;p&gt;استطاعت مراس القابضة تغيير بعض المفاهيم السائدة كإنشاء معلم سياحي فيه تجمع للمطاعم والتي تحملها شاحنات تحمل أسماء تجارية مهمة في قطاع المطاعم والمقاهي..&lt;/p&gt;&lt;p&gt;لاست إكزت تضم ٢٠ مطعما على شاحنات طعام بطريقة مبتكرة لم تألفها دولة الإمارات العربية المتحدة وهي الأول من نوعها في المنطقة وبطريقة فريدة، تعمل على مدار ٢٤ ساعة، حيث أن المشروع إتخذ لنفسه مكانا استراتيجيا فهو يقع على الشارع الرئيسي بين دبي وأبوظبي عند المدخل ١١ على شارع الشيخ زايد بمساحة ١٥٠٠ متر مربع، طريقة تصميم لاست إكزت مميزة ومختلفة فامتاز تصميمها بالطريقة العصرية الفريدة من نوعها فقد تتخللها قطع السيارات المنتشرة في أرجاء المكان، يعتبر هذا المكان هي الفرصة الأخيرة لقائدي السيارات التوجه لهذه الوجهة والإستمتاع بمختلف أنواع الطعام وألذها وأخذ استراحة قصيرة من عناء الطريق بالتلذذ بطعام فريد من نوعه والاستمتاع بهذه التجربة الرائعة..&amp;nbsp;&lt;/p&gt;&lt;p&gt;جاءت فكرة لاست إكزت من مراس القابضة نفسها لتلبية حاجة سائقي السيارات القادمين من إمارة أبوظبي لدبي ليستمتعوا بمختلف أنواع الطعام حتى تكون الخيارات متنوعة وعدم حصرها على ما تقدمه محطات البترول أو مطاعم الوجبات السريعة، فالمطاعم المتوافرة&amp;nbsp; في لاست إكزت متنوعة فهناك مطاعم أميركية ولاتينية وعالمية وتتميز بالخدمة السريعة..&lt;/p&gt;&lt;p&gt;لاست إكزت تحتوي على ١٤ عربة طعام في الوقت الحالي إلى جانب عدد من المتاجر ومنطقة لعب للأطفال ومواقف للسيارات بطاقة استيعابية ١٠٩ مواقف سيارات، أرادت مراس القابضة أن توفر للسياح والمقيمين وجهات ترفيهية جديدة غير نمطية وقد يتوسع هذا المشروع بإضافة نقاط أخرى مختلفة على مستوى الشوارع الرئيسية، خصوصا أن المشروع لاقى إقبالا كبيرا سواء من حيث عدد الزوار أو التفاعل في مواقع التواصل الإجتماعي..&amp;nbsp;&lt;/p&gt;&lt;p&gt;لاست إكزت تحتوي على منطقتين فالمنطقة الخارجية تستوعب ١٨٠ زائرا والمنطقة الداخلية مكيّفة وتستوعب ٩٠ زائرا..&lt;/p&gt;&lt;p&gt;لاست إكزت فكرة فريدة من نوعها ومكان يساحق الزيارة سواءً من المقيم أو الزائر لمعرفة وخوض هذه التجربة الرائعة.&lt;/p&gt;&lt;p&gt;إذا لم تتسنى لكم الفرصة لزيارة هذا المكان حتى الآن، يمكنكم التخطيط لأسبوعكم القادم.&lt;br&gt;&lt;/p&gt;', ''),
(38, 1, 'ذا جرين بلانيت', '', ''),
(38, 2, 'ذا جرين بلانيت', '&lt;p&gt;تحتضن مدينة دبي مشروع “ ذا جرين ب انيت”، ويُعدُّ هذا المشروع أول وجهة ترفيهية، ل وتعليمية، وصديقة للبيئة في المنطقة. هو عبارة عن تصميم مذهل، يُحاكي نمط حياة الغابات الاستوائية، ويتميّز بتنوّعهِ البيئي والحيوي المتكامل، ولذلك فهو يُذْهل كل زوّراه من خ ال ل مشاهدة 3000 نوع من نباتات وحيوانات الغابات&amp;nbsp; الاستوائية النّادرة.&lt;/p&gt;&lt;p&gt;ولأنّ دبي عوّدتنا دائماً على احتضان المشاريع النّادرة والأولى من نوعها، فإنّها تسعى من خ ال هذا ل المشروع المستدام أن تكون المدينة الأولى عالمياً الصديقة للبيئة، من خ ال مزج التطوّر العمراني ل بالبيئة، ونشر التوعية بأهمية المحافظة على البيئة، والحفاظ على الحيوانات النّادرة، وحمايتها من الانقراض.&lt;/p&gt;&lt;p&gt;وعلى المستوى المعماري، يُعدّ تصميم هذا المبنى نادراً من نوعهِ في المنطقة، فهو جميل وغريب في الوقت ذاته، إذ اُسْتلهمت فكرة تصميمهِ الهندسيّ من خ ال محاكاة فن الأوريغامي، وهو فنّ مرتبط ل بالثقافة اليابانية، ومعناهُ فن طي الورق. ومما زاد جمال هذا التصميم ذلك المزج الرائع بين قطع الزجاج واللون الأبيض. والجدير بالذكر أيضاً أنّ الفن الأوريغامي هو أحد الفنون اليابانية المحببة لدى جميع الأعمار، ويُعنى هذا الفنّ بتصميم أشكال هندسية متميزة ث اثية ورباعية الأبعاد، عن طريق لاستخدام الورق فقط.&lt;/p&gt;&lt;p&gt;أمّا التصميم الداخلي لهذه القطعة الهندسيّة الفريدة، فهو مستوحاة من طبيعة الغابات الاستوائيّة، وبمجرّد دخولك ستلحظ مظلة الأوراق التي تغطي سقف المبنى، والأشجار الضخمة، والممشى الطبيعي الرطب، والش ال، بالإضافة إلى ل الحوض المائي الضخم الذي يضمّ كمّاً هائ اً من ل الحيوانات المائيّة النّادرة. ناهيك عن أكبر شجرة صنعها الإنسان.&lt;/p&gt;&lt;p&gt;ويُتيح هذا المشروع – على المستوى التثقيفي - لكلّ زوّارهِ فرصة القيام برحلة ترفيهية واستكشافية رائعة ونادرة لا تُنْسى، إذ يستخدم الزائر كلّ حواسه للتعرّف على عالميْ الحيوانات والنباتات الفريديْن من نوعها، كما يُتيح لهم عيش مناخ الغابات الاستوائية المتميّز بحرارتهِ ورطوبته. والاطّ اع على خفايا وأسرار نمط عيش هذهِ الكائنات.&lt;/p&gt;&lt;p&gt;“ ذا جرين ب انيت” تحفة معمارية جديدة أُضيفت إلى ل رصيد مدينة دبي، هي عالم آخر، وهي أرض العجائب، تُتيح لك رؤية كل ما هو غريب، ونادر، وفريد.&lt;br&gt;&lt;/p&gt;', ''),
(39, 1, 'دار دبي للأوبرا', '', ''),
(39, 2, 'دار دبي للأوبرا', '&lt;p&gt;دار الأوبرا عبارة عن مسرح يحتوي على منصة منها مساحة للأوكسترا ومقاعد للمشاهدين ومرافق خلفية للأزياء ومتطلبات العرض ومن أشهرها في العالم دار الأوبرا في سيدني ودار الأوبرا في فيينا ودار الأوبرا متروبوليتان في مدينة نيويورك ودار الأوبرا المجرية ومؤخرا تم بناء دار الأوبرا دبي..&amp;nbsp;&lt;/p&gt;&lt;p&gt;تم إختيار الراحلة زها حديد لتصميم دار الأوبرا دبي والتي توفيت مؤخرا في مارس ٢٠١٦ تميزت أعمالها باتجاه معماري واضح والمعروف بالمسمى العمل التفكيكي أو التهديمي وهذه الأعمال تتميز بأنها تنطوي بشكل معقد وهندسة غير منتظمة وبطريقة انسيابية فهي ساهمت بدور كبير جدا لتغيير مفهوم العمارة في العالم وأسهمت في خلق عالم أجمل عبر تصاميمها الراقية فتم إطاق مسمى التجريد الميكانيكي على أعمالها والأجمل من ذلك أنها تركت بصمتها في الكثير من الدول..&lt;/p&gt;&lt;p&gt;أحد بصمات زها حديد في دبي دار دبي للأوبرا الذي قامت بتصميمه مع شريكها باتريك شوماجر، فقد استوحيا التصميم من الطبيعة الصحراوية والتي تتميز بها دولة الإمارات العربية المتحدة، فالطبيعة الصحراوية للمدينة وما تحويها من جبال وكثبان رملية حيث أن سطح المبنى يبدأ بالإرتفاع تدريجيا ليصل للقمة ومن بعدها يبدأ بالهبوط بطريقة فنية وانسيابية ذات مظهر جذاب فقد اتخذ المبنى شكل الهرم من دون حواف حادة كمظهر التال الرملية وتشكّل المبنى على هيئة سفينة شراعية&amp;nbsp; شامخة تقف على اليابسة فهي استطاعت أن تدمج الماضي بالحاضر بطريقة فنية متميزة..&lt;/p&gt;&lt;p&gt;لقد بدأت عملية البناء في عام ٢٠١٣ وانتهت في عام ٢٠١٦ وعدد المقاعد المتوافرة في الدار يعتمد على طريقة التوزيع أو الأوضاع التي يتم استخدامها، فهي تتحول من مسرح لقاعة حفات موسيقية وتشكل الأرضية المسحطة لتصبح مكاناً لتنظيم الفعاليات. والمساحة المسرح مناسبة لعروض الأوبرا والمسرحيات الموسيقية والباليه والرقص والعروض المسرحية الدرامية الكبيرة والمحاضرات والمؤتمرات حيث أن المكان سيتسع ل ١٩٤٠ حتى ٢٠٤٠ شخصا..&lt;/p&gt;&lt;p&gt;أقيمت الحفلة الأولى في دار دبي للأوبرا في تاريخ ٣١ أغسطس ٢٠١٦ ليحتضن المسرحيات الأوبرالية المترجمة إلى العربية والإنجليزية عبر شاشة كبيرة في أعلى المسرح ويمثل افتتاح هذا الصرح المدينة الأيقونة في المنطقة بعدا ثقافيا هاما في نشر ثقافة العروض الأوبرالية والك اسيكية المختلفة لتلبية ل شغف محبي هذه الفنون من مختلفة الدول..&lt;/p&gt;&lt;p&gt;حصدت دار الأوبرا دبي جائزة أفضل مشروع على مستوى دول مجلس التعاون في عام ٢٠١٥ لشركة دبليو اس آتكينز عن تصميم دار الأوبرا دبي..&lt;br&gt;&lt;/p&gt;', ''),
(41, 1, 'حِينَ تَنْطِقُ القُبّة', '', ''),
(41, 2, 'حِينَ تَنْطِقُ القُبّة', '&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;] }فَلَمّا رَجَعُوا إلى أبِيهِمْ قَالوُُا يا أباَناَ مُنِعَ مِنّا الكَيْلُ فأرسِل مَعَنَا أخَاناَ.. { يوسف ] ٦٣&lt;/b&gt;&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;div style=&quot;text-align: center;&quot;&gt;فمُدّت كلمة )أخانا( و بدأ القارئ بالتلفّت يميناً و شمالاً و رأى أن المسجد خالٍ .. و الوقت آن ذاك قد قارب الحادية عشرة لياً .. فكانت ردّة فعله أن إبتلع ريقه و إسترجع شهيقاً عميقاً و واصل القراءة إلى آخر الآية .. و لم يكُن من قاطعه إلا هوَ .. و ما جعله يواصل التاوة إلا هوَ .. فالصوت صوته هوَ و الرد ردّه هوَ .. فرفع رأسه للأعلى مناظراً القبّة.&lt;/div&gt;&lt;div style=&quot;text-align: center;&quot;&gt;&lt;br&gt;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;للمباني صوت ونبرة كالإنسان .. لكل مبنى أو قاعة صوت معيّن وخاص بها ويختلف من قاعة إلى أخرى.. وقد ن احظ هذه الميزة في ل كل مكان نزورهُ.. غرفة النوم.. غرفة المعيشة.. المطبخ.. الحمام.. المستودع.. وحين ننتقل إلى منزل جديد يختلف صوته وهو فارغ من غير أثاث .. وحين يُفرش بالسجاد ويُم بالأرائك والستائر أ يختلف أيضاً.. وخارج المنزل تتكرّر نفس الحكاية.. فالأصوات في الصالة الرياضيّة تختلف طبيعتها عن طبيعة الأصوات في مخزن مليئ بالصناديق والأجهزة الإلكترونيّة.. وكذلك محلّات الستائر الهادئة يختلف حسّها عن محلّات الأرضيّات.. وقد ت احظ عزيزي القارئ أنّ الأنفاق لها صدى ل يمتد لثوانٍ كثيرة مقارنةً بالشوارع المفتوحة.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;وهكذا يكون للمكان المعيّن صوت معيّن وإحساس معيّن.. فتشعر بالإنشراح في الصالة الرياضيّة لوسعها وكبر حجمها.. بينما تشعر بالضّيق في مستودع التخزين.. ليس فقط لنقص التهوية فيه، بل لكتمة الصوت فيها فيُخيّل لك أنّك تسمع دقّات قلبك.. وفي قاعة الأوبرا تشعر بالاندماج المفعم بالتكيّف مع الموسيقى.. وفي المسجد النّبوي روحانيّة لا تنتهي لكبر حجمه وانتشار صوت القارئ بوجود القبّة فيُهيّأ لك أن الصوت قد جاء إليك من السّماء.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;إذاً للمكان لغة صوت ندركها ونتفاعل معها ولكن لا نفهمها.. إن مستودع الأقمشة الذي يحتوي على أطنان وأطنان من مادّة البولييستر المستخدمة في صنع السّجاد.. تعتبر من أكبر أعداء الصوت لدرجة أنّها تستخدم من قِبل مهندسي الصوت في عزل الصوت ومنع الصّدى وإرتداده.. يُغطّى جدار استوديوهات التسجيل بمادّة البولييستر لتساعد على تشتيت الصّوت وعدم إرتداده ليعطي جودة عالية ولا يتلف التسجيل.. فهي تساعد في كتم الغرفة وعزلها&amp;nbsp;&amp;nbsp;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;من الصدى لأن القماش سطح غير أملس ولا يساعد في إرتداد الصوت وليس مُتنفّساً له فيزيائياً.. وعلى هذا الأساس نفهم أن كثرة وجود مادّة البولييستر في مكان مغلق يجعل منه مكتوم الصوت والنّفس ويوشك أن يكون مكان لا يُطاق.. بسبب كثرة إرتكاز الصوت وإحتكاره في مكانه لدرجة أن دقّات القلب يُمكن أن تُسمع بالأذن فعاً!&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;أمّا القاعات والجوامع الكبيرة فعادةً تُحيي شعائر مفعمة بالحيويّة الروحيّة.. فتحتاج إلى قدر كبير من الإنتشار والتوسّع الصّوتي.. فبإمكان المهندس أن يبني الجدران والقُبب بمادّة الرّخام كونه سطح أملس.. فالأسطح الملساء تستقبل الموجة الصوتيّة وتقوم بإعادة توجيهها بشكل أكبر في إتجاهات مختلفة.. تماماً كأنك تسلّط أشعّة ضوء على مرآة.. ولكن بدرجات توسع الإتجاهات جميعها.. مقارنةً بالضّوء فيكون إنعكاسه مجرد شعاع مستقيم.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;أمّا بالنسبة للقبّة.. فبطبيعة شكلها المكوّر فهي تستقبل الصّوت من زاوية وتعيد إرتداده إلى الزاوية الموازية كما هو موضّح في الرسم أدناه.. وهو السبب الواضح لسماع الصّوت قادماً من الأعلى.. ليعطي جماليّة للمستمع بالإستماع للتاوة من شتّى الإتجاهات و ب ٣٦٠ درجة.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;هذا بخصوص الطبيعة الصوتيّة في المكان.. ولكن مكبّرات الصوت تلعب دور مهم أيضاَ.. فعلى جميع المصلّين الإستماع لخطبة الجمعة بشكل واضح أيّاً كانوا يجلسون.. الذي يجلس في الزاوية يسمع الإمام بنفس الشدّة التي يسمعها من يجلس قرب المنبر.. ونحن نعرف أنّ في الفيزياء كلّما ابتعدنا من المصدر تضعف قوّته.. نعم قد تضعف قوّة الصّوت عند وصوله إلى من يجلس في الزاوية.. ولكن جدران وأعمدة الجامع ساهما في صنع عدد من الإرتدادات الصوتيّة لكي تنشر الصّوت لكل زاوية في الجامع.. بالإضافة إلى توفّر مكبّرات في الزوايا الأربعة لتجعله مستحي اً ل على الصوت أن يتخلّف عن الحضور في أي زاوية من زوايا الجامع.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;br&gt;&lt;/div&gt;&lt;br&gt;&lt;/p&gt;', ''),
(40, 1, 'كيف أكملت ماراثون دبي بحاسة السمع', '', ''),
(40, 2, 'كيف أكملت ماراثون دبي بحاسة السمع', '&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;أينما تتواجد المقاطع الصوتية تتواجد الحركة المتناسبة معها كالتلويح، التصفيق، والضرب بالرجل على الأرض وغيرها من الحركات التي نندمج فيها مع الشيء المسموع، فلكل حضارة أو بلد أنواع مميّزة من الصوتيات بغض النظر ما إن كانت موسيقى أو موشحات أو أناشيد، كما تختلف الحركات التابعة لها حسب نوعها وسرعتها وكلماتها ونشأتها وغيرها. فجميع المسموعات سواءً الغنائية أو الإنشاديّة لها ٣ مركّبات تكوّنها، الرتم والسرعة والوزن.&amp;nbsp;&lt;/b&gt;&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;br&gt;&lt;/b&gt;&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Rhythm— &lt;b&gt;الرتم الموسيقي&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;هو الطريقة المتخذة لتوزيع الأصوات المكوّنة للعمل الصوتي كالطبل أو التصفيق أو الآهات البشرية.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;BPM/Beats Per Minute—&lt;b&gt; السرعة&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;تحسب السرعة بعدد الدفقات في الدقيقة الواحدة، و هي وحدة قياس سرعة الرتم التي يحددها المنتج، من الممكن أن يكون لدينا رتمين مختلفين و بتفاصيل مختلفة و لكن بنفس السرعة.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Harmonic Motion—&lt;b&gt; التناغم الحركي&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;تُستخدم الموسيقى في الصّالات الرياضية و النوادي لكونها تلعب دور كبير في إندماج الشخص و عدم شعوره بالوقت و الجهد عند تأدية التمارين، و هناك رياضات معيّنة ينجبر لاعبيها بإستخدام الموسيقى كونها جزء منها مثل الآيروبك و الزومبا اللتان تتماشيان مع رتم موسيقي و تعتمد الحركة دائما على حاسة السمع. حيث يتحقق التناغم في رياضة الجري بإتباع الخطوات تماشياً مع الدفقات المناسبة لرغبة العدّاء، كما يختلف الأداء و التأقلم مع المقطع الصوتي من شخص لآخر فهي ليست نظرية ثابتة للجميع.&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;b&gt;هل تزداد سرعة الأداء مع سرعة الرتم؟&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;من الأسباب الرئيسية التي جبرتني للإشتراك في مراثون دبي هي جمع إحصائيات تفيدني في كتابة هذا المقال، و لإثبات ما قد إقتنعت به كباحث صوتيات هل هو فعاً صحيح و يُستند عليه؟، ليس مطلقاً و لكن تقريبياً. حيث أن الصوتيات تساعد في تحسين الأداء و تنظيم الحركة، و لكنّها لا تقوم بجبر ممارس الرياضة بزيادة سرعة التمارين إن لم يجبر نفسه بالتماشي مع الرتم، فاللياقة و الإقبال على هذا العمل حافزان مهمان لتحقيقه.&amp;nbsp;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;الإحصائية أعاه تحتوي على قائمة المسموعات التي أعددتها للمراثون و كانت بنفس الترتيب ابتداءً مع طلقة البداية و حتى خط النهاية كما هوَ موضّح. ن احظ أن لكل مقطع صوتي سرعة ل رتمية مختلفة و إن مدّة إنهاء الكيلومتر الواحد غالباً تكون أقصر كلما زادت سرعة الرتم، كما نرى الفرق بين الكيلومتر التاسع و العاشر، فالعاشر و الأخير عادةً ما يكون الأصعب من بين الكل إلا أن مدة إكماله كانت أقصر مع الرتم السريع.&amp;nbsp;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;Motivational Contents—&lt;b&gt; المحتوى المحفّز&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;“وقت الشدايد للفعول أصنافي .. و عيال زايد هم قروم أشرافي”&lt;/div&gt;&lt;/p&gt;&lt;div&gt;بإستطاعة الكلمات أو المقصد أن يؤثران إيجابياً في التحفيز للإستمرار و عدم التوقّف عن الإستماع و الإندماج مع المقطع، بالتالي تتنظم خطوات العدّاء مع وجود الحافز المطلوب خاصةً إذا كانت المحتويات منسوبة لعرقه أو لمثله الأعلى. على سبيل المثال صادف الكيلومتر الثالث أبطئ سرعة رتم بين جميع المقاطع و يحتوي على رتم مقتبس من تغرودة تراثية تعود للمغفور له الشيخ زايد بن سلطان فكان تأثيرها على نفسيتي أكبر من الرتم السريع ذو المحتوى الإعتيادي، و بالتالي كانت سرعة إكمال الكيلومتر&amp;nbsp;&lt;/div&gt;&lt;div&gt;&lt;div&gt;الثالث أسرع من الثاني.&lt;/div&gt;&lt;div&gt;م احظة: المعلومات المذكورة قد تتحقق مع ل مجموعة من التجارب، و لكنها قد لا تتحقق مع التجارب الأخرى، فكما سبق ذكره فهو معتمد بجزء كبير على الشخص و معنوياته، مشاعره، و لياقته.&lt;br&gt;&lt;/div&gt;&lt;br&gt;&lt;br&gt;&lt;/div&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;/div&gt;&lt;br&gt;&lt;/p&gt;', ''),
(42, 1, 'الفتى المتيم والمعلم', '', ''),
(42, 2, 'الفتى المتيم والمعلم', '&lt;p&gt;بين الصبي جهان ذو الاثنا عشر عاماً والذي يصل إلى اسطنبول مع هدية السلطان، الفيل الأبيض شوتا والمعماري سنان أشهر معماري الدولة العثمانية. تعود أليف بالقارئ عبر الزمن إلى العصر الذهبي للدولة العثمانية، عالم القصور والجواري عالم يرى من وجهات نظر مختلفة حيث قصص الحب والعشق والمكر والدسائس.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وجد جهان نفسه فجأة بعد أن كان لصا تحول لسائس فيل يعيش في القصر العثماني، مجيئه إلى القصر غير حياته، لقائه بالسلطانة ميهرماه ابنة السلطان سليمان القانوني غير روحه، ولقائه وعمله مع المعماري سنان غير ذاته وشخصيته بأكملها.&amp;nbsp;&lt;/p&gt;&lt;p&gt;على مدى 600 صفحة تأخذ أليف القارئ عبر مدة حكم السلطان سليمان، ابنه سليم ومن ثم السلطان مراد. قصص عديدة تروى بين صفحات الرواية وليست قصة جهان والمعلم سنان وحسب، قصص مساجد بنيت وتحف معمارية شيدها سنان، مباني صامدة حتى الآن خلدت اسم سنان وت اميذه الذي ل أصبح جهان أحدهم فيما بعد، سنان الذي كان يختار الت اميذ ليس المميزين ل بل المهمشين والموهوبين في الوقت ذاته لأنهم لم يجدوا المكان المناسب للتعبير عما بدواخلهم.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;تقول أليف على لسان المعماري سنان في الرواية: &quot; العمارة مرآة تعكس الانسجام والتوازن الذين يحفل بهما الكون وإذا لم تحتضن هذه السجايا في فؤادك فإنك لن تتمكن من البناء&quot;&lt;/p&gt;&lt;p&gt;رواية كهذه تدخل القارئ في تفاصيل العمارة الإسامية في أجمل عصورها على يد أحد أمهر المعمارين عبر التاريخ سنان. رواية عن التاريخ والحب والعمارة.&lt;/p&gt;&lt;p&gt;بالرغم من كون أليف تنقلت بين العديد من المدن والدول منذ ولادتها إلا أن اسطنبول دائما حاضرة في روايتها وبدأت فكرة هذه الرواية في اسطنبول عندما كانت أليف في سيارة أجرة في وسط زحام اسطنبول الا متناهي تنظر إلى مسجد ما جلبي أحدى المساجد التي بناها سنان ولكن لم تلقى شهرة واسعة. من هنا كانت البداية لرواية عظيمة تسحر قراءها، وتسافر بهم عبر الزمن.&amp;nbsp;&lt;/p&gt;&lt;p&gt;. ترجم الرواية للعربية محمود درويش لدار الآداب ونشرت في مايو 2&lt;br&gt;&lt;/p&gt;', ''),
(43, 1, 'بروكسل من ساحة معركة إلى مدينة سياحية', '', '');
INSERT INTO `sp_news_description` (`news_id`, `language_id`, `name`, `description`, `tag`) VALUES
(43, 2, 'بروكسل من ساحة معركة إلى مدينة سياحية', '&lt;p&gt;هذه المدينة الساحرة التي تقع في قلب أوروبا القارة العجوز، يسهل الوصول لها بالقطار من مختلف العواصم والمدن الأوروبية الأخرى فعلى سبيل المثال: تبعد بروكسل عن مدينة دوسلدورف في ألمانيا ساعتان وخمس عشر دقيقة فقط ومن باريس تستغرق الرحلة ساعة ونصف لا أكثر.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أول ما تلفت له في بروكسل هو عراقة المباني وقدمها وعلم الاتحاد الأوروبي المرفوع على العديد من هذه المباني. تاريخياً، بروكسل كانت دائماً محط أنظار الممالك المجاورة لموقعها، وحاربت من أجل حمايتها جيوش عديدة وعاشت صراعات عنيفة لدرجة تسميتها ب &quot;ساحة المعركة في أوروبا&quot; .&amp;nbsp;&lt;/p&gt;&lt;p&gt;اليوم تعتبر بروكسل عاصمة الاتحاد الأوروبي وبها مركز البرلمان الأوروبي ومجلس الاتحاد الأوروبي وتضم العديد من المجالس، البلديات والمراكز الأخرى المهمة ل اتحاد الأوروبي. ل أغلب هذه المباني والبلديات تقع في ساحة&amp;nbsp; ال&quot; غراند باس&quot; ولا يقل عمر المبنى الواحد منها عن ٦٠٠ عام.&lt;/p&gt;&lt;p&gt;وتعكس هذه الساحة بمبانيها الشامخة المستوى الثقافي والاجتماعي لشعب بروكسل في العصور الوسطى. كما تعكس مبانيها مزيج من نماذج معمارية وفنية من الثقافات الغربية وهذا يبين مدى استراتيجية المكان وحيويته في السابق. بالرغم من الدمار الذي عم المدينة على يد جيوش لويس الرابع عشر في القرن السابع عشر نهضت المدينة من جديد وبقيت الساحة شاهدة على هذا الصراع وعلى شموخ الطبقة البرجوازية في بروكسل الذين اختاروا إعادة شموخ وعزة المدينة عوضا عن بناءها بطراز معاصر آخر.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;بجانب تاريخ بلجيكا العريق تشتهر هذه الدولة بكونها موطن العديد من الأطعمة اللذيذة وأهمها الشكولا، الوافل أو الكعك المحلى والبطاطس المقلية. محات الشكولاتة والحلويات تنتشر عبر شوارع بروكسل، منها ما هو عريق وذو جودة عالية وأخرى تبيع أنواع مقلدة من الشكولاتة وقد يقع العديد من السياح في هذا الفخ لذا عندما تكون في بروكسل لا تشتري الشكولاتة إلا من المحات الكبيرة والمعروفة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;شيء آخر يميز بلجيكا، شيء من ذكريات الطفولة، مغامرات السنافر والرحالة تان تان التي كنا نترقب إط التها علينا من خال شاشة ل التلفاز. ك اهما ينتميان إلى بلجيكا وقبل أن ل تتحول إلى مسلسات كرتونية مشهورة كانت عبارة عن قصص مصورة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;هناك متحفان مختصان بهذه الشخصيات، الأول هو المركز البلجيكي للرسوم ويقع في وسط مدينة بروكسل. والثاني هو متحف والذي يختص بتاريخ وسيرة حياة Musee Herge مبتكر شخصية تان تان. Herge الرسام&lt;/p&gt;&lt;p&gt;من المباني التاريخية الزاهرة بالتفاصيل الدقيقة والجميلة إلى السنافر الصغار المختيئين في طرقاتها ومحات الشوكولا والحلويات اللذيذة التي يحبها الجميع، هذه هي بروكسل التي تجمع ما بين العراقة والمرح في مدينة واحدة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;استراحت بروكسل من النزاعات والصراعات لتصبح الآن مقراً لأحد أكثر الع اقات الاقتصادية ل قوة وجذباً لآلاف السياح.&lt;br&gt;&lt;/p&gt;', ''),
(44, 1, 'دوسلدورف مدينة المباني المعاصرة', '', ''),
(44, 2, 'دوسلدورف مدينة المباني المعاصرة', '&lt;p&gt;من أغرب المباني والأعمال الفنية حول العالم، هذا المبنى الفني المطل على نهر الراين تغطي واجهته مجموعة من الأجسام المتسلقة ذات الألوان الجذابة ويبلغ عددها تسعة وعشرون “ ” . Flossi مجسما ويطلق عليها بالألمانية اسم&lt;/p&gt;&lt;p&gt;مصممة هذا المبنى هي الفنانة الألمانية روزيل المعروفة بتصاميمها المبتكرة والمعاصرة، وقد افتتح هذا المبنى في عام 1998 ومن خال هذه الأجسام المتسلقة أعادت روزيل الحياة لمبنى أصم لا يكاد يرى وتحول بذلك إلى أحد أبرز معالم&amp;nbsp; المدينة&lt;/p&gt;&lt;p&gt;خال القرن السابع من المياد أنشأت قرية صغيرة لصيد السمك على ضفاف نهر دوسل وهو أحد روافد نهر الراين، وكلمة قرية في اللغة الألمانية هي دورف ومن هنا عند جمع الكلمتين مع بعضهما أتى اسم هذه المدينة “دوسلدورف” أي قرية دوسل. مع مرور الزمن كبرت القرية وأصبحت اليوم مدينة من أهم مدن ألمانيا، مدينة ساحرة تجمع بين عراقة التاريخ المتمثل في البلدة القديمة و مركزا للأزياء والموضة في شارع الملوك ونمط عمارة عصري وفريد من نوعه. يقال بأن فن الهندسة المعمارية هو فن إعطاء الوجه الجميل للمدن وفي دوسلدوررف نشأ نمط معماري معاصر وفريد ويعود هذا للنمو والتطور السريع الذي شهدته المدينة بعد الحرب العالمية الثانية. تتركز أغلب هذه المباني الفريدة والغريبة معماريا في منطقة تسمى ب “المرفأ الإع امي” ل او كما يسمى أيضا ميل الهندسة المعمارية في دوسلدورف Media harborوتشكل مثالا حيا لنمط الهندسة المعمارية المعاصرة وهنا مجموعة من أبرز هذه المباني:&amp;nbsp;&lt;/p&gt;&lt;p&gt;هي عبارة عن ثاث مبان متجاورة لكل منها تصميم فريد ومختلف عن الآخر ولكن يجمع بينها نوافذها الكبيرة والبارزة والتي صممت بطريقة تظهر كل نافذة كأنها بداخل صندوق وهذه المباني من تصميم المهندس المعماري فرانك جيري. المبنى الأول ناصع البياض، المبنى الأخير مغطى بالطوب الأحمر ويربط بين المبنيين مبنى مغطى بجدار عاكس ومتعرج من الفولاذ المقاوم للصدأ ليعكس للمارة المبنيين المجاورين. رمزية المباني الث اثة والطريقة التي ل يربط بها المبنى الأوسط المباني الثاث ببعضها البعض تهدف لإظهار الروابط بين الشمال والجنوب.&amp;nbsp;&lt;/p&gt;&lt;p&gt;مبنى من أطول المباني وأكثرها بهجة في المرفأ الإع امي في دوسلدورف، بواجهته ل الزجاجية الملونة يبلغ طول هذا المبنى اثنان وستون مترا وسبعة عشر طابقا ومغطى بطبقة من حوالي الفي قطعة زجاجية ملونة تشكل واجهة زجاجية مميزة تجذب انتباه كل من يمر بالمرفأ الإع امي. يعد هذا المبنى ثاني أعلى ل مبنى في دوسلدورف وبهذا التصميم المعاصر الذي صممه المهندس البريطاني وليام السوب في عام 2002 يهدف التصميم لتمثيل الواجهة الثقافية الجديدة والعصرية للمرفأ الإع امي. ل حاليا المبنى عبارة عن فندق راق بتصميم حديث ومعاصر ويضم مطعما ومقهى في الطابق الأخير يوفر منظرا بانوراميا للمدينة ونهر الراين.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(45, 1, 'تحديات قبطان', '', ''),
(45, 2, 'تحديات قبطان', '&lt;p&gt;كم من سف نة غرقت بسبب غفوة ی قبطانها ولهو بحارتها، في السف نة بعد غرقها ی لا تعتذر خٔشابها لأهالي الضحا ا، و نٕ اعتذرت ی ا ا فالاعتذار لن عوض الخسائر.&lt;/p&gt;&lt;p&gt;إن كنت تر د نٔ تكون قائداً ف جب عل ك الانتباه ی ی ی ا لأمر ن :&lt;/p&gt;&lt;p&gt;١( لما دور على ظهر سف نتك، لأن البحَّارة هم ی ی من د رون سف نتك مٔا نٔت فتقودها فقط!&lt;/p&gt;&lt;p&gt;٢( تقلبات البحر مٔامك؛ فالناس وحتى موظف ك ی ا لن تعني اعتذاراتك لهم بشيء بعد الخسارة.&lt;/p&gt;&lt;p&gt;من نجاح المشروع تعرف الفريق ومن نجاح الفريق تعرف القائد:&lt;/p&gt;&lt;p&gt;ح ن تتولى منصباً ق اد اً اعمل على نٔ تكون بعد ی ی ی ا عزلك عنه شخصاً يذكر ب نٔه كان )قائداً( ول س ی ا )مد راً( وشتان ب ن الاثنان؛ فالمد ر همه نٔ نجح ی ی ی ی ا المشروع فقط، أما القائد من نجح مشروعه ی وفريقه، وتراهم من بعده قادة صغار.&amp;nbsp;&lt;/p&gt;&lt;p&gt;المد ر من أخذ منك عملك مٔا القائد من ی ی ا عط ك هو من خبرته، والمد ر كث ر الراحة ی ی ی ی و نتظر منك نٔت الانجاز، أما القائد معلم ی ا ومعطاء لمُ خَلق ل نام حتى وإن غفى فريقه ی ی من التعب.&amp;nbsp;&lt;/p&gt;&lt;p&gt;غرفة القيادة ليست أهدأ غرفة على ظهر ا لسفينة :&lt;/p&gt;&lt;p&gt;قد بتلى القائد بفر ق س ئ، ولا كتشف ذلك ی ی ی ی إلا بعد إبحاره بهم! قد تبتلى بهم إذا لم كن ی أعضاء فر قك من اخت ارك، بل فرضوا عل ك ی ی ی لظروف ما. ستجد منهم الكسول الذي ر د نٔ ی ی ا ترسوا به السف نة في الم ناء دون نٔ سحب ی ی ی ا حباً. وستجد الحاسد الذي عتقد انه هو الأحق ی منك بالجلوس خلف المقود، ناه ك عن البحاره ی السلب ین كث ري الشكوى، والمتغطرس ن ی ی ی والمتنمر ن على من هم ضٔعف منهم.&amp;nbsp;&lt;/p&gt;&lt;p&gt;لا تكترث لسلوك بحارتك المش ن، فإن لم تقدر ی على إص احهم، تذكر إنك قبطان ولتبق السف نة ل ی هي محل اهتمامك لا سلوكهم.. فالعجب س یتٔ ك ف ما بعد! في الأزمات سترى أنظار ا ی ی جم ع البحارة متجة نحوك؛ في انتظار الحل ی وهذا مٔر طب عي! فالقائد من نُظر إليه إذا هبت ی ی ا العواصف! ونظرتهم تلك هي إشادة بقيادتك.&amp;nbsp;&lt;/p&gt;&lt;p&gt;ل س عيباً نٔ ترك القبطان غرفة الق ادة عندما ی ی ی ا كون البحر هادئاً؛ للنظر في أمور البحارة یوالسف نة. ولا تنتظر حتى تغرق السف نة لتتحرك ی ی وتذكر ما قلناه عند الغرق!&lt;/p&gt;&lt;p&gt;تعلم من أخطائك، وابحث عن سٔبابها واستشر ا من حولك لحلها، و إ اك نٔ ق دك ال س إذا ی ی ی ی أ فشلت في مٔر ما؛ فالبحث عن سبب غرق ا السف نة لن عود بالموتى للح اة مرة خٔرى؛ ی ی ی ا لكنه س ق نا من نفس العاصفة في المستقبل.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أخيراً الميناء:&lt;/p&gt;&lt;p&gt;في النهاية تذكر يا عزيزي القائد، أنه قد لا يشكر البحارة والركاب القبطان بعد وصولهم للميناء، لا لأن الرحلة كانت سيئة أو إنك قبطان فاشل؛ بل لأن فرحة الوصول ألهتهم.&lt;/p&gt;&lt;p&gt;تلك هي حياة القادة، قاسية بعض الأحيان؛ لأنها ليست كحياة المدراء، تقاس بجودة العمل لا بإنجازه فقط.&lt;br&gt;&lt;/p&gt;', ''),
(46, 1, 'فصول منفرة.. وفصول جاذبة', '', ''),
(46, 2, 'فصول منفرة.. وفصول جاذبة', '&lt;p&gt;&lt;b&gt;فصول منفرة: !&lt;/b&gt;&lt;/p&gt;&lt;p&gt;عندما كنت في الثانوية، دخلت معلمة الرياضيات الفصل وكان هذا الحوار :&lt;/p&gt;&lt;p&gt;&lt;img src=&quot;http://localhost/bitbucket/emirates-diaries/image/catalog/news/exciting-and-attractive-classes/exciting.png&quot; style=&quot;width: 100%; float: none;&quot;&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;وما أن انتهى الفصل الأول ألا و ٣٠ طالبة كان تحصيلهم في مادة الرياضيات بين الرسوب و ا لمقبول!&lt;/p&gt;&lt;p&gt;&lt;b&gt;فصول جاذبة:&lt;/b&gt;&lt;/p&gt;&lt;p&gt;دخلت علينا معلمة اللغة العربية في اليوم الدراسي الأول في مرحلة الأول ثانوي كذلك وكان هذا الحوار:&lt;/p&gt;&lt;p&gt;&lt;img src=&quot;http://localhost/bitbucket/emirates-diaries/image/catalog/news/exciting-and-attractive-classes/bubbles.png&quot; style=&quot;width: 100%;&quot;&gt;&lt;/p&gt;&lt;p&gt;جذبت المعلمة بدرية طاقة التركيز لدى الطالبات بثقتها بشرحها المبتكر، وهي فعً ا كانت مبتكرة، ل فقد كانت تروي لنا قصصاً في بداية كل حصة أو تعطينا معلومة محفزة وتحضر معاها الكتاب الذي قرأت منه وتكتب اسمه واسم المؤلف على السبورة. فبهذه الطريقة كانت الطالبات يشترين الكتب المفيدة وأصبحن يتحدثن الفصحى بط اقة ل ويكتبن التعبير ببراعة. فماذا يريد منا الإبداع؟&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;- يريد الابداع آلآف المعلمين والمعلمات مثل بدرية ليتخرج منها آلاف المثقفين والمبدعين.&lt;/p&gt;&lt;p&gt;- الاحتواء، كانت المعلمة بدرية دائما تقول: مهنة المعلم هي المهنة الوحيدة التي يتخرج من تحتها آلاف المهن.. لذآ احترم المهندسة والطيارة والمحامية والرئيسة اللواتي في صفي ولم يكبرن بعد.&amp;nbsp;&lt;/p&gt;&lt;p&gt;- يريد الإبداع عقوبات صارمة لمن يعاديه.&lt;/p&gt;&lt;p&gt;- الإبداع لا يريد مدارس جميلة بل فصول مبتكرة، فلو تأملنا حال الطلبة اللذين يهربون من المدرسة للاحظنا بأن الطالب يبدأ بالهرب من الحصص في البداية.&lt;/p&gt;&lt;p&gt;- إهدار أعمار الطلاب بالرسوب يعد شروعاً بالقتِل أيضاً!&lt;/p&gt;&lt;p&gt;- الابتكار والإبداع يريدان الحرية، لا يحدهما سقف ولاطاقة!&lt;br&gt;&lt;/p&gt;', ''),
(47, 1, 'رمي الصحيفة', '', ''),
(47, 2, 'رمي الصحيفة', '&lt;p&gt;&quot;صوت منبّه يتصاعد تدريجيّا&quot;، خلفه صوت تلفاز لبرنامج مزعج لا تستلطفه الأذن ، وضوء الشمس يملأ المدى البصري ويضايق العين لتضيء تلك الغرفة المقلوبة رأساً على عقب، لتفتح عين ذلك الشخص الذي لم يترك ساعة من ساعات الليل الطوال إلا وسهر فيه ب ا هدف.&lt;/p&gt;&lt;p&gt;جهاز التكييف ينفخ على الأرائك ليحوّلها إلى صقيع قاتل لاتتحمله العظام لشدة البرودة، فتح صديقنا عيناه مشمئزّاً من صوت المنبّه فأقفله بعنف والتفت إلى الغرفة يميناً وشمالاً، اتجهت عيناه إلى ذلك التلفاز الذكي فبدأ صاحبنا بسرد ما تبقّى من حكايته لنا قائ اً: ل &quot;بمجرّد جلوسي مقاب اً التلفاز، قام بتشغيل نفسه ل بنفسه متعرّفاً على م امح وجهي، وحين ظهرت ل أوّل شاشة بدأتُ بتحريك سبّابتي وكأنّي أشير لخادم لأستدرجه نحوي، وما حدث أن التلفاز كان ذلك الخادم الذي يطيع كل الأوامر، ففتح لي بوّابة أخذتني للعالم الخارجي وبلمحة بصر أتى بأخبار العالم، والكوميديا، والمسارح العالميّة، وعروض المواهب. تماماً كعرش بلقيس، هذا ما حدث بواسطة يدي اليمنى فقط، و باليد اليسرى أمسكت بهاتفي الذكي كذلك، وكان الهاتف متعرّفاً على م امح وجهي أيضاً كالتلفاز، ففتح لي ل عصارة من الرسائل والمحادثات المتراكمة بعد نومٍ طال ١٧ ساعة فأخذت أرد عليها واحدة تلو الأخرى هنأت الذي هنأني بمناسبة عيد الأضحى، وأرسلت لمهندس الديكور مواصفات غرفة المائدة ليستأنف العمل، ولم أنسى القيام بترتيب حفلة العشاء بمناسبة تخرّجي بواسطة رسالة واتساب واحدة لمجموعة من الزم اء ل وما أن&amp;nbsp; قرأها الجميع، حتى طلبت مائدة السهرة كلّها وقمت بدفعها إلكترونيّاً من غير تحريك إصبع آخر من أصابعي الخمسة، وكيف لي أن أنسى أنّ مشروع تخرّجي قد وصلني جاهزاً من قِبل شخص يبيع الأبحاث عبر الإنترنت، وحان الوقت أن أرسل له أجرته عبر الدفع الإلكتروني.&lt;/p&gt;&lt;p&gt;وبجانب كل هذه الأمور طلبت وجبة الإفطار المفضّلة&amp;nbsp; لدي من تطبيق المأكولات عبر الهاتف، كم يخجلني أن أدفع لموظّف المطعم نقداً و أنا لا أملك إلا ١٥ درهماً في محفظتي، ولحسن الحظ قام المطعم بتوفير جهاز الدفع ببطاقة الإئتمان وإلا لكنت سألجأ للمصعد وأمشي مسافة ٢٠ متراً لأقوم بسحب النقود. يا لها من مسافة طويلة !&lt;/p&gt;&lt;p&gt;أخذت الدنيا دورها في ترهيلي وإلصاقي بالأريكة وعدم السماح لي بالخروج أو الحركة، حتى أوشكت على نسيان كيفيّة المشي، أصبحت الدعوات بالفيسبوك، والتهنئة على الإنستغرام، والحديث والمناقشة والاجتماعات من خ ال الواتساب، ولولا الم احظات الصوتيّة وصور ل ل الملف الشخصي قد ننسى أصوات وأشكال أهلنا وأصدقائنا، أصبحت الدنيا مصفاة تمحي كل من ليس لديه جهاز ذكي، تماماً مثل قانون البقاء للأقوى، يأكل الحيوان القوي الحيوان الضعيف، وتتزاوج الطيور، وتطغى الحيوانات القويّة على الحيوانات الضعيفة، ولكننا نسينا أن الحيوانات الضعيفة قد تلتهم البكتيريا لتنقذ البشريّة من أية أمراض خطيرة أو مميتة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;بعد التطوّر الهائل الذي مرّ به العالم الحالي، وجرّد كل من هو &quot;غير ذكي&quot; أو &quot;غير متّصل بالإنترنت&quot; أو بمعنى آخر &quot;غير موجود في النظام&quot; ، نتج عنه ضحايا كثيرة، فتقاعد ساعي البريد من عمله لأن الجريدة أصبحت إلكترونيّة، وعاد النادل لبلده لأن المطعم تحوّل إلى مطعم إلكتروني يستقبل الطلبات عبر الإنترنت، فالذنب ليس ذنبهم ولكن جريان الرياح لم يأتي بما تشتهي سفنهم، كما فشل موزّع الصحف إثبات أنّ حرفته تضم عدّة قوانين من الفيزياء التقليديّة، وأن الصحيفة لن تصل من غير إتقان سياقة الدراجة الناريّة والتركيز على سرعة رمي الصحيفة لتقع تماماً أمام منزل الزبون في نفس الزاوية يوميّاً، مراعياً لعوامل الرياح والمطر والأرض المبلّلة وقانون السقوط الحر، و أن سرعة انتقال الدراجة سيأثر على سرعة رمي الصحف، وهي عوامل يتعلّمها بالممارسة لا بشراء أداة تفعل له كل ما سبق، أمّا المشتري للجهاز الإلكتروني الذكي، لم يبتكر منه شيء ولم يجتهد في إيجاده.. و لم يعلم أن هذا النظام خلفه لغة برمجة معقّدة إذا تعلّمها ربّما يصبح أفضل مئة مرّة من الشركة الصانعة له.&amp;nbsp;&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(48, 1, 'قارب النجاة', '', ''),
(48, 2, 'قارب النجاة', '&lt;p&gt;تُصنعّ السفن الضخمة من الفولاذ مما تصل تكلفتها الى آلآف وم ايين الدراهم، ل وتُصنّع قوارب النجاة من الخشب والمطاط وتكون زهيدة الثمن مقارنة بالسفن.&lt;/p&gt;&lt;p&gt;عند اشتداد العواصف يقفز الركاب من السفن الضخمة الى قوارب النجاة الصغيرة لتنقلهم الى بر الأمان، ويتركون خلفهم السفن تغرق بما فيها في المحيط الهائج.&lt;/p&gt;&lt;p&gt;قد تمر بظروف قاسية تجبرك على التخلي عن الاشياء العظيمة التي تمتلكها وتحوّل وجهتك الى أشياء وأماكن أقل مما تطمح به، مما يؤدي بك للإحباط والحزن وندب الحظ. لكن الإنسان الطموح فعاً يعلم إن هذه المرحلة ما هي إلا لحظة القفز إلى قارب النجاة الصغير ليصل الى ما هو أهم، وإن التمسك بالماضي سيغرقه.&lt;/p&gt;&lt;p&gt;مرحلة القفز إلى قارب النجاة مر بها الكثير من العظماء في حياتهم، والأجدر بالذكر رسولنا الكريم محمد صلى الله عليه وسلّم، حين طُرد من مسقط رأسه مكة الى المدينة المنورة التي لجأ إليها هارباً مجروحاً من بطش قومه الذين ترعرع بينهم، ليكّون ويؤسس مع المهاجرين والأنصار دولة إسامية عظيمة فتح بها مكة ومازالت مستمرة إلى يومنا هذا.&amp;nbsp;&lt;/p&gt;&lt;p&gt;سَخِر قوم نبي الله نوح عليه السام منه عندما جائه أمر الله له ببناء السفينة، وقال بعضهم إنك كنت تزعم إنك نبي فكيف أصبحت نجاراً ؟! وبعد سنتين من العمل نجا هو بتلك السفينة وغرقوا هم في الطوفان.&lt;/p&gt;&lt;p&gt;فا تفقد الأمل مهما غيّر القدر مساره، قد يبدو الحاضر صعباً لكنك سترتاح في المستقبل، استعن بالله واستشر من هم أكثر منك خبرة، وكما قال الكاتب جون لينون:&amp;nbsp; ”كل شيء جيد في النهاية، إن لم يكن جيداًفليست النهاية&quot;.&lt;br&gt;&lt;/p&gt;', ''),
(49, 1, 'حي البستكية: المنطقة الصامدة أمام ناطحات السحاب', '', ''),
(49, 2, 'حي البستكية: المنطقة الصامدة أمام ناطحات السحاب', '&lt;p&gt;على امتداد ضفاف خور دبي تبرز أحد المعالم المعمارية التاريخية الشاهدة على تميز مدينة دبي، ألا وهو “حي البستكية”، هذه المنطقة التراثية الصامدة بعمقها التاريخي وعمارتها الفذة أمام ناطحات السحاب.&lt;/p&gt;&lt;p&gt;يعتبر حي البستكية من أقدم أحياء دبي، يعود تاريخة إلى سنة 1890 م، حيث كان تسكنه العائات المحلية الثرية، لكن بعد التطور الاقتصادي واكتشاف النفط انتقلت هذه العائات للسكن في مناطق أخرى.&lt;/p&gt;&lt;p&gt;وأكثر ما يميز حي البستكية كثرة البراجيل الشاهقة الارتفاع المتعانقة مع السماء، فهذه البراجيل عبارة عن أبراج لتلطيف الهواء وإنعاش الجو، وتتميز البستكية كذلك بممراتها الضيقة ))السكيك(( التي تساعد على وجود الظل خاصة في فصل الصيف، إضافة إلى العديد من الزخارف الهندسية التي تزين منازل الحي، فتجذب أنظار الزوار، وتترك انطباعات عميقة على من يشاهدها.&lt;/p&gt;&lt;p&gt;ونظرا للأهمية التاريخية والعمرانية للبستكية، حرصت بلدية دبي على إحياء هذه المنطقة وترميمها بما يتناسب مع المشهد الجمالي لمدينة دبي، فقد تم ترميم أكثر من ) 57 ( مبنى تقريبا، ثم استثمرت هذه المباني لتكون كمراكز للعديد من الأنشطة الثقافية والفنية المتناغمة مع مدينة دبي العصرية ودورها الحضاري، مثل إقامة: متاحف، معارض، فنادق، جمعيات، مطاعم، مقاهي، أسواق تراثية، فعاليات وأنشطة ثقافية... سيظل حي البستكية علامة بارزة وشاهد&amp;nbsp; حضاري تاريخي على تميز فن العمارة التراثية المحلية في دبي، وسيبقى منطقة جذب سياحي&amp;nbsp; &amp;nbsp;وثقافي للزوار من مختلف أنحاء العالم.&lt;br&gt;&lt;/p&gt;', ''),
(50, 1, 'كتابات غير صالحة للنشر', '', ''),
(50, 2, 'كتابات غير صالحة للنشر', '&lt;p&gt;هل تعلم أن كتابة الشخص ليومياته تساعده على التخلص من القلق وتقلبات المزاج وتحسن من صحته الجسدية والنفسية؟ هل جربت كتابة يومياتك وأفرغت جميع مشاعرك على الورق؟ ثم قرأتها مرة أخرى لتكتشف أمور لم تكن مدركها أو لتنظر الى الاحداث بنظرة مختلفة!! أن لم تكن قد فعلت ذلك من قبل،&amp;nbsp; &amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;فيمكنك البدء الآن.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;لا شرط أن تكون الكتابة متكاملة ووافية لكل الشروط ولكن أن نكتب بدون أن نفكر بنتائج الكتابة، أو إلى أين سننتهي ،لأنها كتابات غرضها التواصل مع النفس والنظر الى الأمور بنظرة حيادية بعيدة عن الانفعالات وإن كانت هذه الانفعالات سلبية أو إيجابية.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أن الكتابة اليومية والتفريغ لمشاعرنا على الورق بكل صراحة ومن دون أي رتوش، تزييف أو تزيين تساعدنا على التخلص من المشاعر السلبية وتقلبات المزاج التي قد تحصل لنا خال اليوم، والتي قد تكون من أمور لا ع اقة لنا بها، فقد ل نسمع خبرا في التلفزيون أو نقرا خبرا مؤلما في الجريدة أو حدثا ما، قد يضايقنا احدكم في الطريق من خال السياقة المتهورة، أو قد نواجه بعض المشاكل في العمل، الأصدقاء أو العائلة وغيرها من الأمور قد نحسبها أمور تحصل وننساها. ولكن مشاعرنا السلبية لطالما لا&amp;nbsp; ترحل من داخلنا ما لم نتعامل معها ونتخلص منها نهائيا، فما نقوم به من ت افي الموضوع ل أو تناسيته هو طمس هذه المشاعر في داخلنا وبالتالي ومع مرور الزمن تتراكم هذه المشاعر لتنفجر في يوم ما لسبب بسيط جدا وذلك لأنها تكون في أوجها وتحتاج الى التنفس خارج جسدنا.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;لذا إن خصصت وقتاً قلياً كل يوم لنقل أحداث يوميك على الورق وتعاملت مع مشاعرك بكل حرية وفسحت لها المجال بالتعبير الكامل من دون تحفظات، ستكتشف حينها أن هنالك الكثير من هذه المشاعر الغاضبة والسعيدة وعليه فيمكنك التعامل مع المشاعر الغاضبة والتخلص منها والاحتفاظ بالمشاعر الإيجابية السعيدة والعمل على تثبيتها. قد تواجه بعض الصعوبات في البداية وقد لا يمكنك أن تختزل مشاعرك على الورق وأن تنقل التعبير الكامل لمشاعرك أو هنالك تضارب بين ما تريد نقله على الورق وما تفكر به.&lt;/p&gt;&lt;p&gt;وهذا طبيعي، فهذا الصراع بين العقل الواعي والعقل الغير واعي فأكثر الأشخاص يمرون بهذا وخصوصا عندما يكون هنالك خوفاً من مواجهة الواقع أو يقومون بأعمال لا يرغبون القيام بها ويكون آخر النهار بالنسبة لهم هو مواجهة الواقع ومواجهة الواقع مع نفسك في بعض الأحيان أصعب كثيراً من مواجهة الآخرين لأنك مهما حاولت أن تكبت تلك المشاعر فإنها ستطفو إلى السطح وت احقك إلى أن تواجهها ل وتتعامل معه.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;اذا كنت ممن لم يكتبوا يومياتهم من قبل فلربما تواجه بعض الصعوبات وما عليك إلا أن تجلس في مكان محدد هادئ و تبدأ بالكتابة من دون تفكير مسبق حتى وإن بدأت بكتابة مقولة سمعتها، بيت شعر قرأته أو تذكرته هذا اليوم ودع الأفكار تتسلسل إليك من دون بذل جهد كبير فيما تريد كتابته، فالكتابة الصادقة تأتي مباشرةً من دون تخطيط مسبق لها. لا تتوقف حتى لو استمريت في كتابة أمور أخرى، فكلها تعبر عن ما في داخلك. الكتابة اليومية قد تكون عن يوم ممتع قضيته وتسطر تلك المشاعر على الورق لتثبت هذه المشاعر في عقلك ال اوعي وتكون معك على الدوام.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;أما الكتابة عن ما يزعجك فذلك يساعدك على النظر إلى الأمور بشكل مختلف وإيجاد معان جديدة للأمور والنظر إليها بزاوية أخرى تساعدك على مواجهة العواطف المكبوتة بدل الهروب منها.&lt;/p&gt;&lt;p&gt;اليوميات الغير صالحة للنشر هي يوميات صالحة لمواجهة النفس ولتنقيتها من كل المشاعر السلبية ومواجهة الأمور التي نهرب منها، كما وتساعدنا على تثبيت المشاعر الإيجابية التي عشناها لتكون معنا على الدوام.&lt;br&gt;&lt;br&gt;&lt;/p&gt;', ''),
(51, 1, 'إبراهيم الهاشمي:', '', ''),
(51, 2, 'إبراهيم الهاشمي:', '&lt;p&gt;&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&quot; الدولة تدعونا لأن نبدأ بالقراءة من الآن وألاّ نتوقف &quot;&lt;/b&gt;&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;إبراهيم الهاشمي، أحد الأقام الإماراتية البارزة نشأ في بيت مثقف لأم حديثها شعر وحكم وأمثال، وأب عصامي كان يستذكر معه دروسه، وجد ذو خط جميل، وخال مثقف يقرأ ويجمع أبناء العائلة ليسرد عليهم الحكايا ويقرأ عليهم الشعر بنبرة صوت أخاذ يثبت في الذاكرة. في بيئة مثقفة كهذه كان من الطبيعي أن تنتج رجاً مثقفاً وشاعراً.&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;/p&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;br&gt;&lt;/div&gt;&lt;div&gt;&lt;b&gt;بداية نود التعرف على إبراهيم الهاشمي&amp;nbsp; وكيف استهوته القراءة؟&lt;/b&gt;&lt;br&gt;&lt;/div&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;أولاً هي طبيعة الأسرة، عندما تنشأ كطفل صغير تبدأ في محاكاة أسرتك. فحين أحصل على كتاب أهدته لي خالتي عند عودتها من رحلة العمرة، هذا يشكل دافع لي لأقرأ، وخالي أيضاً كان يقرأ لنا بصوت رخيم وجميل ومرنم وكان لديه مكتبة، لذا الأسرة كانت بشكل عام تحب القراءة والكتابة وكانت الأسرة أغلبها ذات خطوط يد جميلة. الأندية أيضاً في ذلك الوقت كانت مركز إشعاع ثقافي وليست لكرة القدم فقط، مثل الأهلي والوصل والنصر والشباب كانت تصدر مجات ثقافية اجتماعية أكثر من رياضية، في بيئة كتلك تنشأ وتحاكي من هم أكبر منك، كانت خالتي تحضر لي كتب عندما تعود من الحج ومن هنا بدأت رحلتي مع القراءة، بدأت بالقراءة محاكاةً لمن هم أكبر مني من أفراد العائلة، وأصبح هناك تنافس ما بين أطفال العائلة، فالأسرة لها دور كبير في نشأة الطفل.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;الدور الثاني كان للمدرسة، فأول مقال نشر لي على سبيل المثال لم أرسله أنا شخصياً بل أرسله أستاذ لي وكان عن الوحدة العربية وتفاجأت بالمقال منشوراً في إحدى الصحف، ومن ثم بدأت أكتب بتشجيع منه.&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما الذي جعلك تحتفظ بهذه الكتب؟&lt;/b&gt;&lt;br&gt;الأسرة بشكل عام، وأنت كفرد تحاكي الآخرين، فعندما ترى الناس من حولك تقرأ وتحب المطالعة والمعرفة كفعل يومي يكون لديك شغف بالقراءة، الشغف ليس فقط أن تقرأ بل أن يكون لديك مكتبة فتقول لزم ائك لدي مكتبة ل ويمكنكم أن تستعيروا منها، فلدي الكتاب الف اني ل للكاتب الف اني عندها يكون لديك نوع من التميز ل من بينهم لأنك تقرأ وهذا يساعد على أن تكون مختلفاً في النشاط المدرسي فيدعوك الأستاذ لأن تقرأ نصاً أو قصيدةً على زم ائك وهذا يخلق لديك ل المزيد من الشغف للمعرفة .&amp;nbsp;&lt;/p&gt;&lt;p&gt;شخصياً ورثت هذا الشغف من والدتي، فرأيت أنها تحتفظ بأشياء صغيرة جداً، فعلى سبيل المثال احتفظت بأشياء كانت لي عندما كنت طفاً، تحتفظ بالصور القديمة كصور أفراد العائلة جميعهم وبين الصور صورة للشيخ زايد رحمه الله لم تنشر في أي كتاب أو مجلة، إلى جانب قصاصات ورق لقصائد بخط اليد لشعراء مختلفين، اليوم لدي أرشيف لا يملكه الكثيرون، لدي ما نشر من مقابات مع الشعراء والكتاب والمقابات التي أجريت مع بعض أفراد العائلة والأصدقاء جميعها&amp;nbsp; محفوظة ومأرشفة لدي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما الذي يميز مكتبتك عن غيرها؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;مكتبتي تحوي على أشياء أخرى غير الكتب، وأنا ولله الحمد قارئ في جميع المجالات فعندما تقرأ في مجالات مختلفة كالفلسفة والتاريخ والإعجاز القرآني والعلوم واللغة وغيرها جميعها تساعدك في تطوير قدراتك ورصيدك المعرفي واللغوي وتساهم في أن تكون كاتباً جيداً وتكون كتاباتك أقوى وأنضج. لكن مكتبتي تتميز بأنها تحتوي على أرشيف لمعظم الكُتّاب من الإمارات، خصوصاً من تعجبني كتاباتهم، قد لا يملكونه هم شخصياً وبعضها بخط اليد حصلت عليها خال بعض الأمسيات مثل الشاعر محمد بن حاضر - رحمه الله - أصدرت عنه كتابين يتناولان سيرته و مختارات من قصائده الفصيحة والعامية من أرشيف جمعته أنا شخصياً، كذلك يضم أرشيفي كتابات لمحمد المر والدكتور سعيد حارب وإبراهيم بوملحه والشاعر عارف الخاجة والأستاذ عبدالغفار حسين وغيرهم من الشعراء والكتاب. أنا أقرأ ثاث جرائد يومياً والذي تتفق ذائقتي معه، بالإضافة إلى الدراسات والمقالات التي أحفظها للمستقبل، فمثاً في اتحاد كُتّاب وأدباء الإمارات نصدر مجلة قدمنا فيها » قاف « متخصصة في الشعر اسمها عدة ملفات لعدد من الشعراء والكتاب بالاستعانة بأرشيفي الشخصي. كذلك أحتفظ بالمجات، حيث أحاول دائماً الحصول على أول عدد لأي مجل أو إصدار أو مطبوعة تصدر في الإمارات ونملك أنا وزميلي سعيد حمدان أكبر مجموعة صادرة في الإمارات للمطبوعات الإماراتية.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;لماذا العدد الأول؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أنا بالعادة أي مجلة أتابعها أطلع على أول خمسة عشر عدد منها وإذا استشعرت أنني أستفيد منها أستمر في قراءتها لذا أحرص على الحصول على العدد الأول وهي نوع من الهواية مثلما يحب البعض الاحتفاظ بالطوابع والبطاقات، أحتفظ أنا بالعدد الأول من المجات ولدي حالياً عدد كبير من مختلف المجات مثل زهرة الخليج، والشروق، وماجد، والأزمنة العربية، والمسيرة، والخليج، والبيان، وأوراق، وغيرهم من الإصدارات التي أصدرت من جامعة الإمارات ومن الكليات وإصدارات أخرى صدر منها عدد واحد فقط فأصبح ذلك شغف لدي لازلت مستمراً فيه. أعتقد أن هذه المطبوعات جزء من تاريخ وثقافة الدولة وإذا أردت أن تكون جزءاً من هذه الثقافة يجب أن تكون متابعاً لهذه الإصدارات وهذا أيضاً جزءٌ من المحاكاة الذي يتحول فيما بعد إلى هاجس، المجموعة التي ترافقك تؤثر فيك، فأثناء دراستي الجامعية بجامعة الإمارات كنت مع زم ائي الّذين تركوا بصمة في هذا المجال منهم: ل الشاعر أحمد راشد ثاني، والشاعر ظاعن شاهين، والشاعر خالد البدور، والمسرحي ناجي الحاي والمسرحي إسماعيل عبدالله وغيرهم من الكُتّاب والأدباء والشعراء، وذلك يخلق جو أدبي وتنافسي فيما تقرأ وتكتب. كنا مجموعة تشربت الثقافة حتى أصبحت جزءاً من حياتنا اليومية.&lt;/p&gt;&lt;p&gt;&lt;b&gt;هل هناك تنافس اليوم بينك وبين آخرين من هواة جمع الكتب والإصدارات؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أسمع عن الكثير ممن لديهم هذا الهوس لجمع الإصدارات وغيرها من المطبوعات فهناك على سبيل المثال الأستاذ عبدالكريم في رأس الخيمة مهتم بجمع الإصدارات، وبعضهم يتواصل معي للحصول على نسخ من بعض الاصدارات وهذا يسعدني لأنه نوع من نشر المعرفة وهذه ثروة تستفيد منها الدولة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;هذا الفكر الذي تملكه من ناحية مشاركة الآخرين المعلومات التي تملكها من أين أتت؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;المنافسة موجودة ولكن عندما يأتي إلي باحث يبحث عن معلومة أو وثيقة معينة لا أتراجع أبداً في إعطاءه هذه المعلومة وأرشده أو أزوده بصورة عن الوثيقة والمرجع الذي يحتاجه، وأنا على قناعة تامة بأن هذه المطبوعات والوثائق يجب أن تصور وتتوفر للباحثين في مراكز الأبحاث.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;هل هناك صعوبة من قبل الشركات والمراكز لتحويل هذه المصادر إلى نسخ إلكترونية؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أكثر مركز ساعدني من هذه الناحية وتعاون معي بشكل كبير ومميز هو مركز جمعة الماجد. فالمعلومة يجب أن تتوفر للباحثين ويكون الحصول عليها سه اً لذا هذا الأمر يعتمد على ل جهود المؤسسات في الحصول على المعلومة وتوفيرها للباحثين.&lt;/p&gt;&lt;p&gt;&lt;b&gt;نعود لموضوع العائلة، ما هو باعتقادك الذي اختلف عن السابق؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;في السابق كان العنصر السمعي أي الثقافة الشفاهية هي السائدة، والآن طريقة التعاطي مع الثقافة اختلفت. طريقة تأسيسنا كانت مختلفة اعتمدت على القرآن من ناحية اللغة والنطق مما جعل اللغة أرقى وذات تأثير مهم. تأثري بوالدتي - رحمها الله - كان أيضاً مهم جداً في تكويني فهي موسوعة متنقلة، بالإضافة إلى شمولية معارفها ومحفوظاتها فهي تحتفظ بالتفاصيل الدقيقة كالصور والوثائق والمتعلقات الشخصية مما جعلني أحب الاحتفاظ بالأشياء مثلها ومن ثم التدوين والتوثيق.&lt;/p&gt;&lt;p&gt;الثقافة في السابق كانت ثقافة شفاهية، فهناك تحدي وتنافس من ناحية حفظ القصائد. والدتي كانت تحفظ الكثير من القصائد والأهازيج الشعبية والأمثال والحكايا والتاريخ، كما كانت ثقافة أجدادنا عالية يتعاطون مع الثقافة بحب وشغف واحترام ويتعلمون منها من خال الممارسة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;هل كمية المعلومات التي نحصل عليها اليوم مقارنة بالسابق تؤثر على الثقافة حالياً؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;كم المعلومات أصبح هائاً مع التطور التكنولوجي، لكن اليوم التخصص أصبح ملحاً و مطلوباً، نحتاج إلى المتخصصين في الفيزياء، وعلوم الفضاء والكيمياء، نحتاج إلى المتخصصين في كل مجال للنهوض بالمجتمع لأن تدفق المعلومات اليوم أصبح هائاً ولا تستطيع أن تخفي شيئاً.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;بخصوص عام القراءة، هل تعتقد بأن المسابقات والمبادرات تشجع على القراءة؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أنا أعتقد بأن الحكومة عندما قررت عام 2016 عاماً للقراءة فهذا دليل وعي كبير من الحكومة بأهمية القراءة. هناك العديد من الخطط الاستراتيجية التي وضعتها الحكومة لتطوير الدولة وتملك الوعي بأن عليها تنويع مصادر الدخل وأنها لا تستطيع الاستمرار بهذا التطوير إلا من خال العلم وهذا العلم لا يتأتى إلا بالقراءة. القراءة هي مفتاح المعرفة وأول خطواته وهو البداية فقط، الدولة لا تطلب منا أن نقرأ في هذه السنة فقط ثم نتوقف الدولة تدعونا لأن نبدأ من الآن ولا نتوقف أبداً عن القراءة، وطلب العلم والمعرفة. اليوم القراءة غير محصورة في مجال معين كالقصة والرواية، دع كل شخص يقرأ في مجاله، في الفضاء، والسيارات، والاقتصاد كلٌّ منا يقرأ ما يهمه لنحقق التخصص. القراءة تنمي الوعي والثقافة و 2016 عام البداية وستأتينا أعوام ستقود الدولة فيها مسيرة تحاكي به النماذج الحضارية في العالم.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أدعوا شركات القطاع الخاص والقطاعات الأخرى إلى دعم هذه المبادرة والمشاركة فيها لنشر الثقافة، يجب أن نعلم أطفالنا كيف يصادقوا الكتب ووسائل الاعام يجب أن تسلط الضوء على المبدعين وصانعي الإبداع في الدولة وعرض نماذج أعمال الشباب وإبداعاتهم حتى يحاكوا هؤلاء المبدعين. سياسات الحكومة تتطلع لأن تقفز إلى المستقبل بكل ثبات وقوة، ويجب أن نقدم نحن التضحيات لنشر الثقافة، والقطاع الخاص يجب أنيكون له دور ليرد الجميل لهذا البلد الذي لا يفرض عليه الضرائب ويقدم له التسهي ات حتى يكون ل المستقبل مشرقاً لأبنائنا وأحفادنا وهذا جزء من بناء الإنسان. بيل غيتس على سبيل المثال تبرع&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;ب 90 % من ثروته للتعليم والتطوير وهناك أمثلة من دولة الإمارات مثل جمعة الماجد، وعبدالله الغرير، لكن المطلوب فعل مجتمعي أكبر من رجال الأعمال والقطاع الخاص، يجب أن تكون هناك ثقافة خدمة المجتمع لدي القطاع الخاص، و با شك ستثمن وتقدر الحكومة هذه المبادرات كما دعمت ذلك القطاع دائماً لينمو ويتطور .&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;دولة الإمارات تحاول الآن أن تحول اقتصادها إلى اقتصاد مبني على المعرفة، كيف سيسهم عام القراءة في ذلك؟&amp;nbsp;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;عندما يكون هناك هذا النوع من المبادرات سوف تبنى المعرفة با شك، مع العلم بأن المعرفة ليست فقط بالقراءة بل في مختلف المجالات والفنون أيضاً ولكن القراءة هي الأساس، وتلك المبادرات تحتاج أيضاً إلى تكاتف مجتمعي لتحقيقها من مختلف القطاعات سواءً القطاع الخاص أو العام أو أفراد المجتمع بشكل عام.&lt;/p&gt;&lt;p&gt;&lt;b&gt;مكتبة الشيخ محمد بن راشد التي أعلن عنها في بداية هذا العام، ما رأيك بهذا المشروع؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;متطور جداً يواكب آخر المستجدات التكنولوجية وسيساهم با شك في تطوير العقول وتنمية المعرفة، فهي صرح حضاري به مكتبة تحوي كتب ورقية وكتب مسموعة ومحمولة على أجهزة الحاسوب، كما ستقوم بطباعة ونشر الكتب والمعرفة وتوفير أماكن للقراءة والمصادر ال ازمة ل للبحث وتوفير أماكن لذوي الاحتياجات الخاصة ما يناسبهم أيضاً، وستسبب با جدال نقلة حضارية. الشيخ محمد يمثل نموذج للحاكم العربي الذي يفكر في مستقبل وطنه وأمته، يفكر فيما بعد النفط، يرى أن بناء الإنسان هي البناء الحقيقي، أن المباني يمكن أن تزول لكن المعرفة لا تزول ولا زلنا اليوم بعد مئات السنين نتذكر أف اطون ل وهموميروس والفارابي والجاحظ والمتنبي، بريطانيا لا تزال تحتفل بشكسبير لأنه يمثل الفكر والثقافة لذا تفتخر به باده. المعرفة والثقافة هي أساس كل شيء، وهذا المشروع دليل وعي بأهمية الثقافة في بناء كل شيء .&lt;/p&gt;&lt;p&gt;&lt;b&gt;هل تشعر أحياناً بأنك تريد إجبار أبنائك على اتباع نهجك؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أود ذلك ولكن لكل شخص شخصيته، ابنتي ريم على سبيل المثال اتبعت نهجي فهي تحب القراءة وشغوفة بها بينما ابني عمر يحب عالم تقنية السيارات وعلى اطاع واسع فيه، لم أجبر أبنائي على دراسة شيء معين ولم أجبرهم على قراءة معينة، وعمر أيضاً محب للشعر ويتابعهه، ولكن حبه للسيارات دفعه لدراسة الهندسة الميكانيكية والإلكترونية. أما هاشم فهو يحب الأرقام والتكنولوجيا فدرس المالية والتدقيق ويحب القراءة في مجاله بشكل متخصص. قدمت لهم الأدوات لكن لم أجبرهم فأنا مؤمن بأن كل إنسان له شخصيته ومجاله.&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هي نصيحتك لقارئ مجلة مذكرات الإمارات؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;القراءة أول مفتاح للمعرفة وأول كلمة نزلت في والقراءة هي الوسيلة للوصول » اقرأ « القرآن هي لكل هدف، لأن تصبح صانع أو فيزيائي أو شاعر أو كاتب قصة أو ناقد أو قائد تحتاج الى المعرفة، فالمعرفة تأتي بالقراءة وبدونها أنت لا تساوي شيئاً. المعرفة هي الوسيلة لتحويلك لإنسان آخر، والقراءة هي التي تفتح لك أفق المستقبل وتسمح لك بالتطور. إذا لم تقرأ ستبقى في مكانك مهما تغير شكلك ولبسك، فأمسك بأول مفتاح وافتح كل الأبواب والنوافذ وانطلق.&lt;/p&gt;&lt;p&gt;القراءة تجعلك إنساناً قوياً تملك الوعي والمعرفة والمستقبل .&lt;br&gt;&lt;/p&gt;', ''),
(52, 1, 'سعادة أحمد بن ركاض العامري', '', ''),
(52, 2, 'سعادة أحمد بن ركاض العامري', '&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&quot; القراءة هي المنارة الكاشفة لطرق الغد &quot;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;البطاقة الشخصية&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يحمل سعادة أحمد بن ركاض العامري درجة البكالوريوس في إدارة الأعمال من كلية ستراير بواشنطن في الولايات المتحدة الأمريكية، ودرجة الماجستير التنفيذي في إدارة الأعمال من جامعة الشارقة. وبعد التخرج عمل في مجالات متعددة، غلب عليها الشأن المالي، ولكن اهتماماته الثقافية أخذت حيّزاً مهماً من وقته، ففي الشارقة لا يمكنك أن تحيا من دون الثقافة، والاستمتاع بالقراءة، وتذوق الفنون.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وقبل عدة أعوام، تشرف سعادة أحمد بن ركاض العامري باختياريه من قبل صاحب السمو الشيخ الدكتور سلطان بن محمد القاسمي، عضو المجلس الأعلى حاكم الشارقة، ليكون مديراً لمعرض الشارقة&amp;nbsp; الدولي للكتاب، ومن ثم تولى قبل عامين رئاسة هيئة الشارقة للكتاب، للعمل تحت رعاية سموه من أجل تعزيز مكانة الشارقة الثقافية والأدبية، على الساحتين الإقليمية والدولية، ودعم وتشجيع جميع المبادرات التي من شأنها أن تسهم في الارتقاء بالإنسان إلى أعلى مراتب ومواقع الوعي والمعرفة والعلم.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;أهمية معرض الشارقة الدولي للكتاب في منظور العامري&lt;/b&gt;&lt;/p&gt;&lt;p&gt;معرض الشارقة الدولي للكتاب هو أحد المحافل الثقافية والأدبية الكبيرة، التي ساهمت في تشكيل وجداننا الثقافي والأدبي قبل أكثر من ث اثين عاماً، ل كما أن المعرض يحظى بمكانة مرموقة على المستويين المحلي والعالمي، وتلك المكانة لم تأتِ من فراغ، بل جاءت نتيجة مستحقة لما بذله صاحب السمو حاكم الشارقة من جهود ومتابعة على مدار أربعة عقود لرفعة الإنسان بالثقافة والكتاب، وكان وما زال جُل همه أن يؤسس لأجيال تستقي العلم والمعرفة، وتؤمن بأنها السبيل الوحيد ل ارتقاء والعلو.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وقد حقق المعرض العديد من الإنجازات، التي يعود الفضل فيها إلى رؤية وتوجيهات سموه الهادفة إلى إحداث تأثير فعال ومستمر في الاهتمام بالكتاب، وجعل القراءة عادة يهتم بها أفراد الأسرة كافة، للنهوض بواقعهم، وتنمية مهاراتهم، وفهم&amp;nbsp; الماضي لمعايشة الحاضر والاستعداد للمستقبل، والحمد لله المعرض اليوم في طريقه لأن يكون ثالث أكبر معرض بالعالم، وأصبح على أجندة كل مثقف ومحب للكتاب، ليس في الشارقة ودولة الإمارات فحسب، وإنما في المنطقة والعالم أيضاً.&lt;/p&gt;&lt;p&gt;&lt;b&gt;دعم صاحب السمو حاكم الشارقة للمثقفين والكتّاب&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يحدثنا سعادته عن دعم سمو الحاكم قائاً: لا يخفى على أحد الدعم الكبير وغير المحدود الذي&amp;nbsp; يقدمه صاحب السمو الشيخ الدكتور سلطان بن محمد القاسمي، عضو المجلس الأعلى حاكم الشارقة، للمثقفين والأدباء بل وإلى دور النشر كذلك، وعلى سبيل المثال وتنفيذاً لتوجيهات سموه، ينظم معرض الشارقة الدولي للكتاب سنوياً عدداً من الجوائز، ومنها جائزة شخصية العام الثقافية التي تمنح بناءً على معايير محددة تقاس من خ الها نسبة الإنجازات التي حققها ل المرشح لنيل هذه الجائزة، وجائزة الشارقة للكتاب الإماراتي، وجائزة أفضل كتاب إماراتي لمؤلف إماراتي )في مجال الإبداع(، وجائزة أفضل كتاب إماراتي )في مجال الدراسات(، وجائزة أفضل كتاب إمارتي )مترجم عن الإمارات(، والعديد من الجوائز الأخرى.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وهناك أيضاً صندوق منحة معرض الشارقة الدولي للكتاب للترجمة والحقوق، الذي أُنشىء في عام 2011 احتفالاً بالذكرى ال 30 لانطاق معرض الشارقة الدولي للكتاب، ويهدف الصندوق المدعوم من حكومة الشارقة إلى تشجيع ترجمة الكتب لرفد الثقافة العربية والعالمية بالمعارف والعلوم التي تخص عالمنا الثقافي المعاصر بما يدعم الترجمة والتواصل الشبكي والبرامج التعليمية، وساهم الصندوق منذ إنشائه في دعم ترجمة مئات الكتب من وإلى العديد من اللغات.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;عام القراءة&lt;/b&gt; &lt;b&gt;2&lt;/b&gt;&lt;/p&gt;&lt;p&gt;مبادرة 2016 عاماً للقراءة في دولة الإمارات، التي أطلقها صاحب السمو الشيخ خليفة بن زايد آل نهيان، رئيس الدولة، حفظه الله، تعكس النظرة الثاقبة لقيادتنا الرشيدة، وحرصها على تزويد مواطني دولة الإمارات العربية المتحدة والمقيمين فيها بالمعارف والعلوم، كما تعكس إيمان قيادتنا الحكيمة بأهمية القراءة والثقافة في الوصول إلى مجتمع المعرفة الذي ننشده، وقد ضاعفت هذه المبادرة من مسؤولياتنا في هذا الإطار، حيث انتهينا مؤخراَ، من إعداد خطة متكاملة تتضمن العديد من الفعاليات والمبادرات تماشياً مع هذه » نادي القراءة « المبادرة الرائدة، ومن بينها مبادرة التي أطلقناها في فبراير الماضي، والتي تعتبر أول ناد حكومي للقراءة في منطقة الشرق الأوسط، وتهدف إلى جعل القراءة عادة وأسلوب حياة للتطور والتنمية بين الموظفين وذلك من خال منح كل موظف في كافة إدارات هيئة الشارقة للكتاب وأقسامها نصف ساعة يومياً )ما يعادل ساعتين ونصف الساعة أسبوعياً( خال ساعات الدوام الرسمي لممارسة القراءة والاطاع.&lt;/p&gt;&lt;p&gt;كما شرعنا في تنظيم جلسات فكرية ثقافية بشكل دوري وعلى مدار هذا العام في مكتبة الشارقة العامة، نستضيف من خ الها نخبة من ل الكتاب والأدباء الإماراتيين والعالميين، لتسليط الضوء على تجاربهم في الحقل الثقافي والمعرفي، وقد وفقنا حتى الآن في استضافة كل من الكاتبة أهمية « الإماراتية فتحية النمر التي تحدثت عن والكاتبة والروائية الهندية ،» الكتاب للمؤلف والقارئ غاندانا روي، التي ناقشت موضوع أهمية الكتب والمكتبات ودورهما في بناء مجتمع المعرفة، واستضفنا كذلك الروائي السوداني أمير تاج السر .» الكتابة الروائية « في جلسة تناولت&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;أهم الكتب وأحبها لأحمد العامري&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أهتم كثيراً بقراءة مؤلفات صاحب السمو الشيخ الدكتور سلطان بن محمد القاسمي، عضو المجلس الأعلى حاكم الشارقة، التي أجد فيها توثيقاً دقيقاً لأهم الأحداث التاريخية التي شهدتها منطقة الخليج العربي بشكل خاص، والعالم العربي بشكل عام، إلى جانب عدد من الكتب المتنوعة الأخرى في مجالات الإدارة وتطوير الذات والفكر والأدب والثقافة. وأحب قراءة كتب السير الذاتية للشخصيات الإماراتية والعربية والعالمية التي حققت نجاحات بارزة في مجالاتها، كما أحرص على الاطاع على جديد الكتب المتعلّقة بصناعة النشر، وكيفية الوصول بالكتاب إلى القارىء، والمحافظة على القراءة في زمن التطورات التقنية.&lt;/p&gt;&lt;p&gt;&lt;b&gt;كلمة توجهونها إلى الشباب في الإمارات والعالم.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أقتبس لهم مقولة صاحب السمو حاكم الشارقة القراءة والاطاع سبيل الأمم لتحقيق « الخالدة فالقراءة تسهم بشكل كبير في ،» الرقي والازدهار بث الوعي ونشر التنوير والمعرفة، ونعتبرها المنارة الكاشفة لطرق الغد، والوسيلة التي تخطو بالإنسان نحو المستقبل، فعليكم بالقراءة والاطاع، واجعلوا الكتاب صديقكم الدائم في كل مكان تتواجدون فيه.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(53, 1, 'سيرة: رفيعة غباش عائشة البوسميط نهلة الفهد', '', ''),
(53, 2, 'سيرة: رفيعة غباش عائشة البوسميط نهلة الفهد', '&lt;p&gt;سيدة من سيّدات الإمارات المُلهمات، وإحدى صُنّاع الواقع الجميل في مدينة الأحام، امرأة شغوفة في كلّ أعمالها، وعبر مسيرتها الممتدة استطاعت أن تُترْجم رؤاها إلى واقع مُدهش وملموس جذبت إليهِ القريب والبعيد.&amp;nbsp;&lt;/p&gt;&lt;p&gt;غباش،،، هي أكاديمية بامتياز، وصاحبة علم وثقافة لا حُدود لهما، حاصلة على البكالوريوس في الطب ، والجراحة العامة من كلية الطبّ في القاهرة 1983 أنهت دراستها للدكتوراه في وبائيات الأمراض . النفسيّة من جامعة لندن 1992&lt;/p&gt;&lt;p&gt;الدكتورة/ رفيعة غباش هي أول طبيبة نفسيّة في دولة الإمارات، وأوّل امرأة شغلت بجدارة منصب رئيس جامعة إقليمية، ذلك حينما توّلت رئاسة جامعة الخليج العربي في مملكة البحرين، وفي عام 2011 صُنّفت غباش ضمن قائمة ال) 100 الأكثر عطاءً(من قبل مؤسسة &quot;عطاء النساء&quot;نظير مسيرتها الحافلة،&lt;/p&gt;&lt;p&gt;تُعدّ الدكتورة رفيعة الأكثر صدقاً في إبراز دور المرأة الإماراتية، فهي الصورة والصوت والرّوح، هي المرجع والذّاكرة والعنوان، وربّما تكفينا زيارة واحدة فقط لمتحفها &quot;متحف المرأة&quot; لنعي ونفهم سرّ ذلك الاهتمام الشّديد بتوثيق إبداع المرأة في الإمارات. ومتحفها الذي أسّستهُ بكلّ شغف ليس متحفاً عاديّاً؛ بل هو ذاكرة من عبق، وتاريخ أصيل، وعشق لا ينتهي للحرف والكلمة. وما من زائرٍ تجوّل في أرجاء ذلك المتحف إلاّ وقد أُعجب بفكرها وإبداعها الذي برز في قاعة الشاعرة عوشة بنت خليفة السويدي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وليس هذا فحسب، فهي أيضاً كاتبة تعشق القلم والورقة، لها أربعون بحثاً في الطب وخمسة كتب في الثقافة الأول بعنوان:- &quot;رسائل حزينة: دراسة لحالات الاكتئاب في مجتمع عربي، رسائل نفسية واجتماعية: الاكتئاب &quot;، والثاني:- &quot;الطبّ في الإمارات: الجذور والممارسة&quot;، والثالث:- &quot;أوراق تاريخية من&amp;nbsp; حياة الشاعر حسين بن ناصر آل لوتاه&quot; ٠٨، والرابع:- ٢ &quot;الأعمال الكاملة والسيرة الذاتية للشاعرة عوشة بنت خليفة السويدي ٠١&quot;، والأخير:- &quot;موسوعة ٢ ٢ .&quot; مجوهرات نساء الإمارات في بيت البنات&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;غباش،،، امرأة تفخر بها الإمارات قبل أن نفخر بها&amp;nbsp; حنُ معشر النّساء، هي إضاءة جميلة لواقع جميل يستحق أن نحتفظ بهِ.&lt;/p&gt;&lt;p&gt;&lt;b&gt;عائشة البوسميط&lt;/b&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;هي ابنة الإمارات وخرّيجة إع امية، ل أكاديميّة حاملة لدرجة الدكتوراه مع مرتبة الشرف الأولى في الإعام من جامعة القاهرة، كاتبة صدر لها مؤخراً كتاب الاتصال الحكومي المؤسسي، شاعرة ذات إحساس شجيّ وصاحبة مجموعات شعريّة، هي شهرزاد العائدة إلينا بكلّ قوة وحنين، وهي سيّدة الرفض الأخير.&amp;nbsp;&lt;/p&gt;&lt;p&gt;البوسميط،، امرأة ذات تركيبة مختلفة؛ هي إنسانيّة بالدرجة الأولى احتضنت بين ذراعيها ريم ثمّ حصّة لتكون بذلك أمّاً حاضنة بكلّ فخر وجدارة. وحيثما التفت تجدها في كلّ مكان، فتراها تُلقي بأشعارها وخواطرها في الأمسيات الشعريّة، وحيناً تراها تناقش قضايا اجتماعية فتطلّ عليك صباحاً من خال بثّ رائع ومباشر عبر شاشات التلفاز، وحيناً آخر تراها حاضرة بكلّ قوّة في الأنشطة الرياضية تُزاحم الآخرين لتُغطّي أحداث مجلس دُبي الرياضي بعدستها الهاتفيّة الصّغيرة فتنقلك إلى عالمها الصغير الممتلئ والجميل.&amp;nbsp;&lt;/p&gt;&lt;p&gt;عائشة امرأة بسيطة جداً، تعيشُ الحياة لأجل الحياة تتخطّى الحواجز بكلّ إرادة وعزيمة، وتؤدّي أدوارها بكلّ توازن واتّزان فهي أمّ بالدرجة الأولى، وأستاذة جامعيّة، وموظفة حكومية، وإع امية ل مثقّفة، ومتحدّثة لبقة، وشاعرة رقيقة. هي نموذج رائع وصورة جميلة للمرأة الإماراتية.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;نهلة الفهد&lt;/b&gt;&lt;/p&gt;&lt;p&gt;شابّة إماراتيّة طموحة جداً، عشِقت الوقوف خلف الكاميرا حتّى عشِقتها هي الأُخرى، وظلّت فترة من الزمن تقدّم أعمالها وتُخرجها بكلّ حبّ من خلف عدسة الكاميرات، ولذلك ربّما لم نلمح صورة نهلة في بداياتها، لكن ذاع صيت اسمها مع صيت أعمالها الفنيّة التي قدّمتها.&amp;nbsp;&lt;/p&gt;&lt;p&gt;نهلة،،، هي خرّيجة جامعة دُبي، تخصّصت في الاتصال والإعام الجماهيري، لكن ميولها للإخراج كان أكثر من أيّ شيءٍ آخر، فظلّت تُخرج أعمالها الفنيّة حتّى تجاوزت الستين عماً لشخصيات غنائية لمعت أسماؤها في الوسط الفنّي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وبذلك كان للفهد نصيب الأسد؛ فقد عُدّت نهلة في تلك الفترة الزمينة المخرجة الوحيدة في الإمارات والخليج العربي كما ذكرت بعض المصادر. لكن براعة نهلة ومقدرتها التصويريّة ظهرت أكثر في إخراجها ل أعمال الدرامية في الآونة الأخيرة، فقد ل كشفت لنا أعمالها عن صورة أُخرى رائعة لها، رغم أنّها قليلة الظهور، نهلة في الواقع شخصيّة مرحة&amp;nbsp; جداً، ما إن تكاد أن تتعرّف عليها حتّى تشعر كأنّك تعرفها منذ زمن. وكلّما اقتربت منها أدركت شدّة حبّها لتفاصيل عملها، فتراها تُرافق زم ائها حتّى ل في أصغر المشاهد.&amp;nbsp;&lt;br&gt;الفهد،،، هي قارئة جيّدة لكلّ النصوص الدرامية التي باتت تُعرض عليها، ودقيقة جداً في انتقاء النصّ المميّز، وتظهر عنايتها حتّى في عنوان النصّ الذي ستُخرجهُ، فتُحاول جاهدة أن يكون ذلك العنوان جديداً فريداً غير مستهلك.&amp;nbsp;&lt;/p&gt;&lt;p&gt;تُعدّ نهلة إحدى المخرجات الأوائل والق ائل اللواتي ل مَضيْن على سلّم النجاح بخطوات ثابتة متّزنة، هي صاحبة الفيلم الوثائقي &quot;حجاب&quot; المُتأهّل لجائزة الأوسكار، والذي عُرض في عدّة دول منها: لندن، وإيرلندا، والولايات المتحدة الأميركية، وهي صاحبة شركة إنتاج عُرفت باسم &quot;بيوند استوديو&quot;.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(54, 1, 'الدكتور حمد الحمادي', '&lt;p&gt;ول عمل روائي درامي عن الإمارات » ريتاج «&lt;/p&gt;&lt;p&gt;الدكتور حمد الحمادي أحد الكُتّاب البارزين في دولة الإمارات، درس هندسة الحاسب الآلي من جامعة خليفة للعلوم والتكنولوجيا، وحصل على شهادة الماجستير في هندسة الأنظمة من جامعة ملبورن باستراليا، يحمل الحمادي شهادة الدكتوراه في هندسة الذكاء الاصطناعي من نفس الجامعة بأستراليا، أعلن مؤخراً عن روايته )ريتاج( والتي سوف تتحول إلى مسلسل تلفزيوني يضم مجموعة من الممثلين من دولة الإمارات ودول الخليج، يساراً جهة « للدكتور حمد إصداران آخران وهما .» لأجل غيث « و رواية » الق&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;كيف دخل الدكتور حمد الحمادي مجال الأدب والكتابة؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;حمد الحمادي إماراتي يحاول تقديم ما يستطيع تقديمه ليكون مؤثراً بإيجابية في المجتمع، يقول: قمت بالتدريس الجامعي خ ال فترة جميلة من ل حياتي، فخور جداً بالط اب الذين قمت بتدريسهم ل خ ال تلك الفترة القصيرة والذين أصبحوا يساهمون ل في تنمية المجتمع من خ ال وظائفهم في مختلف ل المؤسسات. حالياً أعمل في مجال الاتصال والابتكار والمشاريع الخاصة في المكتب التنفيذي لصاحب السمو الشيخ محمد بن راشد آل مكتوم نائب رئيس الدولة، رئيس مجلس الوزراء، حاكم دبي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;دخولي مجال الأدب والكتابة جاء بمحض الصدفة، فلم يكن هذا المجال ضمن خططي الحياتية أبداً. بدأت عن طريق وسائل التواصل الاجتماعي، وبالأخص تويتر حيث اعتدت على كتابة بعض العبارات الحياتية في حسابي، كتبت عن الأح ام، والعاطفة، والإيجابية، ومختلف ل المواقف الحياتية. كانت معظم العبارات التي أكتبها تعبر عن مواقف أصادفها خ ال اليوم، ووجدت بعض ل التفاعل الجيد من المتابعين الذين كانوا يضيفون لي الكثير من خ ال تعقيبهم، وموافقتهم، وحتى ل مخالفتهم في وجهة النظر لما أكتب.&amp;nbsp;&lt;/p&gt;&lt;p&gt;مع هذا التفاعل صنفت ما أكتب إلى نطاقين، نطاق يتناول المنطق والأمور العق انية، ونطاق يتناول ل لمشاعر والأمور العاطفية. ومن هنا ظهرت فكرة كتابي الأول )يساراً جهة القلب( الذي يتكون من نصوص وقصص قصيرة تهدف إلى أن يختار القارئ في نهايته طريقته في التغيير نحو الأفضل من خ ال ل التغيير العاطفي أو التغيير المنطقي أو الاثنين معاً. ما كتبته في يساراً جهة القلب لا يعبر عن وجهة نظري وحدي، بل جاء نتيجة تأثري بأفكار المتابعين في تويتر سواءً أولئك الذين كانوا يوافقونني الرأي أو أولئك الذين كانوا يختلفون معي في وجهة النظر. أستطيع القول بأن صفحات الكتاب تمثل جهداً فكرياً لكل من عارضني ولكل من أيدني ولكل من غير وجهة نظري في بعض الأمور.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;بالرغم من تخصصك في مجال هندسة الذكاء الاصطناعي لك حضور بارز في الساحة الأدبية فمن أين يأتي هذا الشغف بالأدب والكتابة؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;كثيرون يعتقدون أن هندسة الحاسب الآلي لها ارتباط بالذكاء الاصطناعي وتعتبر مجالاً علمياً بحتاً.&lt;/p&gt;&lt;p&gt;شخصياً أؤكد أن ذلك ليس صحيحاً، فهندسة الحاسب الآلي بالذات تعلم المنطق، واتخاذ القرارات، والتفكير المنظم، وحتى غير المنظم، فممارسة بسيطة مثل البرمجة كفيلة بأن تغير طريقة تفكيرك واتخاذ قراراتك والبحث عن الثغرات.&lt;/p&gt;&lt;p&gt;كما أن الكتابة ليست بعيدة عن ذلك، فكتابة الروايات الناجحة حسب اعتقادي ليست تلك المكتوبة بأسلوب أدبي جميل فحسب، بل تلك التي تجمع المنطق والثغرات والحلول من خ ال الحبكة. هذا رأي شخصي ل قد يختلف معه البعض، ولا يوجد أسلوب صحيح وآخر خاطئ في كتابة الروايات، فالناس أذواق فيما يقرؤون، ولولا اخت اف أذواق القراءة لبارت المكتبات!&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ماذا يعني تحولك من الأدب المقروء إلى الأدب المرئي إن صح التعبير من خلال تحويل روايتك إلى عمل درامي؟ » ريتاج «&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أنا لم أتحول من الأدب المقروء، بل سيظل الأدب وكتابة الروايات جزء من حياتي الشخصية. أما تحويل إلى عمل درامي فهذا لايعني أنني » ريتاج « روايتي الأولى تركت الكتابة وتحولت ل أدب المرئي، لكنه جاء بطلب ل من قبل شركتين للإنتاج الدرامي، وتم الاتفاق مع إحداهن لتنفيذ العمل لصالح تلفزيون أبوظبي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;إلى عمل درامي أمر أعتبره إضافة » ريتاج « تحويل رواية لي ككاتب إماراتي لعدة أسباب، أولها: أنها أول عمل روائي لي، واختيارها من قبل أكثر من شركة إنتاج لتتحول إلى عمل درامي يمثل حافزاً إيجابياً قوياً لي » ريتاج « ككاتب مبتدئ في مجال الرواية. وثانياً: أن رواية أصبحت أول رواية عن الإمارات يتم تحويلها إلى عمل درامي وهذا إضافة لي ككاتب جديد في عالم الرواية، وثالثاَ: على الجانب الدرامي الذي لا أعتبر نفسي جزءً منه فإن الرواية تمثل أول مسلسل وطني في تاريخ الدراما الإماراتية، وهنا لا أتحدث عن الإضافة لي ككاتب، بل عن التأثير الذي يستطيع الأدباء فرضه في ساحات خارج نطاقهم. كما أن العمل الدرامي يصل بالرسالة التي يريدها الروائي إلى جمهور أوسع ممن لا يقرؤون الروايات.&lt;/p&gt;&lt;p&gt;&lt;b&gt;مارأيك بتخصيص عام 2016 عاماً للقراءة؟ وهل تعتقد بأن المسابقات والمبادرات المطروحة تشجع على القراءة ؟&amp;nbsp;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;إع ان عام 2016 عاماً للقراءة يمثل أحد أهم المُمَكنات ل للثقافة، وهذا الإع ان جاء ليسد فجوة لا تقتصر على ل مجتمعنا فقط بل تمتد إلى الكثير من المجتمعات العربية، فسلوك القراءة الذي كان رائجاً في السابق أصبح محصوراً على النخبة المثقفة إن صح التعبير. القراءة مفتاح للتطور والرقي في مختلف مناحي الحياة، ولهذا فإن هذا الإع ان سيمتد تأثيره لعقود قادمة ل كون تأثيره السلوكي لا يقتصر على العام الحالي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أعتقد أن المبادرات التي طرحتها المؤسسات في الدولة تتجه نحو تحقيق الأهداف المرجوة من عام القراءة، فكل مؤسسة ابتكرت طريقتها الخاصة في سد فجوة القراءة وفق الجمهور الذي تتعامل معه. هناك تفاعل كبير مع سلوك لم يكن يهتم به أحد، وأتوقع أن يكون هناك تغيير كبير مع قياس النتائج في نهاية العام، وهنا لا أتحدث عن عدد المبادرات بل عن قدرتها على التغيير ونشر القراءة لجعلها عادة دائمة. أؤمن تماماً أن عام القراءة جاء لتكون جميع الأعوام بعده أعواماً للقراءة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;مارأيك في مشروع مكتبة الشيخ محمد بن راشد التي أعلن عنها في بداية هذا العام ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;المكتبة التي أطلقها صاحب السمو الشيخ محمد بن راشد آل مكتوم نائب رئيس الدولة رئيس مجلس الوزراء حاكم دبي - رعاه الله - ليست مكتبة بالمعنى التقليدي بل مشروعاً معرفياً ثقافياً رائداً يواكب التغيير الذي يجب أن يحدثه عام القراءة في المجتمع الإماراتي. فهذا المشروع بما سيحويه من نطاق معرفي عريض يمتد إلى 4.5 مليون كتاب، بينها كتب مطبوعة، وإلكترونية، ومسموعة، وبعدد المستفيدين المتوقعين سنوياً يبلغ 42 مليون مستفيد، سيكون رافداً ثقافياً ليس للإمارات فحسب بل للعالم العربي من خ ال توفير الكتب الالكترونية على سبيل المثال.&amp;nbsp;&lt;/p&gt;&lt;p&gt;هذا بالإضافة إلى تنظيم المكتبة لأكثر من 100 فعالية ثقافية ومعرفية سنوية، ومعرضاً دائماً للفنون، واحتضانها لأهم المؤسسات المتخصصة بدعم المحتوى العربي. هذه المكتبة الغير تقليدية ستكون رافداً معرفياً للعالم العربي من خ ال طباعة وتوزيع ل 10 م ايين كتاب في العالم العربي وترجمة 25 ألف ل عنوان خ ال الأعوام المقبلة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هي نصيحتك لقارئ مجلتنا مذكرات الإمارات؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;نصيحتي هي قراءة ما بين السطور، فسلوك القراءة لا يقتصر على اكتساب المعرفة الأولية مما نقرأ بل على التمعن بالكلمات والمعاني واستج اب الرسائل ل المبطنة المخفية بين السطور، فما يقصده الكاتب لا يقتصر على الكلمات التي يكتبها في كل سطر، بل أيضاً على ربط الجمل التي يكتبها والتفكير بها لاستج اب أفكار جديدة عنها لكنه لم يكتبها للعامة ل بل للخاصة الذين يتمعنون جيداً فيما بين السطور.&amp;nbsp;&lt;br&gt;&lt;br&gt;&lt;/p&gt;', '');
INSERT INTO `sp_news_description` (`news_id`, `language_id`, `name`, `description`, `tag`) VALUES
(54, 2, 'الدكتور حمد الحمادي', '&lt;p&gt;ول عمل روائي درامي عن الإمارات » ريتاج «&lt;/p&gt;&lt;p&gt;الدكتور حمد الحمادي أحد الكُتّاب البارزين في دولة الإمارات، درس هندسة الحاسب الآلي من جامعة خليفة للعلوم والتكنولوجيا، وحصل على شهادة الماجستير في هندسة الأنظمة من جامعة ملبورن باستراليا، يحمل الحمادي شهادة الدكتوراه في هندسة الذكاء الاصطناعي من نفس الجامعة بأستراليا، أعلن مؤخراً عن روايته )ريتاج( والتي سوف تتحول إلى مسلسل تلفزيوني يضم مجموعة من الممثلين من دولة الإمارات ودول الخليج، يساراً جهة « للدكتور حمد إصداران آخران وهما .» لأجل غيث « و رواية » الق&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;كيف دخل الدكتور حمد الحمادي مجال الأدب والكتابة؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;حمد الحمادي إماراتي يحاول تقديم ما يستطيع تقديمه ليكون مؤثراً بإيجابية في المجتمع، يقول: قمت بالتدريس الجامعي خ ال فترة جميلة من ل حياتي، فخور جداً بالط اب الذين قمت بتدريسهم ل خ ال تلك الفترة القصيرة والذين أصبحوا يساهمون ل في تنمية المجتمع من خ ال وظائفهم في مختلف ل المؤسسات. حالياً أعمل في مجال الاتصال والابتكار والمشاريع الخاصة في المكتب التنفيذي لصاحب السمو الشيخ محمد بن راشد آل مكتوم نائب رئيس الدولة، رئيس مجلس الوزراء، حاكم دبي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;دخولي مجال الأدب والكتابة جاء بمحض الصدفة، فلم يكن هذا المجال ضمن خططي الحياتية أبداً. بدأت عن طريق وسائل التواصل الاجتماعي، وبالأخص تويتر حيث اعتدت على كتابة بعض العبارات الحياتية في حسابي، كتبت عن الأح ام، والعاطفة، والإيجابية، ومختلف ل المواقف الحياتية. كانت معظم العبارات التي أكتبها تعبر عن مواقف أصادفها خ ال اليوم، ووجدت بعض ل التفاعل الجيد من المتابعين الذين كانوا يضيفون لي الكثير من خ ال تعقيبهم، وموافقتهم، وحتى ل مخالفتهم في وجهة النظر لما أكتب.&amp;nbsp;&lt;/p&gt;&lt;p&gt;مع هذا التفاعل صنفت ما أكتب إلى نطاقين، نطاق يتناول المنطق والأمور العق انية، ونطاق يتناول ل لمشاعر والأمور العاطفية. ومن هنا ظهرت فكرة كتابي الأول )يساراً جهة القلب( الذي يتكون من نصوص وقصص قصيرة تهدف إلى أن يختار القارئ في نهايته طريقته في التغيير نحو الأفضل من خ ال ل التغيير العاطفي أو التغيير المنطقي أو الاثنين معاً. ما كتبته في يساراً جهة القلب لا يعبر عن وجهة نظري وحدي، بل جاء نتيجة تأثري بأفكار المتابعين في تويتر سواءً أولئك الذين كانوا يوافقونني الرأي أو أولئك الذين كانوا يختلفون معي في وجهة النظر. أستطيع القول بأن صفحات الكتاب تمثل جهداً فكرياً لكل من عارضني ولكل من أيدني ولكل من غير وجهة نظري في بعض الأمور.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;بالرغم من تخصصك في مجال هندسة الذكاء الاصطناعي لك حضور بارز في الساحة الأدبية فمن أين يأتي هذا الشغف بالأدب والكتابة؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;كثيرون يعتقدون أن هندسة الحاسب الآلي لها ارتباط بالذكاء الاصطناعي وتعتبر مجالاً علمياً بحتاً.&lt;/p&gt;&lt;p&gt;شخصياً أؤكد أن ذلك ليس صحيحاً، فهندسة الحاسب الآلي بالذات تعلم المنطق، واتخاذ القرارات، والتفكير المنظم، وحتى غير المنظم، فممارسة بسيطة مثل البرمجة كفيلة بأن تغير طريقة تفكيرك واتخاذ قراراتك والبحث عن الثغرات.&lt;/p&gt;&lt;p&gt;كما أن الكتابة ليست بعيدة عن ذلك، فكتابة الروايات الناجحة حسب اعتقادي ليست تلك المكتوبة بأسلوب أدبي جميل فحسب، بل تلك التي تجمع المنطق والثغرات والحلول من خ ال الحبكة. هذا رأي شخصي ل قد يختلف معه البعض، ولا يوجد أسلوب صحيح وآخر خاطئ في كتابة الروايات، فالناس أذواق فيما يقرؤون، ولولا اخت اف أذواق القراءة لبارت المكتبات!&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ماذا يعني تحولك من الأدب المقروء إلى الأدب المرئي إن صح التعبير من خلال تحويل روايتك إلى عمل درامي؟ » ريتاج «&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أنا لم أتحول من الأدب المقروء، بل سيظل الأدب وكتابة الروايات جزء من حياتي الشخصية. أما تحويل إلى عمل درامي فهذا لايعني أنني » ريتاج « روايتي الأولى تركت الكتابة وتحولت ل أدب المرئي، لكنه جاء بطلب ل من قبل شركتين للإنتاج الدرامي، وتم الاتفاق مع إحداهن لتنفيذ العمل لصالح تلفزيون أبوظبي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;إلى عمل درامي أمر أعتبره إضافة » ريتاج « تحويل رواية لي ككاتب إماراتي لعدة أسباب، أولها: أنها أول عمل روائي لي، واختيارها من قبل أكثر من شركة إنتاج لتتحول إلى عمل درامي يمثل حافزاً إيجابياً قوياً لي » ريتاج « ككاتب مبتدئ في مجال الرواية. وثانياً: أن رواية أصبحت أول رواية عن الإمارات يتم تحويلها إلى عمل درامي وهذا إضافة لي ككاتب جديد في عالم الرواية، وثالثاَ: على الجانب الدرامي الذي لا أعتبر نفسي جزءً منه فإن الرواية تمثل أول مسلسل وطني في تاريخ الدراما الإماراتية، وهنا لا أتحدث عن الإضافة لي ككاتب، بل عن التأثير الذي يستطيع الأدباء فرضه في ساحات خارج نطاقهم. كما أن العمل الدرامي يصل بالرسالة التي يريدها الروائي إلى جمهور أوسع ممن لا يقرؤون الروايات.&lt;/p&gt;&lt;p&gt;&lt;b&gt;مارأيك بتخصيص عام 2016 عاماً للقراءة؟ وهل تعتقد بأن المسابقات والمبادرات المطروحة تشجع على القراءة ؟&amp;nbsp;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;إع ان عام 2016 عاماً للقراءة يمثل أحد أهم المُمَكنات ل للثقافة، وهذا الإع ان جاء ليسد فجوة لا تقتصر على ل مجتمعنا فقط بل تمتد إلى الكثير من المجتمعات العربية، فسلوك القراءة الذي كان رائجاً في السابق أصبح محصوراً على النخبة المثقفة إن صح التعبير. القراءة مفتاح للتطور والرقي في مختلف مناحي الحياة، ولهذا فإن هذا الإع ان سيمتد تأثيره لعقود قادمة ل كون تأثيره السلوكي لا يقتصر على العام الحالي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أعتقد أن المبادرات التي طرحتها المؤسسات في الدولة تتجه نحو تحقيق الأهداف المرجوة من عام القراءة، فكل مؤسسة ابتكرت طريقتها الخاصة في سد فجوة القراءة وفق الجمهور الذي تتعامل معه. هناك تفاعل كبير مع سلوك لم يكن يهتم به أحد، وأتوقع أن يكون هناك تغيير كبير مع قياس النتائج في نهاية العام، وهنا لا أتحدث عن عدد المبادرات بل عن قدرتها على التغيير ونشر القراءة لجعلها عادة دائمة. أؤمن تماماً أن عام القراءة جاء لتكون جميع الأعوام بعده أعواماً للقراءة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;مارأيك في مشروع مكتبة الشيخ محمد بن راشد التي أعلن عنها في بداية هذا العام ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;المكتبة التي أطلقها صاحب السمو الشيخ محمد بن راشد آل مكتوم نائب رئيس الدولة رئيس مجلس الوزراء حاكم دبي - رعاه الله - ليست مكتبة بالمعنى التقليدي بل مشروعاً معرفياً ثقافياً رائداً يواكب التغيير الذي يجب أن يحدثه عام القراءة في المجتمع الإماراتي. فهذا المشروع بما سيحويه من نطاق معرفي عريض يمتد إلى 4.5 مليون كتاب، بينها كتب مطبوعة، وإلكترونية، ومسموعة، وبعدد المستفيدين المتوقعين سنوياً يبلغ 42 مليون مستفيد، سيكون رافداً ثقافياً ليس للإمارات فحسب بل للعالم العربي من خ ال توفير الكتب الالكترونية على سبيل المثال.&amp;nbsp;&lt;/p&gt;&lt;p&gt;هذا بالإضافة إلى تنظيم المكتبة لأكثر من 100 فعالية ثقافية ومعرفية سنوية، ومعرضاً دائماً للفنون، واحتضانها لأهم المؤسسات المتخصصة بدعم المحتوى العربي. هذه المكتبة الغير تقليدية ستكون رافداً معرفياً للعالم العربي من خ ال طباعة وتوزيع ل 10 م ايين كتاب في العالم العربي وترجمة 25 ألف ل عنوان خ ال الأعوام المقبلة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هي نصيحتك لقارئ مجلتنا مذكرات الإمارات؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;نصيحتي هي قراءة ما بين السطور، فسلوك القراءة لا يقتصر على اكتساب المعرفة الأولية مما نقرأ بل على التمعن بالكلمات والمعاني واستج اب الرسائل ل المبطنة المخفية بين السطور، فما يقصده الكاتب لا يقتصر على الكلمات التي يكتبها في كل سطر، بل أيضاً على ربط الجمل التي يكتبها والتفكير بها لاستج اب أفكار جديدة عنها لكنه لم يكتبها للعامة ل بل للخاصة الذين يتمعنون جيداً فيما بين السطور.&amp;nbsp;&lt;br&gt;&lt;br&gt;&lt;/p&gt;', ''),
(55, 1, 'مكتبة في كل بيت', '', ''),
(55, 2, 'مكتبة في كل بيت', '&lt;p&gt;برعاية كريمة من سمو الشيخ الدكتور سلطان القاسمي حاكم الشارقة أطلقت مؤسسة ثقافة ب ا حدود مشروع “مكتبة في كل بيت”. ل هي عبارة عن مكتبات مجانية توزع على العوائل في إمارة الشارقة، وتحتوي هذه المكتبات على مجموعة متنوعة من الكتب السياسية، والدينية، والثقافية وكتب الأطفال والتي تناسب جميع أفراد العائلة. يهدف هذا المشروع إلى نشر الثقافة بين الأسر الإمارتية والتوعية بأهمية القراءة ورفع المستوى الثقافي لأفراد المجتمع.&lt;/p&gt;&lt;p&gt;بدأ العمل على المشروع في عام 2008 وبميزانية مفتوحة من حاكم الشارقة، بينما بدأ توزيع المكتبات فعلياً في فبراير 2015 . استهدف المشروع 13 ألف أسرة إماراتية وانطلقت عملية التوزيع في ث اث مجالس للضواحي بعملية سهلة وبسيطة. ل أولاً يجب على المستلمين التسجيل إلكترونياً عبر الموقع ومن ثم الذهاب إلى مجلس الضاحية وإحضار نسخة من الهوية والجواز وخ اصة القيد ل لاست ام المكتبة وستتواصل هذه العملية خ ال ل ل العامين الجاري والقادم.&lt;/p&gt;&lt;p&gt;يأتي هذا المشروع استكمالاً لمسيرة الشيخ الدكتور سلطان القاسمي ورؤيته لمدينة الشارقة لتصبح عاصمة ثقافية بامتياز بالرغم من صغر مساحتها الجغرافية. ففي عام 1981 ذكر الشيخ الدكتور سلطان القاسمي أن الثقافة يمكن أن تكون العنوان الرئيسي للنهضة التي تشهدها الدولة وبناء على ذلك اهتمت إمارة الشارقة ببناء الثقافة والمعرفة بينما كانت باقي الإمارات تتنافس في البناء العمراني ونتيجة لذلك اختيرت الشارقة . عاصمة للثقافة العربية في عام 1998&amp;nbsp;&lt;/p&gt;&lt;p&gt;“مكتبة في كل بيت” هي مثال واحد من عدة مشاريع أطلقتها مؤسسة ثقافة ب ا حدود. إضافة إلى توزيع ل المكتبات أطلقت المؤسسة مكتبات متنقلة على شكل حاف ات تتنقل بين المدارس ومختلف ل الأماكن لتقدم فعاليات ثقافية مختلفة بالإضافة إلى جلسات خارجية للقراءة.&lt;br&gt;&lt;/p&gt;', ''),
(56, 1, 'كُتّاب كافيه', '', ''),
(56, 2, 'كُتّاب كافيه', '&lt;p&gt;القهوة والقراءة، طقسان مترافقان في العزلة والترف. من هنا استوحيت فكرة كُتّاب كافيه والذي يعد أول مقهى ثقافي في الإمارات. في البداية أنشأتُ دار كُتّاب للنشر في عام 2010 والذي يهتم برعاية الكُتّاب الإماراتيين وتضم حالياً نخبة من الأقام الإماراتية الموهوبة في مختلف المجالات، فللدار إصدارات أدبية متنوعة في التاريخ، والروايات، والشعر، والمقالات وبعض الإصدارات للأطفال.&lt;/p&gt;&lt;p&gt;تم افتتاح أول فرع من كُتّاب كافيه في مارس 2013 في منطقة أبتاون مردف في دبي، ينظم المقهى العديد من الفعاليات الثقافية بالإضافة لاحتوائه على مكتبة، وتوفيره للأغذية والمشروبات وخدمة الاتصال بشبكة الإنترنت، كما يستضيف المقهى حفات توقيع وإطاق الكتب، وورش عمل، وأماكن مخصصة للدراسة والأنشطة، وإمكانية حجز الصالون وغرف الاجتماعات.&lt;/p&gt;&lt;p&gt;تأسست دار كُتّاب ومقهى كُتّاب من قبل الكاتب الإماراتي جمال الشحي لتقديم الثقافة بشكل مختلف وجذاب، كما أن تصميم المقهى الداخلي يعكس هذه الرؤية فالمكتبة هي جزء واحد فقط من المقهى، وتحتوي على إصدارات دار كُتّاب ودور النشر الأخرى باللغتين العربية والانجليزية لتوفر للقارئ كل ما قد يبحث عنه بالإضافة إلى قسم مخصص للموسيقى والبيانو.&amp;nbsp;&lt;/p&gt;&lt;p&gt;في عام 2014 افتتح الفرع الثاني من كتاب كافيه في أبوظبي ليوفر البيئة الثقافية ذاتها لسكان إمارة أبوظبي وبنفس المزايا الموجودة في الفرع الأول في دبي. بالإضافة للمكتبة والمقهى يوفر كُتّاب كافيه مناطق مختلفة لإقامة حفات التوقيع والندوات الثقافية كمنطقة الصالون ومنطقة الأريكة وغرفة اجتماعات تتسع لخمسة عشر شخصاً كحد أقصى.&amp;nbsp;&lt;/p&gt;&lt;p&gt;مهرجان كُتّاب للقراءة « تزامناً مع إطاق عام 2016 عاماً للقراءة في دولة الإمارات أطلقت دار كُتّاب خال شهر يناير من العام الجاري على مدى ثلاثة أيام. ضم المهرجان مجموعة من الفعاليات »2016 كالأمسيات الأدبية، ومعرض مصغر للكُتّاب، وإمكانية التبرع بالكتب المستعملة واستضافات لمجموعة من الكُتّاب والأدباء. وتهدف هذه المبادرة إلى جعل فعل القراءة هدفاً ثقافياً وغاية لكل أفراد المجتمع ودعم رؤية حكومة دولة الإمارات وقادتها للتشجيع على القراءة بين أفراد المجتمع، كما حرص المهرجان على تقديم القراءة بصورة جذابة للجمهور من خال إقامة مجموعة من الحوارات والجلسات المفتوحة للجمهور في الهواء الطلق، إلى جانب ركن مخصص للأطفال استضاف مجموعة من الرواة لتقديم مختارات من القصص.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(57, 1, 'اسم الكتاب: ص. ب. 100', '', ''),
(57, 2, 'اسم الكتاب: ص. ب. 100', '&lt;p&gt;&lt;b&gt;الكاتب&lt;/b&gt;: سلطان العميمي كاتب وروائي من الإمارات&lt;/p&gt;&lt;p&gt;&lt;b&gt;دار النشر&lt;/b&gt;: الدار العربية للعلوم ناشرون&lt;/p&gt;&lt;p&gt;&lt;b&gt;سنة النشر&lt;/b&gt;: 2&lt;/p&gt;&lt;p&gt;&lt;b&gt;عن الكتاب:&lt;/b&gt;&lt;/p&gt;&lt;p&gt;خال حقبة الثمانينيات كانت المراسلة بالبريد تعد إحدى الهوايات الرائجة بين جيل الشباب، ومنهم علياء الطالبة الإماراتية التي تدرس في بريطانيا، وعيسى بن حشيد الموظف في مدينة الذيد في الإمارات. يتبادل عيسى وعلياء عدة رسائل تحدثا فيها عن اهتماماتهما وهوايتاهما رغبة في التواصل أو من باب الفضول. كان كل شيء طبيعياً حتى انقطعت أخبار عيسى فجأة لعدة أسابيع. هذا الانقطاع جاء نتيجة لوفاة عيسى في حادث سير، ولكن بفضول من موظف البريد الجديد اطلع على رسائل علياء الواردة وقرر تقمص شخصية عيسى لمراسلتها مما أدى إلى أن تأخذ القصة منحنى آخر غير متوقع.&lt;/p&gt;&lt;p&gt;تتميز هذه الرواية ببنيان معماري فريد، فعلى مدى 230 صفحة يسافر القارئ بين لندن والذيد وتتحدث كل شخصية عن نفسها وحتى الجمادات في هذه الرواية، الصندوق والأقام والأوراق. بذلك يكون العميمي نجح في الابتعاد عن الأسلوب الروائي المعتاد والذي يكون رتيباً في بعض الأحيان.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أيضاً أسلوب الرواية اللغوي كان سه اً وبسيطاً ل وجمياً في الوقت ذاته. بالرغم من كون سلطان العميمي شاعر قبل أن يكون روائياً إلا أنه لم يوظف الأسلوب الشعري في روايته ربما لتصل روايته للجميع وتناسب مستويات القراء المختلفة.&lt;/p&gt;&lt;p&gt;من خال أحداث الرواية، أيقظ العميمي الحنين إلى حقبة الرسائل البريدية من جديد مذكراً القراء بجمال وببساطة ذاك الزمن، عندما كان التواصل بسيطاً وجمياً بعيداً عن ضجيج وسائل التواصل الاجتماعي الحديثة.&lt;/p&gt;&lt;p&gt;ختاماً أترككم مع أجمل ما كتبه العميمي في نحن أوراق الرسائل، نفتخر بأن أوراقنا « هذه الرواية احتضنت الكثير من الكلمات والجمل المعبرة عن أحاسيس البشر في مختلف مناسباتهم، وأن هناك دموعاً انسكبت بين أسطرها وهم يكتبون على أوراقنا وأخرى انسكبت في أثناء قراءتهم ما فيها، وأيدي عطرتها وأنوفاً بحثت عن رائحة من تحب، وشفاهاً لثمت حروفها وصدوراً احتضنتها، فهل .»! تعلمون أي مشاعر تجمعنا مع البش&amp;nbsp;&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(58, 1, 'تاريخ القراءة', '', ''),
(58, 2, 'تاريخ القراءة', '&lt;p&gt;يبدأ ألبيرتو مانغويل بوصف القراءة على أنها ضرورية للحياة كالتنفس، فحبه وشغفه بالقراءة دفعه لتأليف عدة كتب في هذا المجال ومنها كتاب تاريخ القراءة. مانغويل كاتب أرجنتيني لكنه يجيد العديد من اللغات كالإنجليزية، والأسبانية والفرنسية.&lt;/p&gt;&lt;p&gt;عمل ألبيرتو في مجال الترجمة، والنشر، والتحرير لمدة طويلة، ومن خ ال هذا الكتاب يوثق ويؤرخ ألبيرتو مانغويل ل تاريخ النصوص المقروءة والمكتوبة على مر العصور.&lt;/p&gt;&lt;p&gt;في حوالي 600 صفحة يأخذنا مانغويل في رحلة عبر التاريخ من عصر السموريين والفينيقيين الذي وضعوا أسس الكتابة إلى التوثيق في عصر أف اطون وسقراط. ل يقول مانغويل أن النصوص المكتوبة لم تكن منتشرة في القرن الخامس قبل المي اد ولكن أف اطون قرر ل ل أن يجمع دروس معلمه سقراط ليحفظ المعلومات ل أجيال القادمة، ومن بعدها تم اتباع هذه الطريقة ل في مختلف المدارس للتعليم. بعض القادة والرؤساء أدركوا أهمية القراءة وبعضهم عارض القراءة ومنع الكتب خوفاً من تمرد الشعب. من جهة أخرى كان العبيد ممنوعين من القراءة باعتبار القراءة أداة للقوة والتحرر. لكن هناك قادة حرصوا على العلم كالإسكندر الأكبر الذي قام ببناء مكتبة الاسكندرية والتي تعتبر إحدى أكبر مكتبات العالم. من الملفت للنظر أن كل السفن التي كانت تمر بالإسكندرية يلزم عليها أن تنزل كل ما عليها من كتب حتى تنسخ من قبل المكتبة.&lt;/p&gt;&lt;p&gt;يحتوي الكتاب على العديد من الفصول والتفاصيل عن الكتب وعملية القراءة وأنواع القراءة المختلفة، كالقراءة الصامتة والقراءة للجمهور وأيضاً هناك فصل مخصص يتحدث عن شكل الكتاب ففي ب اد ل الرافدين على سبيل المثال كانت الكتب على شكل ألواح صلصالية مستطيلة يمكن حملها في محافظ جلدية بينما في إيطاليا كانت المخطوطات تكتب على&amp;nbsp; لورق وكانت تترك مساحة على الحواف حتى يكتب القارئ م احظاته.&lt;/p&gt;&lt;p&gt;عشاق القراءة سيعجبون بهذا الكتاب، فمانغويل عندما يكتب عن الكتب والقراءة يكتب بلغة جميلة لا&amp;nbsp; &amp;nbsp;ينافسه فيها أحد. يتخلل الكتاب مجموعة من اللوحات والصور لأناس من مختلف الثقافات والعصور كلهم اجتمعوا على حب القراءة، صورة لأرسطو وهو غارق في عالمه الخاص يقرأ من لفيفة ورقية، وطالبان مسلمان من القرن الثاني عشر يلقيان نظرة أخيرة على مقطع في كتاب قبل التوجه الى امتحانهما، ومشهد لكاتب&amp;nbsp; يدعى إزاك ولتن وهو يستمتع بمطالعة كتاب على ضفة جدول اتش ، وقد خطت تحت الصورة عبارة “ اقرأ لتهدأ”.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وفي الختام يرى مانغويل أن تاريخ القراءة الفعلي هو تاريخ كل قارئ مع القراءة ، ويرى مانغويل أن القراءة للآخرين فن ولكنه ليس سه اً لأن على القارئ ل اختيار المادة المناسبة للمستمع والمكان والزمان المناسبين للقراءة، هي تجربة مختلفة ولكنها مسلية.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(59, 1, 'خمس أسباب لزيارة طوكيو', '', ''),
(59, 2, 'خمس أسباب لزيارة طوكيو', '&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;دليل سياحي لأهم الوجهات السياحية في طوكيو.&lt;/b&gt;&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;زيارة طوكيو عاصمة اليابان حلم لطالما داعب خيال العديد منا، فهي تجمع بين حداثة المدن وعراقة التاريخ مما يجعل السفر إليها تجربة ثقافية مذهلة. يعتقد البعض أن زيارة اليابان تحتاج إلى ميزانية كبيرة وأن الشعب الياباني لا يتحدث اللغة الانجليزية أبداً! لكني اكتشفت من خ ال سفري إليها بأن هذه المخاوف لا صحة لها، تعرفوا على أهم خمس أسباب لتجذبكم لزيارة طوكيو.&lt;/p&gt;&lt;p&gt;&lt;b&gt;1. الغوص في قلب الثقافة اليابانية&lt;/b&gt;&lt;/p&gt;&lt;p&gt;تتيح لك زيارة منطقة “أساكوسا” السفر عبر التاريخ الياباني، كيف لا وهي تحتضن أقدم معبد في طوكيو، معبد “سينسوجي” الذي تم بناءه في عام 645 مي ادي. اكتشفوا “أساكوسا” بعربة ال”جينريكشا” ل التي لا تجرها الأحصنة، بل يجرها شاب ياباني قوي البنية ومرح، حينها سيروي لكم تاريخ وأسرار المنطقة. وبالتأكيد ف ا تكتمل التجربة الثقافية ل دون تذوق الربيان المقلي “تامبورا” ألذ المأكولات التي تشتهر بها “أساكاوسا”، يفتخر مطعم “ناكاسي” بإعداده ال”تامبورا” بالوصفة السرية التي تناقلتها الأجيال منذ افتتاح المطعم في عام 1870 . نكمل رحلتنا بالتجول عبر أزقة السوق الشعبي الذي ينبض بالحياة، فعلى اليمين محل لبيع وتأجير اللباس الياباني ال”كيمونو” بكامل اكسسوارته، وعلى&amp;nbsp; اليسار كشك لبيع حلوى كرات ال”دانجو” المحشوة بال”أنكو” أي كريمة الفاصولياء الحمراء، وهي بمثابة الشوكولاته في الحلويات اليابانية.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;2. طوكيو وجهة عالمية للتسوق&lt;/b&gt;&lt;/p&gt;&lt;p&gt;التسوق في طوكيو متعة لا يمكن وصفها! والأهم أنها لا تقتصر فقط على عشاق الموضة فحسب، بل تشمل محبي الالكترونيات، والرسوم المتحركة ال”إنمي” وكل ما يخطر على البال، إذاً هل تودون التعرف على أبرز وجهات للتسوق في طوكيو؟&amp;nbsp;&lt;/p&gt;&lt;p&gt;أولاً، حي “غينزا” الفاخر الذي يشتهر بوجود أعرق المتاجر اليابانية مثل “ميتسوكوشي” و“ماتسويا”، بالإضافة إلى الماركات العالمية. ثانياً، منطقة “هاراجوكو” وهي عكس “غينزا” تماماً، فهي مصدر آخر صيحات الموضة الشبابية وتكثر فيها متاجر الم ابس التنكرية الفكتورية الخاصة بموضة ل ”لوليتا“. ثالثاً، إن كنتم من محبي الرسوم المتحركة اليابانية “الإنمي” أو تبحثون عن الإلكترونيات الحديثة المتطورة، أو حتى ألعاب الفيديو الك اسيكية من ل زمن الطيبين، فإن منطقة “أكيهابارا” هي وجهتكم المثالية للتسوق و الاستمتاع بأجوائها المميزة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;3. الاستجمام بالطبيعة الخلابة عند جبل فوجي&lt;/b&gt;&lt;/p&gt;&lt;p&gt;لنبتعد قلي ا عن حياة المدينة في طوكيو لنزور ل أيقونة اليابان جبل “فوجي” المحاط بخمس بحيرات في غاية الجمال. أنصحكم بزيارة بحيرة “كاواجوتشي” فهي أجمل البحيرات، وتبعد ساعتين فقط عن طوكيو. تتنوع الوجهات السياحية في “كاواجوتشي”، فمحبي الاستجمام سيستمتعون بصعود تلفريك “كاجي كاجي” والتجول في الغابة الموسيقية. أما أصحاب القلوب القوية فيقضون أمتع أوقاتهم في مدينة م اهي “فوجي كيو”، ل فالقطارات الافعوانية هناك حطمت أرقام جينيس القياسية! وكما ترون فالاستمتاع في هذه المنطقة لا يكفيها يوم واحد فقط، لذلك احرصوا على السكن في الفنادق التقليدية “ريوكان”، فهي تجربة استثنائية فيها ستعيشون فنون الضيافة اليابانية، وتسترخون في حوض الينابيع الحارة “الأونسين” التي تتميز بإط الة رائعة على جبل “فوجي”.&lt;/p&gt;&lt;p&gt;&lt;b&gt;4. منتجع ديزني طوكيو&lt;/b&gt;&lt;/p&gt;&lt;p&gt;“ديزني” موجودة في طوكيو؟! نعم بل يوجد منتجع متكامل، به مدينتين للم اهي هما “ديزني لاند” ل الك اسيكية، و “ديزني سي” الموجودة فقط في ل طوكيو. اكتشفوا سحر “ديزني سي” المستوحى من 7 سواحل تأخذكم في رحلة حول العالم، لنبدأ رحلتنا من الساحل العربي وتحديداً مدينة “أغربة” من فيلم ع اء الدين. ثم لنركب “الجاندولا” في ل فينيسيا ضمن ميناء البحر المتوسط. و بعدها دعونا نغوص تحت سطح الماء إلى مملكة حورية البحر “إيريل” ل استمتاع بعرضها الموسيقي الرائع. ولم ل تنتهي رحلتنا بعد، فنحن لم نكتشف واجهة المياه الأمريكية والجزيرة الغامضة وميناء الاستكشافات ودلتا النهر الضائع. تتوسط “ديزني سي” بحيرة كبيرة تقام بها عروض شخصيات ديزني باستخدام التقنيات&amp;nbsp; المذهلة، ف ا تفوتوا العرض الليلي “فانتاسميك&quot;.&lt;/p&gt;&lt;p&gt;&lt;b&gt;5. طوكيو مدينة العجائب&lt;/b&gt;&lt;/p&gt;&lt;p&gt;زيارة طوكيو بالنسبة لي كانت أشبه بزيارة أليس إلى ب اد العجائب، في طوكيو احتسيت القهوة في ل مقهى ملييء بالبوم وآخر بالقطط، أليس ذلك غريباً؟ كما تذوقت “كريب هاراجوكو” الشهير، المختلف كلياً عن الكريب الفرنسي بإضافة مكونات جديدة مثل قطع “التشيز كيك” و“التيراميسو” وحتى “الماتشا” أي الشاي الأخضر الياباني. في طوكيو رأيت شخصيات “ماريو” تتسابق في سيارات السباق في شوارع “أكيهابارا”، وروبوت عم اق طوله 18 متراً في ل وسط منطقة “أودايبا”، أليس هذا عجيباً! ستشعرون حقا بأنكم في كوكب آخر عند زيارة اليابان، إذاً هل أنتم مستعدين لخوض هذه المغامرة واستكشاف عجائب طوكيو بأنفسكم؟&lt;br&gt;&lt;/p&gt;', ''),
(60, 1, 'القراءة.. حياة', '', ''),
(60, 2, 'القراءة.. حياة', '&lt;p&gt;في مكان مظلم بعيد معزول عن أعين البشر، نزلت أول كلمة ربانية على الرسول -صلى الله عليه وسلم- فأنارت قلبه وعقله وكونه، ألا وهي كلمة ) اقرأ ( التي افتتح بها الله تعالى رسالة الإس ام.&amp;nbsp;&lt;/p&gt;&lt;p&gt;إن اختيار الله -عز وجل- لهذه الكلمة بأن تكون أول ما ينزل على قلب الرسول - صلى الله عليه وسلم- فيها حكمة ربانية للبشرية، تدل على أهمية القراءة، فهي مفتاح المعرفة والعلم والقوة والقدرة، إذ أن هذه الأحرف الأربعة ) اقرأ ( غيّرت مجرى حياة أمّة محمد -صلى الله عليه وسلم- بعد أن كانت غارقة في ظ ام الجاهلية، ل وأعلت مكانتها بين الأمم، وأصبح يشار إليها بالبنان، فهذه الرسالة الربانية تشير إلى أن نهضة الأمم، وبناء الحضارات، والارتقاء بالقيم الإنسانية... كل ذلك يتم من خ ال الارتقاء ل بالفكر وذلك بالقراءة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;ورغم أهمية القراءة العظيمة فإنه ل أسف ل هناك من يعتقد أن القراءة مجرد ترف فكري، وغالباً ما يضعونها في خانة الهوايات، في حين أن القراءة ضرورة حياتية أشبه بالأكل والشرب والهواء للإنسان، فهي غذاء العقل، كما أن الكتاب يعد نافذة القارئ على عقول وعوالم جديدة مختلفة، لذا لابد من غرس حب القراءة في قلوب أفراد المجتمع، فهي أكثر من مجرد هواية، بل إن القراءة هي أسلوب حياة، وحياة للمجتمع، فسبحان من جعل من القراءة كل عقل حي ...&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(61, 1, 'قصّتي مع القراءة', '', ''),
(61, 2, 'قصّتي مع القراءة', '&lt;p&gt;أن تنفتح غرفات العقل على العالم دون أن يتحرك الشخص، هي من المعجزات التي لا بد من الإقرار بها والإعجاب بها. فما القراءة إلا قدرة إعجازية رزقها الله لنا كي تنفتح لنا آفاق المعرفة. فكم سافرت ومشيت في هذا العالم دون أن أسافر فع&lt;/p&gt;&lt;p&gt;فهي متعة لا تفوقها متعة، فأنسى كل ما أنا فيه من حاضر لأسرح وأتعلم مع أساتذة تارة، ومع أبطال قصة خرافية تارة أخرى. فأصبحتُ أميرة وأماً ومقاتلة أسطورية في آن واحد. أليس من الإعجاز أن يصبح الإنسان كل ذلك مرة واحدة، دون أن ت امس قدماه ل خارج حدود غرفته؟&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;لقد كان الكتاب صديقي المفضل في سنوات نشأتي، فقد علمني كيف أقوي ذاكرتي، وعلمني سر الإيجابية، وحكى لي حكايات حقيقية وحكايات خرافية. وينتصف الليل دون أن أشعر بظ امه، وأنا ومصباحي اليدوي ل وكتابي مختبؤون تحت م اءة صفراء، كي لا نوقظ أختي ل النائمة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;لم تكن القراءة شيئاً مهماً أحرص على فعله كل يوم، بل كان عادة طبيعية كالتنفس وشرب الماء. فأُفضل قراءة كتاب على مغادرة المنزل أحياناً، أو حتى مجالسة العائلة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;بالرغم من أن والدي لم يكن يقرأ سوى الصحف اليومية، إلا أنه كان يشجع القراءة بشكل كبير دون أن يأمرنا بذلك. ف ا زلت أتذكر الرحلة السنوية يوم ل الجمعة إلى معرض الشارقة للكتاب، حيث أذهب معه ومع إخوتي، ونشتري ما أردنا من الكتب دون مساومة. وكلما أمسكت بكتاب ما وسألت والدي عن رأيه، كان يشجعني ويحثني على هذا الاختيار الرائع، فأسعد أكثر، وأتحرق شوقاً للرجوع إلى البيت والبدء بقراءة هذه الكتب الجديدة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وقد كان حب القصص منغرساً فيني منذ الصغر، فقد كانت أمي تمسك كتاباً وتحكي لنا حكاية قبل النوم، وأحياناً تقوم هي بسرد حكاية ورثتها عن جدتها أو خالتها. فتخبرني عن أخوات يهربن من ساحرة شريرة، وتخبرني عن سلحفاة وأرنب، فتؤكد بتلك الحكايات مبادئ وقيم انغرست فيني، كأهمية العمل الدؤوب والعمل الجماعي، وأهمية العائلة.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(62, 1, 'القراءة للناجحين', '', ''),
(62, 2, 'القراءة للناجحين', '&lt;p&gt;» مارك زوكربيرج « عشرة كتب قرأها مؤسس موقع الفيسبوك، ومؤسس نادي كتب فيسبوك للقراءة، تجعلنا ندرك بأن اهتماماته المجتمعية كانت سبباً في دعم نجاح ابتكار الفيسبوك وتطوره.&lt;/p&gt;&lt;p&gt;لذي » مقدمة ابن خلدون « من » مارك « بدء اعتبره أول أفضل كتاب قرأه، حيث تكمن أهميته إلى استعراض طريقة تفكير البشر في العالم وعبر التاريخ، ويعتبر من أشهر الموسوعات كقيمة فكرية تدرس تاريخ الإنسان، وتطور حضارته بمقارنة فلسفية عميقة.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;الذي » مات ريدلي « ل » المتفائل الرشيد « أما كتاب العدد 7 - القراءة يتحدث عن نظرية التقدم الإنساني في المجالات الاجتماعية والنفسية وتطور الفرد من خال عملية » مارك « حركة السوق فهو الآخر يعكس اهتمام » كتاب جيم كرو الجديد « من الناحية الاجتماعية، و العنصرية » ميشيل ألكسندر « الذي تناقش مؤلفته وهمومها.&amp;nbsp;&lt;/p&gt;&lt;p&gt;يتحدث عن نظرية التقدم الإنساني في المجالات الاجتماعية والنفسية وتطور الفرد من خال عملية » مارك « حركة السوق فهو الآخر يعكس اهتمام » كتاب جيم كرو الجديد « من الناحية الاجتماعية، و العنصرية » ميشيل ألكسندر « الذي تناقش مؤلفته وهمومها.&amp;nbsp;&lt;/p&gt;&lt;p&gt;كتاب لماذا تسقط « قراءة كتاب » مارك « وقد اختار لمؤلفيه داريم اسميلجو وجيمس » الأمم روبينسون عن سبب تقدم مجتمعات وتخلف الأخرى، ليفهم أسباب الفقر، وكيفية محاربته. الذي ألفه هنري كيسنجر » النظام العالمي « وكتاب بحثاً عن الوصول إلى السام العالمي، بالإضافة إلى » ويليام جيمز « ل » أصناف من الخبرة الدينية «&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;لفهم الكيفية التي يدرك فيها البشر معنى الدين فهو دراسة » وزير الفقراء « في الحياة، بينما كتاب الحياة المالية لأفقر الطبقات في الهند وجنوب أفريقيا وبنج اديش.&lt;/p&gt;&lt;p&gt;لمعرفة كيف » شركة الإبداع « كتاب » مارك « وقرأ » الكائن العاقل « تُبنى الشركات الإبداعية، وكتاب بنية الثورات « عن تطور الإنسان العاقل، وكتاب لكونه من أهم الكتب الفلسفية . » العلمية&lt;/p&gt;&lt;p&gt;عراب عالم » بيل جيتس « قرأ » مارك « ومثل الأعمال والبرمجيات والساعي لمحاربة الفقر والأمراض والداعم لجهود التعليم وغيرها من الأعمال الانسانية ما يشبع فضوله المعرفي، ويمأ عقله بما يلزم عن العالم والحياة. مثل الذي أقرّ فيه كاتبه » الطريق إلى الشخصية « كتاب أن المجتمع الأمريكي قد غرس في مواطنيه فضائل كتابة السيرة الذاتية التي يتهافت عليها أرباب العمل لكنه لم يغرس السمات التي تقودهم إلى السام للكاتب راندول مونرو عن » شارح الأشياء « الداخلي. و كيفية عمل الهواتف الذكية وفقاً لدستور الولايات المتحدة باستعمال 1000 من الكلمات الإنجليزية الأكثر شيوعاً، وأسلوب خطة العمل المصورة بواسطة الرسوم البيانية.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;لإيفان طوماس الذي » من يكون نيكسون « وكتاب » نيكسون « يكشف الكثير والمثير عن شخصية والذي كان يعتبر داهية ومحتالاً، بينما يتحدث عن تخليص العالم من الأمراض ل أبد، ل » الاجتثاث « وكيف تسهم جهود إبادة الأمراض في راحة المواد المستدامة « نفس الانسان. ويشرح كتاب الكيفية التي يمكن بها خفض » بعينين مفتوحتين الانبعاثات من المواد التي يستخدمها الانسان » العقلية « بنسبة تصل 50 % دون تضحيات كبيرة و عن معتقدات الإنسان حول قدراته والتي تؤثر على تعلمه.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(63, 1, 'جرّب.. أن تقرأ', '', ''),
(63, 2, 'جرّب.. أن تقرأ', '&lt;p&gt;هكذا كان يقول » لا يوجد شخص يكره القراءة.. هناك فقط أشخاص يقرؤون الكتب الخاطئة « جيمس باترسون مؤلف روايات أليكس كروس. وأنا أوافقه الرأي تماماً. عندما تجدني إحدى صديقاتي أمسك كتاباً، وتخبرني بأنها تكره القراءة وتسألني بدهشة كيف تقرئين؟ تطفو على رأسي ع امات ل التعجب. بالنسبة لهم القراءة مهمة شاقة تتطلب مجهوداً كبيراً ووقت فراغ طويل، وبالنسبة لي الحصول على إجابة مناسبة لتساؤلهم الغريب؛ تلك هي المهمة المستحيلة. لا أعرف كيف أجيب على سؤالٍ كهذا؟ كيف أقرأ؟ لا توجد إجابة محددة مختصرة سهلة الفهم تفي بالغرض. هم يعتقدون بأن القراءة أمر صعب ومن يقرؤون يملكون نوعاً من القوى الخارقة، ولا يعلمون بأنه وبعد أول كتاب يصبح الأمر أشبه بالروتين اليومي، نوعٌ من الإدمان الجميل الذي يجعلك تعود بلهفة لتصفح ما تبقى لك من ذلك الكتاب. ليس صعباً أن تقرئي صديقتي، فقط عليك أن تجربيه.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;وأنتَ أيضاً، فقط عليكَ أن تقرأ.. فقط عليكَ أن تُجرِّب.&lt;/p&gt;&lt;p&gt;ماذا ستخسر لو نفضت الغبار عن مكتبة أبيك، أو زرت خلسةً إحدى مكتبات حيّك، أو استعرت كتاباً من أستاذك وجربت أن تقف أمام غ افه مُلقياً بنفسك بعيداً جداً عن عالم البشر لتغوص حيثُ تجد من ل الحروفِ ما يعانق روحك. جرب أن تُغلف عقلك وتُهديه لمن يحترم ذاته، ويقدّس تساؤلاته، ويأخذ به إلى حيث يقف السؤال ممسكاً بيده الإجابة ويقدمها لك بِكامل حلتها البهية لتنير لك درباً مظلماً.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;جرب أن تقرأ، وأن تطير بجناحيك بعيداً عن تعوجات العالم وإنحنائاته الصعبة إلى حيث توجد الاستقامة. جرب أن تقرأ، وأن تعيش حياة أخرى؛ في بحة صوت تلك العجوز، وبراءة ضحك تلك الطفلة ودموع قهر ذلك الشاب. جرب أن تقرأ، وأن تخلق للكلمات صوتاً وتستلذ بالرقص على أوتار الجُمَل كما لو أنك تتعلق بآخر حبلٍ من أحبال الحياة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أن لا تقرأ يعني أنك توافق على جرائم التاريخ ضد القراءة، حين قام مجرمو التتار بتدمير وإحراق المكتبة البغدادية التي أسسها هارون الرشيد والتي احتوت على فكر المسلمين في قلبها لأكثر من ستمئة عام. أن لا تقرأ يعني أنك كنت معهم عندما حملوا م ايين الكتب ودفنوها في نهر دجلة حتى تحول ل لونه إلى السواد.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أن لا تقرأ يعني أنك توافق على حرق كتب مكتبة غرناطة وقرطبة. أن لا تقرأ يعني أن تحمل بيدك فتياً وتشارك الجهلة في حرق مكتبة عسق ان ومكتبة طرابلس.&lt;/p&gt;&lt;p&gt;أن لا تقرأ يعني أن تضع يدك في يد كمبيس - أحد القساوسة في عام 636 م - و أن تجمع الكتب معه، و تدفنها في حفرة كبيرة و تضرم فيها النيران التي لم تسكن إلا بعد 20 يوماً. أن لا تقرأ يعني أن تتفق مع التي اعتمدها الجيش الياباني، حين حرق ونهب الكثير من الممتلكات الصينية » الفظائع الثاث « سياسة منها الكتب التي كانت تخط بخط اليد ولم يكن يوجد منها سوى نسخة واحدة لكل كتاب. أن لا تقرأ يعني أن تمضي مع خوان كسيمانيز في جيشه وتدمر الكتب بهدف القضاء التام على التراث العربي والحضارة الإسامية. أن لا تقرأ يعني أن تساهم مع الإستعمار الفرنسي في نهم وتدمير مكتبة جامعة الجزائر التي كانت تضم أكثر من نصف مليون كتاب. أن لا تقرأ يعني أن تجلس في زاوية مدينة الري وتصفق بحرارة لمحمود الغزنوي وهو يستخرج كتب الفلسفة والفقه ويأمر بإحراقها. أن لا تقرأ يعني أن ترى تعهد المنصور الموحدي حين حلف أن لا يُبقي شيئاً من كتب الحكمة في باده.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;أن لا تقرأ يعني أن تقف أمام التاريخ موافقاً على فعائل المستبدين ضد ثروة الكتب. فجرب أن تقرأ.&lt;/p&gt;&lt;p&gt;جرب أن تُقدس الحروف، وتفهمها وأن تقف أمام التتار وكمبيس مدافعاً عنها بروحك، وأن ترفض سياسة الفظائع الثاث، وأن تصفع خوان كسيمانيز وأن تحفظ الكتب في مكانٍ آمن بعيداً عن كل من يتربص بها ويتآمر عليها، وأن تقرأ منها صفحةً كل يوم.&lt;/p&gt;&lt;p&gt;جرب، أن تفتح لنفسك عالماً مليئاً بالجمال، تضع مفاتيح أبوابه في جيبك وتهرب إليه متى ما أنهكتك&amp;nbsp; متاعب الحياة.&lt;br&gt;&lt;/p&gt;', ''),
(64, 1, 'يوسف المطوع', '', ''),
(64, 2, 'يوسف المطوع', '&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;تحتضن الأفكار العالمية وتأسر » تيرن 8 « مبادرة قلوب المستثمرين&lt;/b&gt;&lt;/p&gt;&lt;p&gt;بعيداً عن القوالب التقليدية والمألوفة، وسعياً للوصول إلى أساليب أكثر كفاءة وفاعلية، والتي تسعى إلى رفد الساحة الإماراتية بالابتكارات » تيرن 8 « أطلقت موانئ دبي العالمية مبادرة المبدعة ذات الطابع الاقتصادي في مجال تقنية المعلومات.&lt;/p&gt;&lt;p&gt;&lt;b&gt;الابتكار والابداع بحاجة لعقول تفكر بطريقة خلاقة ومبدعة&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;يوسف المطوع في سطور.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يوسف المطوع المدير التنفيذي لشؤون تقنية ومدير مبادرة » موانئ دبي العالمية « المعلومات في حالياً، حاصل على درجة البكالوريوس » تيرن 8 « في الهندسة الكهربائية والإلكترونية من جامعة كاليفورنيا الحكومية، في ساكرامنتو، بالولايات المتحدة الأمريكية. عمل بعد تخرجه لدى العديد من الشركات في كاليفورنيا وفي مناصب مختلفة، مما أكسبه خبرة فريدة في التدرج لمناصب مختلفة منها مستشار للأعمال الإلكترونية وأخصائي في إدماج الأنظمة وخبير في مجال الإنترنت آن ذاك.&lt;/p&gt;&lt;p&gt;» دو « ولدى عودته للإمارات عمل في شركة بمنصب نائب الرئيس لتطبيق شبكات الاتصالات الثابتة، وكانت له مساهمات واضحة وبشكل رئيسي في المراحل المبكرة لتأسيس موانئ « وبعد ذلك عمل في .» دو « شركة .» دبي العالمية&amp;nbsp;&lt;/p&gt;&lt;p&gt;بالإضافة إلى ذلك، ترأس مكتب إدارة البرامج لقسم الاتصالات في مدينة دبي للانترنت، والتي انضمت لاحقاً وبصفته خبيراً في قطاع الاتصالات، .» دو « إلى شركة يقول يوسف: توليت نشر شبكات الجيل التالي لمشاريع التطوير العقاري على امتداد إمارة دبي، كما كان لي شرف بالمساهمة كعضو في فريق إدارة تفنية المعلومات لمشروع حكومة دبي الإلكترونية خ ل ا مراحله التأسيسية.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;بدايات المطوع في موانئ دبي.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;المطوع قائ :ً لقد أتاح لي العمل في موانئ دبي ا العالمية التي تولي أهمية خاصة لتشجيع الكوادر البشرية الإماراتية وتفتح أمامها آفاقا واعدة من خ ل التدريب والتوجيه الوظيفي وتحفيز الابتكار، ا وطرح ممارسات منهجية للابتكار لاستكشاف الأفق المستقبلية للنمو. وقد استطعت إرواء شغفي بهذه الأمور من خ ل عملي كمدير تنفيذي ا لشؤون تقنية المعلومات في الشركة حيث توليت مسؤوليات إدارة شؤون تقنية المعلومات والاستراتيجية الرقمية، والفرص الجديدة التي ا ،» تيرن 8 « تحقق العائدات، وقمت بإط ق مبادرة وهو برنامج حضانة الأفكار المبتكرة وتحويلها إلى مشاريع يمكن تسويقها، وذلك في إطار سعي موانئ دبي العالمية لنشر ثقافة الابتكار وريادة الأعمال داخل المجموعة وخارجها.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;مبادرة “تيرن 8” الفكرة والهدف منها.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;“ تيرن 8” هو برنامج حضانة الأفكار المصمم لتشجيع ريادة الأعمال المبتكرة أطلقته “موانئ دبي العالمية” في سعيها لتشجيع ريادة الأعمال المبتكرة على مستوى العالم انطلاقاً من دبي. ويهدف البرنامج إلى الكشف عن الأفكار الخلاقة والمبتكرة التي يمكن تطويرها من خ ل حضانتها ا لمدة 120 يوماً لتصبح مشاريع تجارية مجدية اقتصادياً، حيث يتم توجيه وتدريب الفرق المشاركة على كيفية صقل أفكارها المبتكرة وإدخالها إلى السوق كما يتم تمويلها خ ل فترة حضانتها في ا دبي لمدة أربعة أشهر وتسهيل عرض مشاريعها على المستثمرين مقابل حصة في الشركات الناشئة التي تؤسسها هذه الفرق.&lt;/p&gt;&lt;p&gt;يتم البحث عن الأفكار المبتكرة من خ ل حملة ا على شبكة الإنترنت، وفعاليات نقوم بتنظيمها في عدد من البلدان المختارة والتي تتمتع بثقافة شركات ناشئة فعالة ومن ضمنها دولة الإمارات العربية المتحدة ومصر والأردن وسنغافورة وماليزيا وأوكرانيا والولايات المتحدة الأمريكية.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هو توجه موانئ دبي العالمية من اطلاق هذه المبادرة؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يقول المطوع في سياق توجهات المؤسسة: نهدف إلى نشر ثقافة الشركات ا » تيرن 8 « من خ ل برنامج الناشئة والابتكار في المجتمع على نطاق واسع وذلك تماشياً مع توجهات دولة الإمارات العربية المتحدة الراعية لريادة الأعمال، ورؤيتها لأن تصبح مركزاً إقليمياً للابتكار من خ ل تأسيس مركز ا متكامل للابتكار في دبي. ويكمل: نحن في موانئ دبي العالمية نؤمن بأن الابتكار هو مفتاح النمو طويل الأمد والاستدامة والسبيل نحو التقدم، إذ يعمل ليس فقط على تحفيز النمو، إنما يعزز الكفاءة ويحافظ على مكانتنا الطليعية في القطاع. ونحن ملتزمون بنشر ثقافة الابتكار في المجتمعات التي نعمل فيها.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما الهدف من تقديم الرعاية والدعم المادي ؟” للمشاريع في مبادرة “تيرن&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يعد الإبداع أحد القيم الأساسية التي تقوم عليها أعمال موانئ دبي العالمية، وهو ما ينسجم مع حقيقة أن الإبداع كان ومازال يمثل المحرك الرئيسي لنهضة دبي الاقتصادية الحديثة، حيث كان إط ق ا مشروع ميناء جبل على ضرباً من ضروب الإبداع آنذاك. ومن المؤسف أن يقف ضعف الإمكانيات المادية لأصحاب الأفكارالمبتكرة عائقاً في طريق تطوير مشاريع قد تغير من طريقة أدائنا لأعمالنا اليومية ومن حياتنا على نطاق أوسع. ومن هنا تؤمن موانئ دبي العالمية بأهمية توفير البيئة المناسبة والتمويل ال زم لتشجيع ريادة الأعمال ا ونشر ثقافة الابتكار.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما نوع المجالات التي يتم تسليط الضوء عليها في ؟” مبادرة “تيرن&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يمكن أن تغطي الأفكار المقترحة عدداً من القطاعات مثل السفر والتجارة وإدارة سلسلة الإمداد والشحن البحري والنقل والخدمات اللوجيستية وأمن الخدمات اللوجيستية والس مة والاستدامة ا والبيئة ومنصات التواصل الإجتماعي وغيرها.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;من وجهة نظرك، هل يوجد دعم كافي لطرح هذه الابتكارات في الأسواق المحلية أو العالمية؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يعد الابتكار في دولة الإمارات أولوية وطنية للتقدم وأساس لتحقيق رؤية 2021 فقد أطلق صاحب السمو الشيخ محمد بن راشد آل مكتوم، نائب رئيس الدولة رئيس مجلس الوزراء حاكم دبي “رعاه الله”، الاستراتيجية الوطنية للابتكار، التي تهدف لجعل الإمارات ضمن الدول الأكثر ابتكاراً على مستوى العالم خ ل السنوات السبع المقبلة. ا وأعقب ذلك إط ق العديد من المبادرات. يقول ا المطوع: لا ينقصنا التشجيع والدعم ولكننا نحتاج إلى تعاون كافة فئات المجتمع ومؤسساته وأفراده والتنسيق عبر قطاعات الاقتصاد كافة لإيجاد أساليب وطرق جديدة مبتكرة في تأدية الأعمال وتقديم الخدمات التي ترتقي بالمجتمع وتدفع نمو الاقتصاد القائم على المعرفة، بالإضافة إلى وضع التشريعات المحفزة. وبرأيي فإن أبرز تحد يواجد الابتكار هو مقاومة التغيير والتردد في المخاطرة في الاستثمار بالأفكار المبدعة خوفاً من عدم نجاحها.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هي المعوقات التي واجهتكم في مبادرة “تيرن 8” منذ انطلاقتها؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;لقد قطعت المبادرة التي تدخل الآن دورتها الخامسة شوطاً كبيراً في اجتذاب العقول المبدعة إلى دبي وبالتالي إرساء البنية التحتية للإبتكار في منطقة الشرق الأوسط التي تفتقر دولها إلى دعائم وأسس هذه الصناعة وأهمها الأطر القانونية والتشريعية التي تيسر تأسيس الشركات الناشئة بلإضافة إلى تشريعات تشجع البنوك على تمويل المشاريع الناشئة وغيرها من العوائق، ناهيك عن تخوف المستثمرين من الدخول في شراكات في شركات ناشئة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وعلى الرغم من ذلك فإن دبي والإمارات العربية المتحدة مهيأة أكثر من غيرها لأن تكون مركزاً إقليمياً رائداً للتميز والإبداع. ومن شأن التوجه نحو تأسيس حضانات للأفكار ومراكز للتميز والإبداع ومبادرات على غرار “تيرن 8” أن تسد الفجوات القائمة وتنشر الوعي بأهمية إيجاد البيئة الجاذبة لرواد الأعمال والمبتكرين.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هو أهم ابتكار تم تطويره من خلال مبادرة “تيرن 8” إلى الآن؟ ولماذا يعد هذا الابتكار مهما؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يقول المطوع: نجحت مبادرة ”تيرن 8” لحضانة الأفكار المبتكرة ودعم أصحابها ومساعدتهم لتحويلها إلى أعمال ناشئة في استقطاب أكثر من 50 فريق من مختلف أنحاء العالم منذ إطلاقها عام 2013 ونتج عن المبادرة تبني العديد من ألأفكار الناجحة وتم حتى الآن تأسيس وتسجيل 23 شركة ناشئة بقيمة سوقية تبلغ 30 مليون دولار أمريكي. ونذكر على سبيل المثال لا الحصر فكرة “ديهبس” وهي شبكة تواصل إجتماعي تتيح لمستخدميها حرية التعبير عن أنفسهم من خ ل نشر ما ا يريديون قوله مباشرة على لوحات رقمية في الشوارع. وسيتم رسمياً إط ق أعمال الشركة في ا مدينة سياتل الأمريكية خ ل شهر أكتوبر القادم&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ماذا يعني لك استشراف المستقبل؟ وماهي ؟ ” افكاركم المستقبلية لتطوير مبادرة “تيرن&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يوسف قائ :ً نسعى لتطوير المبادرة بحيث تتحول ا إلى مساهم فعال في نشر الابتكار وريادة الأعمال من خ ل العمل مع شركائنا في القطاعين ا الحكومي والخاص لاستقطاب المواهب وتأمين كل ما يلزم للاستثمار في تنميتها وتطويرها من خ ل إرساء الأسس والأطر القانونية والتشريعية ا&amp;nbsp; الملائمة.&lt;/p&gt;&lt;p&gt;&lt;b&gt;أخيراَ، نصيحة تقدمها لجيل الشباب المبدع والمبتكر؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;نصيحتي لكل مبتكر ومبدع ألا يكتف بالحصول على وظيفة حكومية ويقع فريسة العمل الروتيني، بل عليه التفكير بعيداً عن القوالب التقليدية والمألوفة وذلك لأن التعرف على الفرص الكامنة يحتاج إلى عقول تفكر بطريقة خلاقة ومبدعة سعياً للوصول إلى أساليب أكثر كفاءة وفاعلية.&lt;/p&gt;&lt;p&gt;كما أحث الشباب على التوجه نحو ريادة الأعمال وتحويل الأفكار المبتكرة إلى أساليب وأدوات تساعد في أداء العمل بشكل مختلف وتسهم في خلق وظائف للآخرين، وعلى الاستفادة من المبادرات التي تضعهم على بداية الطريق الصحيح ومنها برنامج “تيرن 8” الذي آمل أن يستقطب أعدادا أكبر من الإماراتيين المبدعيين الساعين نحو إيجاد حلول قائمة على التكنولوجيا تسهم في إفادة الوطن وفي توفير نماذج جديدة من الأنشطة الاقتصادية تغير من أسس الاقتصاد والمعام ت بين الناس.&lt;br&gt;&lt;/p&gt;', ''),
(65, 1, 'أديب سليمان البلوشي', '', ''),
(65, 2, 'أديب سليمان البلوشي', '&lt;p&gt;أحد نوابغ الشرق الأوسط والعالم أجمع ، إداراكه لإصابة أبيه بالشلل دفعه إلى ابتكار أداة تخدم هذه الفئة من المجتمع، اتسم حواره بالبساطة والعفوية، فقبل أن يكون العالم والمخترع ، هو طفل ذو عشرة أعوام ، من مواليد شهر يناير لعام 2004 ، طالب في مدرسة جيمس ويلنغتون الدولية بدبي. بدأ فعلياً في تطبيق اختراعاته عام 2008 حيث قدمت له دبي روح الإبتكار والتميز والإبداع، وساهمت في صقل مهاراته ونقل صورة ابن الإمارات الناجح للعالم.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;أديب البلوشي طفل قبل أن يكون مخترع، هل لا زلت تمارس عاداتك الطفولية أم إنك تعديت هذه المرحلة ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;مازلت أديب الطفل، أمارس حياتي كبقية الأطفال، فعند عودتي من المدرسة أقضي وقتي في حل واجباتي المدرسية، وبعد انتهائي منها أتابع نشاطاتي و اختراعاتي، ولكن حين أسافر خارج الدولة لحضور المؤتمرات واللقاءات العلمية فأنا أديب المُخترع، المطلع على كل ماهو جديد في عالم الابتكار.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ماهي هواياتك، وكيف تقضي وقت فراغك؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أحب ممارسة هواياتي في أوقات الفراغ كالسباحة وقراءة الكتب العلمية، وقصص رحلات الفضاء، إلى جانب عشقي للألعاب الإلكترونية كالطائرات والتي أقضيها بصحبة أصدقائي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;حبذا لو تعرفنا على اختاراعاتك وابتكاراتك ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;هناك سبع اختراعات منها ما طُبق ومنها ماهو قيد الإنشاء كالحذاء الطبي ضد الماء، وعصا المكفوفين الذكية وهي تعمل على حساسات لتنبيه حاملها أثناء المشي ، والمكنسة الآلية الذكية والتي تستخدم للتنظيف في الأماكن الضيقة. أما بالنسبة للإختراعات والتي هي قيد الإنشاء، حزام الأمان الذكي الذي يتواصل مع أهل السائق أو الشرطة في حال حدوث حالة طارئة له، والكرسي المتحرك الذكي المتصل بالأقمار الصناعية ، إلى جانب الباب الذكي للحيوانات والذي يتعرف على الحيوان بمجرد سماع صوته عند الباب، بالإضافة إلى خوذة الإطفائي الذكية المزودة بنظام نقل مكان تواجد الإطفائي لباقي الفريق في حال فقدان الاتصال به.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;الإمارات العربية المتحدة دائماً تحتضن وتبرز مواهب جديدة وفريدة من نوعها. فهل ترى في ذلك سبب لإصرارك على إكمال هذه المسيرة بالرغم من صغر سنك ؟&amp;nbsp;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;نعم بالتأكيد ، فدولة الإمارات ساعدتني كثيراً حتى أصل للمكان الذي أنا فيه اليوم ، فهي قدمت لي منحة دراسية شاملة، إلى جانب تشجيع وحرص سمو الشيخ حمدان بن محمد بن راشد آل مكتوم ولي عهد دبي - حفظه الله ورعاه على ابتعاثي للخارج لحضور المؤتمرات والندوات العلمية، إلى جانب دورة ناسا الفضائية التي كانت ممتعة ومفيدة جداً بالنسبة إلي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;أهم الأسباب التي أحيت روح الإبتكار و أنمت مدارك أديب البلوشي ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أمي، وأبي لهما الفضل الكبير في إظهار مواهبي العلمية، كما أن حرص سمو الشيخ حمدان بن محمد بن راشد آل مكتوم ومتابعته لجولاتي العلمية مهّد لي » وأختي أحياناً « البيئة الخصبة للإبداع والابتكار، ويضح&lt;/p&gt;&lt;p&gt;إلى جانب مدرستي ، وشيوخنا، وحكومتنا الرشيدة. ففي البداية لم أفكر أن أصبح مخترع ، بل كان هدفي مساعدة أبي المصاب بالشلل وعندما انتهيت من تحقيقه ، تساءلت بيني وبين نفسي: إن كنت قادراً على مساعدة أبي لما لا أساعد الجميع ، لذلك اخترت أن أكون عالماً.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;إلى أي مدى تريد أن تصل في مجال الإبتكار العلمي والإبداعي ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أطمح بأن أصبح عالم من علماء دولة الإمارات العربية المتحدة القادرين على تقديم أشياء عظيمة في جميع مجالات الحياة، إلى جانب تطبيق جميع الاختراعات التي تناسب وتساعد الجميع وتعم عليهم بالخير والفائدة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;كيف أسهمت جولاتك العلمية في أوروبا ومشاركاتك في الندوات والمؤتمرات العلمية على تطوير وتغذية فكرك؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;علمتني أشياء كثيرة ، منها حصولي على أجوبة كثيرة لتساؤلات كانت تدور في مخيلتي، والتحدث إلى كبار العلماء والتعرف بهم، ومناقشتهم في مواضيع شتى، فمثلاً عندما كنت اخترع إحدى الاختراعات كنت أطرح أسئلة لم أكن قادراً على الإجابة عنها ، لكن اليوم بإمكاني أن أجيب نفسي بنفسي. كما أن مشاركتي بكل هذه المحافل جعلتني أفتخر بنفسي وبما أقدمه للمجتمع.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;رسالة توجهها لكل من يملك موهبة علمية ويتردد في استغلالها وتوظيفها في خدمة من يحتاج إليها.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;لا تستسلموا أبداً، لأن الإستسلام سيقفل أبواب النجاح و التميز في وجوهكم، وحتى إذا وصلتم لمرحلة الانهزام، حاولوا مرة أخرى ، فإن فشلتم ، كرروا المحاولة مرة بعد مرة ، وبالتأكيد سوف تنجح في إحدى المحاولات، و الأهم من ذلك أن تتحلوا بثقة النفس.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هي الصعوبات والتحديات التي واجهتك ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;من أهم الصعوبات تداخل الوقت وتعب السفر من أوروبا لأمريكا ورجوعاً إلى دبي لحضور الندوات والملتقيات العلمية، والتي سببت لي الضغط الكبير. أما بالنسبة للتحديات أذكر في مرة من المرات كنت أجري تجربة الفقاعات المائية وفشلت في تطبيقها ، وكنت في كل مرة أعيد تطبيقها أفشل، حتى أوشكت على الإستسلام ، لكن والداي قالا لي : “إذا استسلمت لن تقدر أن ترجع ، حاول يا أديب.” فرجعت وأعدت التجربة من جديد واكتشفت أنني كنت أجربها في درجة حرارة غير مناسبة وحين اكتشفت الخطأ ، أصلحته ومن ثم نجحت في تطبيقها.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما شعورك كونك أحد أبناء دبي ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;سعيد بأني ابن » وايد وايد فخور « دبي ، فهي قدمت لي العلم ، وحُب الشيوخ حفظهم الله ورعاهم ، وحُب الشيخ حمدان بن محمد بن راشد آل مكتوم حفظه الله ورعاه&amp;nbsp; الذي مازال يقدم دعمه لي ، وأنا دائماً .» توأم روحي « أقول بأنه&lt;/p&gt;', ''),
(66, 1, 'يا علم', '', ''),
(66, 2, 'يا علم', '&lt;p&gt;يا علم شاهد على عز اتحاد دولة فوق السحاب مقامها يا علم يا محتوي وحدة ب د ا ينحني التاريخ متواضع أمامها يا علم متربع بعرش الفؤاد لك عيون الشعب يتجافا منهامها حافظينك يا علم عن كل عاد مانهاب الحرب مالكين ازمامها لو ينادي للوغى أية مناد كل أرواح العدا أقبل فطامها شايلين أرواحنا قبل العتاد جند زايد ما تخيب اسهامها كل شكراً واجبة لحامي ب د ا ينحني التاريخ يتواضع أمامها&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;', ''),
(67, 1, 'صم غيابك', '', ''),
(67, 2, 'صم غيابك', '&lt;p style=&quot;text-align: center; &quot;&gt;ينقُصُني الكثير في غيابك،&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;وجُزءٌ مني مفقود..&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;ليّت للخيّال حقُّ في خلقِ الواقع،&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;لكُنتَ أول حُلمٍ يتحقق..&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;لا أُطالب في دُنياي إلاكَ&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;وأظنُ بأنك حقٌ لا يبُطَل..&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;وحُبك أمرٌ معروف لا يُنكر!&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;سيدي؛&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;شوقُكَ مرضٌ يعُادي كُل أطرافي&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;وما للقلب حيلةٌ عليَّ..&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;اقترب..&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;لعلّ صُمُّ هذه الحياة ينتهي؛&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;ويُطربُني بكَ حباً..&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;كُن بخيّر..&lt;/p&gt;', ''),
(68, 1, 'احلق في سمائك لأنني طيار', '', ''),
(68, 2, 'احلق في سمائك لأنني طيار', '&lt;p&gt;يرن المنبه ليعلن عن اقتراب موعد الرحلة، فأستعد للتأهب، ففي انتظاري طائرة ستحلق بي في سماء ب دي خلف تلك السحب البيضاء ا المبهجة، ومنظر غروب الشمس الذي يضفي رونقاً آخر من الجمال. كل ذلك يمثل لوحة فنية يعجز الرسام عن إتقان رسمها مهما بلغت موهبته.&amp;nbsp;&lt;/p&gt;&lt;p&gt;ارتديتُ بدلتي ذات اللون الكحلي، يزينها شعارٌ مطلي باللون الذهبي ويتوسطه دائرة كُتب عليها الإمارات، ثم أمسكت حقيبتي السوداء تهيُّأً للذهاب إلى المطار.&lt;/p&gt;&lt;p&gt;كان الطريق إلى المطار مليئ بالحيوية والنشاط، فمنظر الأبراج التي تعلو السماء بأضوائها الخافتة، والمركبات التي تسير على الطريق وخيوط غروب الشمس الممزوجة بالألوان المتجانسة والمتناسقة منح للمكان نبض من الروعة لا يخلو من الابداع. ولا أنسى ذلك الابتكار الذي ساعد مدينة دبي خاصةً في تسهيل حركة المرور لوجود الترام والقطار، كل تلك التكنولوجيا وغيرها الكثير والتي يسرت لنا سبل العيش والراحة. فالكثير من المدن مازالت تعاني ليومنا هذا من الازدحام المروري، وضعف توفير التكنولوجيا في مدنهم. ولكن دولة الإمارات بفضل الله وفرت لمواطنيها ولجميع من يعيش على أرضها كل سبل الرخاء والراحة، فالحمدلله دائماً وأبداً لما رزقنا من نعم أكست بلادنا وزادتها جمالاً إلى جمالها الخ ب.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وصلت إلى المطار الذي كان يكتظ بالازدحام دلالة على كثرت المسافرين على متن خطوط طيران الإمارات، فالبعض منهم قادم للسياحة والآخر قادم لزيارة أقاربه، وربما الآخر قادم للعمل. فدبي تعتبر المدينة العصرية لمن أراد أن يعيش برفاهية.&amp;nbsp;&lt;/p&gt;&lt;p&gt;شهد مطار دبي قفزة في التطور من ناحية البنية التحتية، فالتصميم العصري للمطار يواكب التطورات التي تشهدها مدينة دبي، فمن الإنجازات التي شهدتها إمارة دبي تيسير إجراءات دخول وعبور الركاب من البوابات بكل سلاسة، إلى جانب سهولة الإجراءات أثناء السفر. كما تم تزويد مطار دبي بقطارات وحاف ت لنقل الركاب والذي سهل ا عملية ركوب المسافرين ونزولهم من الطائرة واختصار الوقت والجهد، وانصبت هذه الجهود بهدف توفير الراحة للمسافرين وتزويدهم بأفضل الخدمات، هذا الأمر الذي جعل لإمارة دبي بصمة واضحة في التطور والابتكار.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وها أنا في أحد مقاهي المطار احتسي كوباً من القهوة أمام ذلك المنظر الرائع الذي يعجز عن الوصف، والذي ارتسم في مقلة عيني، حيث كنت أتمعن وأترصد حركة إق ع الطائرات وهبوطها بين ا المدرجات والتي أتت من جميع أنحاء العالم.&amp;nbsp;&lt;/p&gt;&lt;p&gt;ومع غروب أشعة الشمس، أخذت تلك الأشعة تختلس نوافذ المطار، وم ت المكان بضوء أ غروبها الخ ب، وأصبح جزء من الظ م يغطي ا ا سماء دبي إلى أن بانت تلك الأضواء الجميلة التي تزين المدرجات والطائرات.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وبعد إنهائي إجراءات السفر وتفتيش الحقائب، التقيت مع طاقم الرحلة وألقيت التحية عليهم، وتبادلنا الأحاديث بما يخص شأن الرحلة ووجهة السفر، وأيضا سرعة تجهيز الطائره لراحة الركاب والمسافرين ثم ذهبنا قاصدين ركوب الطائرة، وخ ل لحظات وجيزة تم الانتهاء من التأكد من ا جاهزية الطائرة بالكامل واستعدادها للإق ع. ا وإذا بصوت النداء يعلن عن ابتداء ركوب الركاب للطائرة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;اكتظ المكان بالركاب وعلى ملامحهم يبدو الارتياح والطمأنينة، ألقيت تحيتي على الركاب من مقصورة الطائرة، وأطلعتهم على إجراءات الس مة، ا وقواعد ركوب الطائرة والتي تمثلت في ربط حزام الأمان والجلوس في المقاعد وغيرها للإع ن ا عن الإقلاع.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وها هي طائرتي تعاود تحليقها في سماء الإمارات قاصدين رحلة أخرى، واكتشاف آخر يضيف إلى خبرتنا وابتكاراتنا وانجازاتنا. لنكشف عن العالم أن الإمارات كانت ومازالت في أوج نهضتها وتطورها.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(69, 1, 'حين يولد الإبتكار', '', '');
INSERT INTO `sp_news_description` (`news_id`, `language_id`, `name`, `description`, `tag`) VALUES
(69, 2, 'حين يولد الإبتكار', '&lt;p&gt;يولد الابتكار الحقيقي أحياناً من لاشيء ومن أشخاص لم يتوقعوا أن مافعلوه كان ابتكاراً مليئاً بالإبداع ، رغم أن المبتكر صاحب صفات تميزه عن الآخرين .&lt;/p&gt;&lt;p&gt;وبعض الابتكارات تبدأ أحياناً بالمصادفة، فتحول أصحابها إلى مبدعين في يوم وليلة. قد تأتي من نقطة يأس ولا تخرج من نهر أمل ، وقد تأتي بدون تخطيط مسبق ، لتُحدِثَ في العالم بعد مجيئها ضجيجاً أكبر من خطة عمل شديدة الانتظام .&amp;nbsp;&lt;/p&gt;&lt;p&gt;» والت ديزني « وهذا تماماً ما حدث مع الأمريكي مستوحياً » ميكي ماوس « الذي ابتكر شخصية إياها من مجرد فأرٍ ضئيل يربيه في منزله فيمنحُهُ شعبية جارفة يندر أن يحظى بمثلها إنسان .&amp;nbsp;&lt;/p&gt;&lt;p&gt;مز وشعار لشركته » ميكي « من » ديزني « لقد صنع ، مدفوعاً بغضبه ويأسه لتلقين الشركة المنتجة التي كان » أوزوالد الأرنب المحظوظ « لشخصية&amp;nbsp; يرسمها درساً بعد رفض الشركة دفع ميزانية إنتاج كبيرة لأفلامه ، ويقرر التوقف عن رسم وينشيء استوديو خاص يخلصه من » أوزوالد « ارتباطاته المرهقة مع الآخرين ، فحوله ابتكاره إلى أشهر منتج لأكثر » ميكي ماوس « شخصية شخصية مفضلة ومحبوبة في العالم.&amp;nbsp;&lt;/p&gt;&lt;p&gt;ذي ا » النمر الوردي « أما بطل الأف م الكرتونية&amp;nbsp; يثير المشاكل بسذاجته وتصرفاته الحمقاء لكنه فريزر « لايقع فيها، فقد بدأ بفشل ذريع لمبتكره الذي بدأ حياته المهنية موظفاً في أحد » فريلينغ ثم انتقل » كانساس سيتي « أستوديوهات مدينة الأخوة وارنر( ( » وارنر بروس « للعمل في شركة مديراً للإنتاج الكرتوني لأكثر من 0 عاماً حتى 3 قرر الإخوة إغ ق قسم الرسوم المتحركة في ا الشركة، فاضطر للتعاون مع المسؤول التنفيذي لإنتاج بعض الأف م ا » ديفيد ديباتي « في الشركة الذي » النمر الوردي « الكرتونية ومن بينها سرعان ما احتل مقعد الشهرة متحولاً إلى أكثر شخصية ممتعة يحبها الأطفال والكبار في العالم .&amp;nbsp;&lt;/p&gt;&lt;p&gt;الذي اكتشف » إدغار رايس « وعندما حرم المرض الأطباء وجود خلل في إحدى صمامات قلبه من متابعة حياته العسكرية بعد تخرجه من الأكاديمية العسكرية في ميتشيغان وتسريحه من الخدمة العسكرية ، اضطر للعمل بائعاً للقرطاسية في أحد الأكشاك، حتى وقع بصره على مجلة قصص إذا كان « مصورة ، وقال لنفسه وهو يتصفحها الكتاب يقبضون ثمناً لكتابة مثل هذه الخزعب ت اا .» فبإمكاني كتابة خزعب ت أحسن منها بكثير أولى قصصه عن » رايس « و في نفس العام نشر التي تحولت » طرزان الرجل القرد « طرزان بعنوان إلى السينما وتم تجسيدها بفيلم لاقى اهتماماً جماهيرياً هائ . وقبل أن يتوفى رايس كتب في ا ،» أنا أكتب لأهرب..لأهرب من الفقر « : مذكراته 1 2 ولم يغمض عينيه إلا بعد أن كتب 9 رواية 6 منها كانت عن شخصية طرزان. وقد باعت كتبه ملايين النسخ وترجمت إلى نحو 0 لغة.&lt;br&gt;&lt;/p&gt;', ''),
(70, 1, 'ثلاثية محمد بن راشد', '', ''),
(70, 2, 'ثلاثية محمد بن راشد', '&lt;p&gt;ما إن أشار صاحب السمو الشيخ محمد بن راشد آل مكتوم بأصابعه الث ث في فبراير 03 م 1 ا 2 في مؤتمر القمة الحكومية معلنا تضمنها المعاني الث ث: الفوز والنصر والحب حتى أصبحت هذه ا العلامة شعاراً يرفعه المجتمع الإماراتي بشرائحه المختلفة، وتتسابق المؤسسات والدوائر الحكومية في ترسيخ معانيه في قلوب موظفيها للتحفيز على الإنتاج المبدع الخ ق، بل تحولت هذه الثلاثية ا في الأسواق الإماراتية، » ماركة « إلى منتج تجاري ولم يقتصر تأثير هذه العلامة على المجتمع المحلي وإنما وصل تأثيرها إلى المجتمعات العربية والعالمية، فنجد الكثير من مشاهير العالم ممن قاموا برفع هذه العلامة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;والسؤال: ما هو سر هذه الثلاثية السحرية؟&lt;/p&gt;&lt;p&gt;إن هذه العلامة الابتكارية وما أحدثته من صدى واسع جعلني أتأمل في لغة الإشارة وتأثيرها وكاريزما أصحابها، فلو نظرنا إلى الأصابع الث ث: الإبهام، ا والسبابة، والوسطى لوجدنا أن لكل منها دلالة ومعنى، فالإبهام يدل على القوة والحزم والقدرة على تنفيذ التفاصيل الدقيقة، والسبابة تدل على القيادية وتحمل المسؤولية والرغبة الملحة في تصدر الصفوف الأولى، أما الوسطى تدل على الاتزان والوسطية والحكمة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;من ناحية أخرى يوحي شكل الأصابع البارزة من اليد بشكل النخلة الباسقة ذات الطلع النضيد، فهذه النخلة طويلة ترتفع إلى السماء، تبلغ مبلغاً لا يبلغه كثير من الأشجار، ثابتة في الأرض، قادرة على مقاومة أقسى ظروف الطبيعة، مباركة بما تجود به من ثمار.&amp;nbsp;&lt;/p&gt;&lt;p&gt;كل هذه المعاني الاستثنائية اكتسبت قيمتها ومصداقيتها في قلوب الناس كونها صدرت من ثُلاثيّة محمد بِن راشد السحريّة شخصية تتميز بالصدق والكفاءة والقدرة الهائلة على تحقيق الغايات المنشودة، فالشيخ محمد بن راشد شخصية قيادية غير عادية، يتميز بمميزات فذة، يربط الأح م بالواقع عن طريق أحرف ث ث هي ا ا فهو إن قال فعل، ووعوده تؤكدها إنجازاته ،» عمل « خاصة دبي التي تعد مثالاً واضحاً على أن المستحيل صار ممكناً، فهو لايعرف المستحيل، يكره الأبواب المغلقة والمكوث في الأبراج العاجية، ولعل من أبرز ما يميزه عشقه للتواصل الحي مع الناس بكل الوسائل الممكنة، وهو ما أضفى كاريزما خاصة على شخصيته، فأصبحت أقواله وأفعاله وحتى إشاراته لها تأثير السحر على من حوله.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(71, 1, 'قواعد الحياة', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', ''),
(71, 2, 'قواعد الحياة', '&lt;p&gt;دليل شخصي للحصول على حياة أفضل وأسعد وأكثر نجاحاً&lt;/p&gt;&lt;p&gt;عن الكتاب :&lt;/p&gt;&lt;p&gt;كتاب قواعد الحياة هو أحد سلسلة كتب ذات نفس الطابع والفكرة، مثل قواعد الحب، والعمل، والإدارة لمؤلفها: “ريتشارد تمبلر”.&lt;/p&gt;&lt;p&gt;يقدم تمبلر على مدى 290 صفحة مئة قاعدة تساعد القارئ على الحصول على حياة أفضل. الكتاب ينقسم إلى خمس فصول، أولا:ً قواعد للشخص نفسه، ثانياً: قواعد عامة للحياة، ثالثاَ: قواعد للعائلة والأصدقاء، رابعاً: قواعد للحياة الاجتماعية، وأخيرا قواعد للسعادة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;في مقدمة الكتاب يقول الكاتب أن بعض الأشخاص يجدون الحياة سهلة للغاية ويعرفون دائماً ما هوالتصرف اللائق لكل موقف ويبدون سعيدين ومتكيفين مع الحياة دائماً. يأتي هذا الكتاب كملخص للدروس التي تعلّمها الكاتب خلال حياته ليساعد كل من يقرأه أن تكون له حياة أفضل وأسعد.&amp;nbsp;&lt;/p&gt;&lt;p&gt;الكاتب “ريتشارد تمبلر” متخصص في الموارد البشرية، وصاحب ثلاثين عاماً من الخبرة في هذا المجال، حققت كتب هذه السلسلة نسبة مبيعات عالية جداً وترجمت للعديد من اللغات. يعد الكتاب ممتازاً لمحبي القراءة السريعة لأسلوبه السلس والبسيط. يلخص تمبلر الحياة في أنها تجربة غنية ومثيرة للتحدي، ولكنها تمضي بسرعة كبيرة أكثر مما نتصور، لذا على كل فرد أن يفكر بأنه يريد الأفضل من هذه الحياة حتى يحقق ما يستدعي الفخر والتذكر.&amp;nbsp;&lt;/p&gt;&lt;p&gt;يصنف الكاتب الأشخاص إلى صنفان، الأول: الأشخاص كثيري التذمر والشكوى ويثيرون الاحباط وينشرون الطاقة السلبية من حولهم لذا يجب علينا الابتعاد عنهم للحصول على حياة أسعد وأفضل. والصنف الثاني: هم من يرون أن الحياة تحدي، ودائماً ما يكونون مفعمين بالحيوية وينشرون المشاعر الإيجابية من حولهم، لذا مرافقة هؤلاء ينعكس ايجابياً على الشخص وتصرفاته في الحياة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;يركز الكاتب على أهمية الحلم، وأن لا نضع حدوداً لأحلامنا،وإذا أردنا أن نحقق أحلامنا يجب أن تكون هناك خطة واقعية مدروسة بكل ما نحتاجه هو السعي والمثابرة لتحقيق الحلم.&amp;nbsp;&lt;/p&gt;&lt;p&gt;الكتاب بشكل عام ذو محتوى بسيط لكنه عميق، يركز ويتحدث عن أحداث وقرارات يومية بسيطة، لكن على المدى البعيد يبدو أعمق مما هو عليه، وتطبيق هذه القواعد شيئاً فشيئاً يجعل الحياة تبدو أفضل وأسهل.&lt;br&gt;&lt;/p&gt;', ''),
(72, 1, 'لماذا نكتب', '', ''),
(72, 2, 'لماذا نكتب', '&lt;p&gt;&lt;b&gt;لماذا نقوم بفعل الكتابة ؟ولماذا يُكتب الكِتاب ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;وكيف يواجه الكَاتب تلك الحالات عندما تستعصي&lt;/p&gt;&lt;p&gt;&lt;b&gt;عليه الكتابة؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أسئلة كثيرة من هذا النوع تواجه مختلف الكُتّاب من حول العالم وفي محاولة لإجابة هذه الأسئلة راودت “ميراديث ماران” فكرة هذا الكتاب. تقول ميريدث أنها تكتب لتجيب عن أسئلتها وبسبب أسئلتها الكثيرة عن عالم الكتابة الإبداعية قررت أن تقوم بمقابلة مجموعة من المؤلفين المبدعين في هذا المجال والذين استطاعوا أن يحولوا هذه الموهبة إلى عمل يدر عليهم الأموال.&amp;nbsp;&lt;/p&gt;&lt;p&gt;من خلال ثلاثمئة صفحة حاولت ميريدث أن تحصل على إجابات لأسئلتها، وذلك عن طريق مقابلتها لعشرين كاتباً ومؤلفاً، ابتداءً من “إيزابيل الليندي” ل “جيمس فري” و “سارا غروي” وانتهاء ب “ميغ واليتزر”. قسمت ميريدث الكتاب إلى عشرين فصلاً، كل فصل عن كاتب معين. في بداية كل فصل هناك اقتباس من إحدى روايات أو مؤلفات الكاتب ومعلومات عامة عن الكاتب كعدد مبيعات كتبه وبعض الجوائز التي حاز عليها. يليها صفحة بكل المعلومات الأساسية عن الكاتب كالنشأة، والتعليم والحياة العائلية بالإضافة إلى قائمة بالمؤلفات. وأخيراً إجابات الكاتب على الأسئلة المتعلقة بالكتابة وطقوسها وبعض النصائح إلى الكُتّاب الطموحين.&amp;nbsp;&lt;/p&gt;&lt;p&gt;ترجمة هذا الكتاب إلى العربية هو جزء من مشروع تكوين وهو مشروع ثقافي متخصص في الكتابة الإبداعية ويهدف إلى بناء قاعدة بيانات مختصة بهذا الفن. جميع المترجمين العرب الذين عملوا على هذا المشروع شاركوا بالترجمة تطوعاً منهم وبدون مقابل. الجزء الأهم في هذا الكتاب أن ريعه بالكامل مخصص لصالح تعليم الأطفال.&amp;nbsp;&lt;/p&gt;&lt;p&gt;من ناحية أخرى أسلوب الكتاب سلس للغاية ومليء بالاقتباسات المحرضة على الكتابة. قرأت كتب من قبل بنفس الفكرة عن طقوس الكتاب والمؤلفين وعاداتهم أثناء الكتابة ولكنها لم تكن ممتعة مثل “لماذا نكتب”. لم تتبع ميريدث أسلوب المقابلات الرتيب بل وضعت الإجابات بأسلوب سردي جميل يشد القارئ.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أعتبر هذا الكتاب مرجعاً مهماً لكل المهتمين بالكتابة الإبداعية، لكل المتسائلين عن هذه الرغبة الملحة داخلنا بأن ندون ضجيج الأفكار الذي يكمن في عقولنا على الورق.&amp;nbsp;&lt;/p&gt;&lt;p&gt;أختم نقدي لهذا الكتاب بإجابة “تيري وليمز” عندما سألت لماذا تكتب فقالت “ أكتب لأتصالح مع الأشياء التي لا أستطيع السيطرة عليها. أكتب لكي أصنع نسيجاً في عالم يظهر غالباً بالأسود والأبيض. أكتب لأكتشف. أكتب لكي أواجه أشباحي. أكتب لكي أبدأ حواراً. أكتب لأتخيل الأشياء على نحو مختلف، وبتخيل الأشياء ربما يمكن للعالم أن يتغير”.&lt;br&gt;&lt;/p&gt;', ''),
(73, 1, 'مركز الجليلة', '', ''),
(73, 2, 'مركز الجليلة', '&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;مركز الجليلة لثقافة الطفل&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p style=&quot;text-align: center; &quot;&gt;&lt;b&gt;تعزيز روح الإبداع والابتكار في إطار المشاركة والتعاون&lt;/b&gt;&lt;/p&gt;&lt;p&gt;في بيئة مليئة بالمرح والابتكار ترحب بالأطفال وعائلاتهم، ونحو إثراء حياة الطفل بالبرامج الفنية والثقافية الواسعة، وغرس الموروث الثقافي له وتعزيز قدراته ومهاراته على فهم العالم من حوله، إلى جانب توفير بيئة تعلُّم متميزة تحض على الرغبة في التعاون والمشاركة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;في هذا الإطار تحدثنا شيماء خوري المدير التنفيذي في مركز الجليلة لثقافة الطفل- دبي أكثر عن أهمية المركز في تعزيز الابتكار وتأصيل الهوية&amp;nbsp; والثقافة الإماراتية لدى الأطفال.&lt;/p&gt;&lt;p&gt;&lt;b&gt;أولاً نود أن نعرف متى تأسس هذا المركز وما هو الهدف الأساسي منه؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;تم تدشين مركز الجليلة لثقافة الطفل في شهر ديسمبر من العام 2014 ، ليكون وجهة إماراتية متخصصة في تمكين الحياة الثقافية لدى الأطفال، واحتضان إبداعاتهم وطموحاتهم من خلال توفير بيئة آمنة تعمل على توظيف مهاراتهم وطاقاتهم في أماكنها الصحيحة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما الذي يجعل مركز الجليلة لثقافة الطفل مختلفاً عن أي مركز آخر ؟ و ما الذي يميزه عن غيره؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يتميز مركز الجليلة لثقافة الطفل باعتماد منهج عمل يرتكز على الإبداع، عبر وسائل فنية مختلفة مفيدة ومسلية في الوقت ذاته. كما ننطلق في أفكارنا و توجهاتنا نحو التراث الثقافي الإماراتي خصوصاً والعربي عموماً لنحافظ على شعور الطفل&amp;nbsp; بالانتماء والارتباط مع موروثه، دون أن نغفل عن الروح العصرية ومتطلبات جيل هذا الزمن، فمن أساسيات اهتماماتنا تعزيز الوعي لدى الأطفال وإرساء فهمهم لمحيطهم.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما أهمية تعزيز ثقافة الإبتكار عند الأطفال في الصغر؟ وهل هذا يؤثر في شخصيته عند الكبر؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أثبتت الدراسات أن ذاكرة الطفل في سنوات وعيه الأولى تشكل أفضل مراحلها في التلقّي، حيث يكون ذهن الطفل مثل التربة الخصبة، كل ما يُزرع فيه يثمر حين يكبر. ودورنا يكمن في حثّه من عمر مبكر على الإبداع والتفكير والاختراع والاستكشاف، خصوصاً أن العصر الحديث قدم للطفل كل ما من شأنه أن يفكر بالنيابة عنه، هذا ما ينعكس سلباً على طاقاته ونشاطه الدماغي وهذا ما لا نحبذه. فنحن نتطّلع في مركز الجليلة لثقافة الطفل إلى تعزيز ثقافة الابتكار في الطفل وتأهيل جيل من القادة المبادرين والمثابرين وهذا واجبنا تجاه أبنائنا ومجتمعنا.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هي الأساليب المتبعة في مركز الجليلة لثقافة الطفل لتشجيع الأطفال على أن يكونوا أكثر إبداعاً وابتكاراً؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;في كافة أقسام مركز الجليلة لثقافة الطفل نقدم المعلومة بطرق فنية ممتعة، و مبتكرة، و إبداعية وتطبيقية مع ترك مساحة من الحرية للطفل لتحفيز خياله ووضع بصمته الخاصة، على سبيل المثال حين نقدم عملاً مسرحياً نستقي أفكاره من الأطفال أنفسهم ونترك لهم فرصة المشاركة في كتابة النص والتمثيل أيضاً في المرسم، كذلك وفي الإذاعة ومختلف الأقسام.&amp;nbsp;&lt;/p&gt;&lt;p&gt;وعلى مستوى التراث فإننا دوماً نجعلهم يعاينون وينفذون بأنفسهم، صناعة الدخون، والضيافة الإماراتية أو ابتكار أدوات من عمق البيئة الإماراتية مثل المباخر وغيرها من خلال المخيمات.&amp;nbsp;&lt;/p&gt;&lt;p&gt;كما أننا قدمنا هذه السنة عملاً إبداعياً ضخماً وهو “مندوس” الذي تمكنّا من خلاله أن نوصل جزءاً من تراثنا الإماراتي للطفل عبر مسلسل ثلاثي الأبعاد عصري وممتع.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هي أقسام المركز المختلفة التي تحفز الأطفال على الابتكار؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;تتنوع أقسام المركز في توجهاتها بين الفنية، والحرفية، والأدائية والفكرية، وهي كالتالي&lt;/p&gt;&lt;p&gt;&lt;img src=&quot;http://localhost/bitbucket/emirates-diaries/image/catalog/news/center-of-the-majestic/chart.jpg&quot; style=&quot;width: 100%;&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هي الأعمار التي يستهدفها المركز، وكيفية الانضمام له ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;خدمات المركز متاحة للأطفال من 4 إلى 16 سنة، من مختلف الجنسيات، كما نستقبل الأطفال وفق جدول دورات منتظم، إلى جانب نظام العضوية السنوية ونصف السنوية للاستفادة من كافة أقسام المركز، و نقدم دورات متخصصة نتعاون من خلالها مع الجهات المحلية والمشاريع الإبداعية الإماراتية منها دورة صناعة الطائرات الذكية والخط العربي.. وغيرها.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ختاماً كيف يمكن للجمهور أن يصل إلى مركز الجليلة لثقافة الطفل ؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;مركز الجليلة لثقافة الطفل موجود في دبي بمنطقة أم سقيم 3 شارع الوصل، مقابل أكاديمية الشرطة، ويمكن الاستفسار هاتفياً عبر الرقم: 043169999 www. كما يمكن زيارة موقعنا لمزيد من التفاصيل أو حسابات المركز في مواقع التواصل ajccc.ae&amp;nbsp; AJCCCUAE@ : الاجتماع&amp;nbsp;&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(74, 1, 'نوراي', '', ''),
(74, 2, 'نوراي', '&lt;p&gt;&lt;b&gt;نُوراي،، قلب الإمارات.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;ربمّا تكون القلّة منّا قد قرأت عن نوُراي، وسحر نوراي، وروعة نوراي، وجمال نوراي الخلاّب!! وقد تكون القلّة الأخرى قد زارت “نوُراي” تلك التي تقبع في قلب الإمارات، وتعيش على امتداد مياه خليجها الرقراقة، وتنام بكلّ هدوء وسحرٍ ورقة على شواطئها الفيروزية الصافية. نُوراي، مشروع هندسي مبتكر وآسر، هي جزيرة تقع في قلب مدينة أبوظبي، وتُعدّ قريبة جداً من جزيرة السّعديات الآسرة هي الأخرى.&lt;/p&gt;&lt;p&gt;&lt;b&gt;جزيرة الحُسن.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;نُوراي اسم مشتق من “نور”، ويُقال إنّهُ: قد أطلق عليها هذا الاسم لحُسنها الظاهر واللافت، ولونها الأزرق المشرق المشع والمنعكس عليها من مياه الخليج العربية الدافئة، وعلى الرغم من أنّها صناعة بشرية، إلاّ&amp;nbsp; أن الناظر إليها يكادُ يحسبها جنّةٌ على الأرض.&lt;/p&gt;&lt;p&gt;&lt;b&gt;حقائق وأرقام.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;نوُراي هي الجزيرة العذراء!! والفردوس المفقود!! إذ يقُال: إنّهُ لم تطأ شواطئها الرملية الأقدام من قبل. وتمتدّ نُوراي على مساحة 306 ألف متر مربع، وهي بعيدة كل البعد عن صخب المدينة ومتاعب الحياة، كما تبعد مسافة 10 دقائق بالمركب عن جزيرتيّ ياس والسعديات، و 15 دقيقة عن مطار أبوظبي الدولي، و 60 دقيقة عن قلب مدينة دبي، حيثُ يمكن الوصول إليها من كافة موانئ&amp;nbsp; السفر الكبرى في دولة الإمارات.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;فخامة نُوراي.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;نُواري منتجع ذو فخامة عالية لكلّ من يبحث عن الراحة والهدوء، صُمم بمعايير عالمية فائقة بحيث يمنح كل من يزورهُ أقصى درجات الاستجمام والدلال، وتحتوي الجزيرة على مطاعم كثيرة مختلفة وتقدّم بعض تلك المطاعم الأغذية العضوية القادمة من المزارع المحلية.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;وإلى جانب المطاعم يضمّ المنتجع نادياً صحياً للكبار والأطفال، ومركزاً للغواصين المحترفين، وفللاً جميلة مطلّة على البحر، إذ تُتيح هذهِ الأخيرة لكل زائر لها بأن يعيش الحلم حقيقة، فهي الملاذ الآمن، والجنّة الخضراء، والمرسى لمن ينشد السكينة والهدوء.&amp;nbsp;&lt;/p&gt;&lt;p&gt;نُواري هي المكان الساحر العطر، وهي الجنّة الصغيرة على الأرض، تلك التي تحظى بجمالها الطبيعي مع روعة المعمار الإنساني فيها.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(75, 1, 'مسجد الشيخ زايد الكبير', '', ''),
(75, 2, 'مسجد الشيخ زايد الكبير', '&lt;p&gt;يُعتبر جامع الشيخ زايد الكبير من المعالم الشهيرة في أبوظبي، إذ إنه ثالث أكبر مسجد في العالم بعد المسجد الحرام بمكة المكرمة، والمسجد النبوي بالمدينة المنورة في المملكة العربية السعودية، و يعتبر صرحاً إسلامياً بارزاً في دولة الإمارات العربية المتحدة، ومعلماً يحتفي بالحضارة الإسلامية، ومركزاً مهماً لعلوم الدين الإسلامي، وقد بلغت مساحته 412،22 متراً مربعاً، ويتسع المسجد لأكثر من 7000 مصلٍ في الداخل،ومن الممكن استعمال المساحات الخارجية والتي تتسع لحوالي 40،000 مصلٍ لكافة أقسام مبنى المسجد، ومن معالمه المميزة وجود أربعة مآذن في أركان الصحن الخارجي بارتفاع 107 أمتار للمأذنة والمكسية بالكامل بالرخام الأبيض.&lt;/p&gt;&lt;p&gt;&lt;b&gt;نظرة مستقبلية&lt;/b&gt;&lt;/p&gt;&lt;p&gt;بإشراف خاص من رئيس الدولة الشيخ زايد بن سلطان آل نهيان - رحمه الله- ، فقد وجه ببناء هذا المسجد في العام 1996 ساعيًا بذلك لأن يكون هذا المسجد صرحاً إسلامياً يرسخ ويعمق الثقافة الإسلامية ومفاهيمها وقيمها الدينية السمحة، ومركزاً لعلوم الدين الإسلامي، و أول صلاة قامت في المسجد كانت صلاة عيد الأضحى المبارك في العام 1428 هجرياً ) 19 ديسمبر 2007 ( ولكن آنذاك لم تنته أعمال البناء في المسجد تماماً، وتم الانتهاء من أعمال البناء في مارس 2008 ، حيث بلغ إجمالي تكلفة المشروع مليارين و 167 مليون درهم إماراتي.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;عزيمة و إصرار&lt;/b&gt;&lt;/p&gt;&lt;p&gt;تم بناء المسجد على ارتفاع 9 أمتار عن مستوى الشارع&amp;nbsp; بناءً على أوامر الشيخ زايد شخصيًا، بحيث يمكن رؤية المسجد من زوايا مختلفة ومن مسافة بعيدة، كما قامت عدة شركات بالعمل على المشروع، وتوقفت عدة شركات عن العمل في أوقات مختلفة لأسباب عدة. وبدأت شركة هالكرو الإنجليزية أعمالها كاستشاري على المشروع في 2001 ، وعملت مع شركة المقاولات الإيطالية امبريجلو لإنهاء المشروع.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;تصميم حضاري فريد&lt;/b&gt;&lt;/p&gt;&lt;p&gt;تعد قبة المسجد الرئيسية أكبر قبة في العالم، حيث يبلغ ارتفاعها 83 متراً وبقطر داخلي يبلغ 32،8 . وتزن القبة ألف طن، وزخرفت من الداخل بالجبس المقوى بالألياف، صممه فنانون عرب بزخارف نباتية فريدة، صممت خصيصاً للمسجد، بالإضافة إلى كتابة آيات قرآنية، ويصل عدد القباب في هذا المسجد 82 قبة مختلفة الأحجام، تغطي الأروقة الخارجية والمداخل الرئيسية والجانبية، وجميعها مكسوة من الخارج بالرخام الأبيض المتميز ومن الداخل بالزخارف المنفذة من الجبس التي قام بتنفيذها فنيون مهرة متخصصون بمثل هذا النوع من الأعمال.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;فخامة البناء&lt;/b&gt;&lt;/p&gt;&lt;p&gt;استخدم المصممون الفسيفساء لتغطية ساحة المسجد بالكامل ) 17 ألف متر مربع(، وهي تُعدّ من أكبر ساحات المساجد في العالم ، وتزين المسجد سبع ثريات كريستال مختلفة الأحجام، ومصنوعة من كريستال شواروفسكي ومطلية بالذهب، وقد قامت كبرى الشركات العالمية المتخصصة بتصنيعها.&lt;/p&gt;&lt;p&gt;أوقات الزيارة&lt;/p&gt;&lt;p&gt;يفتح جامع الشيخ زايد الكبير أبوابه للمصلين طوال اليوم. أما لغير المسلمين، فقد تم تحديد وقت الزيارة لهم ما بين الساعة التاسعة صباحاً والعاشرة مساءً كل يوم ما عدا الفترات الصباحية من أيام الجمعة، إضافة على ذلك تقوم هيئة أبوظبي للسياحة والثقافة بتنظيم جولات سياحية في المسجد.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;المسجد بأرقام قياسية&lt;/b&gt;&lt;/p&gt;&lt;p&gt;يتميز المسجد بمجموعة من الصفات المعمارية التي تجعله إحدى أروع التحف المعمارية على مستوى العالم.&lt;/p&gt;&lt;p&gt;تبلغ المساحة الإجمالية للمسجد 22،412 متراً مربعاً.&lt;/p&gt;&lt;p&gt;تم استخدام 33،000 طناًّ من الحديد و 250،000 متراً مكعباً من الإسمنت، كما يقوم المسجد على 6،500 عمود أساس&lt;/p&gt;&lt;p&gt;تتضمن ساحة المسجد 1048 عموداً&lt;/p&gt;&lt;p&gt;توجد في المسجد 82 قبة&lt;/p&gt;&lt;p&gt;تعدّ قبة المسجد الرئيسية أكبر قبة مسجد في العالم، ويبلغ ارتفاعها 85 متراً وقطرها 32،8 متراً&lt;/p&gt;&lt;p&gt;يتسع المسجد لنحو 40،000 مصلٍّ، وقاعة الصلاة الرئيسة لنحو 7،000 مصلٍّ.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;', ''),
(76, 1, 'مركز محمد بن راشد للابتكار الحكومي', '', ''),
(76, 2, 'مركز محمد بن راشد للابتكار الحكومي', '&lt;p&gt;مع إعلان صاحب السمو الشيخ خليفة بن زايد آل نهيان رئيس الدولة أن عام 2015 هو عاماً للإبتكار. تم بعد ذلك إزاحة الستار عن الاستراتيجية الوطنية للابتكار و تأسيس مركز محمد بن راشد للابتكار الحكومي والذي حمل اسمه صاحب السمو الشيخ محمد بن راشد آل مكتوم نائب رئيس الدولة رئيس مجلس الوزراء حاكم دبي في أواخر العام 2014 ليصبح مركزاً عالمياً لتحفيز الابتكار بشكل يومي في القطاع الحكومي، وتطبيقاً لرؤية صاحب السمو الشيخ محمد بن راشد آل مكتوم الهادفة إلى تطوير العمل الحكومي، إلى جانب تعزيز تنافسية دولة الإمارات خارجياً، والانتقال من مبادرات الابتكار المتفرقة إلى إطار مؤسّسي متكامل من خلال تبنّي المركز أحدث نظريّات الابتكار التي من شأنها أن تعزّز مكانة الحكومة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;تطمح دولة الإمارات العربية المتحدة ان تكون في مصاف الدول المتقدمة في الابتكار. لذلك، يعمل المركز على تحويل الابتكار الحكومي إلى عمل مؤسّسي منظّم وركيزة أساسية من ركائز حكومة دولة الإمارات العربية المتحدة.&lt;/p&gt;&lt;p&gt;“هدفنا هو خلق بيئة متكاملة للابتكار، لذا نعمل على إنشاء الأجيال، وتشجيعهم على الابتكار منذ الصغر، وحضانة وتطبيق&amp;nbsp; الأفكار لمعرفة مدى فعاليتها باستمرار، لأن الابتكار هو رأس المال المستقبلي”&lt;/p&gt;&lt;p&gt;صاحب السمو الشيخ محمد بن راشد آل مكتوم نائب رئيس الدولة رئيس مجلس الوزراء وحاكم دبي.&lt;/p&gt;&lt;p&gt;مجلة “مذكرات إماراتية” تتفرد بمقابلة حصرية مع هدى الهاشمي، المدير التنفيذي لمركز محمد بن راشد للابتكار الحكومي&amp;nbsp; لمعرفة المزيد عن هذا المركز الفريد من نوعه&lt;/p&gt;&lt;p&gt;&lt;b&gt;بدايةً، ما الدور الذي تقومون به في مركز محمد بن&amp;nbsp; راشد للابتكار الحكومي؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;من خلال هذا المركز، تسعى دولة الإمارات العربية المتحدة إلى تطوير مجموعة من الأدوات الحديثة الشاملة لمساعدة الجهات الحكومية على الابتكار في قطاع السياسيات والخدمات المقدمة للجمهور، على سبيل المثال لا الحصر. ومن خلال هذه الأدوات يهدف المركز إلى تحويل الابتكار الحكومي إلى نظام عمل منظم وذو دور مهم كمؤشر رئيسي في استراتيجية حكومة دولة الإمارات.&amp;nbsp;&lt;/p&gt;&lt;p&gt;عملنا يعتمد على خلق ثقافة عمل عبر الجهات الحكومية ودفعها إلى تطبيق مفهوم الابتكار. ولتحقيق ذلك نعمل نحن في المركز على توفير الأدوات، وتطوير الورش العملية، واستضافة النقاشات والحوارات والمحاضرات بالإضافة إلى برنامج الدبلوم في الابتكار في القطاع الحكومي وهو برنامج تجريبي أنشأ للموظفين الحكوميين.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما هي المجالات الرئيسية التي يركز عليها المركز؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;في مركز محمد بن راشد للابتكار الحكومي يتم التركيز على ثلاثة ركائز أساسية وهي: “التجربة، والتمكين،&amp;nbsp; والإثراء”&lt;/p&gt;&lt;p&gt;فالركيزة الأولى وهي “التجربة” تعمل على توفير مساحة على مستوى عالمي لخلق وتجربة وتشجيع الابتكار بجميع نواحيه، في حين يركز “التمكين” أكثر على بناء القدرات الابتكارية من خلال تزويدهم بالمعارف والممارسات الأساسية، من أجل تطوير المجتمع من ذوي المهارات العالية واتصال جيد من المبدعين لتمكين الموظفين من الابتكار في القطاع الحكومي، أما الركيزة الأخيرة تهدف إلى إثراء ثقافة الابتكار في مجتمع الإمارات وجعل الابتكار ممارسة يومية في حكومة الدولة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;ما الهدف الذي يسعى المركز لتحقيقه؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;من خلال المركز تسعى دولة الإمارات العربية المتحدة إلى أن تكون في مصاف الدول المتقدمة في الابتكار. وبذلك، يعمل المركز على تحويل الابتكار الحكومي إلى عمل مؤسّسي منظّم وركيزة أساسية من ركائز حكومة دولة الإمارات العربية المتحدة.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;من هي الفئة التي يستهدفها المركز؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;بشكل عام نستهدف موظفي القطاع الحكومي بما أن أغلب برامجنا حول الابتكار في القطاع الحكومي. من جهة أخرى يقوم الأفراد من مختلف الجهات الاتحادية والحكومات المحلية بحضور الورش والبرامج المقدمة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;هل يمكنك أن تخبرينا أكثر عن الورش والبرامج&amp;nbsp; التي يقدمها المركز؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;هناك العديد من البرامج والورش التي يقدمها المركز والتي نسعى من خلالها أن تكون بشكل شهري، على سبيل المثال :&amp;nbsp;&lt;/p&gt;&lt;p&gt;١- مختبر الابتكار الحكومي، وهو عبارة عن مجموعة من جلسات وورش عمل مبنية على منهجية مدروسة تستخدم أحدث الطرق والوسائل المبتكرة للتوصّل إلى أفكار مبدعة وإيجاد حلول واقعيّة للتحديات التي تواجه الجهات الحكوميّة. ويتمّ ذلك من خلال جمع كافة الأطراف المعنيّة بالمختبر لمناقشة مواضيع ضمن فرق عمل محدّدة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;٢- سلسلة جلسات “حوار الابتكار” وهي عبارة عن جلسات حوارية تعقد على مدار العام وتستخدم طرق ووسائل مبتكرة للتوصّل إلى أفكار مبتكرة وإيجاد حلول واقعيّة للتحديات التي تواجه الجهات الحكوميّة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;٣- سلسلة ندوات “الابتكار الحكومي” وهي سلسلة من الندوات وورش العمل المتخصّصة الهادفة إلى نشر الوعي والمعرفة وتعزيز ثقافة الابتكار، ودعم القطاع الحكومي في إحداث تحول جذري وشامل في أدوات وأساليب عمله بالإضافة الى تطوير مهارات الابتكار لدى الكوادر الوطنيّة ، وذلك بالتعاون مع جامعة “إنسياد”.&amp;nbsp;&lt;/p&gt;&lt;p&gt;٤- سلسلة ندوات “ابتكر” وهي عبارة عن مجموعة من الندوات التي تهدف الى التعريف بمفهوم الابتكار الحكومي وأهدافه، وتعزيز الوعي بأهميته والتعريف بأدواته، من خلال إشراك أكبر عدد من الموظفين الحكوميين، والعمل على بناء القدرات الوطنية المتمكنة في مختلف المجالات عن طريق بناء معارفهم والاستثمار في مواهبهم في الابتكار والريادة للعمل معاً. توفر المبادرة صندوق يسمى بصندوق “ابتكر” والذي يحتوي على مجموعة من الأدوات التي تحفز على التفكير المبتكر.&amp;nbsp;&lt;/p&gt;&lt;p&gt;٥- برنامج دبلوم خبير الابتكار لمدة عام واحد، والذي أطلقه مركز محمد بن راشد للابتكار الحكومي، بالتعاون مع جامعة كامبريدج البريطانية، يهدف البرنامج إلى تطوير قدرات الكوادر الإماراتيّة وتزويدها بالمعارف، والمهارات والخبرات، بما يسهم في تفعيل دورها في تعزيز ثقافة الابتكار في مختلف الجهات الحكومية.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;هذه بعض النماذج التي يقدمها مركز محمد بن راشد للابتكار، وهناك العديد من المبادرات والورش التي يمكنكم أن تتعرفوا عليها أكثر من خلال زيارة موقع المركز.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;سمعنا الكثير عن برنامج دبلوم خبير الابتكار&amp;nbsp; الحكومي ، حدثينا عن هذا البرنامج المميز؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أطلق البرنامج من المركز وبالتعاون مع كبار هيئة التدريس بجامعة كامبريدج البريطانية بهدف تطوير قدرات الكوادر الإماراتيّة وتزويدها بالمعارف، والمهارات والخبرات. وأول دورة من البرنامج انطلقت في ابريل 2015 وضمت أكثر من 50 مشارك.&amp;nbsp;&lt;/p&gt;&lt;p&gt;تشمل اهداف البرنامج الأساسية الآتي :&lt;/p&gt;&lt;p&gt;- المساهمة بفعالية في تحقيق رؤية حكومة دولة الإمارات العربية المتحدة لتصبح ضمن أكثر الحكومات . ابتكاراً حول العالم بحلول العام 2021&lt;/p&gt;&lt;p&gt;- تطوير ثقافة الابتكار في مختلف القطاعات الحكومية والوزارات في دولة الامارات. حيث أن كل عناصر البرنامج ستكون مرتبطة برؤية الدولة لعام 2021 للدولة إلى جانب الأولويات الست.&lt;/p&gt;&lt;p&gt;- تحديد ودعم وتنمية المواهب الحكومية المتميزة القادرة على أن تصبح رائدة في الابتكار في كل وزارة أو جهة حكومية في الدولة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;هل هناك أي خطط لتشجيع الأجيال الناشئة على&amp;nbsp; الابتكار؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;هناك العديد من البرامج التي يمكن للأجيال الناشئة الانضمام لها من خلال الاستراتيجية الوطنية للابتكار، حيث نقوم بشكل مستمر على تطوير برامجنا إما من خلال المركز أو بالتعاون مع المؤسسات الأكاديمية حول الإمارات من أجل نشر مفاهيم ومهارات الابتكار في الدولة.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;b&gt;كيف يمكن للمركز أن يساعد أصحاب الأفكار المبتكرة؟&lt;/b&gt;&lt;/p&gt;&lt;p&gt;أطلقت مؤخراً مبادرة “أفكاري” من مركز محمد بن راشد للابتكار الحكومي وهي عبارة عن أول منصة حكومية لدعم ورعاية وتمويل الأفكار المبتكرة لجميع موظفي الحكومة الاتحادية. تهدف المبادرة إلى ترسيخ ثقافة الابتكار في الجهات الحكومية من خلال تنظيم مسابقة للأفكار المبتكرة حيث يقوم موظفو الجهات الحكومية الاتحادية بتقديم أفكارهم ومشاريعهم ذات الطابع الابتكاري وذلك لتحسين وتطوير العمل الحكومي عن طريق المنصة الإلكترونية لأفكاري. وسيقوم صاحب الفكرة أو المشروع باستعراض فكرته أمام لجنة الخبراء ووسائل الإعلام المختارة.&amp;nbsp;&lt;br&gt;&lt;/p&gt;', ''),
(77, 1, 'المصور يوسف الزعابي', '', ''),
(77, 2, 'المصور يوسف الزعابي', '&lt;p&gt;بداية التصوير كانت أثناء غربتي للدراسة في المملكة المتحدة ]بريطانيا[ منذ عام 2006 إلى 2009 حيث كان التصوير مجرد توثيق لحياتي اليومية مع زملائي بكاميرا رقمية والتي أهدتني والدتي بعد تخرجي في عام 2009 وكانت هدية من شقيقي، ومنها Nikon DSLR من الثانوية العامة ، وأول كاميرا استخدمتها بدأت بالانطلاق لتعلم كل معايير التصوير الضوئي عن طريق الشبكة العنكبوتية.&lt;/p&gt;&lt;p&gt;بدأتُ بممارسة كل أنواع التصوير بأشكاله المختلفة، ولكن ماشد انتباهي ولفت نظري تصوير البورتريه و حياة الشارع، فبدأت بممارستها في كل دول الخليج ولم أجد أية صعوبة في ذلك، ولكن حين انطلقت إلى تصوير البورتريه في الدول الأجنبية كسنغافورة، وبريطانيا، والهند والنيبال واجهت العديد من الصعوبات، وأعتقد أن السبب الرئيسي هو اختلاف الثقافات بين الدول الأجنبيه والعربية ، ولكن هذه الصعوبات لابد أن يواجهها المصور المحترف ويستطيع أن يتعامل معها خلال جميع المواقف في مسيرته التصويرية، ولهذا السبب دائما رسالتي تحمل عنوان الإنسانية، وحكمتي التي اتّبعها دائماً هي: “ بين عينيًّ حكاية ترويها عدستي”.&amp;nbsp;&amp;nbsp;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;', ''),
(8, 2, 'When HK Hunts For Food', '', ''),
(10, 2, 'Provedore', '', ''),
(33, 2, 'Are Computers Smarter Than Humans', '', ''),
(31, 2, 'Ana Gow Running', '', ''),
(29, 2, 'Dubai Opera', '', ''),
(30, 2, 'Dubai Water Canal', '', ''),
(27, 2, 'I Have This Thing With Tiles', '', ''),
(12, 2, 'Sheikh Zayed Grand Mosque', '', ''),
(23, 2, 'Design Talk', '', ''),
(25, 2, 'Mohammed Almusabi', '', ''),
(34, 2, 'Alia Bin Omair', '', ''),
(36, 2, 'Bait Al Kandora', '', ''),
(35, 2, 'Pearla', '', ''),
(10, 1, 'Provedore', '&lt;h6&gt;&lt;b&gt;Who founded Provedore?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The owner of Provedore is an Emirati national, Sultan Mohammed Al Falahi. Sultan started his career when he was 18 years old, where he worked as a photographer with the Al Bayan newspaper. With an entrepreneurial spirit present from a young age, Sultan then went on to set up a number of companies in advertising and publishing.&lt;/p&gt;&lt;p&gt;Soon after, Sultan recognized the huge potential in real estate and quickly mobilized to develop a business where real estate was at the core of his focus. Surrounded by an entrepreneurial family, Sultan’s relatives were heavily involved in the hospitality trade, which instilled in him a culinary passion at a young age. He developed a vision and a desire to create an international Artisan food &amp;amp; beverage concept, which would in return diversify his company’s portfolio.&lt;/p&gt;&lt;p&gt;After much market analysis and research, the concept of Provedore was conceived; a home-grown concept and free from the restrictions of a franchise model, and is now already regarded as one of the best Artisan concepts in Dubai.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What’s the meaning and reason behind Provedore the name?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;The name has strong Mediterranean roots, particularly Latin, Spanish and Portuguese, and it basically means to “provide”. The inspiration behind Provedore is to develop the concept of happiness, enjoyment, details, friends, and family and ultimately to celebrate life. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;What makes your restaurant different?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Of course there are other concepts in Dubai that are similar to as far as they also have a restaurant, bakery, gifting, retail, food courts and catering. Not many we admit, but some. What we believe sets us apart is our accessibility and affordability in a relaxing and welcoming environment by the sea.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;We noticed that you support projects and individuals by hosting them at Provedore – tell us more about that?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We want to play an active role in the community through learning and education, as well as reaching out to those in need of support. We currently have a Japanese Flower Arrangement workshop at Provedore, and this runs every Tuesday until the end of September. We also collaborated with Food Drive UAE, a social initiative set up to help the underprivileged families during the holy month of Ramadan. &lt;/p&gt;&lt;h6&gt;&lt;b&gt;What are your top special dishes at Provedore?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Our Ceaser Salad – it’s a medley of smoked duck, grilled chicken, fried goat cheese, olive tapenade paste on freshly baked croûtons, sundried tomatoes, olives and finished with our own take on the Ceaser sauce.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;Do you think social media has played a role in the success of Provedore?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;Not necessarily, although it has helped create some awareness about the quality and affordability of our Artisan food and overall concept; including the food market, gifting, flowers, bakery and our new catering business. However, most of our customers visit Provedore due to recommendations from their own family and friends, which is a testimony to the quality of our service and the unique ambiance at Provedore.&lt;/p&gt;&lt;h6&gt;&lt;b&gt;What innovative strategies has Provedore embraced?&lt;/b&gt;&lt;/h6&gt;&lt;p&gt;We don’t just rely on people visiting Provedore, but we take our concept to the community as well. We do this through our events, catering and retail teams. With regards to the latter, we have just started our retail delivery where we are now delivering products such as meats, fish, cheese, foie gras, fresh pasta and our own Artisan sauces to people’s homes.&lt;br&gt;&lt;/p&gt;', ''),
(15, 1, '&quot;Wanna Read?&quot;', '&lt;h4 style=&quot;text-align: center; &quot;&gt;&lt;b&gt;An interview with Sheikha Shamma bint Sultan bin Khalifa Al Nahyan, the founder of the “Wanna Read?” &lt;/b&gt;&lt;/h4&gt;&lt;h4 style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;initiative that asks young patients if they “Wanna Read?”&lt;/u&gt;&lt;/b&gt;&lt;/h4&gt;&lt;h4 style=&quot;text-align: center; &quot;&gt;&lt;b&gt;&lt;u&gt;&lt;br&gt;&lt;/u&gt;&lt;/b&gt;&lt;/h4&gt;&lt;p style=&quot;text-align: left;&quot;&gt;&lt;b&gt;&lt;u&gt;How important is reading to Sheikha Shamma bint Sultan?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Reading has always been really important to me, especially when I was growing up. My education started at the French school in Abu Dhabi and, because French books were unavailable here in the UAE, I was not only subscribed to monthly French children’s magazines but my family also made sure I had plenty of books.&lt;/p&gt;&lt;p&gt;Research has proved that reading to children improves their intellectual development, enhances their imagination and helps them understand the world&amp;nbsp;they live in.&lt;/p&gt;&lt;p&gt;&lt;u&gt;&lt;b&gt;Tell us more about the “Wanna Read?” initiative. How did it start?&lt;/b&gt;&lt;/u&gt;&lt;/p&gt;&lt;p&gt;The “Wanna Read?” initiative stemmed from the summer of 2013 when I organised a book-reading session of my own children’s book, The Lost Princess, in the library at the Johns Hopkins Hospital in Baltimore. This special library inspired me to create “Wanna Read?” and to provide the children of the UAE with an environment conducive to healing through reading. I chose the name with the aim of persuading children that reading is fun.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;What does the “Wanna Read?” initiative help achieve?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;“Wanna Read?” targets more than one area. We encourage children to read and find an escape through books. We promote parental engagement by urging parents to read to their children and of course, we hope that people will volunteer to read to children who are in hospital.&lt;/p&gt;&lt;p&gt;&lt;u&gt;&lt;b&gt;Why did you choose hospitals for this initiative? How do you believe books can aid in healing patients?&lt;/b&gt;&lt;/u&gt;&lt;/p&gt;&lt;p&gt;Hospitalised children are the most vulnerable in our society and there can be a stigma attached to ill health, and an understandable fear when a family discovers their child is unwell. I feel that through community engagement, parents benefit from a little respite time which can help them cope with what they’re going through.&lt;/p&gt;&lt;p&gt;Our rooms aim to complement the UAE healthcare system by enhancing patient experience within paediatric wards.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;How many hospitals has “Wanna Read?” contributed to?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;So far we have opened 7 rooms in the United Arab Emirates and we plan to open many more in the coming months. Our rooms are only successful because of the commitment and dedication of our volunteers. Supplying books is one thing, but when you read to a child there are many benefits, not only for the child but also for the volunteer.&lt;/p&gt;&lt;p&gt;Our volunteer programme is currently up and running in Sheikh Khalifa Medical City and I aim to implement it in all hospitals that have a “Wanna Read?” room. I am really happy to have received positive responses from many other hospitals interested in encouraging community engagement through “Wanna Read?”.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;How can people participate in this initiative?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Individuals can participate by contacting our office and attending our orientation sessions, which is the first steps to becoming a volunteer.&lt;/p&gt;&lt;p&gt;We have recently set up a “WannaReadathon” Challenge, which asks people to donate a book for every book they read. We are also encouraging them to read and relive their favourite childhood books, because you often don’t realise the true meaning of a particular book when reading it as a child. For instance, when I look back at famous children’s stories such as Winnie the Pooh by A.A. Milne or the Dr Seuss books, I now understand their underlying messages much better.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;Can people fund the initiative and how?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Funding “Wanna Read?” can be done through donating new books, a trolley or a shelf, and through sponsoring rooms. You can also support the initiative by joining as a volunteer.&lt;/p&gt;&lt;p&gt;&lt;b&gt;&lt;u&gt;Any future plans for the “Wanna Read?” initiative? How are you hoping to expand?&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p&gt;We hope to expand and provide books to many more hospitals, not only in the UAE but also in the entire Gulf region.&lt;/p&gt;', '');

-- --------------------------------------------------------

--
-- Table structure for table `sp_news_featured`
--

CREATE TABLE `sp_news_featured` (
  `featured_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_news_featured`
--

INSERT INTO `sp_news_featured` (`featured_id`, `news_id`, `date_added`) VALUES
(131, 10, '2017-11-24 15:34:49'),
(132, 13, '2017-11-24 15:34:49'),
(133, 20, '2017-11-24 15:34:49'),
(134, 12, '2017-11-24 15:34:49'),
(135, 14, '2017-11-24 15:34:49'),
(136, 3, '2017-11-24 15:34:49'),
(137, 37, '2017-11-24 15:34:49'),
(138, 38, '2017-11-24 15:34:49'),
(139, 39, '2017-11-24 15:34:49'),
(140, 40, '2017-11-24 15:34:49'),
(141, 44, '2017-11-24 15:34:49'),
(142, 47, '2017-11-24 15:34:49');

-- --------------------------------------------------------

--
-- Table structure for table `sp_news_image`
--

CREATE TABLE `sp_news_image` (
  `news_image_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_news_image`
--

INSERT INTO `sp_news_image` (`news_image_id`, `news_id`, `image`, `sort_order`) VALUES
(474, 3, 'catalog/news/gafla/gafla-extra-image.jpg', 3),
(704, 35, 'catalog/news/pearla/modal-boat.jpg', 2),
(505, 7, 'catalog/news/outlines-to-ornaments/final-projects.jpg', 1),
(506, 7, 'catalog/news/outlines-to-ornaments/final-project.jpg', 2),
(507, 7, 'catalog/news/outlines-to-ornaments/final.jpg', 3),
(492, 5, 'catalog/news/mbr/mohd-bin-rashid.jpg', 2),
(663, 1, 'catalog/news/osyami/6a9a2605.jpg', 4),
(662, 1, 'catalog/news/osyami/6a9a2607.jpg', 3),
(504, 9, 'catalog/news/nayif/Electrical-Engineering.jpg', 4),
(503, 9, 'catalog/news/nayif/assembling.jpg', 3),
(673, 8, 'catalog/news/hk-hunts/mur-kitchen.jpg', 10),
(672, 8, 'catalog/news/hk-hunts/bread-ahead.jpg', 9),
(671, 8, 'catalog/news/hk-hunts/sumosan.jpg', 8),
(670, 8, 'catalog/news/hk-hunts/kopapa.jpg', 7),
(772, 10, 'catalog/news/provedore/provedore-Interior-shots-001.jpg', 0),
(461, 11, 'catalog/news/Alezan/LAFS-RING-WITH-TUSSEL.jpg', 5),
(462, 11, 'catalog/news/Alezan/LAFS-RING.jpg', 6),
(693, 12, 'catalog/news/sheikh-zayed-grand-mosque/Grand-Mosque-DSC9463.jpg', 2),
(477, 13, 'catalog/news/HIPA/both-reading.jpg', 0),
(476, 13, 'catalog/news/HIPA/reading-books.jpg', 0),
(475, 13, 'catalog/news/HIPA/reading-book.jpg', 0),
(527, 14, 'catalog/news/untitled-chapters/prepare-reading.jpg', 0),
(526, 14, 'catalog/news/untitled-chapters/paper-rolls.jpg', 0),
(473, 3, 'catalog/news/gafla/gafla-extra-show.jpg', 2),
(661, 1, 'catalog/news/osyami/6a9a2735.jpg', 2),
(660, 1, 'catalog/news/osyami/6a9a281.jpg', 1),
(508, 7, 'catalog/news/outlines-to-ornaments/3d-model.jpg', 4),
(459, 11, 'catalog/news/Alezan/EMPRESS-PENDANT.jpg', 3),
(460, 11, 'catalog/news/Alezan/SECRET-GARDEN-LONG-CHAIN-WITH-RUBIES.jpg', 4),
(502, 9, 'catalog/news/nayif/mechanical-lead.jpg', 2),
(692, 12, 'catalog/news/sheikh-zayed-grand-mosque/Grand-Mosque-DSC_6027.jpg', 1),
(770, 10, 'catalog/news/provedore/pistachio-brule-up.jpg', 3),
(669, 8, 'catalog/news/hk-hunts/choco-ice.jpg', 6),
(668, 8, 'catalog/news/hk-hunts/best-hidden-gem-chinese-waffle.jpg', 5),
(667, 8, 'catalog/news/hk-hunts/Koshari-Street.jpg', 4),
(497, 22, 'catalog/news/mira-thani/port.jpg', 4),
(496, 22, 'catalog/news/mira-thani/plot.jpg', 3),
(495, 22, 'catalog/news/mira-thani/design.jpg', 2),
(494, 22, 'catalog/news/mira-thani/hotel.jpg', 1),
(493, 22, 'catalog/news/mira-thani/tent.jpg', 0),
(697, 23, 'catalog/news/design_talk/star.jpg', 4),
(696, 23, 'catalog/news/design_talk/mask.jpg', 3),
(695, 23, 'catalog/news/design_talk/light.jpg', 2),
(458, 11, 'catalog/news/Alezan/TULIP-RING.jpg', 2),
(771, 10, 'catalog/news/provedore/provedore-kitchen-shots-003.jpg', 0),
(521, 24, 'catalog/news/qafiya/rashid.jpg', 4),
(520, 24, 'catalog/news/qafiya/qafiya-02.jpg', 3),
(519, 24, 'catalog/news/qafiya/promotion.jpg', 2),
(700, 25, 'catalog/news/mohammed-almusabi/towers.jpg', 3),
(699, 25, 'catalog/news/mohammed-almusabi/black_and_white.jpg', 2),
(698, 25, 'catalog/news/mohammed-almusabi/city_evening.jpg', 1),
(469, 26, 'catalog/news/fashion-spotlight/sliding.jpg', 1),
(470, 26, 'catalog/news/fashion-spotlight/spotlight.jpg', 2),
(471, 26, 'catalog/news/fashion-spotlight/with_neck.jpg', 3),
(688, 27, 'catalog/news/i-have-this-thing-with-tiles/mansion.jpg', 7),
(687, 27, 'catalog/news/i-have-this-thing-with-tiles/light_red.jpg', 6),
(686, 27, 'catalog/news/i-have-this-thing-with-tiles/green.jpg', 5),
(685, 27, 'catalog/news/i-have-this-thing-with-tiles/flower.jpg', 4),
(488, 28, 'catalog/news/louvre-abu-dhabi/mushroom-structure.jpg', 0),
(525, 32, 'catalog/news/the-parkour-enthusiast/amal.jpg', 2),
(524, 32, 'catalog/news/the-parkour-enthusiast/amal-1.jpg', 1),
(472, 3, 'catalog/news/gafla/gafla.jpg', 0),
(703, 35, 'catalog/news/pearla/modal-white.jpg', 1),
(457, 11, 'catalog/news/Alezan/HORSE-BIT-RING.jpg', 1),
(702, 36, 'catalog/news/bait-al-kandora/purses.jpg', 2),
(701, 36, 'catalog/news/bait-al-kandora/brand.jpg', 1),
(490, 5, 'catalog/news/mbr/mohd-bin-rashid-centre-for-gov-innovation.jpg', 0),
(491, 5, 'catalog/news/mbr/mohd-bin-rashid-centre.jpg', 1),
(489, 5, 'catalog/news/mbr/technology.jpg', 0),
(501, 9, 'catalog/news/nayif/prepping-for-Nayif-1-assembly.jpg', 1),
(666, 8, 'catalog/news/hk-hunts/home-slice.jpg', 3),
(665, 8, 'catalog/news/hk-hunts/nata.jpg', 2),
(554, 37, 'catalog/news/last-edited/e11_exit.jpg', 2),
(553, 37, 'catalog/news/last-edited/coloured_road.jpg', 1),
(733, 38, 'catalog/news/the-green-planet/tree.jpg', 3),
(732, 38, 'catalog/news/the-green-planet/parrot.jpg', 2),
(731, 38, 'catalog/news/the-green-planet/coir_bridge.jpg', 1),
(694, 23, 'catalog/news/design_talk/fathima.jpg', 1),
(684, 27, 'catalog/news/i-have-this-thing-with-tiles/cap.jpg', 3),
(683, 27, 'catalog/news/i-have-this-thing-with-tiles/arc_design.jpg', 2),
(518, 24, 'catalog/news/qafiya/qafiya-01.jpg', 1),
(682, 27, 'catalog/news/i-have-this-thing-with-tiles/pointed.jpg', 1),
(509, 7, 'catalog/news/outlines-to-ornaments/polished-brass.jpg', 5),
(664, 8, 'catalog/news/hk-hunts/Muriel-kitchen.jpg', 1),
(556, 40, 'catalog/news/dubai-marathon/chart.jpg', 0),
(760, 43, 'catalog/news/brussels-is-a-battleground-to-a-tourist-city/old_tower.jpg', 4),
(759, 43, 'catalog/news/brussels-is-a-battleground-to-a-tourist-city/inner_view.jpg', 3),
(758, 43, 'catalog/news/brussels-is-a-battleground-to-a-tourist-city/flowers.jpg', 2),
(723, 44, 'catalog/news/dusseldorf-city/white_tower.jpg', 5),
(722, 44, 'catalog/news/dusseldorf-city/tripple_tower.jpg', 4),
(721, 44, 'catalog/news/dusseldorf-city/spider.jpg', 3),
(720, 44, 'catalog/news/dusseldorf-city/grass_field.jpg', 2),
(719, 44, 'catalog/news/dusseldorf-city/brown-tower.jpg', 1),
(757, 43, 'catalog/news/brussels-is-a-battleground-to-a-tourist-city/brussels_building.jpg', 1),
(756, 51, 'catalog/news/ibrahim-al-hashimi/reading_books.jpg', 2),
(755, 51, 'catalog/news/ibrahim-al-hashimi/hashimi.jpg', 1),
(754, 52, 'catalog/news/ahmed-bin-rakad-al-ameri/rakad-al-ameri.jpg', 2),
(753, 52, 'catalog/news/ahmed-bin-rakad-al-ameri/al-ameri.jpg', 1),
(586, 53, 'catalog/news/biography/Rafia-Ghobash.jpg', 1),
(587, 53, 'catalog/news/biography/Aisha-Al-Busmait.jpg', 2),
(588, 53, 'catalog/news/biography/Nahla-Al-Fahad.jpg', 3),
(589, 54, 'catalog/news/dr-hamad-al-hammadi/dr-hamad-al-hammadi.jpg', 0),
(716, 59, 'catalog/news/five-reasons-to-visit-tokyo/temple.jpg', 12),
(715, 59, 'catalog/news/five-reasons-to-visit-tokyo/street-food.jpg', 11),
(714, 59, 'catalog/news/five-reasons-to-visit-tokyo/strawberry.jpg', 10),
(713, 59, 'catalog/news/five-reasons-to-visit-tokyo/pond.jpg', 9),
(712, 59, 'catalog/news/five-reasons-to-visit-tokyo/mountain.jpg', 8),
(711, 59, 'catalog/news/five-reasons-to-visit-tokyo/lotus.jpg', 7),
(710, 59, 'catalog/news/five-reasons-to-visit-tokyo/game-trato.jpg', 6),
(709, 59, 'catalog/news/five-reasons-to-visit-tokyo/dolls.jpg', 5),
(708, 59, 'catalog/news/five-reasons-to-visit-tokyo/city-hall.jpg', 4),
(707, 59, 'catalog/news/five-reasons-to-visit-tokyo/cake.jpg', 3),
(706, 59, 'catalog/news/five-reasons-to-visit-tokyo/bakery.jpg', 2),
(705, 59, 'catalog/news/five-reasons-to-visit-tokyo/church.jpg', 1),
(618, 65, 'catalog/news/adeeb-al-balushi/adeeb.jpg', 1),
(619, 65, 'catalog/news/adeeb-al-balushi/air-dubai.jpg', 2),
(620, 65, 'catalog/news/adeeb-al-balushi/flight.jpg', 3),
(621, 65, 'catalog/news/adeeb-al-balushi/in-lab.jpg', 4),
(622, 65, 'catalog/news/adeeb-al-balushi/interview.jpg', 5),
(623, 65, 'catalog/news/adeeb-al-balushi/shuttle-demo.jpg', 6),
(624, 65, 'catalog/news/adeeb-al-balushi/ticket.jpg', 7),
(625, 65, 'catalog/news/adeeb-al-balushi/with-king.jpg', 8),
(751, 66, 'catalog/news/o-flag/floor.jpg', 4),
(750, 66, 'catalog/news/o-flag/child-with-flag.jpg', 3),
(749, 66, 'catalog/news/o-flag/between-walls.jpg', 2),
(748, 66, 'catalog/news/o-flag/define.jpg', 1),
(636, 70, 'catalog/news/mbr/mohd-bin-rashid-centre.jpg', 1),
(637, 70, 'catalog/news/mbr/mohd-bin-rashid.jpg', 2),
(739, 73, 'catalog/news/center-of-the-majestic/green-hall.jpg', 4),
(738, 73, 'catalog/news/center-of-the-majestic/toys-making.jpg', 3),
(737, 73, 'catalog/news/center-of-the-majestic/clay-art.jpg', 2),
(736, 73, 'catalog/news/center-of-the-majestic/class-room.jpg', 1),
(745, 74, 'catalog/news/nuray/pool.jpg', 6),
(744, 74, 'catalog/news/nuray/interior-with-light.jpg', 5),
(743, 74, 'catalog/news/nuray/hall.jpg', 4),
(742, 74, 'catalog/news/nuray/dining.jpg', 3),
(741, 74, 'catalog/news/nuray/boffe.jpg', 2),
(740, 74, 'catalog/news/nuray/bath-tub.jpg', 1),
(735, 75, 'catalog/news/sheikh-zayed-grand-mosque/Grand-Mosque-DSC9463.jpg', 2),
(734, 75, 'catalog/news/sheikh-zayed-grand-mosque/Grand-Mosque-DSC_6027.jpg', 1),
(652, 76, 'catalog/news/mbr/technology.jpg', 0),
(729, 77, 'catalog/news/youssef-al-zaabi/scooba.jpg', 6),
(728, 77, 'catalog/news/youssef-al-zaabi/one-man.jpg', 5),
(727, 77, 'catalog/news/youssef-al-zaabi/old-man.jpg', 4),
(726, 77, 'catalog/news/youssef-al-zaabi/mosque.jpg', 3),
(725, 77, 'catalog/news/youssef-al-zaabi/desert.jpg', 2),
(724, 77, 'catalog/news/youssef-al-zaabi/black-and-white.jpg', 1),
(674, 8, 'catalog/news/hk-hunts/honest-burger.jpg', 11),
(675, 8, 'catalog/news/hk-hunts/imli.jpg', 12),
(768, 10, 'catalog/news/provedore/emarati-breakfast-up.jpg', 2),
(769, 10, 'catalog/news/provedore/Provedore-Food-Images-013.jpg', 0),
(689, 27, 'catalog/news/i-have-this-thing-with-tiles/marble.jpg', 8),
(690, 27, 'catalog/news/i-have-this-thing-with-tiles/square-red.jpg', 9),
(691, 27, 'catalog/news/i-have-this-thing-with-tiles/square.jpg', 10),
(717, 59, 'catalog/news/five-reasons-to-visit-tokyo/toys-palace.jpg', 13),
(718, 59, 'catalog/news/five-reasons-to-visit-tokyo/toystory.jpg', 14),
(730, 77, 'catalog/news/youssef-al-zaabi/white-bricks.jpg', 7),
(746, 74, 'catalog/news/nuray/sea-shore.jpg', 7),
(747, 74, 'catalog/news/nuray/tree.jpg', 8),
(752, 66, 'catalog/news/o-flag/relay-running.jpg', 5),
(761, 43, 'catalog/news/brussels-is-a-battleground-to-a-tourist-city/tower.jpg', 5),
(773, 10, 'catalog/news/provedore/provedore-interior-shots--013.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_news_related`
--

CREATE TABLE `sp_news_related` (
  `news_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_news_related`
--

INSERT INTO `sp_news_related` (`news_id`, `related_id`) VALUES
(1, 3),
(1, 26),
(1, 34),
(2, 9),
(2, 16),
(2, 33),
(3, 1),
(3, 7),
(3, 11),
(3, 26),
(3, 34),
(3, 36),
(4, 4),
(4, 8),
(4, 10),
(5, 28),
(5, 29),
(5, 30),
(7, 3),
(7, 26),
(8, 4),
(8, 10),
(9, 2),
(9, 16),
(9, 33),
(10, 4),
(10, 8),
(10, 10),
(11, 3),
(11, 11),
(11, 26),
(12, 17),
(12, 18),
(12, 28),
(12, 29),
(12, 30),
(13, 15),
(13, 22),
(13, 23),
(13, 24),
(13, 25),
(14, 14),
(14, 15),
(14, 16),
(15, 13),
(15, 14),
(15, 15),
(16, 2),
(16, 9),
(16, 14),
(16, 16),
(16, 18),
(16, 21),
(16, 33),
(17, 12),
(17, 17),
(17, 18),
(17, 21),
(17, 27),
(17, 28),
(17, 29),
(17, 30),
(18, 12),
(18, 16),
(18, 17),
(18, 18),
(18, 21),
(18, 27),
(18, 28),
(18, 29),
(18, 30),
(19, 19),
(19, 20),
(20, 19),
(20, 20),
(21, 16),
(21, 17),
(21, 18),
(21, 21),
(22, 13),
(22, 22),
(22, 23),
(22, 24),
(22, 25),
(23, 13),
(23, 22),
(23, 23),
(23, 24),
(23, 25),
(24, 13),
(24, 22),
(24, 23),
(24, 24),
(24, 25),
(25, 13),
(25, 22),
(25, 23),
(25, 24),
(25, 25),
(26, 1),
(26, 3),
(26, 7),
(26, 11),
(26, 26),
(26, 36),
(27, 17),
(27, 18),
(27, 27),
(27, 28),
(27, 29),
(28, 5),
(28, 12),
(28, 17),
(28, 18),
(28, 27),
(28, 28),
(29, 5),
(29, 12),
(29, 17),
(29, 18),
(29, 27),
(29, 29),
(29, 30),
(30, 5),
(30, 12),
(30, 17),
(30, 18),
(30, 29),
(30, 30),
(32, 32),
(33, 2),
(33, 9),
(33, 16),
(33, 33),
(34, 1),
(34, 3),
(34, 34),
(34, 36),
(35, 35),
(36, 3),
(36, 26),
(36, 34),
(36, 36),
(37, 38),
(37, 39),
(37, 40),
(37, 73),
(37, 75),
(38, 37),
(38, 39),
(38, 40),
(38, 77),
(39, 37),
(39, 38),
(39, 40),
(39, 75),
(40, 37),
(40, 38),
(40, 39),
(43, 44),
(43, 59),
(44, 43),
(44, 44),
(44, 59),
(45, 46),
(46, 45),
(46, 46),
(47, 45),
(47, 47),
(48, 45),
(48, 46),
(48, 47),
(48, 48),
(49, 45),
(49, 46),
(49, 47),
(49, 48),
(49, 49),
(50, 45),
(50, 46),
(50, 47),
(50, 48),
(50, 49),
(50, 50),
(51, 52),
(51, 54),
(52, 51),
(52, 52),
(52, 54),
(52, 64),
(53, 53),
(53, 54),
(54, 51),
(54, 52),
(54, 53),
(54, 64),
(56, 55),
(56, 56),
(56, 74),
(58, 57),
(58, 58),
(59, 43),
(59, 44),
(59, 59),
(60, 47),
(60, 48),
(60, 49),
(60, 50),
(60, 60),
(61, 49),
(61, 50),
(61, 60),
(61, 61),
(62, 48),
(62, 49),
(62, 50),
(62, 62),
(63, 48),
(63, 49),
(63, 50),
(63, 63),
(64, 52),
(64, 54),
(64, 65),
(65, 53),
(65, 54),
(65, 64),
(65, 65),
(66, 67),
(67, 66),
(67, 67),
(68, 48),
(68, 49),
(68, 62),
(68, 68),
(69, 45),
(69, 49),
(69, 68),
(69, 69),
(70, 63),
(70, 68),
(70, 69),
(70, 70),
(71, 58),
(71, 71),
(72, 58),
(72, 71),
(72, 72),
(73, 37),
(73, 73),
(74, 56),
(74, 74),
(74, 77),
(75, 37),
(75, 39),
(75, 75),
(76, 76),
(76, 77),
(77, 38),
(77, 74),
(77, 76),
(77, 77);

-- --------------------------------------------------------

--
-- Table structure for table `sp_news_to_newscategory`
--

CREATE TABLE `sp_news_to_newscategory` (
  `news_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_news_to_newscategory`
--

INSERT INTO `sp_news_to_newscategory` (`news_id`, `category_id`) VALUES
(1, 4),
(2, 6),
(3, 4),
(4, 7),
(5, 5),
(7, 4),
(8, 7),
(9, 6),
(10, 7),
(11, 4),
(12, 5),
(13, 1),
(14, 3),
(15, 3),
(16, 6),
(17, 5),
(18, 5),
(19, 8),
(20, 8),
(21, 8),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 4),
(27, 5),
(28, 5),
(29, 5),
(30, 5),
(31, 2),
(32, 2),
(33, 6),
(34, 4),
(35, 4),
(36, 4),
(37, 9),
(38, 9),
(39, 9),
(40, 10),
(41, 10),
(42, 11),
(43, 12),
(44, 12),
(45, 13),
(46, 13),
(47, 13),
(48, 13),
(49, 13),
(50, 13),
(51, 14),
(52, 14),
(53, 14),
(54, 14),
(55, 9),
(56, 9),
(57, 15),
(58, 15),
(59, 12),
(60, 13),
(61, 13),
(62, 13),
(63, 13),
(64, 14),
(65, 14),
(66, 16),
(67, 16),
(68, 13),
(69, 13),
(70, 13),
(71, 15),
(72, 15),
(73, 9),
(74, 9),
(75, 9),
(76, 9),
(77, 9);

-- --------------------------------------------------------

--
-- Table structure for table `sp_product`
--

CREATE TABLE `sp_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_product_attribute`
--

CREATE TABLE `sp_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_product_description`
--

CREATE TABLE `sp_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_product_filter`
--

CREATE TABLE `sp_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_product_image`
--

CREATE TABLE `sp_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_product_related`
--

CREATE TABLE `sp_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_product_to_category`
--

CREATE TABLE `sp_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_product_to_download`
--

CREATE TABLE `sp_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_projects`
--

CREATE TABLE `sp_projects` (
  `project_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `project_client_id` int(11) NOT NULL,
  `project_category_id` int(11) NOT NULL,
  `link` varchar(100) NOT NULL,
  `price` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `project_description` text NOT NULL,
  `subtitle` varchar(1000) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_projects`
--

INSERT INTO `sp_projects` (`project_id`, `name`, `project_client_id`, `project_category_id`, `link`, `price`, `status`, `image`, `project_description`, `subtitle`, `sort_order`, `date_added`, `date_modified`) VALUES
(1, 'Citygold', 2, 2, '', '', 1, '', '', '', 1, '2017-01-07 10:31:45', '2017-01-07 15:35:28'),
(2, 'Savoy', 0, 0, '', '', 1, 'catalog/project/project/2.jpg', 'dfdfdd', 'df', 2, '2017-01-07 11:32:02', '2017-01-07 11:38:26'),
(3, 'errr', 1, 1, 'http://lms.ubrik.com/ges/', 't565', 1, '', 'Our warehousing solutions provide a flexible facility layout and design to meet the needs of every business.\r\nOur warehousing solutions provide a flexible facility layout and design to meet the needs of every business.\r\nOur warehousing solutions provide a flexible facility layout and design to meet the needs of every business.\r\nOur warehousing solutions provide a flexible facility layout and design to meet the needs of every business.', 'df erwtre ertry fgy 57y65 ', 3, '2017-01-07 12:45:51', '2017-01-07 15:36:23'),
(4, 'hh', 4, 1, 'http://lms.ubrik.com/', '3456', 1, 'catalog/project/project/Untitled-1.jpg', 'Hi Vinil,\r\n\r\nPlease put the following attached tenders in News &amp; Events.\r\n\r\n1) Supply and installation of 5 KVA Online UPS for Administrative Office Last Date: Upto 2 PM  on 01.02.2017\r\n\r\n2) Supply and installation of 6 no’s of Desktop computers for ECE Department of LBSCE, Kasaragod.Last Date: Up to 2 PM on 01.02.20173) \r\n  \r\n3) Sealed competitive quotations are invited from the licensed suppliers/dealers forthe supply and installation of test apparatus for strength of materials lab of ME\r\nDepartment. Last Date: 19.01.2017 at 2 pm', 'hh1', 5, '2017-01-07 17:47:32', '2017-01-10 09:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `sp_project_category`
--

CREATE TABLE `sp_project_category` (
  `project_category_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_project_category`
--

INSERT INTO `sp_project_category` (`project_category_id`, `name`, `image`, `sort_order`, `date_added`, `date_modified`) VALUES
(1, 'Web Based', 'catalog/project/category/web.png', 1, '2017-01-04 12:12:18', '2017-01-04 12:45:07'),
(2, 'Ios', 'catalog/project/category/ios.png', 2, '2017-01-04 12:14:27', '2017-01-04 12:44:54'),
(3, 'Android', 'catalog/project/category/android.png', 3, '2017-01-04 12:14:46', '2017-01-04 12:44:34');

-- --------------------------------------------------------

--
-- Table structure for table `sp_project_clients`
--

CREATE TABLE `sp_project_clients` (
  `project_client_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(96) NOT NULL,
  `website` varchar(100) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `client_address` varchar(500) NOT NULL,
  `client_remarks` text NOT NULL,
  `sort_order` int(3) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_project_clients`
--

INSERT INTO `sp_project_clients` (`project_client_id`, `name`, `email`, `website`, `telephone`, `status`, `image`, `client_address`, `client_remarks`, `sort_order`, `date_added`, `date_modified`) VALUES
(1, 'Citygold', '', '', '', 1, '', '', '', 1, '2017-01-05 16:23:24', '2017-01-06 12:38:42'),
(2, 'Wellfit', '', '', '', 1, '', '', '', 2, '2017-01-05 16:27:23', '2017-01-06 12:39:05'),
(3, 'Zask', '', '', '', 1, '', 'Zask intl.\r\nDubai, +973465', 'We are currently working on launching our website. Copyright 2015 All Rights Reserved. Categories. Build (8); Classic (4); Mansory (2); Media (2); Projects (3) ...\r\nYou\'ve visited this page 4 times. Last visit: 27/12/16', 3, '2017-01-06 15:12:05', '2017-01-06 15:24:16'),
(4, 'Jobells', 'jobells@jobells.com', '', '123456', 1, '', '', '', 4, '2017-01-06 16:18:46', '2017-01-06 16:19:18'),
(5, 'Savoy', '', '', '', 1, '', '', '', 5, '2017-01-06 16:31:59', '2017-01-06 16:31:59');

-- --------------------------------------------------------

--
-- Table structure for table `sp_project_image`
--

CREATE TABLE `sp_project_image` (
  `project_image_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_review`
--

CREATE TABLE `sp_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_setting`
--

CREATE TABLE `sp_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_setting`
--

INSERT INTO `sp_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(1769, 0, 'theme_default', 'theme_default_image_news_related_height', '308', 0),
(1768, 0, 'theme_default', 'theme_default_image_news_related_width', '530', 0),
(1767, 0, 'theme_default', 'theme_default_image_news_additional_height', '308', 0),
(1766, 0, 'theme_default', 'theme_default_image_news_additional_width', '530', 0),
(1765, 0, 'theme_default', 'theme_default_image_news_height', '308', 0),
(1764, 0, 'theme_default', 'theme_default_image_news_width', '530', 0),
(1763, 0, 'theme_default', 'theme_default_image_news_popup_height', '680', 0),
(1839, 0, 'news_category', 'news_category_status', '1', 0),
(1970, 0, 'config', 'config_file_max_size', '300000', 0),
(1971, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(1972, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(1973, 0, 'config', 'config_error_display', '1', 0),
(1974, 0, 'config', 'config_error_log', '1', 0),
(1975, 0, 'config', 'config_error_filename', 'error.log', 0),
(1976, 0, 'config', 'config_social_login_status', '1', 0),
(1977, 0, 'config', 'config_social_login_setting', '{\"customer_group\":\"1\",\"providers\":{\"Google\":{\"enabled\":\"0\",\"sort_order\":\"1\",\"id\":\"google\",\"keys\":{\"id\":\"\",\"secret\":\"\"},\"scope\":\"\"},\"Facebook\":{\"enabled\":\"1\",\"sort_order\":\"2\",\"id\":\"facebook\",\"keys\":{\"id\":\"1833120463581481\",\"secret\":\"2eb8d4d9c3c42dc2a9b9b3187824014c\"},\"scope\":\"email\"},\"LinkedIn\":{\"enabled\":\"0\",\"sort_order\":\"5\",\"id\":\"linkedin\",\"keys\":{\"key\":\"\",\"secret\":\"\"}},\"Yahoo\":{\"enabled\":\"0\",\"sort_order\":\"11\",\"id\":\"yahoo\",\"keys\":{\"key\":\"\",\"secret\":\"\"}},\"Twitter\":{\"enabled\":\"0\",\"sort_order\":\"17\",\"id\":\"twitter\",\"keys\":{\"key\":\"\",\"secret\":\"\"},\"scope\":\"\"}}}', 1),
(1965, 0, 'config', 'config_compression', '0', 0),
(1966, 0, 'config', 'config_secure', '0', 0),
(1967, 0, 'config', 'config_password', '1', 0),
(1968, 0, 'config', 'config_shared', '0', 0),
(1969, 0, 'config', 'config_encryption', 'A11BLW7CS22jDfXqe36PFPg1GEUfBhKwSgqFSggiNTZgcbhdwTCQ7Mz7PKzHWtzAxIpadmBSF088DdEAgYPRsXi2qDCWoPlSpLTKwbhKikcPhe4GMKNimoVMkaGUcqB4iiLjJ8Lp4gaTg34fDWDqPNInmExi217qlAHau8TUPwEcdhgk6vE2Pe9dFH2g38372vYMjTV1WXVVet4UPbujjXFa8TlE2fsP9gEvosKgcnI0VxgZtjTmEtE9OsLNjFbjKDu2OePJ6jFlSRAYcSEd5SUJbVuijt8MAjiwUrFTJuYFp65RgzaXINtexnAOJB7FRZX7dtysmBOb4n4R52U6o39HD5QRXbQ7lHJW8B3gjRmoBBSdYktKr184DbU6wYwJIIb7HaFZizfChrOWX0AIW7Djn8eHPo0NDMSnhHwPTMFaq1eGNnP6RXMqfRI4eqbj9LUCNg8pywvyn1Pn7ZdqLbaqlJSjPmJmvYqZ3iUJxkoqK2ZepMjcbRYQu2Qu5m8CznWM8G3HV2eWWzmuAIyKRWRMAE3RxjAmriCkK4cTY3hLuC5dt7dsUZbst73gs3A0o2ucDqi6fzIyK5620UPQc8cX0HkOXXfGHvKBRUciItp6PCrySnBxHqo0OUhEGAdBI5yW9AUQMDkAteXCeNyDpkntgkVykYWPgpf4d4iOnI5svl8H0eZQW506Uiy5dDONAwqR1hqeioO7y7223ADtBTA8eBV0K44LUQiMTH5WLP74f1chovOgRs8GzclhMnUob3LR655eBJtVhHIvYI3NvJp3fZxvNlM6xO0dvv4sqSZhgiPQrmwgoM2iL8xA92a4HM7K5WuSs3MoX2lerPWm9dSkRxcupljZnSmMucGbwktBMTMeC62PXeYWJbhOP6H3gbQk5ReihuFSlj6kqsKaZHWbOcogxo8R2qz7yEK4hY7ielc9t8jPRa8U4lsCe7vzujNH63mbfr0FV6PXPVhUw1XfU0CnY8GY2JY1p5oYzVoMrJ3yg5OpMFruHQ0UU9BU', 0),
(1964, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(1963, 0, 'config', 'config_seo_url', '1', 0),
(1961, 0, 'config', 'config_mail_alert_email', '', 0),
(1962, 0, 'config', 'config_maintenance', '0', 0),
(1960, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(1959, 0, 'config', 'config_mail_smtp_port', '25', 0),
(121, 0, 'reward', 'reward_sort_order', '2', 0),
(122, 0, 'reward', 'reward_status', '1', 0),
(1762, 0, 'theme_default', 'theme_default_image_news_popup_width', '1170', 0),
(1761, 0, 'theme_default', 'theme_default_image_news_thumb_height', '145', 0),
(1759, 0, 'theme_default', 'theme_default_image_news_category_height', '400', 0),
(1760, 0, 'theme_default', 'theme_default_image_news_thumb_width', '250', 0),
(1756, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(1757, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(1758, 0, 'theme_default', 'theme_default_image_news_category_width', '1920', 0),
(1754, 0, 'theme_default', 'theme_default_image_cart_width', '47', 0),
(1755, 0, 'theme_default', 'theme_default_image_cart_height', '47', 0),
(1753, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(1752, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(1751, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(1750, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(1749, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(1748, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(1747, 0, 'theme_default', 'theme_default_image_additional_height', '74', 0),
(1746, 0, 'theme_default', 'theme_default_image_additional_width', '74', 0),
(1743, 0, 'theme_default', 'theme_default_image_popup_height', '500', 0),
(1983, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(1982, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(1980, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(1979, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(1744, 0, 'theme_default', 'theme_default_image_product_width', '270', 0),
(1745, 0, 'theme_default', 'theme_default_image_product_height', '240', 0),
(1981, 0, 'dashboard_customer', 'dashboard_customer_width', '4', 0),
(1978, 0, 'dashboard_online', 'dashboard_online_width', '4', 0),
(1742, 0, 'theme_default', 'theme_default_image_popup_width', '500', 0),
(1958, 0, 'config', 'config_mail_smtp_password', '', 0),
(1957, 0, 'config', 'config_mail_smtp_username', '', 0),
(1956, 0, 'config', 'config_mail_smtp_hostname', '', 0),
(1955, 0, 'config', 'config_mail_parameter', '', 0),
(1954, 0, 'config', 'config_mail_protocol', 'mail', 0),
(1953, 0, 'config', 'config_ftp_status', '0', 0),
(1740, 0, 'theme_default', 'theme_default_image_thumb_width', '228', 0),
(1741, 0, 'theme_default', 'theme_default_image_thumb_height', '228', 0),
(1739, 0, 'theme_default', 'theme_default_image_category_height', '400', 0),
(1738, 0, 'theme_default', 'theme_default_image_category_width', '1920', 0),
(1736, 0, 'theme_default', 'theme_default_news_limit', '20', 0),
(1737, 0, 'theme_default', 'theme_default_news_description_length', '100', 0),
(1734, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(1951, 0, 'config', 'config_ftp_password', '', 0),
(1952, 0, 'config', 'config_ftp_root', '', 0),
(1950, 0, 'config', 'config_ftp_username', '', 0),
(1949, 0, 'config', 'config_ftp_port', '21', 0),
(1948, 0, 'config', 'config_ftp_hostname', 'localhost', 0),
(1947, 0, 'config', 'config_icon', 'catalog/ED/fav.png', 0),
(1946, 0, 'config', 'config_logo', 'catalog/ED/ed-logo.png', 0),
(1735, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(1733, 0, 'theme_default', 'theme_default_status', '1', 0),
(1732, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(1944, 0, 'config', 'config_captcha', '', 0),
(1945, 0, 'config', 'config_captcha_page', '[\"register\",\"review\",\"contact\"]', 1),
(1943, 0, 'config', 'config_account_id', '3', 0),
(1942, 0, 'config', 'config_login_attempts', '5', 0),
(1941, 0, 'config', 'config_customer_price', '0', 0),
(1940, 0, 'config', 'config_customer_group_display', '[\"1\"]', 1),
(1939, 0, 'config', 'config_customer_group_id', '1', 0),
(1938, 0, 'config', 'config_customer_email_verification', '1', 0),
(1937, 0, 'config', 'config_customer_search', '1', 0),
(1936, 0, 'config', 'config_customer_activity', '1', 0),
(1935, 0, 'config', 'config_customer_online', '1', 0),
(1934, 0, 'config', 'config_review_guest', '1', 0),
(1922, 0, 'config', 'config_open', '', 0),
(1923, 0, 'config', 'config_comment', '', 0),
(1924, 0, 'config', 'config_location', '[\"1\"]', 1),
(1925, 0, 'config', 'config_country_id', '99', 0),
(1926, 0, 'config', 'config_zone_id', '1490', 0),
(1927, 0, 'config', 'config_language', 'en-gb', 0),
(1928, 0, 'config', 'config_admin_language', 'en-gb', 0),
(1929, 0, 'config', 'config_currency', 'USD', 0),
(1930, 0, 'config', 'config_currency_auto', '1', 0),
(1931, 0, 'config', 'config_product_count', '1', 0),
(1932, 0, 'config', 'config_limit_admin', '20', 0),
(1933, 0, 'config', 'config_review_status', '1', 0),
(1921, 0, 'config', 'config_image', 'catalog/newjo/newjo-logo.png', 0),
(1920, 0, 'config', 'config_fax', '', 0),
(1919, 0, 'config', 'config_telephone', '9895971627', 0),
(1909, 0, 'config', 'config_meta_title', 'Emirates Diaries', 0),
(1910, 0, 'config', 'config_meta_description', 'Emirates Diaries', 0),
(1911, 0, 'config', 'config_meta_keyword', '', 0),
(1912, 0, 'config', 'config_theme', 'theme_default', 0),
(1913, 0, 'config', 'config_layout_id', '4', 0),
(1914, 0, 'config', 'config_name', 'Emirates Diaries', 0),
(1915, 0, 'config', 'config_owner', 'Newjo', 0),
(1916, 0, 'config', 'config_address', 'Dubai, United Arab Emirates.', 0),
(1917, 0, 'config', 'config_geocode', '', 0),
(1918, 0, 'config', 'config_email', 'info@emiratesdiaries.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sp_subscription`
--

CREATE TABLE `sp_subscription` (
  `subscribe_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sp_theme`
--

CREATE TABLE `sp_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_translation`
--

CREATE TABLE `sp_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_trustees`
--

CREATE TABLE `sp_trustees` (
  `trustees_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_trustees`
--

INSERT INTO `sp_trustees` (`trustees_id`, `image`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(1, 'catalog/trustee/member.jpg', 1, 1, '2017-01-02 17:27:13', '2017-10-13 19:03:53'),
(2, 'catalog/trustee/member.jpg', 4, 1, '2017-01-02 17:53:01', '2017-10-13 19:04:09'),
(3, '', 2, 1, '2017-01-02 18:03:02', '2017-10-13 19:03:58'),
(4, 'catalog/trustee/member.jpg', 3, 1, '2017-09-18 15:32:26', '2017-10-13 19:04:04'),
(5, 'catalog/trustee/member.jpg', 5, 1, '2017-09-18 16:45:36', '2017-10-13 19:04:14'),
(6, 'catalog/trustee/member.jpg', 6, 1, '2017-09-18 17:09:07', '2017-10-13 19:04:19'),
(7, 'catalog/trustee/member.jpg', 7, 1, '2017-09-18 17:09:17', '2017-10-13 19:04:24'),
(8, 'catalog/trustee/member.jpg', 8, 1, '2017-09-18 17:09:28', '2017-10-13 19:04:28');

-- --------------------------------------------------------

--
-- Table structure for table `sp_trustees_description`
--

CREATE TABLE `sp_trustees_description` (
  `trustees_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_trustees_description`
--

INSERT INTO `sp_trustees_description` (`trustees_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Trustee 1', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(3, 1, 'Trustee 2', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(4, 1, 'Trustee 3', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(2, 1, 'Trustee 4', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(5, 1, 'Trustee 5', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(6, 1, 'Trustee 6', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(7, 1, 'Trustee 7', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(8, 1, 'Trustee 8', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(1, 2, 'Trustee 1', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(3, 2, 'Trustee 2', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(4, 2, 'Trustee 3', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(2, 2, 'Trustee 4', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(5, 2, 'Trustee 5', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(6, 2, 'Trustee 6', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(7, 2, 'Trustee 7', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;'),
(8, 2, 'Trustee 8', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;br&gt;&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Table structure for table `sp_upload`
--

CREATE TABLE `sp_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sp_url_alias`
--

CREATE TABLE `sp_url_alias` (
  `url_alias_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_url_alias`
--

INSERT INTO `sp_url_alias` (`url_alias_id`, `query`, `keyword`) VALUES
(371, 'news_id=1', 'osaymi'),
(253, 'news_id=2', 'mobile-aid'),
(267, 'news_id=3', 'gafla-the-arabic-jeweler'),
(48, 'issue_id=3', 'new-beginnings'),
(282, 'news_id=4', 'tawlat'),
(50, 'issue_id=5', 'avant-garde'),
(49, 'issue_id=4', 'leadership'),
(9, 'project_category_id=1', 'web-based'),
(10, 'project_category_id=2', 'ios'),
(11, 'project_category_id=3', 'android'),
(12, 'project_client_id=1', 'citygold'),
(13, 'project_client_id=2', 'wellfit'),
(14, 'project_client_id=3', 'zask'),
(15, 'project_client_id=4', 'jobells'),
(16, 'project_client_id=5', 'savoy-dubai'),
(17, 'project_id=1', 'city-gold'),
(18, 'project_id=4', 'hh-1'),
(125, 'trustees_id=3', 'trustee-2'),
(124, 'trustees_id=1', 'trustee-1'),
(61, 'issue_id=1', 'the-culture'),
(412, 'gallery_id=4', 'when-hk-hunts-for-food'),
(409, 'news_id=10', 'provedore'),
(275, 'news_id=7', 'outlines-to-ornaments'),
(91, 'author_id=3', ''),
(271, 'news_id=5', 'mbr-centre-for-govt-innovation'),
(274, 'news_id=9', 'nayif-1-to-earth'),
(379, 'news_id=8', 'when-hk-hunts-for-foods'),
(256, 'news_id=11', 'alezan-by-sk'),
(114, 'author_id=4', ''),
(386, 'news_id=12', 'sheikh-zayed-grand-mosque'),
(376, 'newscategory_id=7', 'food'),
(126, 'author_id=5', ''),
(268, 'news_id=13', 'hipa'),
(129, 'author_id=6', ''),
(285, 'news_id=14', 'untitled-chapters'),
(411, 'news_id=15', 'wanna-read'),
(132, 'author_id=7', ''),
(255, 'news_id=16', 'a-world-without-barriers'),
(281, 'news_id=17', 'speed-reading'),
(264, 'news_id=18', 'fairytales'),
(149, 'author_id=8', ''),
(279, 'news_id=19', 'read-your-stress-away'),
(151, 'author_id=9', ''),
(283, 'news_id=20', 'the-actual-adventure-within-us'),
(153, 'author_id=10', ''),
(266, 'news_id=21', 'for-the-lovelof-reading'),
(272, 'news_id=22', 'mira-thani'),
(278, 'news_id=24', 'qafiya'),
(387, 'news_id=23', 'design-talk'),
(169, 'author_id=12', ''),
(388, 'news_id=25', 'mohammed-almusabi'),
(174, 'author_id=13', ''),
(265, 'news_id=26', 'fashion-spotlight'),
(385, 'news_id=27', 'i-have-this-thing-with-tiles'),
(180, 'author_id=14', ''),
(270, 'news_id=28', 'louvre-abu-dhabi'),
(182, 'author_id=15', ''),
(383, 'news_id=29', 'dubai-opera'),
(384, 'news_id=30', 'dubai-water-canal'),
(382, 'news_id=31', 'ana-gow-running'),
(284, 'news_id=32', 'the-parkour-enthusiast'),
(190, 'author_id=17', ''),
(381, 'news_id=33', 'are-computers-smarter-than-humans'),
(192, 'author_id=18', ''),
(389, 'news_id=34', 'alia-bin-omair'),
(196, 'author_id=19', ''),
(391, 'news_id=35', 'pearla'),
(390, 'news_id=36', 'bait-al-kandora'),
(212, 'author_id=20', ''),
(219, 'author_id=21', ''),
(292, 'news_id=37', 'last-edited'),
(226, 'author_id=22', ''),
(289, 'news_id=39', 'dubai-opera-house'),
(398, 'news_id=38', 'the-green-planet'),
(396, 'news_id=41', 'when-the-dome-is-pronounced'),
(295, 'news_id=40', 'dubai-marathon'),
(294, 'author_id=23', ''),
(395, 'news_id=42', 'the-handsome-boy-and-the-teacher'),
(405, 'news_id=43', 'brussels-is-a-battleground-to-a-tourist-city'),
(394, 'news_id=44', 'dusseldorf-city-contemporary-buildings'),
(305, 'news_id=45', 'captain-challenges'),
(306, 'author_id=24', ''),
(314, 'news_id=46', 'exciting-and-attractive-classes'),
(313, 'news_id=47', 'throw-the-newspaper'),
(316, 'news_id=48', 'safety-boat'),
(317, 'author_id=26', ''),
(318, 'news_id=49', 'al-bastakiya'),
(319, 'author_id=27', ''),
(320, 'news_id=50', 'writings-are-not-valid-for-publication'),
(328, 'author_id=30', ''),
(404, 'news_id=51', 'ibrahim-al-hashimi'),
(325, 'author_id=29', ''),
(403, 'news_id=52', 'his-excellency-ahmed-bin-rakad-al-ameri'),
(329, 'news_id=53', 'biography-rafia-ghobash-aisha-al-busmait-nahla-al-fahad'),
(330, 'news_id=54', 'dr-hamad-al-hammadi'),
(331, 'news_id=55', 'library-in-every-house'),
(332, 'news_id=56', 'book-cafe'),
(333, 'news_id=57', 's-b-10'),
(334, 'author_id=31', ''),
(335, 'news_id=58', 'date-of-reading'),
(336, 'author_id=32', ''),
(393, 'news_id=59', 'five-reasons-to-visit-tokyo'),
(339, 'news_id=60', 'reading-life'),
(340, 'author_id=33', ''),
(341, 'news_id=61', 'my-story-with-reading'),
(342, 'author_id=34', ''),
(343, 'news_id=62', 'reading-for-the-successful'),
(344, 'author_id=35', ''),
(345, 'news_id=63', 'try-to-read'),
(346, 'author_id=36', ''),
(392, 'news_id=64', 'yousef-al-mutawa'),
(349, 'author_id=37', ''),
(350, 'news_id=65', 'adeeb-sulaiman-al-balushi'),
(352, 'author_id=38', ''),
(402, 'news_id=66', 'o-flag'),
(354, 'author_id=39', ''),
(355, 'news_id=67', 'stop-your-absence'),
(357, 'author_id=40', ''),
(358, 'news_id=68', 'i-am-a-pilot'),
(359, 'news_id=69', 'when-innovation-is-born'),
(360, 'news_id=70', 'mohammed-bin-rashid'),
(361, 'author_id=41', ''),
(363, 'news_id=71', 'rules-of-Life'),
(364, 'news_id=72', 'why-write'),
(400, 'news_id=73', 'center-of-the-majestic'),
(401, 'news_id=74', 'nuray'),
(367, 'author_id=42', ''),
(399, 'news_id=75', 'sheikh-zayed-grand-mosque-innovative-islamic-culture'),
(369, 'news_id=76', 'mohammed-bin-rashid-centre-for-govt-innovation'),
(397, 'news_id=77', 'youssef-al-zaabi'),
(406, 'issue_id=8', 'architecture-and-design'),
(377, 'issue_id=7', 'reading'),
(378, 'issue_id=6', 'innovation'),
(407, 'author_id=43', '');

-- --------------------------------------------------------

--
-- Table structure for table `sp_user`
--

CREATE TABLE `sp_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_user`
--

INSERT INTO `sp_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'dadmin', '89f825be25ae89fd6f9f103b162105a1fdd47cb2', 'cv26RkCSA', 'A R Abid', 'Kunnil', 'abid@sparteqz.com', '', '', '110.227.237.45', 1, '2016-11-07 10:23:22'),
(2, 10, 'edadmin', '6e690376b49905e66b7b8c7edaf8e7e5735c2f5d', 'KerIhiVUI', 'Ed', 'Admin', 'info@emiratesdiaries.com', '', '', '122.174.184.220', 1, '2017-10-11 11:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `sp_user_group`
--

CREATE TABLE `sp_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_user_group`
--

INSERT INTO `sp_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Superadmin', '{\"access\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/product\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/filemanager\",\"contact\\/newsletter\",\"contact\\/subscribers\",\"customer\\/customer\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/language\",\"design\\/layout\",\"design\\/menu\",\"design\\/theme\",\"design\\/translation\",\"event\\/compatibility\",\"event\\/theme\",\"extension\\/analytics\\/google_analytics\",\"extension\\/captcha\\/basic_captcha\",\"extension\\/captcha\\/google_captcha\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/online\",\"extension\\/event\",\"extension\\/extension\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/module\",\"extension\\/extension\\/theme\",\"extension\\/feed\\/google_sitemap\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/installer\",\"extension\\/modification\",\"extension\\/module\\/account\",\"extension\\/module\\/banner\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/latest\",\"extension\\/module\\/latest_news\",\"extension\\/module\\/news_by_category\",\"extension\\/module\\/news_category\",\"extension\\/module\\/news_featured\",\"extension\\/module\\/popular\",\"extension\\/module\\/popular_news\",\"extension\\/module\\/product_category\",\"extension\\/module\\/seo_keyword_generator\",\"extension\\/module\\/slideshow\",\"extension\\/theme\\/theme_default\",\"gallery\\/gallery\",\"gallery\\/videos\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/location\",\"localisation\\/zone\",\"marketing\\/contact\",\"news\\/author\",\"news\\/category\",\"news\\/featured\",\"news\\/issue\",\"news\\/news\",\"report\\/customer_activity\",\"report\\/customer_online\",\"report\\/customer_search\",\"report\\/product_viewed\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/compatibility\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"trustees\\/trustees\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"],\"modify\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/product\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/filemanager\",\"contact\\/newsletter\",\"contact\\/subscribers\",\"customer\\/customer\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/language\",\"design\\/layout\",\"design\\/menu\",\"design\\/theme\",\"design\\/translation\",\"event\\/compatibility\",\"event\\/theme\",\"extension\\/analytics\\/google_analytics\",\"extension\\/captcha\\/basic_captcha\",\"extension\\/captcha\\/google_captcha\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/online\",\"extension\\/event\",\"extension\\/extension\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/module\",\"extension\\/extension\\/theme\",\"extension\\/feed\\/google_sitemap\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/installer\",\"extension\\/modification\",\"extension\\/module\\/account\",\"extension\\/module\\/banner\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/latest\",\"extension\\/module\\/latest_news\",\"extension\\/module\\/news_by_category\",\"extension\\/module\\/news_category\",\"extension\\/module\\/news_featured\",\"extension\\/module\\/popular\",\"extension\\/module\\/popular_news\",\"extension\\/module\\/product_category\",\"extension\\/module\\/seo_keyword_generator\",\"extension\\/module\\/slideshow\",\"extension\\/theme\\/theme_default\",\"gallery\\/gallery\",\"gallery\\/videos\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/location\",\"localisation\\/zone\",\"marketing\\/contact\",\"news\\/author\",\"news\\/category\",\"news\\/featured\",\"news\\/issue\",\"news\\/news\",\"report\\/customer_activity\",\"report\\/customer_online\",\"report\\/customer_search\",\"report\\/product_viewed\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/compatibility\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"trustees\\/trustees\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"]}'),
(10, 'Siteadmin', '{\"access\":[\"common\\/column_left\",\"common\\/filemanager\",\"contact\\/newsletter\",\"contact\\/subscribers\",\"gallery\\/gallery\",\"gallery\\/videos\",\"news\\/author\",\"news\\/category\",\"news\\/featured\",\"news\\/issue\",\"news\\/news\",\"trustees\\/trustees\"],\"modify\":[\"common\\/column_left\",\"common\\/filemanager\",\"contact\\/newsletter\",\"contact\\/subscribers\",\"gallery\\/gallery\",\"gallery\\/videos\",\"news\\/author\",\"news\\/category\",\"news\\/featured\",\"news\\/issue\",\"news\\/news\",\"trustees\\/trustees\"]}');

-- --------------------------------------------------------

--
-- Table structure for table `sp_videogallery`
--

CREATE TABLE `sp_videogallery` (
  `video_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `video` varchar(100) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_videogallery`
--

INSERT INTO `sp_videogallery` (`video_id`, `author_id`, `issue_id`, `video`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES
(1, 0, 1, 'rN6nlNC9WQA', 3, 1, 0, '2018-10-20 10:57:33', '2018-10-20 12:09:35'),
(2, 0, 1, 'DQuhA5ZCV9M', 2, 1, 0, '2018-10-20 10:57:58', '2018-10-20 12:07:55'),
(4, 0, 1, '6lt2JfJdGSY', 1, 1, 0, '2018-10-20 11:02:01', '2018-10-20 12:09:41');

-- --------------------------------------------------------

--
-- Table structure for table `sp_videogallery_description`
--

CREATE TABLE `sp_videogallery_description` (
  `video_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_videogallery_description`
--

INSERT INTO `sp_videogallery_description` (`video_id`, `language_id`, `name`, `description`) VALUES
(1, 2, 'aaa', ''),
(2, 1, 'bbb', ''),
(1, 1, 'aaa', ''),
(4, 2, 'cccc', ''),
(2, 2, 'bbb', ''),
(4, 1, 'cccc', '');

-- --------------------------------------------------------

--
-- Table structure for table `sp_zone`
--

CREATE TABLE `sp_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_zone`
--

INSERT INTO `sp_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M\'Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu\'a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Aragatsotn', 'AGT', 1),
(181, 11, 'Ararat', 'ARR', 1),
(182, 11, 'Armavir', 'ARM', 1),
(183, 11, 'Geghark\'unik\'', 'GEG', 1),
(184, 11, 'Kotayk\'', 'KOT', 1),
(185, 11, 'Lorri', 'LOR', 1),
(186, 11, 'Shirak', 'SHI', 1),
(187, 11, 'Syunik\'', 'SYU', 1),
(188, 11, 'Tavush', 'TAV', 1),
(189, 11, 'Vayots\' Dzor', 'VAY', 1),
(190, 11, 'Yerevan', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Niederösterreich', 'NOS', 1),
(202, 14, 'Oberösterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Brestskaya (Brest)', 'BR', 1),
(338, 20, 'Homyel\'skaya (Homyel\')', 'HO', 1),
(339, 20, 'Horad Minsk', 'HM', 1),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 1),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 1),
(342, 20, 'Minskaya', 'MI', 1),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Higgins', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1),
(848, 53, 'Grad Zagreb', 'GZ', 1),
(849, 53, 'Dubrovačko-neretvanska', 'DN', 1),
(850, 53, 'Istarska', 'IS', 1),
(851, 53, 'Karlovačka', 'KA', 1),
(852, 53, 'Koprivničko-križevačka', 'KK', 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1),
(854, 53, 'Ličko-senjska', 'LS', 1),
(855, 53, 'Međimurska', 'ME', 1),
(856, 53, 'Osječko-baranjska', 'OB', 1),
(857, 53, 'Požeško-slavonska', 'PS', 1),
(858, 53, 'Primorsko-goranska', 'PG', 1),
(859, 53, 'Šibensko-kninska', 'SK', 1),
(860, 53, 'Sisačko-moslavačka', 'SM', 1),
(861, 53, 'Brodsko-posavska', 'BP', 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1),
(863, 53, 'Varaždinska', 'VA', 1),
(864, 53, 'Virovitičko-podravska', 'VP', 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1),
(866, 53, 'Zadarska', 'ZA', 1),
(867, 53, 'Zagrebačka', 'ZG', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan lääni', 'AL', 1),
(1086, 72, 'Etelä-Suomen lääni', 'ES', 1),
(1087, 72, 'Itä-Suomen lääni', 'IS', 1),
(1088, 72, 'Länsi-Suomen lääni', 'LS', 1),
(1089, 72, 'Lapin lääni', 'LA', 1),
(1090, 72, 'Oulun lääni', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-Württemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Thüringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Puducherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'Kalimantan Utara', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1);
INSERT INTO `sp_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Almaty', 'AL', 1),
(1717, 109, 'Almaty City', 'AC', 1),
(1718, 109, 'Aqmola', 'AM', 1),
(1719, 109, 'Aqtobe', 'AQ', 1),
(1720, 109, 'Astana City', 'AS', 1),
(1721, 109, 'Atyrau', 'AT', 1),
(1722, 109, 'Batys Qazaqstan', 'BA', 1),
(1723, 109, 'Bayqongyr City', 'BY', 1),
(1724, 109, 'Mangghystau', 'MA', 1),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 1),
(1726, 109, 'Pavlodar', 'PA', 1),
(1727, 109, 'Qaraghandy', 'QA', 1),
(1728, 109, 'Qostanay', 'QO', 1),
(1729, 109, 'Qyzylorda', 'QY', 1),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 1),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 1),
(1732, 109, 'Zhambyl', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'MY-01', 1),
(1972, 129, 'Kedah', 'MY-02', 1),
(1973, 129, 'Kelantan', 'MY-03', 1),
(1974, 129, 'Labuan', 'MY-15', 1),
(1975, 129, 'Melaka', 'MY-04', 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1),
(1977, 129, 'Pahang', 'MY-06', 1),
(1978, 129, 'Perak', 'MY-08', 1),
(1979, 129, 'Perlis', 'MY-09', 1),
(1980, 129, 'Pulau Pinang', 'MY-07', 1),
(1981, 129, 'Sabah', 'MY-12', 1),
(1982, 129, 'Sarawak', 'MY-13', 1),
(1983, 129, 'Selangor', 'MY-10', 1),
(1984, 129, 'Terengganu', 'MY-11', 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1),
(4035, 129, 'Putrajaya', 'MY-16', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord-Brabant', 'NB', 1),
(2336, 150, 'Noord-Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid-Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Abakan', 'AB', 1),
(2722, 176, 'Aginskoye', 'AG', 1),
(2723, 176, 'Anadyr', 'AN', 1),
(2724, 176, 'Arkahangelsk', 'AR', 1),
(2725, 176, 'Astrakhan', 'AS', 1),
(2726, 176, 'Barnaul', 'BA', 1),
(2727, 176, 'Belgorod', 'BE', 1),
(2728, 176, 'Birobidzhan', 'BI', 1),
(2729, 176, 'Blagoveshchensk', 'BL', 1),
(2730, 176, 'Bryansk', 'BR', 1),
(2731, 176, 'Cheboksary', 'CH', 1),
(2732, 176, 'Chelyabinsk', 'CL', 1),
(2733, 176, 'Cherkessk', 'CR', 1),
(2734, 176, 'Chita', 'CI', 1),
(2735, 176, 'Dudinka', 'DU', 1),
(2736, 176, 'Elista', 'EL', 1),
(2738, 176, 'Gorno-Altaysk', 'GA', 1),
(2739, 176, 'Groznyy', 'GR', 1),
(2740, 176, 'Irkutsk', 'IR', 1),
(2741, 176, 'Ivanovo', 'IV', 1),
(2742, 176, 'Izhevsk', 'IZ', 1),
(2743, 176, 'Kalinigrad', 'KA', 1),
(2744, 176, 'Kaluga', 'KL', 1),
(2745, 176, 'Kasnodar', 'KS', 1),
(2746, 176, 'Kazan', 'KZ', 1),
(2747, 176, 'Kemerovo', 'KE', 1),
(2748, 176, 'Khabarovsk', 'KH', 1),
(2749, 176, 'Khanty-Mansiysk', 'KM', 1),
(2750, 176, 'Kostroma', 'KO', 1),
(2751, 176, 'Krasnodar', 'KR', 1),
(2752, 176, 'Krasnoyarsk', 'KN', 1),
(2753, 176, 'Kudymkar', 'KU', 1),
(2754, 176, 'Kurgan', 'KG', 1),
(2755, 176, 'Kursk', 'KK', 1),
(2756, 176, 'Kyzyl', 'KY', 1),
(2757, 176, 'Lipetsk', 'LI', 1),
(2758, 176, 'Magadan', 'MA', 1),
(2759, 176, 'Makhachkala', 'MK', 1),
(2760, 176, 'Maykop', 'MY', 1),
(2761, 176, 'Moscow', 'MO', 1),
(2762, 176, 'Murmansk', 'MU', 1),
(2763, 176, 'Nalchik', 'NA', 1),
(2764, 176, 'Naryan Mar', 'NR', 1),
(2765, 176, 'Nazran', 'NZ', 1),
(2766, 176, 'Nizhniy Novgorod', 'NI', 1),
(2767, 176, 'Novgorod', 'NO', 1),
(2768, 176, 'Novosibirsk', 'NV', 1),
(2769, 176, 'Omsk', 'OM', 1),
(2770, 176, 'Orel', 'OR', 1),
(2771, 176, 'Orenburg', 'OE', 1),
(2772, 176, 'Palana', 'PA', 1),
(2773, 176, 'Penza', 'PE', 1),
(2774, 176, 'Perm', 'PR', 1),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 1),
(2776, 176, 'Petrozavodsk', 'PT', 1),
(2777, 176, 'Pskov', 'PS', 1),
(2778, 176, 'Rostov-na-Donu', 'RO', 1),
(2779, 176, 'Ryazan', 'RY', 1),
(2780, 176, 'Salekhard', 'SL', 1),
(2781, 176, 'Samara', 'SA', 1),
(2782, 176, 'Saransk', 'SR', 1),
(2783, 176, 'Saratov', 'SV', 1),
(2784, 176, 'Smolensk', 'SM', 1),
(2785, 176, 'St. Petersburg', 'SP', 1),
(2786, 176, 'Stavropol', 'ST', 1),
(2787, 176, 'Syktyvkar', 'SY', 1),
(2788, 176, 'Tambov', 'TA', 1),
(2789, 176, 'Tomsk', 'TO', 1),
(2790, 176, 'Tula', 'TU', 1),
(2791, 176, 'Tura', 'TR', 1),
(2792, 176, 'Tver', 'TV', 1),
(2793, 176, 'Tyumen', 'TY', 1),
(2794, 176, 'Ufa', 'UF', 1),
(2795, 176, 'Ul\'yanovsk', 'UL', 1),
(2796, 176, 'Ulan-Ude', 'UU', 1),
(2797, 176, 'Ust\'-Ordynskiy', 'US', 1),
(2798, 176, 'Vladikavkaz', 'VL', 1),
(2799, 176, 'Vladimir', 'VA', 1),
(2800, 176, 'Vladivostok', 'VV', 1),
(2801, 176, 'Volgograd', 'VG', 1),
(2802, 176, 'Vologda', 'VD', 1),
(2803, 176, 'Voronezh', 'VO', 1),
(2804, 176, 'Vyatka', 'VY', 1),
(2805, 176, 'Yakutsk', 'YA', 1),
(2806, 176, 'Yaroslavl', 'YR', 1),
(2807, 176, 'Yekaterinburg', 'YE', 1),
(2808, 176, 'Yoshkar-Ola', 'YO', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'Gävleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'Jämtland', 'Z', 1),
(3080, 203, 'Jönköping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, 'Örebro', 'T', 1),
(3085, 203, 'Östergötland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'Södermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'Värmland', 'S', 1),
(3091, 203, 'Västerbotten', 'AC', 1),
(3092, 203, 'Västernorrland', 'Y', 1),
(3093, 203, 'Västmanland', 'U', 1),
(3094, 203, 'Västra Götaland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graubünden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Zürich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1);
INSERT INTO `sp_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1),
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakır', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Cherkas\'ka Oblast\'', '71', 1),
(3481, 220, 'Chernihivs\'ka Oblast\'', '74', 1),
(3482, 220, 'Chernivets\'ka Oblast\'', '77', 1),
(3483, 220, 'Crimea', '43', 1),
(3484, 220, 'Dnipropetrovs\'ka Oblast\'', '12', 1),
(3485, 220, 'Donets\'ka Oblast\'', '14', 1),
(3486, 220, 'Ivano-Frankivs\'ka Oblast\'', '26', 1),
(3487, 220, 'Khersons\'ka Oblast\'', '65', 1),
(3488, 220, 'Khmel\'nyts\'ka Oblast\'', '68', 1),
(3489, 220, 'Kirovohrads\'ka Oblast\'', '35', 1),
(3490, 220, 'Kyiv', '30', 1),
(3491, 220, 'Kyivs\'ka Oblast\'', '32', 1),
(3492, 220, 'Luhans\'ka Oblast\'', '09', 1),
(3493, 220, 'L\'vivs\'ka Oblast\'', '46', 1),
(3494, 220, 'Mykolayivs\'ka Oblast\'', '48', 1),
(3495, 220, 'Odes\'ka Oblast\'', '51', 1),
(3496, 220, 'Poltavs\'ka Oblast\'', '53', 1),
(3497, 220, 'Rivnens\'ka Oblast\'', '56', 1),
(3498, 220, 'Sevastopol\'', '40', 1),
(3499, 220, 'Sums\'ka Oblast\'', '59', 1),
(3500, 220, 'Ternopil\'s\'ka Oblast\'', '61', 1),
(3501, 220, 'Vinnyts\'ka Oblast\'', '05', 1),
(3502, 220, 'Volyns\'ka Oblast\'', '07', 1),
(3503, 220, 'Zakarpats\'ka Oblast\'', '21', 1),
(3504, 220, 'Zaporiz\'ka Oblast\'', '23', 1),
(3505, 220, 'Zhytomyrs\'ka oblast\'', '18', 1),
(3506, 221, 'Abu Dhabi', 'ADH', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubai', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3895, 105, 'Medio Campidano', 'VS', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3902, 105, 'Ogliastra', 'OG', 1),
(3903, 105, 'Olbia-Tempio', 'OT', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', 1),
(4038, 117, 'Aizkraukles novads', '0320200', 1),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', 1),
(4040, 117, 'Aizputes novads', '0640600', 1),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', 1),
(4042, 117, 'Aknīstes novads', '0560800', 1),
(4043, 117, 'Aloja, Alojas novads', '0661007', 1),
(4044, 117, 'Alojas novads', '0661000', 1),
(4045, 117, 'Alsungas novads', '0624200', 1),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', 1),
(4047, 117, 'Alūksnes novads', '0360200', 1),
(4048, 117, 'Amatas novads', '0424701', 1),
(4049, 117, 'Ape, Apes novads', '0360805', 1),
(4050, 117, 'Apes novads', '0360800', 1),
(4051, 117, 'Auce, Auces novads', '0460805', 1),
(4052, 117, 'Auces novads', '0460800', 1),
(4053, 117, 'Ādažu novads', '0804400', 1),
(4054, 117, 'Babītes novads', '0804900', 1),
(4055, 117, 'Baldone, Baldones novads', '0800605', 1),
(4056, 117, 'Baldones novads', '0800600', 1),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', 1),
(4058, 117, 'Baltinavas novads', '0384400', 1),
(4059, 117, 'Balvi, Balvu novads', '0380201', 1),
(4060, 117, 'Balvu novads', '0380200', 1),
(4061, 117, 'Bauska, Bauskas novads', '0400201', 1),
(4062, 117, 'Bauskas novads', '0400200', 1),
(4063, 117, 'Beverīnas novads', '0964700', 1),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', 1),
(4065, 117, 'Brocēnu novads', '0840601', 1),
(4066, 117, 'Burtnieku novads', '0967101', 1),
(4067, 117, 'Carnikavas novads', '0805200', 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', 1),
(4069, 117, 'Cesvaines novads', '0700800', 1),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', 1),
(4071, 117, 'Cēsu novads', '0420200', 1),
(4072, 117, 'Ciblas novads', '0684901', 1),
(4073, 117, 'Dagda, Dagdas novads', '0601009', 1),
(4074, 117, 'Dagdas novads', '0601000', 1),
(4075, 117, 'Daugavpils', '0050000', 1),
(4076, 117, 'Daugavpils novads', '0440200', 1),
(4077, 117, 'Dobele, Dobeles novads', '0460201', 1),
(4078, 117, 'Dobeles novads', '0460200', 1),
(4079, 117, 'Dundagas novads', '0885100', 1),
(4080, 117, 'Durbe, Durbes novads', '0640807', 1),
(4081, 117, 'Durbes novads', '0640801', 1),
(4082, 117, 'Engures novads', '0905100', 1),
(4083, 117, 'Ērgļu novads', '0705500', 1),
(4084, 117, 'Garkalnes novads', '0806000', 1),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', 1),
(4086, 117, 'Grobiņas novads', '0641000', 1),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', 1),
(4088, 117, 'Gulbenes novads', '0500200', 1),
(4089, 117, 'Iecavas novads', '0406400', 1),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', 1),
(4091, 117, 'Ikšķiles novads', '0740600', 1),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', 1),
(4093, 117, 'Ilūkstes novads', '0440801', 1),
(4094, 117, 'Inčukalna novads', '0801800', 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', 1),
(4096, 117, 'Jaunjelgavas novads', '0321000', 1),
(4097, 117, 'Jaunpiebalgas novads', '0425700', 1),
(4098, 117, 'Jaunpils novads', '0905700', 1),
(4099, 117, 'Jelgava', '0090000', 1),
(4100, 117, 'Jelgavas novads', '0540200', 1),
(4101, 117, 'Jēkabpils', '0110000', 1),
(4102, 117, 'Jēkabpils novads', '0560200', 1),
(4103, 117, 'Jūrmala', '0130000', 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', 1),
(4105, 117, 'Kandava, Kandavas novads', '0901211', 1),
(4106, 117, 'Kandavas novads', '0901201', 1),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', 1),
(4108, 117, 'Kārsavas novads', '0681000', 1),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', 1),
(4110, 117, 'Kokneses novads', '0326100', 1),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', 1),
(4112, 117, 'Krāslavas novads', '0600202', 1),
(4113, 117, 'Krimuldas novads', '0806900', 1),
(4114, 117, 'Krustpils novads', '0566900', 1),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', 1),
(4116, 117, 'Kuldīgas novads', '0620200', 1),
(4117, 117, 'Ķeguma novads', '0741001', 1),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', 1),
(4119, 117, 'Ķekavas novads', '0800800', 1),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', 1),
(4121, 117, 'Lielvārdes novads', '0741401', 1),
(4122, 117, 'Liepāja', '0170000', 1),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', 1),
(4124, 117, 'Limbažu novads', '0660200', 1),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', 1),
(4126, 117, 'Līgatnes novads', '0421200', 1),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', 1),
(4128, 117, 'Līvānu novads', '0761201', 1),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', 1),
(4130, 117, 'Lubānas novads', '0701400', 1),
(4131, 117, 'Ludza, Ludzas novads', '0680201', 1),
(4132, 117, 'Ludzas novads', '0680200', 1),
(4133, 117, 'Madona, Madonas novads', '0700201', 1),
(4134, 117, 'Madonas novads', '0700200', 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', 1),
(4136, 117, 'Mazsalacas novads', '0961000', 1),
(4137, 117, 'Mālpils novads', '0807400', 1),
(4138, 117, 'Mārupes novads', '0807600', 1),
(4139, 117, 'Mērsraga novads', '0887600', 1),
(4140, 117, 'Naukšēnu novads', '0967300', 1),
(4141, 117, 'Neretas novads', '0327100', 1),
(4142, 117, 'Nīcas novads', '0647900', 1),
(4143, 117, 'Ogre, Ogres novads', '0740201', 1),
(4144, 117, 'Ogres novads', '0740202', 1),
(4145, 117, 'Olaine, Olaines novads', '0801009', 1),
(4146, 117, 'Olaines novads', '0801000', 1),
(4147, 117, 'Ozolnieku novads', '0546701', 1),
(4148, 117, 'Pārgaujas novads', '0427500', 1),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', 1),
(4150, 117, 'Pāvilostas novads', '0641401', 1),
(4151, 117, 'Piltene, Ventspils novads', '0980213', 1),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', 1),
(4153, 117, 'Pļaviņu novads', '0321400', 1),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', 1),
(4155, 117, 'Preiļu novads', '0760202', 1),
(4156, 117, 'Priekule, Priekules novads', '0641615', 1),
(4157, 117, 'Priekules novads', '0641600', 1),
(4158, 117, 'Priekuļu novads', '0427300', 1),
(4159, 117, 'Raunas novads', '0427700', 1),
(4160, 117, 'Rēzekne', '0210000', 1),
(4161, 117, 'Rēzeknes novads', '0780200', 1),
(4162, 117, 'Riebiņu novads', '0766300', 1),
(4163, 117, 'Rīga', '0010000', 1),
(4164, 117, 'Rojas novads', '0888300', 1),
(4165, 117, 'Ropažu novads', '0808400', 1),
(4166, 117, 'Rucavas novads', '0648500', 1),
(4167, 117, 'Rugāju novads', '0387500', 1),
(4168, 117, 'Rundāles novads', '0407700', 1),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', 1),
(4170, 117, 'Rūjienas novads', '0961600', 1),
(4171, 117, 'Sabile, Talsu novads', '0880213', 1),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', 1),
(4173, 117, 'Salacgrīvas novads', '0661400', 1),
(4174, 117, 'Salas novads', '0568700', 1),
(4175, 117, 'Salaspils novads', '0801200', 1),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', 1),
(4177, 117, 'Saldus novads', '0840200', 1),
(4178, 117, 'Saldus, Saldus novads', '0840201', 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', 1),
(4180, 117, 'Saulkrastu novads', '0801400', 1),
(4181, 117, 'Seda, Strenču novads', '0941813', 1),
(4182, 117, 'Sējas novads', '0809200', 1),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', 1),
(4184, 117, 'Siguldas novads', '0801601', 1),
(4185, 117, 'Skrīveru novads', '0328200', 1),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', 1),
(4187, 117, 'Skrundas novads', '0621200', 1),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', 1),
(4189, 117, 'Smiltenes novads', '0941600', 1),
(4190, 117, 'Staicele, Alojas novads', '0661017', 1),
(4191, 117, 'Stende, Talsu novads', '0880215', 1),
(4192, 117, 'Stopiņu novads', '0809600', 1),
(4193, 117, 'Strenči, Strenču novads', '0941817', 1),
(4194, 117, 'Strenču novads', '0941800', 1),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', 1),
(4196, 117, 'Talsi, Talsu novads', '0880201', 1),
(4197, 117, 'Talsu novads', '0880200', 1),
(4198, 117, 'Tērvetes novads', '0468900', 1),
(4199, 117, 'Tukuma novads', '0900200', 1),
(4200, 117, 'Tukums, Tukuma novads', '0900201', 1),
(4201, 117, 'Vaiņodes novads', '0649300', 1),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', 1),
(4203, 117, 'Valka, Valkas novads', '0940201', 1),
(4204, 117, 'Valkas novads', '0940200', 1),
(4205, 117, 'Valmiera', '0250000', 1),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', 1),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', 1),
(4208, 117, 'Varakļānu novads', '0701800', 1),
(4209, 117, 'Vārkavas novads', '0769101', 1),
(4210, 117, 'Vecpiebalgas novads', '0429300', 1),
(4211, 117, 'Vecumnieku novads', '0409500', 1),
(4212, 117, 'Ventspils', '0270000', 1),
(4213, 117, 'Ventspils novads', '0980200', 1),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', 1),
(4215, 117, 'Viesītes novads', '0561800', 1),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', 1),
(4217, 117, 'Viļakas novads', '0381600', 1),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', 1),
(4219, 117, 'Viļānu novads', '0781800', 1),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', 1),
(4221, 117, 'Zilupes novads', '0681801', 1),
(4222, 43, 'Arica y Parinacota', 'AP', 1),
(4223, 43, 'Los Rios', 'LR', 1),
(4224, 220, 'Kharkivs\'ka Oblast\'', '63', 1),
(4225, 118, 'Beirut', 'LB-BR', 1),
(4226, 118, 'Bekaa', 'LB-BE', 1),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1),
(4228, 118, 'Nabatieh', 'LB-NB', 1),
(4229, 118, 'North', 'LB-NR', 1),
(4230, 118, 'South', 'LB-ST', 1),
(4231, 99, 'Telangana', 'TS', 1),
(4232, 44, 'Qinghai', 'QH', 1),
(4233, 100, 'Papua Barat', 'PB', 1),
(4234, 100, 'Sulawesi Barat', 'SR', 1),
(4235, 100, 'Kepulauan Riau', 'KR', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_zone_to_geo_zone`
--

CREATE TABLE `sp_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sp_zone_to_geo_zone`
--

INSERT INTO `sp_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(110, 222, 0, 4, '2016-12-07 13:51:47', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sp_address`
--
ALTER TABLE `sp_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `sp_api`
--
ALTER TABLE `sp_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Indexes for table `sp_api_ip`
--
ALTER TABLE `sp_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Indexes for table `sp_api_session`
--
ALTER TABLE `sp_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Indexes for table `sp_attribute`
--
ALTER TABLE `sp_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `sp_attribute_description`
--
ALTER TABLE `sp_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Indexes for table `sp_attribute_group`
--
ALTER TABLE `sp_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Indexes for table `sp_attribute_group_description`
--
ALTER TABLE `sp_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Indexes for table `sp_author`
--
ALTER TABLE `sp_author`
  ADD PRIMARY KEY (`author_id`);

--
-- Indexes for table `sp_banner`
--
ALTER TABLE `sp_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `sp_banner_image`
--
ALTER TABLE `sp_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `sp_category`
--
ALTER TABLE `sp_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `sp_category_description`
--
ALTER TABLE `sp_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sp_category_filter`
--
ALTER TABLE `sp_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Indexes for table `sp_category_path`
--
ALTER TABLE `sp_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `sp_country`
--
ALTER TABLE `sp_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `sp_currency`
--
ALTER TABLE `sp_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `sp_customer`
--
ALTER TABLE `sp_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `sp_customer_activity`
--
ALTER TABLE `sp_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Indexes for table `sp_customer_authentication`
--
ALTER TABLE `sp_customer_authentication`
  ADD PRIMARY KEY (`customer_authentication_id`),
  ADD UNIQUE KEY `identifier` (`identifier`,`provider`);

--
-- Indexes for table `sp_customer_group`
--
ALTER TABLE `sp_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `sp_customer_group_description`
--
ALTER TABLE `sp_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Indexes for table `sp_customer_history`
--
ALTER TABLE `sp_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Indexes for table `sp_customer_ip`
--
ALTER TABLE `sp_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `sp_customer_login`
--
ALTER TABLE `sp_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `sp_customer_online`
--
ALTER TABLE `sp_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `sp_customer_search`
--
ALTER TABLE `sp_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Indexes for table `sp_download`
--
ALTER TABLE `sp_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `sp_download_description`
--
ALTER TABLE `sp_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Indexes for table `sp_event`
--
ALTER TABLE `sp_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `sp_extension`
--
ALTER TABLE `sp_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Indexes for table `sp_filter`
--
ALTER TABLE `sp_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `sp_filter_description`
--
ALTER TABLE `sp_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Indexes for table `sp_filter_group`
--
ALTER TABLE `sp_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Indexes for table `sp_filter_group_description`
--
ALTER TABLE `sp_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Indexes for table `sp_gallery`
--
ALTER TABLE `sp_gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `sp_gallery_description`
--
ALTER TABLE `sp_gallery_description`
  ADD PRIMARY KEY (`gallery_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sp_gallery_image`
--
ALTER TABLE `sp_gallery_image`
  ADD PRIMARY KEY (`gallery_image_id`),
  ADD KEY `product_id` (`gallery_id`);

--
-- Indexes for table `sp_geo_zone`
--
ALTER TABLE `sp_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `sp_information`
--
ALTER TABLE `sp_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `sp_information_description`
--
ALTER TABLE `sp_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Indexes for table `sp_language`
--
ALTER TABLE `sp_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sp_layout`
--
ALTER TABLE `sp_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `sp_layout_module`
--
ALTER TABLE `sp_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Indexes for table `sp_layout_route`
--
ALTER TABLE `sp_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Indexes for table `sp_location`
--
ALTER TABLE `sp_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sp_magazine_subscription`
--
ALTER TABLE `sp_magazine_subscription`
  ADD PRIMARY KEY (`subscribe_id`);

--
-- Indexes for table `sp_manufacturer`
--
ALTER TABLE `sp_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `sp_menu`
--
ALTER TABLE `sp_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `sp_menu_description`
--
ALTER TABLE `sp_menu_description`
  ADD PRIMARY KEY (`menu_id`,`language_id`);

--
-- Indexes for table `sp_menu_module`
--
ALTER TABLE `sp_menu_module`
  ADD PRIMARY KEY (`menu_module_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `sp_modification`
--
ALTER TABLE `sp_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Indexes for table `sp_module`
--
ALTER TABLE `sp_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `sp_news`
--
ALTER TABLE `sp_news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `sp_newscategory`
--
ALTER TABLE `sp_newscategory`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `sp_newscategory_description`
--
ALTER TABLE `sp_newscategory_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sp_newscategory_path`
--
ALTER TABLE `sp_newscategory_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `sp_newsissue`
--
ALTER TABLE `sp_newsissue`
  ADD PRIMARY KEY (`issue_id`);

--
-- Indexes for table `sp_newsissue_description`
--
ALTER TABLE `sp_newsissue_description`
  ADD PRIMARY KEY (`issue_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sp_news_attribute`
--
ALTER TABLE `sp_news_attribute`
  ADD PRIMARY KEY (`news_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `sp_news_description`
--
ALTER TABLE `sp_news_description`
  ADD PRIMARY KEY (`news_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sp_news_featured`
--
ALTER TABLE `sp_news_featured`
  ADD PRIMARY KEY (`featured_id`),
  ADD KEY `parent_id` (`news_id`);

--
-- Indexes for table `sp_news_image`
--
ALTER TABLE `sp_news_image`
  ADD PRIMARY KEY (`news_image_id`),
  ADD KEY `product_id` (`news_id`);

--
-- Indexes for table `sp_news_related`
--
ALTER TABLE `sp_news_related`
  ADD PRIMARY KEY (`news_id`,`related_id`);

--
-- Indexes for table `sp_news_to_newscategory`
--
ALTER TABLE `sp_news_to_newscategory`
  ADD PRIMARY KEY (`news_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `sp_product`
--
ALTER TABLE `sp_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `sp_product_attribute`
--
ALTER TABLE `sp_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `sp_product_description`
--
ALTER TABLE `sp_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sp_product_filter`
--
ALTER TABLE `sp_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `sp_product_image`
--
ALTER TABLE `sp_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sp_product_related`
--
ALTER TABLE `sp_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `sp_product_to_category`
--
ALTER TABLE `sp_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `sp_product_to_download`
--
ALTER TABLE `sp_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `sp_projects`
--
ALTER TABLE `sp_projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `sp_project_category`
--
ALTER TABLE `sp_project_category`
  ADD PRIMARY KEY (`project_category_id`);

--
-- Indexes for table `sp_project_clients`
--
ALTER TABLE `sp_project_clients`
  ADD PRIMARY KEY (`project_client_id`);

--
-- Indexes for table `sp_project_image`
--
ALTER TABLE `sp_project_image`
  ADD PRIMARY KEY (`project_image_id`),
  ADD KEY `product_id` (`project_id`);

--
-- Indexes for table `sp_review`
--
ALTER TABLE `sp_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sp_setting`
--
ALTER TABLE `sp_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `sp_subscription`
--
ALTER TABLE `sp_subscription`
  ADD PRIMARY KEY (`subscribe_id`);

--
-- Indexes for table `sp_theme`
--
ALTER TABLE `sp_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Indexes for table `sp_translation`
--
ALTER TABLE `sp_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Indexes for table `sp_trustees`
--
ALTER TABLE `sp_trustees`
  ADD PRIMARY KEY (`trustees_id`);

--
-- Indexes for table `sp_trustees_description`
--
ALTER TABLE `sp_trustees_description`
  ADD PRIMARY KEY (`trustees_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sp_upload`
--
ALTER TABLE `sp_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `sp_url_alias`
--
ALTER TABLE `sp_url_alias`
  ADD PRIMARY KEY (`url_alias_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Indexes for table `sp_user`
--
ALTER TABLE `sp_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `sp_user_group`
--
ALTER TABLE `sp_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `sp_videogallery`
--
ALTER TABLE `sp_videogallery`
  ADD PRIMARY KEY (`video_id`);

--
-- Indexes for table `sp_videogallery_description`
--
ALTER TABLE `sp_videogallery_description`
  ADD PRIMARY KEY (`video_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sp_zone`
--
ALTER TABLE `sp_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `sp_zone_to_geo_zone`
--
ALTER TABLE `sp_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sp_address`
--
ALTER TABLE `sp_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_api`
--
ALTER TABLE `sp_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sp_api_ip`
--
ALTER TABLE `sp_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_api_session`
--
ALTER TABLE `sp_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_attribute`
--
ALTER TABLE `sp_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_attribute_group`
--
ALTER TABLE `sp_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_author`
--
ALTER TABLE `sp_author`
  MODIFY `author_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `sp_banner`
--
ALTER TABLE `sp_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sp_banner_image`
--
ALTER TABLE `sp_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `sp_category`
--
ALTER TABLE `sp_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_country`
--
ALTER TABLE `sp_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `sp_currency`
--
ALTER TABLE `sp_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sp_customer`
--
ALTER TABLE `sp_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_customer_activity`
--
ALTER TABLE `sp_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_customer_authentication`
--
ALTER TABLE `sp_customer_authentication`
  MODIFY `customer_authentication_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_customer_group`
--
ALTER TABLE `sp_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sp_customer_history`
--
ALTER TABLE `sp_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_customer_ip`
--
ALTER TABLE `sp_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_customer_login`
--
ALTER TABLE `sp_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_customer_search`
--
ALTER TABLE `sp_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_download`
--
ALTER TABLE `sp_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_event`
--
ALTER TABLE `sp_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sp_extension`
--
ALTER TABLE `sp_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `sp_filter`
--
ALTER TABLE `sp_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_filter_group`
--
ALTER TABLE `sp_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_gallery`
--
ALTER TABLE `sp_gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sp_gallery_image`
--
ALTER TABLE `sp_gallery_image`
  MODIFY `gallery_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `sp_geo_zone`
--
ALTER TABLE `sp_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sp_information`
--
ALTER TABLE `sp_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sp_language`
--
ALTER TABLE `sp_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sp_layout`
--
ALTER TABLE `sp_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sp_layout_module`
--
ALTER TABLE `sp_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `sp_layout_route`
--
ALTER TABLE `sp_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `sp_location`
--
ALTER TABLE `sp_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sp_magazine_subscription`
--
ALTER TABLE `sp_magazine_subscription`
  MODIFY `subscribe_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_manufacturer`
--
ALTER TABLE `sp_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_menu`
--
ALTER TABLE `sp_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_modification`
--
ALTER TABLE `sp_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_module`
--
ALTER TABLE `sp_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `sp_news`
--
ALTER TABLE `sp_news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `sp_newscategory`
--
ALTER TABLE `sp_newscategory`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `sp_newsissue`
--
ALTER TABLE `sp_newsissue`
  MODIFY `issue_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sp_news_featured`
--
ALTER TABLE `sp_news_featured`
  MODIFY `featured_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `sp_news_image`
--
ALTER TABLE `sp_news_image`
  MODIFY `news_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=774;

--
-- AUTO_INCREMENT for table `sp_product`
--
ALTER TABLE `sp_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_product_image`
--
ALTER TABLE `sp_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_projects`
--
ALTER TABLE `sp_projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sp_project_category`
--
ALTER TABLE `sp_project_category`
  MODIFY `project_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sp_project_clients`
--
ALTER TABLE `sp_project_clients`
  MODIFY `project_client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sp_project_image`
--
ALTER TABLE `sp_project_image`
  MODIFY `project_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_review`
--
ALTER TABLE `sp_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_setting`
--
ALTER TABLE `sp_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1984;

--
-- AUTO_INCREMENT for table `sp_subscription`
--
ALTER TABLE `sp_subscription`
  MODIFY `subscribe_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_theme`
--
ALTER TABLE `sp_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_translation`
--
ALTER TABLE `sp_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_trustees`
--
ALTER TABLE `sp_trustees`
  MODIFY `trustees_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sp_upload`
--
ALTER TABLE `sp_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sp_url_alias`
--
ALTER TABLE `sp_url_alias`
  MODIFY `url_alias_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=413;

--
-- AUTO_INCREMENT for table `sp_user`
--
ALTER TABLE `sp_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sp_user_group`
--
ALTER TABLE `sp_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sp_videogallery`
--
ALTER TABLE `sp_videogallery`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sp_zone`
--
ALTER TABLE `sp_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4236;

--
-- AUTO_INCREMENT for table `sp_zone_to_geo_zone`
--
ALTER TABLE `sp_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
