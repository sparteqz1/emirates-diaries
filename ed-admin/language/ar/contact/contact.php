<?php
// Heading
$_['subscribe_title']        = 'Subscribers';
$_['enquiry_title']          = 'Enquiries';
$_['heading_title']          = 'News';

// Text
$_['enquiry_success']        = 'Success: You have modified enquiriess!';
$_['subscribe_success']      = 'Success: You have modified subscribers!';
$_['text_success']           = 'Success: You have modified subscribers!';
$_['text_subscribe_list']    = 'Subscribers List';
$_['text_enquiry']           = 'Enquiry List';
$_['text_subscriber']        = 'Subscriber List';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Name';
$_['column_image']           = 'Image';
$_['column_email']           = 'Email';
$_['column_issue']           = 'Issue';
$_['column_phone']           = 'Phone';
$_['column_subject']         = 'Subject';
$_['column_enquiry']         = 'Enquiry';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';



// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify enquiriess!';
$_['error_subscribe_permission']       = 'Warning: You do not have permission to modify Subscribers!';
$_['error_keyword']          = 'SEO URL already in use!';
