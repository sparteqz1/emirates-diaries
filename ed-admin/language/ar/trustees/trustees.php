<?php
// Heading
$_['heading_title']          = 'Trustees';

// Text
$_['text_success']           = 'Success: You have modified trustees!';
$_['text_list']              = 'Trustees List';
$_['text_add']               = 'Add Trustee';
$_['text_edit']              = 'Edit Trustee';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Trustee Name';
$_['column_status']          = 'Status';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Trustee Name';
$_['entry_description']      = 'Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_image']            = 'Image';
$_['entry_sort_order']       = 'Sort Order';

$_['entry_status']           = 'Status';

// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify trustees!';
$_['error_name']             = 'Trustee Name must be between 2 and 255 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
