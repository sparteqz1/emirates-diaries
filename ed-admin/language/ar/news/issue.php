<?php
// Heading
$_['heading_title']          = 'Issues';

// Text
$_['text_success']           = 'Success: You have modified news issue!';
$_['text_list']              = 'Issue List';
$_['text_add']               = 'Add Issue';
$_['text_edit']              = 'Edit Issue';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Issue Name';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Issue Name';
$_['entry_description']      = 'Description';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword']     = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_parent']           = 'Parent';
$_['entry_image']            = 'Slider Image';
$_['entry_cover']            = 'Cover';
$_['entry_sort_order']       = 'Sort Order';

$_['entry_top']              = 'Top';
$_['entry_status']           = 'Status';

// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

$_['help_top']               = 'Display in the top menu bar. Only works for the top parent issues.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify issues!';
$_['error_name']             = 'Issue Name must be between 2 and 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
$_['error_parent']           = 'The parent issue you have chosen is a child of the current one!';
