<?php
// Heading
$_['heading_title']     = 'Authors';

// Text
$_['text_success']      = 'Success: You have modified authors!';
$_['text_list']         = 'Authors List';
$_['text_add']          = 'Add Authors';
$_['text_edit']         = 'Edit Authors';
$_['text_default']      = 'Default';

// Column
$_['column_name']       = 'Authors Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Authors Name';
$_['entry_keyword']     = 'SEO URL';
$_['entry_image']       = 'Image';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_type']        = 'Type';

// Help
$_['help_keyword']      = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify authors!';
$_['error_name']        = 'Authors Name must be between 2 and 64 characters!';
$_['error_keyword']     = 'SEO URL already in use!';
$_['error_product']     = 'Warning: This authors cannot be deleted as it is currently assigned to %s news!';
