<?php
// Heading
$_['heading_title']     = 'Projects';

// Text
$_['text_success']      = 'Success: You have modified Project!';
$_['text_list']         = 'Project List';
$_['text_add']          = 'Add Project';
$_['text_edit']         = 'Edit Project';
$_['text_default']      = 'Default';
$_['text_percent']      = 'Percentage';
$_['text_amount']       = 'Fixed Amount';


// Column
$_['column_name']       = 'Project Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']            = 'Project Name';
$_['entry_keyword']         = 'SEO URL';
$_['entry_image']           = 'Image';
$_['entry_sort_order']      = 'Sort Order';
$_['entry_type']            = 'Type';
$_['entry_status']          = 'Status';
$_['entry_project_description'] = 'Description';
$_['entry_project_sub_title'] = 'Subtitle';
$_['entry_price']           = 'Price';
$_['entry_category']       = 'Category';
$_['entry_link']            = 'Link';
$_['entry_client']          = 'Client';
$_['entry_additional_image'] = 'Additional Images';

// Help
$_['help_keyword']      = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Project!';
$_['error_name']        = 'Project Name must be between 2 and 64 characters!';
$_['error_keyword']     = 'SEO URL already in use!';
$_['error_product']     = 'Warning: This manufacturer cannot be deleted as it is currently assigned to %s products!';
$_['error_price']       = 'Price does not appear to be valid!';
$_['error_telephone']   = 'Telephone must be between 3 and 32 characters!';
$_['error_link']        = 'Website Link does not appear to be valid!';

$_['error_project_description']= 'Project Description must be greater than 5 characters!';
$_['error_project_sub_title']  = 'Project Subtitle must be between 2 and 1000 characters!';
