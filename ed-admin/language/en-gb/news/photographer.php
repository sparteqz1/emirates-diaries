<?php
// Heading
$_['heading_title']     = 'Photographers';

// Text
$_['text_success']      = 'Success: You have modified photographer!';
$_['text_list']         = 'Photographers List';
$_['text_add']          = 'Add Photographer';
$_['text_edit']         = 'Edit Photographer';
$_['text_default']      = 'Default';

// Column
$_['column_name']       = 'Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Name';
$_['entry_keyword']     = 'SEO URL';
$_['entry_image']       = 'Image';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_type']        = 'Type';

// Help
$_['help_keyword']      = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify photographers!';
$_['error_name']        = 'Name must be between 2 and 64 characters!';
$_['error_keyword']     = 'SEO URL already in use!';
