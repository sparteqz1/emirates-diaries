<?php
// Heading
$_['heading_title']    = 'Products by Category';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified products by category module!';
$_['text_edit']        = 'Edit Products by Category Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_category']   = 'Categories';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';

// Help
$_['help_product']     = '(Autocomplete)';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify products by category module!';