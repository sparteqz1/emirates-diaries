<?php
// Heading
$_['heading_title']                    = 'Default Store Theme';

// Text
$_['text_theme']                       = 'Themes';
$_['text_success']                     = 'Success: You have modified the default store theme!';
$_['text_edit']                        = 'Edit Default Store Theme';
$_['text_general']                     = 'General';
$_['text_product']                     = 'Products';
$_['text_image']                       = 'Images';
$_['text_news']                        = 'News';
$_['text_news_image']                  = 'News Images';

// Entry
$_['entry_directory']                  = 'Theme Directory';
$_['entry_status']                     = 'Status';
$_['entry_product_limit']              = 'Default Items Per Page';
$_['entry_product_description_length'] = 'List Description Limit';
$_['entry_news_limit']                 = 'Default News Per Page';
$_['entry_news_description_length']    = 'List Description Limit';
$_['entry_image_category']             = 'Category Image Size (W x H)';
$_['entry_image_thumb']                = 'Product Image Thumb Size (W x H)';
$_['entry_image_popup']                = 'Product Image Popup Size (W x H)';
$_['entry_image_product']              = 'Product Image List Size (W x H)';
$_['entry_image_additional']           = 'Additional Product Image Size (W x H)';
$_['entry_image_related']              = 'Related Product Image Size (W x H)';
$_['entry_image_compare']              = 'Compare Image Size (W x H)';
$_['entry_image_wishlist']             = 'Wish List Image Size (W x H)';
$_['entry_image_cart']                 = 'Cart Image Size (W x H)';
$_['entry_image_location']             = 'Store Image Size (W x H)';
$_['entry_width']                      = 'Width';
$_['entry_height']                     = 'Height';


$_['entry_image_news_category']         = 'News Category Image Size (W x H)';
$_['entry_image_news_thumb']            = 'News Image Thumb Size (W x H)';
$_['entry_image_news_popup']            = 'News Main Image  Size (W x H)';
$_['entry_image_news']          = 'News Image List Size (W x H)';
$_['entry_image_news_additional']       = 'Additional News Image Size (W x H)';
$_['entry_image_news_related']          = 'Related News Image Size (W x H)';

// Help
$_['help_directory'] 	               = 'This field is only to enable older themes to be compatible with the new theme system. You can set the theme directory to use on the image size settings defined here.';
$_['help_product_limit'] 	           = 'Determines how many catalog items are shown per page (products, categories, etc)';
$_['help_product_description_length']  = 'In the list view, short description character limit (categories, special etc)';
$_['help_news_limit'] 	               = 'Determines how many news items are shown per page ';
$_['help_news_description_length']     = 'In the list view, short description character limit (categories, special etc)';

// Error
$_['error_permission']                 = 'Warning: You do not have permission to modify the default store theme!';
$_['error_limit']       	           = 'Product Limit required!';
$_['error_image_thumb']                = 'Product Image Thumb Size dimensions required!';
$_['error_image_popup']                = 'Product Image Popup Size dimensions required!';
$_['error_image_product']              = 'Product List Size dimensions required!';
$_['error_image_category']             = 'Category List Size dimensions required!';
$_['error_image_additional']           = 'Additional Product Image Size dimensions required!';
$_['error_image_related']              = 'Related Product Image Size dimensions required!';
$_['error_image_compare']              = 'Compare Image Size dimensions required!';
$_['error_image_wishlist']             = 'Wish List Image Size dimensions required!';
$_['error_image_cart']                 = 'Cart Image Size dimensions required!';
$_['error_image_location']             = 'Store Image Size dimensions required!';
$_['error_image_news_thumb']                = 'News Image Thumb Size dimensions required!';
$_['error_image_news_popup']                = 'News Main Image  Size dimensions required!';
$_['error_image_news']              = 'News List Size dimensions required!';
$_['error_image_news_category']             = 'News Category List Size dimensions required!';
$_['error_image_news_additional']           = 'Additional News Image Size dimensions required!';
$_['error_image_news_related']              = 'Related News Image Size dimensions required!';