<?php
// Heading
$_['newsletter_title']        = 'Newsletters';
$_['enquiry_title']          = 'Enquiries';
$_['heading_title']          = 'News';

// Text
$_['enquiry_success']        = 'Success: You have modified enquiriess!';
$_['newsletter_success']      = 'Success: You have modified newsletter!';
$_['text_success']           = 'Success: You have modified newsletter!';
$_['text_newsletter_list']    = 'Newsletter List';
$_['text_enquiry']           = 'Enquiry List';
$_['text_newsletter']        = 'Newsletter List';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Name';
$_['column_image']           = 'Image';
$_['column_email']           = 'Email';
$_['column_subject']           = 'Subject';
$_['column_enquiry']         = 'Enquiry';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';



// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify Newsletters!';
$_['error_newsletter_permission']       = 'Warning: You do not have permission to modify Newsletters!';
$_['error_keyword']          = 'SEO URL already in use!';
