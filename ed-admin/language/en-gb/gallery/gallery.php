<?php
// Heading
$_['heading_title']          = 'Gallery';

// Text
$_['text_success']           = 'Success: You have modified gallery!';
$_['text_list']              = 'Gallery List';
$_['text_add']               = 'Add Gallery';
$_['text_edit']              = 'Edit Gallery';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Title';
$_['column_image']           = 'Image';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Title';
$_['entry_description']      = 'Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_image']            = 'Image';
$_['entry_additional_image'] = 'Additional Images';
$_['entry_text']             = 'Text';
$_['entry_required']         = 'Required';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_category']         = 'Categories';
$_['entry_author']           = 'Author';
$_['entry_issue']            = 'Issue';


// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_category']          = '(Autocomplete)';
$_['help_author']           = '(Autocomplete)';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify gallery!';
$_['error_name']             = 'Title must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 1 and less than 64 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
