<?php
// Heading
$_['heading_title']          = 'ED-admin Panel';
$_['text_customer']          = 'Customers';
$_['text_online']            = 'Customers Online';
$_['text_approval']          = 'Pending approval';
$_['text_product']           = 'Products';
$_['text_review']            = 'Reviews';
$_['text_logout']            = 'Logout';
$_['text_stock']            = 'Stock';
