<?php
// Text
$_['text_footer']  = '<a href="http://www.designfort.com/">Designfort</a> &copy; ' . date('Y') .'-'. date('y', strtotime('+1 year')) . ' All Rights Reserved.';
$_['text_version'] = 'Version %s';