<?php

class ControllerProjectProject extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('project/project');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('project/project');

        $this->getList();
    }

    public function add() {
        $this->load->language('project/project');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('project/project');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_project_project->addProject($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('project/project', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit() {
        $this->load->language('project/project');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('project/project');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_project_project->editProject($this->request->get['project_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('project/project', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('project/project');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('project/project');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $project_id) {
                $this->model_project_project->deleteProject($project_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('project/project', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('project/project', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['add'] = $this->url->link('project/project/add', 'token=' . $this->session->data['token'] . $url, true);
        $data['delete'] = $this->url->link('project/project/delete', 'token=' . $this->session->data['token'] . $url, true);

        $data['projects'] = array();

        $filter_data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $project_total = $this->model_project_project->getTotalProjects();

        $results = $this->model_project_project->getProjects($filter_data);

        foreach ($results as $result) {
            $data['projects'][] = array(
                'project_id' => $result['project_id'],
                'name' => $result['name'],
                'sort_order' => $result['sort_order'],
                'edit' => $this->url->link('project/project/edit', 'token=' . $this->session->data['token'] . '&project_id=' . $result['project_id'] . $url, true)
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('project/project', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
        $data['sort_sort_order'] = $this->url->link('project/project', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $project_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('project/project', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($project_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($project_total - $this->config->get('config_limit_admin'))) ? $project_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $project_total, ceil($project_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('project/project_list', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['project_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_percent'] = $this->language->get('text_percent');
        $data['text_amount'] = $this->language->get('text_amount');
        
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_price'] = $this->language->get('entry_price');
        $data['entry_link'] = $this->language->get('entry_link');
        $data['entry_category'] = $this->language->get('entry_category');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_keyword'] = $this->language->get('entry_keyword');
        $data['entry_project_description'] = $this->language->get('entry_project_description');
        $data['entry_project_sub_title'] = $this->language->get('entry_project_sub_title');
        $data['entry_client'] = $this->language->get('entry_client');
        
        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_customer_group'] = $this->language->get('entry_customer_group');
        
        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_image'] = $this->language->get('tab_image');
        $data['entry_additional_image'] = $this->language->get('entry_additional_image');

        $data['help_keyword'] = $this->language->get('help_keyword');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_attribute_add'] = $this->language->get('button_attribute_add');
        $data['button_image_add'] = $this->language->get('button_image_add');
        $data['button_remove'] = $this->language->get('button_remove');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = '';
        }
        if (isset($this->error['project_description'])) {
            $data['error_project_description'] = $this->error['project_description'];
        } else {
            $data['error_project_description'] = '';
        }
        if (isset($this->error['project_sub_title'])) {
            $data['error_project_sub_title'] = $this->error['project_sub_title'];
        } else {
            $data['error_project_sub_title'] = '';
        }
        
        if (isset($this->error['price'])) {
            $data['error_price'] = $this->error['price'];
        } else {
            $data['error_price'] = '';
        }

        
        if (isset($this->error['link'])) {
            $data['error_link'] = $this->error['link'];
        } else {
            $data['error_link'] = '';
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('project/project', 'token=' . $this->session->data['token'] . $url, true)
        );

        if (!isset($this->request->get['project_id'])) {
            $data['action'] = $this->url->link('project/project/add', 'token=' . $this->session->data['token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('project/project/edit', 'token=' . $this->session->data['token'] . '&project_id=' . $this->request->get['project_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('project/project', 'token=' . $this->session->data['token'] . $url, true);

        if (isset($this->request->get['project_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $project_info = $this->model_project_project->getProject($this->request->get['project_id']);
        }

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($project_info)) {
            $data['name'] = $project_info['name'];
        } else {
            $data['name'] = '';
        }


        if (isset($this->request->post['keyword'])) {
            $data['keyword'] = $this->request->post['keyword'];
        } elseif (!empty($project_info)) {
            $data['keyword'] = $project_info['keyword'];
        } else {
            $data['keyword'] = '';
        }

       
        
        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($project_info)) {
            $data['status'] = $project_info['status'];
        } else {
            $data['status'] = true;
        }
        if (isset($this->request->post['project_description'])) {
            $data['project_description'] = $this->request->post['project_description'];
        } elseif (!empty($project_info)) {
            $data['project_description'] = $project_info['project_description'];
        } else {
            $data['project_description'] = '';
        }
        if (isset($this->request->post['project_sub_title'])) {
            $data['project_sub_title'] = $this->request->post['project_sub_title'];
        } elseif (!empty($project_info)) {
            $data['project_sub_title'] = $project_info['subtitle'];
        } else {
            $data['project_sub_title'] = '';
        }
        
        if (isset($this->request->post['price'])) {
            $data['price'] = $this->request->post['price'];
        } elseif (!empty($project_info)) {
            $data['price'] = $project_info['price'];
        } else {
            $data['price'] = '';
        }

        if (isset($this->request->post['project_category_id'])) {
            $data['project_category_id'] = $this->request->post['project_category_id'];
        } elseif (!empty($project_info)) {
            $data['project_category_id'] = $project_info['project_category_id'];
        } else {
            $data['project_category_id'] = '';
        }
        
        if (isset($this->request->post['link'])) {
            $data['link'] = $this->request->post['link'];
        } elseif (!empty($project_info)) {
            $data['link'] = $project_info['link'];
        } else {
            $data['link'] = '';
        }

        

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($project_info)) {
            $data['sort_order'] = $project_info['sort_order'];
        } else {
            $data['sort_order'] = '';
        }
        if (isset($this->request->post['project_client_id'])) {
            $data['project_client_id'] = $this->request->post['project_client_id'];
        } elseif (!empty($project_info)) {
            $data['project_client_id'] = $project_info['project_client_id'];
        } else {
            $data['project_client_id'] = '';
        }
        if (isset($this->request->post['project_category_id'])) {
            $data['project_category_id'] = $this->request->post['project_category_id'];
        } elseif (!empty($project_info)) {
            $data['project_category_id'] = $project_info['project_category_id'];
        } else {
            $data['project_category_id'] = '';
        }
        
        // Image
        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($project_info)) {
            $data['image'] = $project_info['image'];
        } else {
            $data['image'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($project_info) && is_file(DIR_IMAGE . $project_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($project_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }
        
        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        
        // Images
        if (isset($this->request->post['project_image'])) {
            $project_images = $this->request->post['project_image'];
        } elseif (isset($this->request->get['project_id'])) {
            $project_images = $this->model_project_project->getProjectImages($this->request->get['project_id']);
        } else {
            $project_images = array();
        }

        $data['project_images'] = array();

        foreach ($project_images as $project_image) {
            if (is_file(DIR_IMAGE . $project_image['image'])) {
                $image = $project_image['image'];
                $thumb = $project_image['image'];
            } else {
                $image = '';
                $thumb = 'no_image.png';
            }

            $data['project_images'][] = array(
                'image' => $image,
                'thumb' => $this->model_tool_image->resize($thumb, 100, 100),
                'sort_order' => $project_image['sort_order']
            );
        }
        
        $this->load->model('project/client');
        $data['project_clients'] = $this->model_project_client->getClients();
        
        $this->load->model('project/category');
        $data['project_categories'] = $this->model_project_category->getCategories();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('project/project_form', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'project/project')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 64)) {
            $this->error['name'] = $this->language->get('error_name');
        }
        if ((!empty($this->request->post['project_description'])) && (utf8_strlen($this->request->post['project_description']) < 6) ) {
            $this->error['project_description'] = $this->language->get('error_project_description');
        }
        if ((!empty($this->request->post['project_sub_title'])) && (utf8_strlen($this->request->post['project_sub_title']) < 2) || (utf8_strlen($this->request->post['project_sub_title']) > 2000)) {
            $this->error['project_sub_title'] = $this->language->get('error_project_sub_title');
        }
        
        
        
        
        if ((!empty($this->request->post['link'])) && !filter_var($this->request->post['link'], FILTER_VALIDATE_URL)) {
            $this->error['link'] = $this->language->get('error_link');
        }

        if (utf8_strlen($this->request->post['keyword']) > 0) {
            $this->load->model('catalog/url_alias');

            $url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

            if ($url_alias_info && isset($this->request->get['project_id']) && $url_alias_info['query'] != 'project_id=' . $this->request->get['project_id']) {
                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
            }

            if ($url_alias_info && !isset($this->request->get['project_id'])) {
                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
            }
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'project/project')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

       
        return !$this->error;
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('project/project');

            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'start' => 0,
                'limit' => 5
            );

            $results = $this->model_project_project->getProjects($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    'project_id' => $result['project_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
