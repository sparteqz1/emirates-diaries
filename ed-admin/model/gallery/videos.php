<?php

class ModelGalleryVideos extends Model {

    public function addVideo($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "videogallery SET status = '" . (int) $data['status'] . "', issue_id = '" . $this->db->escape($data['issue_id']) . "', author_id = '" . (int) $data['author_id'] . "', sort_order = '" . (int) $data['sort_order'] . "', video = '" . $data['video'] . "', date_added = NOW()");

        $video_id = $this->db->getLastId();

        foreach ($data['gallery_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "videogallery_description SET video_id = '" . (int) $video_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }


        $this->cache->delete('gallery');

        return $video_id;
    }

    public function editVideo($video_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "videogallery SET status = '" . (int) $data['status'] . "', issue_id = '" . $this->db->escape($data['issue_id']) . "', author_id = '" . (int) $data['author_id'] . "', sort_order = '" . (int) $data['sort_order'] . "', video = '" . $data['video'] . "', date_modified = NOW() WHERE video_id = '" . (int) $video_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "videogallery_description WHERE video_id = '" . (int) $video_id . "'");

        foreach ($data['gallery_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "videogallery_description SET video_id = '" . (int) $video_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }


        $this->cache->delete('gallery');
    }

    

    public function deleteVideo($video_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "videogallery WHERE video_id = '" . (int) $video_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "videogallery_description WHERE video_id = '" . (int) $video_id . "'");

        $this->cache->delete('gallery');
    }

    public function getVideo($video_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "videogallery p LEFT JOIN " . DB_PREFIX . "videogallery_description pd ON (p.video_id = pd.video_id) WHERE p.video_id = '" . (int) $video_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getVideos($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "videogallery p LEFT JOIN " . DB_PREFIX . "videogallery_description pd ON (p.video_id = pd.video_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_image']) && !is_null($data['filter_image'])) {
            if ($data['filter_image'] == 1) {
                $sql .= " AND (p.image IS NOT NULL AND p.image <> '' AND p.image <> 'no_image.png')";
            } else {
                $sql .= " AND (p.image IS NULL OR p.image = '' OR p.image = 'no_image.png')";
            }
        }

        $sql .= " GROUP BY p.video_id";

        $sort_data = array(
            'pd.name',
            'p.status',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    
    public function getVideoDescriptions($video_id) {
        $gallery_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "videogallery_description WHERE video_id = '" . (int) $video_id . "'");

        foreach ($query->rows as $result) {
            $gallery_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description']
            );
        }

        return $gallery_description_data;
    }

    public function getTotalGallerys($data = array()) {
        $sql = "SELECT COUNT(DISTINCT p.video_id) AS total FROM " . DB_PREFIX . "videogallery p LEFT JOIN " . DB_PREFIX . "videogallery_description pd ON (p.video_id = pd.video_id)";

        $sql .= " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

        
        $query = $this->db->query($sql);

        return $query->row['total'];
    }
    
    public function getIssues($data = array()) {
        $sql = "SELECT c.issue_id AS issue_id, "
                . "cd.name FROM " . DB_PREFIX . "newsissue c "
                . "LEFT JOIN " . DB_PREFIX . "newsissue_description cd ON (c.issue_id = cd.issue_id) "
                . "WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "'"
                . " AND c.status = '1' ORDER BY c.sort_order ASC " ;
        
        $query = $this->db->query($sql);

        return $query->rows;
    }

}
