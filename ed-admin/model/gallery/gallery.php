<?php

class ModelGalleryGallery extends Model {

    public function addGallery($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "gallery SET status = '" . (int) $data['status'] . "', issue_id = '" . $this->db->escape($data['issue_id']) . "', author_id = '" . (int) $data['author_id'] . "', sort_order = '" . (int) $data['sort_order'] . "', date_added = NOW()");

        $gallery_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "gallery SET image = '" . $this->db->escape($data['image']) . "' WHERE gallery_id = '" . (int) $gallery_id . "'");
        }

        foreach ($data['gallery_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "gallery_description SET gallery_id = '" . (int) $gallery_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }


        if (isset($data['gallery_image'])) {
            foreach ($data['gallery_image'] as $gallery_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "gallery_image SET gallery_id = '" . (int) $gallery_id . "', image = '" . $this->db->escape($gallery_image['image']) . "', sort_order = '" . (int) $gallery_image['sort_order'] . "'");
            }
        }


        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'gallery_id=" . (int) $gallery_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }


        $this->cache->delete('gallery');

        return $gallery_id;
    }

    public function editGallery($gallery_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "gallery SET status = '" . (int) $data['status'] . "', issue_id = '" . $this->db->escape($data['issue_id']) . "', author_id = '" . (int) $data['author_id'] . "', sort_order = '" . (int) $data['sort_order'] . "', date_modified = NOW() WHERE gallery_id = '" . (int) $gallery_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "gallery SET image = '" . $this->db->escape($data['image']) . "' WHERE gallery_id = '" . (int) $gallery_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "gallery_description WHERE gallery_id = '" . (int) $gallery_id . "'");

        foreach ($data['gallery_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "gallery_description SET gallery_id = '" . (int) $gallery_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "gallery_image WHERE gallery_id = '" . (int) $gallery_id . "'");

        if (isset($data['gallery_image'])) {
            foreach ($data['gallery_image'] as $gallery_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "gallery_image SET gallery_id = '" . (int) $gallery_id . "', image = '" . $this->db->escape($gallery_image['image']) . "', sort_order = '" . (int) $gallery_image['sort_order'] . "'");
            }
        }

        

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'gallery_id=" . (int) $gallery_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'gallery_id=" . (int) $gallery_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }



        $this->cache->delete('gallery');
    }

    

    public function deleteGallery($gallery_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "gallery WHERE gallery_id = '" . (int) $gallery_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "gallery_description WHERE gallery_id = '" . (int) $gallery_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "gallery_image WHERE gallery_id = '" . (int) $gallery_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'gallery_id=" . (int) $gallery_id . "'");

        $this->cache->delete('gallery');
    }

    public function getGallery($gallery_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'gallery_id=" . (int) $gallery_id . "') AS keyword FROM " . DB_PREFIX . "gallery p LEFT JOIN " . DB_PREFIX . "gallery_description pd ON (p.gallery_id = pd.gallery_id) WHERE p.gallery_id = '" . (int) $gallery_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getGallerys($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "gallery p LEFT JOIN " . DB_PREFIX . "gallery_description pd ON (p.gallery_id = pd.gallery_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_image']) && !is_null($data['filter_image'])) {
            if ($data['filter_image'] == 1) {
                $sql .= " AND (p.image IS NOT NULL AND p.image <> '' AND p.image <> 'no_image.png')";
            } else {
                $sql .= " AND (p.image IS NULL OR p.image = '' OR p.image = 'no_image.png')";
            }
        }

        $sql .= " GROUP BY p.gallery_id";

        $sort_data = array(
            'pd.name',
            'p.status',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    
    public function getGalleryDescriptions($gallery_id) {
        $gallery_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "gallery_description WHERE gallery_id = '" . (int) $gallery_id . "'");

        foreach ($query->rows as $result) {
            $gallery_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description']
            );
        }

        return $gallery_description_data;
    }

    
    public function getGalleryImages($gallery_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "gallery_image WHERE gallery_id = '" . (int) $gallery_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getTotalGallerys($data = array()) {
        $sql = "SELECT COUNT(DISTINCT p.gallery_id) AS total FROM " . DB_PREFIX . "gallery p LEFT JOIN " . DB_PREFIX . "gallery_description pd ON (p.gallery_id = pd.gallery_id)";

        $sql .= " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

        
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

   
    
    public function getTotalGallerysByAuthorId($author_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "gallery WHERE author_id = '" . (int) $author_id . "'");

        return $query->row['total'];
    }
    
    public function getIssues($data = array()) {
        $sql = "SELECT c.issue_id AS issue_id, "
                . "cd.name FROM " . DB_PREFIX . "newsissue c "
                . "LEFT JOIN " . DB_PREFIX . "newsissue_description cd ON (c.issue_id = cd.issue_id) "
                . "WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "'"
                . " AND c.status = '1' ORDER BY c.sort_order ASC " ;
        
        $query = $this->db->query($sql);

        return $query->rows;
    }

}
