<?php

class ModelTrusteesTrustees extends Model {

    public function addTrustee($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "trustees SET sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $trustees_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "trustees SET image = '" . $this->db->escape($data['image']) . "' WHERE trustees_id = '" . (int) $trustees_id . "'");
        }

        foreach ($data['trustee_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "trustees_description SET trustees_id = '" . (int) $trustees_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'trustees_id=" . (int) $trustees_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('trustees');

        return $trustees_id;
    }

    public function editTrustee($trustees_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "trustees SET sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW() WHERE trustees_id = '" . (int) $trustees_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "trustees SET image = '" . $this->db->escape($data['image']) . "' WHERE trustees_id = '" . (int) $trustees_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "trustees_description WHERE trustees_id = '" . (int) $trustees_id . "'");

        foreach ($data['trustee_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "trustees_description SET trustees_id = '" . (int) $trustees_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'trustees_id=" . (int) $trustees_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'trustees_id=" . (int) $trustees_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('trustee');
    }

    public function deleteTrustee($trustees_id) {
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "trustees WHERE trustees_id = '" . (int) $trustees_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "trustees_description WHERE trustees_id = '" . (int) $trustees_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'trustees_id=" . (int) $trustees_id . "'");

        $this->cache->delete('trustees');
    }

    public function getTrustee($trustees_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT DISTINCT keyword "
                . "FROM " . DB_PREFIX . "url_alias WHERE query = 'trustees_id=" . (int) $trustees_id . "') "
                . "AS keyword FROM " . DB_PREFIX . "trustees c "
                . "LEFT JOIN " . DB_PREFIX . "trustees_description cd2 ON (c.trustees_id = cd2.trustees_id) "
                . "WHERE c.trustees_id = '" . (int) $trustees_id . "' "
                . "AND cd2.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getTrustees($data = array()) {
        $sql = "SELECT c.trustees_id AS trustees_id, "
                . "GROUP_CONCAT(cd.name ORDER BY c.sort_order) AS name, c.status, "
                . "c.sort_order FROM " . DB_PREFIX . "trustees c "
                . "LEFT JOIN " . DB_PREFIX . "trustees_description cd ON (c.trustees_id = cd.trustees_id) "
                . "WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY c.trustees_id";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTrusteeDescriptions($trustees_id) {
        $trustee_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "trustees_description WHERE "
                . "trustees_id = '" . (int) $trustees_id . "'");

        foreach ($query->rows as $result) {
            $trustee_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description']
            );
        }

        return $trustee_description_data;
    }
    
    public function getTotalTrustees() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "trustees");

        return $query->row['total'];
    }

}
