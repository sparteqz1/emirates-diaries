<?php

class ModelNewsPhotographer extends Model {

    public function addPhotographer($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "photographer SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int) $data['sort_order'] . "'");

        $photographer_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "photographer SET image = '" . $this->db->escape($data['image']) . "' WHERE photographer_id = '" . (int) $photographer_id . "'");
        }

       

        if (isset($data['keyword'])) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'photographer_id=" . (int) $photographer_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('photographer');

        return $photographer_id;
    }

    public function editPhotographer($photographer_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "photographer SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int) $data['sort_order'] . "' WHERE photographer_id = '" . (int) $photographer_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "photographer SET image = '" . $this->db->escape($data['image']) . "' WHERE photographer_id = '" . (int) $photographer_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'photographer_id=" . (int) $photographer_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'photographer_id=" . (int) $photographer_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('photographer');
    }

    public function deletePhotographer($photographer_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "photographer WHERE photographer_id = '" . (int) $photographer_id . "'");
       
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'photographer_id=" . (int) $photographer_id . "'");

        $this->cache->delete('photographer');
    }

    public function getPhotographer($photographer_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'photographer_id=" . (int) $photographer_id . "') AS keyword FROM " . DB_PREFIX . "photographer WHERE photographer_id = '" . (int) $photographer_id . "'");

        return $query->row;
    }

    public function getPhotographers($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "photographer";

        if (!empty($data['filter_name'])) {
            $sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    

    public function getTotalPhotographers() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "photographer");

        return $query->row['total'];
    }

}
