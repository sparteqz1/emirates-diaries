<?php

class ModelNewsNews extends Model {

    public function addPost($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "news SET status = '" . (int) $data['status'] . "', `langage` = '" . $this->db->escape($data['langage']) . "', published_date = '" . $this->db->escape($data['published']) . "', arrangement = '" . $this->db->escape($data['arrangement']) . "', issue_id = '" . (int) $data['issue_id'] . "', author_id = '" . (int) $data['author_id'] . "', sort_order = '" . (int) $data['sort_order'] . "', date_added = NOW()");

        $news_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "news SET image = '" . $this->db->escape($data['image']) . "' WHERE news_id = '" . (int) $news_id . "'");
        }

        foreach ($data['news_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "news_description SET news_id = '" . (int) $news_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "'");
        }

        if (isset($data['news_attribute'])) {
            foreach ($data['news_attribute'] as $news_attribute) {
                if ($news_attribute['attribute_id']) {
                    // Removes duplicates
                    $this->db->query("DELETE FROM " . DB_PREFIX . "news_attribute WHERE news_id = '" . (int) $news_id . "' AND attribute_id = '" . (int) $news_attribute['attribute_id'] . "'");

                    foreach ($news_attribute['news_attribute_description'] as $language_id => $news_attribute_description) {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "news_attribute WHERE news_id = '" . (int) $news_id . "' AND attribute_id = '" . (int) $news_attribute['attribute_id'] . "' AND language_id = '" . (int) $language_id . "'");

                        $this->db->query("INSERT INTO " . DB_PREFIX . "news_attribute SET news_id = '" . (int) $news_id . "', attribute_id = '" . (int) $news_attribute['attribute_id'] . "', language_id = '" . (int) $language_id . "', text = '" . $this->db->escape($news_attribute_description['text']) . "'");
                    }
                }
            }
        }


        if (isset($data['news_image'])) {
            foreach ($data['news_image'] as $news_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_image SET news_id = '" . (int) $news_id . "', image = '" . $this->db->escape($news_image['image']) . "', sort_order = '" . (int) $news_image['sort_order'] . "'");
            }
        }

        

        if (isset($data['news_category'])) {
            foreach ($data['news_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_to_newscategory SET news_id = '" . (int) $news_id . "', category_id = '" . (int) $category_id . "'");
            }
        }

        if (isset($data['news_photographer'])) {
            foreach ($data['news_photographer'] as $photographer_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_to_photographer SET news_id = '" . (int) $news_id . "', photographer_id = '" . (int) $photographer_id . "'");
            }
        }        

        if (isset($data['news_related'])) {
            foreach ($data['news_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "news_related WHERE news_id = '" . (int) $news_id . "' AND related_id = '" . (int) $related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_related SET news_id = '" . (int) $news_id . "', related_id = '" . (int) $related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "news_related WHERE news_id = '" . (int) $news_id . "' AND related_id = '" . (int) $news_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_related SET news_id = '" . (int) $news_id . "', related_id = '" . (int) $news_id . "'");
            }
        }



        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_id=" . (int) $news_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }


        $this->cache->delete('news');

        return $news_id;
    }

    public function editPost($news_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "news SET status = '" . (int) $data['status'] . "', `langage` = '" . $this->db->escape($data['langage']) . "', published_date = '" . $this->db->escape($data['published']) . "', issue_id = '" . (int) $data['issue_id'] . "', arrangement = '" . $this->db->escape($data['arrangement']) . "', author_id = '" . (int) $data['author_id'] . "', sort_order = '" . (int) $data['sort_order'] . "', date_modified = NOW() WHERE news_id = '" . (int) $news_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "news SET image = '" . $this->db->escape($data['image']) . "' WHERE news_id = '" . (int) $news_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int) $news_id . "'");

        foreach ($data['news_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "news_description SET news_id = '" . (int) $news_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "'");
            /*if($this->db->escape($value['name']) != "") {
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_description SET news_id = '" . (int) $news_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "'");
            }*/
            
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "news_attribute WHERE news_id = '" . (int) $news_id . "'");

        if (!empty($data['news_attribute'])) {
            foreach ($data['news_attribute'] as $news_attribute) {
                if ($news_attribute['attribute_id']) {
                    // Removes duplicates
                    $this->db->query("DELETE FROM " . DB_PREFIX . "news_attribute WHERE news_id = '" . (int) $news_id . "' AND attribute_id = '" . (int) $news_attribute['attribute_id'] . "'");

                    foreach ($news_attribute['news_attribute_description'] as $language_id => $news_attribute_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "news_attribute SET news_id = '" . (int) $news_id . "', attribute_id = '" . (int) $news_attribute['attribute_id'] . "', language_id = '" . (int) $language_id . "', text = '" . $this->db->escape($news_attribute_description['text']) . "'");
                    }
                }
            }
        }




        $this->db->query("DELETE FROM " . DB_PREFIX . "news_image WHERE news_id = '" . (int) $news_id . "'");

        if (isset($data['news_image'])) {
            foreach ($data['news_image'] as $news_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_image SET news_id = '" . (int) $news_id . "', image = '" . $this->db->escape($news_image['image']) . "', sort_order = '" . (int) $news_image['sort_order'] . "'");
            }
        }

        

        $this->db->query("DELETE FROM " . DB_PREFIX . "news_to_newscategory WHERE news_id = '" . (int) $news_id . "'");

        if (isset($data['news_category'])) {
            foreach ($data['news_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_to_newscategory SET news_id = '" . (int) $news_id . "', category_id = '" . (int) $category_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "news_to_photographer WHERE news_id = '" . (int) $news_id . "'");

        if (isset($data['news_photographer'])) {
            foreach ($data['news_photographer'] as $photographer_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_to_photographer SET news_id = '" . (int) $news_id . "', photographer_id = '" . (int) $photographer_id . "'");
            }
        }

       
        $this->db->query("DELETE FROM " . DB_PREFIX . "news_related WHERE news_id = '" . (int) $news_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "news_related WHERE related_id = '" . (int) $news_id . "'");

        if (isset($data['news_related'])) {
            foreach ($data['news_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "news_related WHERE news_id = '" . (int) $news_id . "' AND related_id = '" . (int) $related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_related SET news_id = '" . (int) $news_id . "', related_id = '" . (int) $related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "news_related WHERE news_id = '" . (int) $related_id . "' AND related_id = '" . (int) $news_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "news_related SET news_id = '" . (int) $related_id . "', related_id = '" . (int) $news_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'news_id=" . (int) $news_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_id=" . (int) $news_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }



        $this->cache->delete('news');
    }

    public function copyPost($news_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "news p WHERE p.news_id = '" . (int) $news_id . "'");

        if ($query->num_rows) {
            $data = $query->row;

            $data['viewed'] = '0';
            $data['keyword'] = '';
            $data['status'] = '0';

            $data['news_attribute'] = $this->getNewsAttributes($news_id);
            $data['news_description'] = $this->getNewsDescriptions($news_id);
            $data['news_image'] = $this->getNewsImages($news_id);
            $data['news_related'] = $this->getNewsRelated($news_id);
            $data['news_category'] = $this->getNewsCategories($news_id);
            $data['news_photographer'] = $this->getNewsPhotographers($news_id);

            $this->addPost($data);
        }
    }

    public function deletePost($news_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "news WHERE news_id = '" . (int) $news_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "news_attribute WHERE news_id = '" . (int) $news_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int) $news_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "news_image WHERE news_id = '" . (int) $news_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "news_related WHERE news_id = '" . (int) $news_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "news_related WHERE related_id = '" . (int) $news_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "news_to_newscategory WHERE news_id = '" . (int) $news_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "news_to_photographer WHERE news_id = '" . (int) $news_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'news_id=" . (int) $news_id . "'");

        $this->cache->delete('news');
    }

    public function getPost($news_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'news_id=" . (int) $news_id . "') AS keyword FROM " . DB_PREFIX . "news p LEFT JOIN " . DB_PREFIX . "news_description pd ON (p.news_id = pd.news_id) WHERE p.news_id = '" . (int) $news_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getPosts($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "news p LEFT JOIN " . DB_PREFIX . "news_description pd ON (p.news_id = pd.news_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_image']) && !is_null($data['filter_image'])) {
            if ($data['filter_image'] == 1) {
                $sql .= " AND (p.image IS NOT NULL AND p.image <> '' AND p.image <> 'no_image.png')";
            } else {
                $sql .= " AND (p.image IS NULL OR p.image = '' OR p.image = 'no_image.png')";
            }
        }

        $sql .= " GROUP BY p.news_id";

        $sort_data = array(
            'pd.name',
            'p.status',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getPostsByCategoryId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news p LEFT JOIN " . DB_PREFIX . "news_description pd ON (p.news_id = pd.news_id) LEFT JOIN " . DB_PREFIX . "news_to_photographer p2c ON (p.news_id = p2c.news_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int) $category_id . "' ORDER BY pd.name ASC");

        return $query->rows;
    }

    public function getNewsDescriptions($news_id) {
        $news_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int) $news_id . "'");

        foreach ($query->rows as $result) {
            $news_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'tag' => $result['tag']
            );
        }

        return $news_description_data;
    }

    public function getNewsCategories($news_id) {
        $news_category_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_to_newscategory WHERE news_id = '" . (int) $news_id . "'");

        foreach ($query->rows as $result) {
            $news_category_data[] = $result['category_id'];
        }

        return $news_category_data;
    }

    public function getNewsPhotographers($news_id) {
        $news_photographer_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_to_photographer WHERE news_id = '" . (int) $news_id . "'");

        foreach ($query->rows as $result) {
            $news_photographer_data[] = $result['photographer_id'];
        }

        return $news_photographer_data;
    }


    public function getNewsAttributes($news_id) {
        $news_attribute_data = array();

        $news_attribute_query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "news_attribute WHERE news_id = '" . (int) $news_id . "' GROUP BY attribute_id");

        foreach ($news_attribute_query->rows as $news_attribute) {
            $news_attribute_description_data = array();

            $news_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_attribute WHERE news_id = '" . (int) $news_id . "' AND attribute_id = '" . (int) $news_attribute['attribute_id'] . "'");

            foreach ($news_attribute_description_query->rows as $news_attribute_description) {
                $news_attribute_description_data[$news_attribute_description['language_id']] = array('text' => $news_attribute_description['text']);
            }

            $news_attribute_data[] = array(
                'attribute_id' => $news_attribute['attribute_id'],
                'news_attribute_description' => $news_attribute_description_data
            );
        }

        return $news_attribute_data;
    }

    public function getNewsImages($news_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_image WHERE news_id = '" . (int) $news_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    
    public function getNewsRelated($news_id) {
        $news_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_related WHERE news_id = '" . (int) $news_id . "'");

        foreach ($query->rows as $result) {
            $news_related_data[] = $result['related_id'];
        }

        return $news_related_data;
    }

    public function getTotalPosts($data = array()) {
        $sql = "SELECT COUNT(DISTINCT p.news_id) AS total FROM " . DB_PREFIX . "news p LEFT JOIN " . DB_PREFIX . "news_description pd ON (p.news_id = pd.news_id)";

        $sql .= " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

        
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

   
    public function getTotalPostsByAttributeId($attribute_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_attribute WHERE attribute_id = '" . (int) $attribute_id . "'");

        return $query->row['total'];
    }
    public function getTotalNewsByAuthorId($author_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news WHERE author_id = '" . (int) $author_id . "'");

        return $query->row['total'];
    }

}
