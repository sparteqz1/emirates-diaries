<?php

class ModelProjectCategory extends Model {

    public function addCategory($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "project_category SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int) $data['sort_order'] . "', date_modified = NOW(), date_added = NOW()");

        $project_category_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "project_category SET image = '" . $this->db->escape($data['image']) . "' WHERE project_category_id = '" . (int) $project_category_id . "'");
        }

       

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'project_category_id=" . (int) $project_category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        return $project_category_id;
    }

    public function editCategory($project_category_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "project_category SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int) $data['sort_order'] . "', date_modified = NOW() WHERE project_category_id = '" . (int) $project_category_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "project_category SET image = '" . $this->db->escape($data['image']) . "' WHERE project_category_id = '" . (int) $project_category_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'project_category_id=" . (int) $project_category_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'project_category_id=" . (int) $project_category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

    }

    public function deleteCategory($project_category_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "project_category WHERE project_category_id = '" . (int) $project_category_id . "'");
       
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'project_category_id=" . (int) $project_category_id . "'");

        
    }

    public function getCategory($project_category_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'project_category_id=" . (int) $project_category_id . "') AS keyword FROM " . DB_PREFIX . "project_category WHERE project_category_id = '" . (int) $project_category_id . "'");

        return $query->row;
    }

    public function getCategories($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "project_category";

        if (!empty($data['filter_name'])) {
            $sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    

    public function getTotalCategories() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "project_category");

        return $query->row['total'];
    }
    
    public function getTotalProjectsByCategoryId($project_category_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "projects WHERE project_category_id = '" . (int) $project_category_id . "'");

        return $query->row['total'];
    }

}
