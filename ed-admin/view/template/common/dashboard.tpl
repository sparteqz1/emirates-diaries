<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_install) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_install; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php foreach ($rows as $row) { ?>
    <div class="row">
      <?php foreach ($row as $dashboard_1) { ?>
      <?php $class = 'col-lg-' . $dashboard_1['width'] . ' col-md-3 col-sm-6'; ?>
      <?php foreach ($row as $dashboard_2) { ?>
      <?php if ($dashboard_2['width'] > 3) { ?>
      <?php $class = 'col-lg-' . $dashboard_1['width'] . ' col-md-12 col-sm-12'; ?>
      <?php } ?>
      <?php } ?>
      <div class="<?php echo $class; ?>"><?php echo $dashboard_1['output']; ?></div>
      <?php } ?>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-lg-4 col-md-12 col-sm-12">
        <div class="tile">
          <div class="tile-heading">Trustees</div>
          <div class="tile-body"><i class="fa fa-black-tie"></i>
            <h2 class="pull-right"><?php echo $trustees_total; ?></h2>
          </div>
          <div class="tile-footer"><a href="<?php echo $trustees_link; ?>">View more...</a></div>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12">
        <div class="tile">
          <div class="tile-heading">Gallery</div>
          <div class="tile-body"><i class="fa fa-picture-o"></i>
            <h2 class="pull-right"><?php echo $gallery_total; ?></h2>
          </div>
          <div class="tile-footer"><a href="<?php echo $gallery_link; ?>">View more...</a></div>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12">
        <div class="tile">
          <div class="tile-heading">Subscribers</div>
          <div class="tile-body"><i class="fa fa-envelope"></i>
            <h2 class="pull-right"><?php echo $subscriber_total; ?></h2>
          </div>
          <div class="tile-footer"><a href="<?php echo $subscriber_link; ?>">View more...</a></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-book"></i> Articles</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="tile">
                  <div class="tile-heading">Articles </div>
                  <div class="tile-body"><i class="fa fa-book"></i>
                    <h2 class="pull-right"><?php echo $news_total; ?></h2>
                  </div>
                  <div class="tile-footer"><a href="<?php echo $news_link; ?>">View more...</a></div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="tile">
                  <div class="tile-heading">Issues </div>
                  <div class="tile-body"><i class="fa fa-question-circle"></i>
                    <h2 class="pull-right"><?php echo $issues_total; ?></h2>
                  </div>
                  <div class="tile-footer"><a href="<?php echo $issues_link; ?>">View more...</a></div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="tile">
                  <div class="tile-heading">Categories </div>
                  <div class="tile-body"><i class="fa fa-bars"></i>
                    <h2 class="pull-right"><?php echo $category_total; ?></h2>
                  </div>
                  <div class="tile-footer"><a href="<?php echo $category_link; ?>">View more...</a></div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="tile">
                  <div class="tile-heading">Authors </div>
                  <div class="tile-body"><i class="fa fa-user"></i>
                    <h2 class="pull-right"><?php echo $author_total; ?></h2>
                  </div>
                  <div class="tile-footer"><a href="<?php echo $author_link; ?>">View more...</a></div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="tile">
                  <div class="tile-heading">Featured </div>
                  <div class="tile-body"><i class="fa fa-bookmark"></i>
                    <h2 class="pull-right"><?php echo count($featured_total); ?></h2>
                  </div>
                  <div class="tile-footer"><a href="<?php echo $featured_link; ?>">View more...</a></div>
                </div>
              </div>                            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>