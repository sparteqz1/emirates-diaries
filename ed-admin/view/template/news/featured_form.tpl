<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-featured" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-featured" class="form-horizontal">

                    <div class="tab-content">                      

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                            <div class="col-sm-10">
                                <input type="text" name="news_featured" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                                <div id="news-related" class="well well-sm" style="height: 150px; overflow: auto;">
                                    <?php foreach ($news_featureds as $news_featured) { ?>
                                    <div id="news-related<?php echo $news_featured['news_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $news_featured['name']; ?>
                                        <input type="hidden" name="news_featured[]" value="<?php echo $news_featured['news_id']; ?>" />
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
    <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
    <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
    <script type="text/javascript"><!--

        // Related
        $('input[name=\'news_featured\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=news/news/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['news_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'related\']').val('');

                $('#news-related' + item['value']).remove();

                $('#news-related').append('<div id="news-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="news_featured[]" value="' + item['value'] + '" /></div>');
            }
        });

        $('#news-related').delegate('.fa-minus-circle', 'click', function () {
            $(this).parent().remove();
        });
        //--></script>




    <script type="text/javascript"><!--
  $('#language a:first').tab('show');
        //--></script></div>
<?php echo $footer; ?>
