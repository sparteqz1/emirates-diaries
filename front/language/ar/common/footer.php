<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Text
$_['text_information']  	= 'معلومات';
$_['text_contact']      	= 'اتصل بنا';
$_['text_newsletter']   	= 'القائمة البريدية';
$_['text_desc']    	        = 'مرحباً بكم في مذكرات الإمارات! نحن فريق إماراتي من محررين، وكُتّاب، ومصممين، ومصورين. معاً يداً بيد استطعنا أن نوصل رسالتنا بما يتناسب مع مسار دعم المواهب الإمارتية والتي تقودنا إلى عرض انجازاتهم اللامحدودة إلى جانب تقديم الكُتّاب الإماراتيين فرصة لعرض أعمالهم على أكبر شريحة من الجمهور. ';

// Menu
$_['menu_media_kit']   		= 'الأدوات الإعلامية ';
$_['menu_about_us']   		= 'عن المجلة ';
$_['menu_contact_us']   	= 'تواصل معنا';
$_['menu_career']   		= 'تطوع معنا';
$_['menu_privacy_policy']   = 'سياسة الخصوصية ';
$_['menu_advertise_us']   	= ' أعلن معنا';
$_['text_copy_rights']   	= 'جميع الحقوق محفوظة لمجلة مذكرات الإمارات ';

$_['text_follow_us_on']  	= 'تابعنا على';
$_['text_instagram']      	= 'انستغرام';
$_['text_follow_us']      	= 'تابعنا ';
$_['text_subscribe']      	= 'اشترك';
$_['text_subscribe_desc']  	= 'اشترك الآن واحصل على نسخة مجانية من العدد الجديد';
$_['text_follow_us_desc']  	= 'تابعونا @emiratesdiaries لتعرفوا جديدنا';
$_['text_call_us']      	= 'اتصل بنا الآن';
$_['text_send_us']      	= 'تواصل معنا';
$_['text_office_tele']      = 'رقم الهاتف';
$_['text_reach_us']      	= 'تواصل معنا ';
$_['text_email']      		= 'البريد الإلكتروني';
$_['text_magazine']      	= 'المجلة';
$_['text_get_the']      	= 'احصل على';