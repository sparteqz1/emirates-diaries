<?php
// Heading
$_['heading_title']  			= 'Contact Us';

// Text
$_['text_location']  			= 'Our Location';
$_['text_store']     			= 'Our Stores';
$_['text_contact']   			= 'Contact Form';
$_['text_address']   			= 'Address';
$_['text_telephone'] 			= 'Telephone';
$_['text_fax']       			= 'Fax';
$_['text_open']      			= 'Opening Times';
$_['text_comment']   			= 'Comments';
$_['text_success']   			= '<p>Your enquiry has been successfully sent to the store owner!</p>';

// Entry
$_['entry_name']     			= 'Your Name';
$_['entry_email']    			= 'E-Mail Address';
$_['entry_enquiry']  			= 'Enquiry';

// Email
$_['email_subject']  			= 'Enquiry %s';

// Errors
$_['error_email']    			= '<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>E-Mail Address does not appear to be valid!';
$_['error_issue']  				= '<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>Select Issue!';
$_['error_null_email']  		= '<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>Enter Your Email';
$_['error_mobile']      		= '<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>Enter Phone Number';
$_['error_digits']      		= '<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>Only Numbers Allowed - Phone';
$_['error_name']        		= '<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>Enter Your Name';
$_['error_letters']     		= '<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>Only Letters Allowed - Name';

$_['success_subscription']		= "Your are successfully subscribed to our newsletter services!!";
$_['success_subscribtion'] 		= "Your are successfully subscribed!!";
$_['error_exist'] 				= "Email already exists!!!";

$_['text_reach_us']  			= 'تواصل معنا ';
$_['text_get_in_touch']  		= 'تواصل معنا';
$_['text_write_story']  		= 'تود كتابة مقالة عن مذكرات الإمارات؟';
$_['text_general_enquiries']	= 'للإستفسارات العامة';
$_['text_advertise']  			= 'اعلن مع مجلة مذكرات الإمارات';
$_['text_want_write']  			= 'هل تود أن نكتب عنك؟';
$_['text_contact_banner']  		= 'تواصل معنا';