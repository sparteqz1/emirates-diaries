<?php
// Heading
$_['heading_title']     = ' Authors';

// Text
$_['text_brand']        = 'Author';
$_['text_index']        = 'Author Index:';
$_['text_error']        = 'Author not found!';
$_['text_empty']        = 'There are no news to list.';
$_['text_author']       = 'Author:';
$_['text_sort']         = 'Sort By:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_limit']        = 'Show:';