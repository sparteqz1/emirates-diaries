<?php
// Heading
$_['heading_title']     = 'Search';
$_['heading_tag']		= 'Tag - ';

// Text
$_['text_search']       = 'News meeting the search criteria';
$_['text_keyword']      = 'Keywords';
$_['text_sort']         = 'Sort By:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_limit']        = 'Show:';

// Entry
$_['entry_search']      = 'Search Criteria';
$_['text_empty']        = 'No news found!';