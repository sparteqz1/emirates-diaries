<?php
// header
$_['heading_title']  = 'Verify your email';

// Text
$_['text_account']   = 'Account';
$_['text_success']   = 'Success: Your email has been successfully verified.';

$_['error_code']     = 'Verification code is invalid or was used previously!';