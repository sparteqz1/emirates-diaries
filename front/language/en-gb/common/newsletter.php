<?php


// Text
$_['text_subscribe']       			= 'Subscribe Now.';
$_['text_newsletter']      			= 'Newsletter';
$_['text_signup']          			= 'Sign up for our newsletter and stay in the know.';
$_['text_newsletter_placeholder']	= 'Your Email Address';