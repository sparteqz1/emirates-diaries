<?php

class ModelGalleryGallery extends Model {
    
    public function getGallerys() {
        $query = $this->db->query("SELECT gd.name, gd.description, a.name AS author, g.issue_id, g.date_added, g.image, g.gallery_id FROM " . DB_PREFIX . "gallery_description gd "
                . "LEFT JOIN " . DB_PREFIX . "gallery g ON (gd.gallery_id = g.gallery_id) "
                . "LEFT JOIN " . DB_PREFIX . "author a ON (g.author_id = a.author_id) "
                . "WHERE gd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND g.status = '1' ORDER BY g.date_added DESC");

        return $query->rows;
    }
    
     public function getGalleryImages($gallery_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "gallery_image WHERE gallery_id = '" . (int) $gallery_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }
    
    public function getIssue($issue_id) {
        $query = $this->db->query("SELECT ni.status, nid.name AS issueName FROM " . DB_PREFIX . "newsissue_description nid "
                . "LEFT JOIN " . DB_PREFIX . "newsissue ni ON (nid.issue_id = ni.issue_id) "
                . "LEFT JOIN " . DB_PREFIX . "gallery g ON (g.issue_id = ni.issue_id) "
                . "WHERE g.gallery_id = '" . (int) $issue_id . "' AND ni.status = '1'");

        return $query->rows;
    }
    
}