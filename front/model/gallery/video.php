<?php

class ModelGalleryVideo extends Model {
    
    public function getGallerys() {
        $query = $this->db->query("SELECT gd.name, gd.description, a.name AS author, g.video, g.issue_id, g.date_added, g.video_id FROM " . DB_PREFIX . "videogallery_description gd "
                . "LEFT JOIN " . DB_PREFIX . "videogallery g ON (gd.video_id = g.video_id) "
                . "LEFT JOIN " . DB_PREFIX . "author a ON (g.author_id = a.author_id) "
                . "WHERE gd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND g.status = '1' ORDER BY g.sort_order DESC");

        return $query->rows;
    }
    
    public function getIssue($issue_id) {
        $query = $this->db->query("SELECT ni.status, nid.name AS issueName FROM " . DB_PREFIX . "newsissue_description nid "
                . "LEFT JOIN " . DB_PREFIX . "newsissue ni ON (nid.issue_id = ni.issue_id) "
                . "LEFT JOIN " . DB_PREFIX . "videogallery g ON (g.issue_id = ni.issue_id) "
                . "WHERE g.video_id = '" . (int) $issue_id . "' AND ni.status = '1'");

        return $query->rows;
    }
    
}