<?php

class ModelInformationAbout extends Model {
    
    public function getTrustees() {
        $query = $this->db->query("SELECT td.name, td.description, t.image, t.trustees_id FROM " . DB_PREFIX . "trustees_description td "
                . "LEFT JOIN " . DB_PREFIX . "trustees t ON (td.trustees_id = t.trustees_id) WHERE td.language_id = '" . (int) $this->config->get('config_language_id') . "' AND t.status = '1' ORDER BY t.sort_order ASC");

        return $query->rows;
    }
    
}