<?php

class ModelContactContact extends Model {

    public function addContact($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "contact SET name = '" . $this->db->escape($data['fname']) . "', lastname = '" . $this->db->escape($data['lname']) . "', type = 'contact', email = '" . $this->db->escape($data['email']) . "', subject = '" . $this->db->escape($data['subject']) . "', contact_text = '" . $this->db->escape($data['message']) . "', date_added = NOW()");

        $contact_id = $this->db->getLastId();

        return $contact_id;
    }

    public function addEnquiry($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "contact SET name = '" . $this->db->escape($data['fname']) . "', lastname = '" . $this->db->escape($data['lname']) . "', type = 'enquiry', email = '" . $this->db->escape($data['email']) . "', subject = '" . $this->db->escape($data['subject']) . "', contact_text = '" . $this->db->escape($data['message']) . "', date_added = NOW()");

        $contact_id = $this->db->getLastId();

        return $contact_id;
    }

    public function addSubscription($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "subscription SET email = '" . $this->db->escape($data['subscribemail']) . "', date_added = NOW()");

        $subscribe_id = $this->db->getLastId();

        return $subscribe_id;
    }

    public function countSubscription($data) {
        $sql = "SELECT COUNT(email) AS total FROM " . DB_PREFIX . "subscription WHERE email = '" . $this->db->escape($data['subscribemail']) . "'";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

     public function addMagazineSubscription($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "magazine_subscription SET name = '" . $this->db->escape($data['sbe_name']) . "', email = '" . $this->db->escape($data['sbe_email']) . "', phone = '" . $this->db->escape($data['sbe_phone']) . "', issue = '" . $this->db->escape($data['issue']) . "', date_added = NOW()");

        $subscribe_id = $this->db->getLastId();

        return $subscribe_id;
    }

}
