<?php

class ModelAccountLoginSocial extends Model {

    public function getCustomerByIdentifier($provider, $identifier) {
        $result = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customer_authentication WHERE provider = '" . $this->db->escape($provider) . "' AND identifier = MD5('" . $this->db->escape($identifier) . "') LIMIT 1");

        if ($result->num_rows) {
            return (int) $result->row['customer_id'];
        } else {
            return false;
        }
    }

    public function getCustomerByEmail($email) {
        $result = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' LIMIT 1");

        if ($result->num_rows) {
            return (int) $result->row['customer_id'];
        } else {
            return false;
        }
    }

    public function login($customer_id) {

        $result = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int) $customer_id . "' LIMIT 1");

        if (!$result->num_rows) {
            return false;
        }

        $this->session->data['customer_id'] = $result->row['customer_id'];

        $this->db->query("UPDATE " . DB_PREFIX . "customer SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int) $result->row['customer_id'] . "'");

        return true;
    }

    public function addAuthentication($data) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_authentication SET " .
                "customer_id = '" . (int) $data['customer_id'] . "', " .
                "provider = '" . $this->db->escape($data['provider']) . "', " .
                "identifier = MD5('" . $this->db->escape($data['identifier']) . "'), " .
                "web_site_url = '" . $this->db->escape($data['web_site_url']) . "', " .
                "profile_url = '" . $this->db->escape($data['profile_url']) . "', " .
                "photo_url = '" . $this->db->escape($data['photo_url']) . "', " .
                "display_name = '" . $this->db->escape($data['display_name']) . "', " .
                "description = '" . $this->db->escape($data['description']) . "', " .
                "first_name = '" . $this->db->escape($data['first_name']) . "', " .
                "last_name = '" . $this->db->escape($data['last_name']) . "', " .
                "gender = '" . $this->db->escape($data['gender']) . "', " .
                "language = '" . $this->db->escape($data['language']) . "', " .
                "age = '" . $this->db->escape($data['age']) . "', " .
                "birth_day = '" . $this->db->escape($data['birth_day']) . "', " .
                "birth_month = '" . $this->db->escape($data['birth_month']) . "', " .
                "birth_year = '" . $this->db->escape($data['birth_year']) . "', " .
                "email = '" . $this->db->escape($data['email']) . "', " .
                "email_verified = '" . $this->db->escape($data['email_verified']) . "', " .
                "phone = '" . $this->db->escape($data['phone']) . "', " .
                "address = '" . $this->db->escape($data['address']) . "', " .
                "country = '" . $this->db->escape($data['country']) . "', " .
                "region = '" . $this->db->escape($data['region']) . "', " .
                "city = '" . $this->db->escape($data['city']) . "', " .
                "zip = '" . $this->db->escape($data['zip']) . "', " .
                "date_added = NOW()");
    }

    public function addCustomer($data) {

        $this->load->model('account/customer_group');

        $customer_group_id = $this->config->get('config_customer_group_id');
        $customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);


        $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int) $customer_group_id . "', language_id = '" . (int) $this->config->get('config_language_id') . "',  firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "',  salt = '', password = '', newsletter = '" . (isset($data['newsletter']) ? (int) $data['newsletter'] : 0) . "',  social_image = '" . $this->db->escape($data['social_image']) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1',approved = '" . (int) !$customer_group_info['approval'] . "', date_added = NOW()");
        $customer_id = $this->db->getLastId();

        $this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int) $customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', telephone = '" . $this->db->escape($data['telephone']) . "',   address_1 = '" . $this->db->escape($data['address_1']) . "',  city = '" . $this->db->escape($data['city']) . "'");
        $address_id = $this->db->getLastId();

        $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int) $address_id . "' WHERE customer_id = '" . (int) $customer_id . "'");

        $namekey = substr($this->db->escape($data['firstname']), 0, 4);
        $reference_code = 'SP' . $namekey . '0' . $customer_id;
        $reference_code = preg_replace('/\s+/', '', $reference_code);
        $this->db->query("UPDATE " . DB_PREFIX . "customer SET uniqueid = '" . $reference_code . "' WHERE customer_id = '" . (int) $customer_id . "'");



        $this->load->language('mail/customer');

        $subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

        $message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

        if (!$customer_group_info['approval']) {
            $message .= $this->language->get('text_login') . "\n";
        } else {
            $message .= $this->language->get('text_approval') . "\n";
        }

        $message .= $this->url->link('account/login', '', true) . "\n\n";
        $message .= $this->language->get('text_services') . "\n\n";
        $message .= $this->language->get('text_thanks') . "\n";
        $message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');


        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject($subject);
        $mail->setText($message);
        $mail->send();





        if ($data['email']) {
            $mail->setTo($data['email']);
            $mail->send();
        }

        
        
         // Send to main admin email if new account email is enabled
        if (in_array('account', (array) $this->config->get('config_mail_alert'))) {
            $message = $this->language->get('text_signup') . "\n\n";
            $message .= $this->language->get('text_website') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
            $message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
            $message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
            $message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
            $message .= $this->language->get('text_email') . ' ' . $data['email'] . "\n";
            $message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($this->config->get('config_email'));
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
            $mail->setText($message);
            $mail->send();

            // Send to additional alert emails if new account email is enabled
            $emails = explode(',', $this->config->get('config_alert_email'));

            foreach ($emails as $email) {
                if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $mail->setTo($email);
                    $mail->send();
                }
            }
        }

        return $customer_id;
    }

    public function getCountryIdByName($country) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE LOWER(name) LIKE '" . $this->db->escape(utf8_strtolower($country)) . "' OR iso_code_2 LIKE '" . $this->db->escape($country) . "' OR iso_code_3 LIKE '" . $this->db->escape($country) . "' LIMIT 1");

        if ($query->num_rows) {
            return $query->row['country_id'];
        } else {
            return false;
        }
    }

    public function getZoneIdByName($zone) {
        $query = $this->db->query("SELECT zone_id FROM " . DB_PREFIX . "zone WHERE LOWER(name) LIKE '" . $this->db->escape(utf8_strtolower($zone)) . "' OR code LIKE '" . $this->db->escape($zone) . "' LIMIT 1");

        if ($query->num_rows) {
            return $query->row['zone_id'];
        } else {
            return false;
        }
    }

}
