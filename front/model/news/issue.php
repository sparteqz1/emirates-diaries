<?php

class ModelNewsIssue extends Model {

    
    public function getIssue($issue_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "newsissue ni LEFT JOIN " . DB_PREFIX . "newsissue_description nid ON (ni.issue_id = nid.issue_id)  WHERE ni.issue_id = '" . (int) $issue_id . "' AND nid.language_id = '" . (int) $this->config->get('config_language_id') . "'  AND ni.status = '1'");

        return $query->row;
    }

    public function getIssues() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "newsissue ni LEFT JOIN " . DB_PREFIX . "newsissue_description nid ON (ni.issue_id = nid.issue_id) WHERE nid.language_id = '" . (int) $this->config->get('config_language_id') . "'   AND ni.status = '1' ORDER BY ni.sort_order ASC");

        return $query->rows;
    }

}
