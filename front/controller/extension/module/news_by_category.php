<?php

class ControllerExtensionModuleNewsByCategory extends Controller {

    public function index($setting) {
        $this->load->language('module/news_by_category');

        $data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('news/category');

        $this->load->model('news/news');

        $this->load->model('tool/image');

        $data['categories'] = array();

        if (!$setting['limit']) {
            $setting['limit'] = 4;
        }

        if (!empty($setting['category'])) {
            $categories = array_slice($setting['category'], 0, (int) $setting['limit']);

            $index = 0;
            foreach ($categories as $category_id) {
                $category_info = $this->model_news_category->getCategory($category_id);

                if ($category_info) {

                    $index = $index + 1;
                    $news = array();

                    $filter_data = array(
                        'filter_category_id' => $category_id,
                        'filter_sub_category' => true,
                        'start' => 0,
                        'limit' => $setting['limit']
                    );


                    $results = $this->model_news_news->getAllNews($filter_data);


                    foreach ($results as $result) {
                        if ($result) {
                            if ($result['image']) {
                                $image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
                            } else {
                                $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                            }


                            $news[] = array(
                                'news_id' => $result['news_id'],
                                'thumb' => $image,
                                'name' => $result['name'],
                                'date' => $result['ago'],
                                'author' => $result['author'],
                                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_news_description_length')) . '..',
                                'href' => $this->url->link('news/news/details', 'news_id=' . $result['news_id'])
                            );
                        }
                    }
                    $class = "";
                    $fade = "";
                    if ($index == 1) {
                        $class = "active";
                        $fade = "active in";
                    }
                    $data['categories'][] = array(
                        'category_id' => $category_info['category_id'],
                        'news' => $news,
                        'class' => $class,
                        'fade' => $fade,
                        'name' => $category_info['name'],
                        'href' => $this->url->link('news/news', 'news_path=' . $category_info['category_id'])
                    );
                }
            }
        }
        if ($data['categories']) {
            return $this->load->view('extension/module/news_by_category', $data);
        }
    }

}
