<?php

class ControllerGalleryVideo extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('gallery/video');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css');

        $data['heading_title'] = $this->language->get('heading_title');
        $data['banner_title'] = $this->language->get('banner_title');
        $data['text_goto'] = $this->language->get('text_goto');
        $data['text_featured'] = $this->language->get('text_featured');

        $this->load->model('tool/image');

        if ($this->config->get('config_image')) {
            $data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
        } else {
            $data['image'] = false;
        }

        // Photo Library
        $data['galleries'] = array();
        $this->load->model('gallery/video');
        $this->load->model('news/issue');
        $this->load->model('tool/image');
        $all_galleries = $this->model_gallery_video->getGallerys();

        foreach ($all_galleries as $gallery_data) {
            
            $issueresults = $this->model_news_issue->getIssue($gallery_data['issue_id']);

            $data['galleries'][] = array(
                'video_id' => $gallery_data['video_id'],
                'issue' => $issueresults['name'],
                'author' => $gallery_data['author'],
                'name' => $gallery_data['name'],
                'video' => $gallery_data['video'],
                'day' => date("F Y", strtotime($gallery_data['date_added'])),
                'description' => strip_tags(html_entity_decode($gallery_data['description'], ENT_QUOTES, 'UTF-8'))
            );
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $data['galry'] = $this->url->link('gallery/video');

        $this->response->setOutput($this->load->view('gallery/video', $data));
    }

}
