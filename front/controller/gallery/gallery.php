<?php

class ControllerGalleryGallery extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('gallery/gallery');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');
        $data['banner_title'] = $this->language->get('banner_title');
        $data['text_goto'] = $this->language->get('text_goto');
        $data['text_featured'] = $this->language->get('text_featured');

        $this->load->model('tool/image');

        if ($this->config->get('config_image')) {
            $data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
        } else {
            $data['image'] = false;
        }

        // Photo Library
        $data['galleries'] = array();
        $this->load->model('gallery/gallery');
        $this->load->model('news/issue');
        $this->load->model('tool/image');
        $all_galleries = $this->model_gallery_gallery->getGallerys();

        foreach ($all_galleries as $gallery_data) {
            
            $issueresults = $this->model_news_issue->getIssue($gallery_data['issue_id']);
            /*$issue = array();
            foreach ($issueresults as $issueresult) {
                if ($issueresult) {                    

                    $issue[] = array(
                        'issue' => $issueresult['issueName']
                    );
                }
            }*/

            $imageresults = $this->model_gallery_gallery->getGalleryImages($gallery_data['gallery_id']);
            $images = array();
            foreach ($imageresults as $imageresult) {
                if ($imageresult) {
                    if ($imageresult['image']) {
                        $popup = HTTP_SERVER . 'image/' . $imageresult['image'];
                    } else {
                        $popup = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
                    }

                    $images[] = array(
                        'popup' => $popup
                    );
                }
            }

            if ($gallery_data['image']) {
                $image = $this->model_tool_image->resize($gallery_data['image'], 900, 600);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 900, 600);
            }

            $data['galleries'][] = array(
                'gallery_id' => $gallery_data['gallery_id'],
                'image' => $image,
                'popup' => $images,
                'issue' => $issueresults['name'],
                'author' => $gallery_data['author'],
                'name' => $gallery_data['name'],
                'day' => date("F Y", strtotime($gallery_data['date_added'])),
                'description' => strip_tags(html_entity_decode($gallery_data['description'], ENT_QUOTES, 'UTF-8'))
            );
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $data['galry'] = $this->url->link('gallery/gallery');

        $this->response->setOutput($this->load->view('gallery/gallery', $data));
    }

}
