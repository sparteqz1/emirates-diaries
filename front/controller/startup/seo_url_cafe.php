<?php

class ControllerStartupSeoUrl extends Controller {

    private $url_list = array(
        'common/home' => '',
        'account/wishlist' => 'wishlist',
        'account/account' => 'account',
        'account/edit' => 'account/edit',
        'account/password' => 'account/password',
        'account/address' => 'account/address',
        'account/address/edit' => 'account/address/edit',
        'account/reward' => 'account/reward',
        'account/login' => 'account/login',
        'account/logout' => 'account/logout',
        'account/order' => 'history',
        'account/order/info' => 'account/order/details',
        'account/newsletter' => 'email-settings',
        'account/forgotten' => 'account/recover',
        'account/download' => 'account/downloads',
        'account/return' => 'account/returns',
        'account/return/add' => 'returns',
        'account/transaction' => 'account/transaction',
        'account/register' => 'account/register',
        'account/recurring' => 'account/recurring',
        'account/voucher' => 'gifts',
        'account/voucher/redeem' => 'redeem',
        'account/voucher/mygifts' => 'my-gifts',
        'account/voucher/details' => 'my-gifts/details',
        'account/voucher/download' => 'my-gifts/download',
        'affiliate/account' => 'affiliate',
        'affiliate/edit' => 'affiliate/edit',
        'affiliate/password' => 'affiliate/password',
        'affiliate/payment' => 'affiliate/payment',
        'affiliate/tracking' => 'affiliate/tracking',
        'affiliate/transaction' => 'affiliate/transaction',
        'affiliate/logout' => 'affiliate/logout',
        'affiliate/forgotten' => 'affiliate/recover',
        'affiliate/register' => 'affiliate/register',
        'affiliate/login' => 'affiliate/login',
        'checkout/cart' => 'cart',
        'checkout/checkout' => 'checkout',
        'checkout/voucher' => 'vouchers',
        'checkout/success' => 'success',
        'information/contact' => 'contact',
        'information/sitemap' => 'sitemap',
        'product/special' => 'specials',
        'product/manufacturer' => 'brands',
        'product/compare' => 'compare',
        'product/search' => 'search',
        'product/search' => 'search',
        'coffee/coffee' => 'coffee',
        'coffee/story' => 'coffee/story',
        'coffee/roastery' => 'coffee/roastery',
        'coffee/menu' => 'coffee/menu',
        'culture/culture' => 'culture',
        'culture/story' => 'culture/story',
        'culture/gallery' => 'culture/gallery',
        'culture/blog/details/' => 'blog',
        'culture/blog/details' => 'blog',
        'culture/event' => 'event',
        'machine/machine' => 'machine',
        'machine/machine/index2' => 'machine2',
        'machine/story' => 'machine/story',
        'machine/bikes' => 'machine/bikes',
        'machine/bikes/details/' => 'machine/bikes/details/',
        'machine/services' => 'machine/services',
        'collection/collection' => 'collections',
        'post/classifieds' => 'post/classifieds',
        'rental/rental' => 'rental'
    );

    public function index() {
        // Add rewrite to url class
        if ($this->config->get('config_seo_url')) {
            $this->url->addRewrite($this);
        }

        // Decode URL
        if (isset($this->request->get['_route_'])) {
            $parts = explode('/', $this->request->get['_route_']);



            // remove any empty arrays from trailing
            if (utf8_strlen(end($parts)) == 0) {
                array_pop($parts);
            }

            foreach ($parts as $part) {

                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "'");


                if ($query->num_rows) {
                    $url = explode('=', $query->row['query']);

                    if ($url[0] == 'product_id' && $parts[0] == "machine-bikes") {

                        $this->request->get['bike_id'] = $url[1];
                    }
                    if ($url[0] == 'product_id' && $parts[0] == "classifieds-details") {

                        $this->request->get['classifieds_id'] = $url[1];
                    } elseif ($url[0] == 'product_id') {
                        $this->request->get['product_id'] = $url[1];
                    }

                    if ($url[0] == 'category_id' && ( $url[1] == 12 || $url[1] == 11 || $url[1] == 10)) {

                        $this->request->get['category_id'] = $url[1];
                    } elseif ($url[0] == 'category_id' && $parts[0] == "collection") {


                        $this->request->get['category_id'] = $url[1];
                        $this->request->get['collection'] = TRUE;
                    } elseif ($url[0] == 'category_id' && $parts[0] == "collection-product") {


                        $this->request->get['category_id'] = $url[1];
                        $this->request->get['collection-product'] = TRUE;
                    } elseif ($url[0] == 'category_id' && $parts[0] == "classifieds") {
                        $this->request->get['category_id'] = $url[1];
                        $this->request->get['classifieds'] = TRUE;
                    } elseif (($url[0] == 'category_id' ) && $url[1] != 2) {
                        if (!isset($this->request->get['path'])) {
                            $this->request->get['path'] = $url[1];
                        } else {
                            $this->request->get['path'] .= '_' . $url[1];
                        }
                    }

                    if ($url[0] == 'manufacturer_id') {
                        $this->request->get['manufacturer_id'] = $url[1];
                    }
                    if ($url[0] == 'post_id') {
                        $this->request->get['post_id'] = $url[1];
                    }

                    if ($url[0] == 'information_id') {
                        $this->request->get['information_id'] = $url[1];
                    }

                    if ($query->row['query'] && $url[0] != 'information_id' && $url[0] != 'manufacturer_id' && $url[0] != 'category_id' && $url[0] != 'product_id' && $url[0] != 'post_id' && $url[0] != 'bike_id' && $url[0] != 'classifieds_id') {
                        $this->request->get['route'] = $query->row['query'];
                    }
                } else {

                    if ($part != "machine-bikes" && $part != "classifieds-details" && $part != "collection" && $part != "classifieds" && $part != "collection-product") {
                        $this->request->get['route'] = 'error/not_found';

                        if ($_s = $this->setURL($this->request->get['_route_'])) {
                            $this->request->get['route'] = $_s;
                        }
                        break;
                    }
                }
            }


            if (!isset($this->request->get['route'])) {
                if (isset($this->request->get['bike_id'])) {
                    $this->request->get['route'] = 'machine/bikes/details';
                } elseif (isset($this->request->get['classifieds_id'])) {
                    $this->request->get['route'] = 'classifieds/classifieds/details';
                } elseif (isset($this->request->get['product_id'])) {
                    $this->request->get['route'] = 'product/product';
                } elseif (isset($this->request->get['path'])) {
                    $this->request->get['route'] = 'product/category';
                } elseif (isset($this->request->get['manufacturer_id'])) {
                    $this->request->get['route'] = 'product/manufacturer/info';
                } elseif (isset($this->request->get['information_id'])) {
                    $this->request->get['route'] = 'information/information';
                } elseif (isset($this->request->get['post_id'])) {
                    $this->request->get['route'] = 'culture/blog/details/';
                } elseif (isset($this->request->get['category_id']) && isset($this->request->get['collection'])) {
                    $this->request->get['route'] = 'collection/collection/categories';
                } elseif (isset($this->request->get['category_id']) && isset($this->request->get['collection-product'])) {
                    $this->request->get['route'] = 'collection/collection/products';
                } elseif (isset($this->request->get['category_id']) && isset($this->request->get['classifieds'])) {
                    $this->request->get['route'] = 'classifieds/classifieds';
                } elseif (isset($this->request->get['category_id'])) {
                    $this->request->get['route'] = 'machine/bikes/products';
                }
            }

            if (isset($this->request->get['route'])) {
                return new Action($this->request->get['route']);
            }
        }
    }

    public function rewrite($link) {
        $url_info = parse_url(str_replace('&amp;', '&', $link));

        $url = '';

        $data = array();

        parse_str($url_info['query'], $data);

        foreach ($data as $key => $value) {


            if (isset($data['route'])) {


                if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id') || ($data['route'] == 'culture/blog/details/' && $key == 'post_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'machine/bikes/products' && $key == 'category_id')) {

                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/bikes/' . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif ($data['route'] == 'machine/bikes/details' && $key == 'bike_id') {

                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'product_id=" . (int) $value . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/machine-bikes/' . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif ($data['route'] == 'classifieds/classifieds/details' && $key == 'classifieds_id') {

                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'product_id=" . (int) $value . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/classifieds-details/' . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'collection/collection/categories' && $key == 'category_id')) {

                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/collection/' . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'classifieds/classifieds' && $key == 'category_id')) {

                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/classifieds/' . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'collection/collection/products' && $key == 'category_id')) {

                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/collection-product/' . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif ($key == 'path') {
                    $categories = explode('_', $value);

                    foreach ($categories as $category) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int) $category . "'");

                        if ($query->num_rows && $query->row['keyword']) {
                            $url .= '/' . $query->row['keyword'];
                        } else {
                            $url = '';

                            break;
                        }
                    }

                    unset($data[$key]);
                }
            }
        }

        if ($_u = $this->getURL($data['route'])) {


            $url .= $_u;

            if (isset($data['address_id'])) {
                $url .='?address_id=' . $data['address_id'];
            }
            if (isset($data['post_id'])) {
                $url .='&post_id=' . $data['post_id'];
            }
            if (isset($data['order_id'])) {
                $url .='?order_id=' . $data['order_id'];
            }
            if (isset($data['gift_id'])) {
                $url .='?gift_id=' . $data['gift_id'];
            }
            if (isset($data['page'])) {
                $url .='?page=' . $data['page'];
            }
            unset($data[$key]);
        }
        if ($url) {
            unset($data['route']);

            $query = '';

            if ($data) {
                foreach ($data as $key => $value) {
                    $query .= '&' . rawurlencode((string) $key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string) $value));
                }

                if ($query) {
                    $query = '?' . str_replace('&', '&amp;', trim($query, '&'));
                }
            }

            return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
        } else {
            return $link;
        }
    }

    public function getURL($route) {

        if (count($this->url_list) > 0) {
            foreach ($this->url_list as $key => $value) {

                if ($route == $key) {
                    return '/' . $value;
                }
            }
        }
        return false;
    }

    public function setURL($_route) {
        //echo $_route;
        if (count($this->url_list) > 0) {
            foreach ($this->url_list as $key => $value) {
                if ($_route == $value) {
                    //echo $_route;
                    return $key;
                }
            }
        }
        return false;
    }

}
