<?php

class ControllerCommonHeader extends Controller {

    public function index() {
        // Analytics
        $this->load->model('extension/extension');

        $data['analytics'] = array();

        $analytics = $this->model_extension_extension->getExtensions('analytics');

        foreach ($analytics as $analytic) {
            if ($this->config->get($analytic['code'] . '_status')) {
                $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
        }

        $data['title'] = $this->document->getTitle();

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts();
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['name'] = $this->config->get('config_name');

        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        $this->load->language('common/header');

        $data['text_home'] = $this->language->get('text_home');
        $data['menu_all_magazines'] = $this->language->get('menu_all_magazines');
        $data['menu_latest_articles'] = $this->language->get('menu_latest_articles');
        $data['menu_top_articles'] = $this->language->get('menu_top_articles');
        $data['menu_featured_articles'] = $this->language->get('menu_featured_articles');
        $data['menu_most_read'] = $this->language->get('menu_most_read');
        $data['menu_photo_library'] = $this->language->get('menu_photo_library');
        $data['text_magazine'] = $this->language->get('text_magazine');


        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

        $data['text_account'] = $this->language->get('text_account');
        $data['text_register'] = $this->language->get('text_register');
        $data['text_login'] = $this->language->get('text_login');

        $data['text_logout'] = $this->language->get('text_logout');

        $data['text_category'] = $this->language->get('text_category');
        $data['text_all'] = $this->language->get('text_all');
        $data['menu_about'] = $this->language->get('menu_about');
        $data['menu_contact'] = $this->language->get('menu_contact');
        $data['menu_advertise'] = $this->language->get('menu_advertise');
        $data['menu_privacy'] = $this->language->get('menu_privacy');
        $data['menu_career'] = $this->language->get('menu_career');

        $data['logged'] = $this->customer->isLogged();
        $data['account'] = $this->url->link('account/account', '', true);
        $data['register'] = $this->url->link('account/register', '', true);
        $data['login'] = $this->url->link('account/login', '', true);

        $data['logout'] = $this->url->link('account/logout', '', true);

        $data['contact'] = $this->url->link('information/contact');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['email'] = $this->config->get('config_email');

        // Menu Categories
        $this->load->model('common/header');

        $data['menucategories'] = array();

        $menucategories = $this->model_common_header->getMenuCategories();

        foreach ($menucategories as $menucategory) {

                $data['menucategories'][] = array(
                    'name' => $menucategory['name'],
                    'href' => $this->url->link('information/category', 'newscategory_id=' . $menucategory['category_id'])
                );
        }

        // Magazines

        $data['magazine_menu'] = array();
        $this->load->model('magazines/magazines');
        $all_magazines = $this->model_magazines_magazines->getMagazines();

        foreach ($all_magazines as $magazines_data) {

            $data['magazine_menu'][] = array(
                'issue_id' => $magazines_data['issue_id'],
                'sort' => $magazines_data['sort_order'],
                'name' => $magazines_data['name'],
                'href' => $this->url->link('magazines/magazines/details', 'issue_id=' . $magazines_data['issue_id'])
            );
        }

        $data['language'] = $this->load->controller('common/language');

        $data['home'] = $this->url->link('common/home');
        $data['contact'] = $this->url->link('information/contact');
        $data['about'] = $this->url->link('information/about');
        $data['terms'] = $this->url->link('information/terms');
        $data['gallery'] = $this->url->link('gallery/gallery');
        $data['videogallery'] = $this->url->link('gallery/video');
        $data['magazines'] = $this->url->link('magazines/magazines');        
        $data['latest'] = $this->url->link('information/latest');
        $data['read'] = $this->url->link('information/read');
        $data['top'] = $this->url->link('information/top');
        $data['featured'] = $this->url->link('information/featured');

        return $this->load->view('common/header', $data);
    }

}
