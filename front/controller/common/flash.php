<?php

class ControllerCommonFlash extends Controller {

    public function index() {

        $this->load->model('news/news');

        $this->load->model('tool/image');
        $this->load->model('news/category');
        $data['latestnews'] = array();

        $results = $this->model_news_news->getLatestNews(12);

        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
                }


                $data['latestnews'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'date' => $result['ago'],
                    'badge' => $result['badge'],
                    'author' => $result['author'],
                    'viewed' => $result['viewed'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_news_description_length')) . '..',
                    'href' => $this->url->link('news/news/details', 'news_id=' . $result['news_id'])
                );
            }
        }

        return $this->load->view('common/flash', $data);
    }

}
