<?php

class ControllerCommonNewsletter extends Controller {

    public function index() {  

    	$this->load->language('common/newsletter');


    $data[] = array();       

    $data['text_subscribe'] = $this->language->get('text_subscribe');    
    $data['text_newsletter'] = $this->language->get('text_newsletter');
    $data['text_signup'] = $this->language->get('text_signup');
    $data['text_newsletter_placeholder'] = $this->language->get('text_newsletter_placeholder');

        return $this->load->view('common/newsletter', $data);
    }

}
