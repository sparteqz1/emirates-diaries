<?php

class ControllerCommonFooter extends Controller {

    public function index() {
        $this->load->language('common/footer');

        $data['scripts'] = $this->document->getScripts('footer');

        $data['text_information'] = $this->language->get('text_information');

        $data['telephone'] = $this->config->get('config_telephone');
        $data['email'] = $this->config->get('config_email');
        $data['address'] = $this->config->get('config_address');
        $data['text_extra'] = $this->language->get('text_extra');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['manufacturer'] = $this->url->link('product/manufacturer');
        $data['home'] = $this->url->link('common/home');

        $data['name'] = $this->config->get('config_name');

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        $data['text_service'] = $this->language->get('text_service');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_sitemap'] = $this->language->get('text_sitemap');
        $data['sitemap'] = $this->url->link('information/sitemap');
        $data['contact'] = $this->url->link('information/contact');

        $data['menu_media_kit'] = $this->language->get('menu_media_kit');

        $data['text_account'] = $this->language->get('text_account');
        $data['text_magazine'] = $this->language->get('text_magazine');
        $data['text_get_the'] = $this->language->get('text_get_the');
        $data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_follow_us_on'] = $this->language->get('text_follow_us_on');
        $data['text_instagram'] = $this->language->get('text_instagram');
        $data['text_follow_us'] = $this->language->get('text_follow_us');
        $data['text_follow_us_desc'] = $this->language->get('text_follow_us_desc');
        $data['text_subscribe'] = $this->language->get('text_subscribe');
        $data['text_subscribe_desc'] = $this->language->get('text_subscribe_desc');
        $data['text_call_us'] = $this->language->get('text_call_us');
        $data['text_send_us'] = $this->language->get('text_send_us');
        $data['text_office_tele'] = $this->language->get('text_office_tele');
        $data['text_reach_us'] = $this->language->get('text_reach_us');
        $data['text_email'] = $this->language->get('text_email');
        $data['menu_about_us'] = $this->language->get('menu_about_us');
        $data['menu_contact_us'] = $this->language->get('menu_contact_us');
        $data['menu_career'] = $this->language->get('menu_career');
        $data['menu_privacy_policy'] = $this->language->get('menu_privacy_policy');
        $data['menu_advertise_us'] = $this->language->get('menu_advertise_us');
        $data['text_copy_rights'] = $this->language->get('text_copy_rights');
        $data['text_desc'] = $this->language->get('text_desc');

        $data['account'] = $this->url->link('account/account', '', true);
        $data['newsletter'] = $this->url->link('account/newsletter', '', true);


        $this->load->model('catalog/information');

        $data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            if ($result['bottom']) {
                $data['informations'][] = array(
                    'title' => $result['title'],
                    'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }

        $data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

        // Whos Online
        if ($this->config->get('config_customer_online')) {
            $this->load->model('tool/online');

            if (isset($this->request->server['REMOTE_ADDR'])) {
                $ip = $this->request->server['REMOTE_ADDR'];
            } else {
                $ip = '';
            }

            if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
                $url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
            } else {
                $url = '';
            }

            if (isset($this->request->server['HTTP_REFERER'])) {
                $referer = $this->request->server['HTTP_REFERER'];
            } else {
                $referer = '';
            }

            $this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
        }

        $data['search'] = $this->load->controller('common/search');

        if (isset($this->request->get['route'])) {
            $data['current_root'] = $this->request->get['route'];
        } else {
            $data['current_root'] = '';
        }
        if($this->language->get('direction') == 'rtl') {
            $data['dire'] = 'true';
        } else {
            $data['dire'] = 'false';
        }

        $this->load->model('news/issue');
        $this->load->model('news/news');
        
        $data['article_issue'] = $this->model_news_issue->getIssues();

        $data['issues'] = array();
        $issues = $this->model_news_issue->getIssues();
       foreach ($issues as $issue) {
                $issue_id = $issue['issue_id'];
                $issue_info = $this->model_news_issue->getIssue($issue_id);

                if ($issue_info) {
                   

                    $articles = array();

                    $filter_data = array(
                        'filter_issue_id' => $issue_id,
                        'start' => 0,
                        'limit' => 3
                    );


                    $results = $this->model_news_news->getAllNews($filter_data);

                    if(count($results) > 0) {
                    
                    $data['issues'][] = array(
                        'issue_id' => $issue_info['issue_id'],
                        'issue_order' => $issue_info['sort_order']
                    );
                    }

                    
                }
        }

        $data['contact'] = $this->url->link('information/contact');
        $data['about'] = $this->url->link('information/about');
        $data['terms'] = $this->url->link('information/terms');

        return $this->load->view('common/footer', $data);
    }

    public function enquiry() {
        $this->load->language('information/contact');

        $this->load->model('contact/contact');
        $json = array();


        if (empty($this->request->post['subscribemail'])) {
            $json['error'] = $this->language->get('error_null_email');
        } elseif (!filter_var($this->request->post['subscribemail'], FILTER_VALIDATE_EMAIL)) {
            $json['error'] = $this->language->get('error_email');
        }


        if (!$json) {

            $count = $this->model_contact_contact->countSubscription($this->request->post);
            if ($count != 0) {
                
                 $json['error'] = $this->language->get('error_exist');
                
            } else {
                $this->model_contact_contact->addSubscription($this->request->post);

                $json['success'] = $this->language->get('success_subscription');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function subscribtion() {
        $this->load->language('information/contact');

        $this->load->model('contact/contact');
        $json = array();

        if (empty($this->request->post['issue'])) {
            $json['error'] = $this->language->get('error_issue');
        }
        if (empty($this->request->post['sbe_phone'])) {
            $json['error'] = $this->language->get('error_mobile');
        } elseif ((!empty($this->request->post['sbe_phone'])) && (!ctype_digit($this->request->post['sbe_phone']))) {
            $json['error'] = $this->language->get('error_digits');
        }
        if (empty($this->request->post['sbe_email'])) {
            $json['error'] = $this->language->get('error_null_email');
        } elseif (!filter_var($this->request->post['sbe_email'], FILTER_VALIDATE_EMAIL)) {
            $json['error'] = $this->language->get('error_email');
        }
        if (empty($this->request->post['sbe_name'])) {
            $json['error'] = $this->language->get('error_name');
        } elseif ((!empty($this->request->post['sbe_name'])) && (!preg_match("/^[a-zA-Z\s]+$/", $this->request->post['sbe_name']))) {
            $json['error'] = $this->language->get('error_letters');
        }


        if (!$json) {

            $this->model_contact_contact->addMagazineSubscription($this->request->post);

            $json['success'] = $this->language->get('success_subscribtion');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
