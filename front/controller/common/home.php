<?php

class ControllerCommonHome extends Controller {

    public function index() {

        $this->load->language('common/home');

        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));


        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical');
        }

        $this->load->model('news/news');

        $this->load->model('tool/image');
        $this->load->model('news/category');
        $this->load->model('news/issue');

        $data['popularnews'] = array();

        $results = $this->model_news_news->getPopularNews(3);

        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], 900, 600);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', 900, 600);
                }
                $issue_popular = $this->model_news_issue->getIssue($result['issue_id']);
                $category = $this->model_news_category->getCategoryByNewsId($result['news_id']);

                $data['popularnews'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'category' => $category['name'],
                    'popular_issue' => $issue_popular['name'],
                    'category' => $category['name'],
                    'date' => date("j M, Y", strtotime($result['date_added'])),
                    'author' => $result['author'],
                    'href' => $this->url->link('news/news/details', 'news_id=' . $result['news_id'])
                );
            }
        }


        $data['latestnews'] = array();

        $results = $this->model_news_news->getLatestNews(3);

        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], 900, 600);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', 900, 600);
                }
                $issue_latest = $this->model_news_issue->getIssue($result['issue_id']);
                $category = $this->model_news_category->getCategoryByNewsId($result['news_id']);
                $data['latestnews'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'category' => $category['name'],
                    'latest_issue' => $issue_latest['name'],
                    'date' => date("j M, Y", strtotime($result['date_added'])),
                    'author' => $result['author'],
                    'href' => $this->url->link('news/news/details', 'news_id=' . $result['news_id'])
                );
            }
        }

        //Featured News

        $data['featured'] = $this->load->controller('news/featured');         

        $data['issues'] = array();
        $issues = $this->model_news_issue->getIssues();
       foreach ($issues as $issue) {
                $issue_id = $issue['issue_id'];
                $issue_info = $this->model_news_issue->getIssue($issue_id);

                if ($issue_info) {
                    if ($issue_info['image']) {
                                $issue_slider = $this->model_tool_image->resize($issue_info['image'], 3000, 2000);
                            } else {
                                $issue_slider = $this->model_tool_image->resize('placeholder.png', 3000, 2000);
                            }

                    $articles = array();

                    $filter_data = array(
                        'filter_issue_id' => $issue_id,
                        'start' => 0,
                        'limit' => 3
                    );


                    $results = $this->model_news_news->getAllNews($filter_data);

                    if(count($results) > 0) {

                        foreach ($results as $result) {
                        if ($result) {
                            if ($result['image']) {
                                $image = $this->model_tool_image->resize($result['image'], 900, 600);
                            } else {
                                $image = $this->model_tool_image->resize('placeholder.png', 900, 600);
                            }
                            $category = $this->model_news_category->getCategoryByNewsId($result['news_id']);

                            $articles[] = array(
                                'news_id' => $result['news_id'],
                                'thumb' => $image,
                                'name' => $result['name'],
                                'date' => $result['ago'],
                                'badge' => $result['badge'],
                                'category' => $category['name'],
                                'author' => $result['author'],
                                'viewed' => $result['viewed'],
                                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_news_description_length')) . '..',
                                'href' => $this->url->link('news/news/details', 'news_id=' . $result['news_id'])
                            );
                        }
                    }
                    
                    $data['issues'][] = array(
                        'issue_id' => $issue_info['issue_id'],
                        'issue_order' => $issue_info['sort_order'],
                        'articles' => $articles,
                        'issue_slider' => $issue_slider,
                        'name' => $issue_info['name'],
                        'href' => $this->url->link('news/news', 'news_path=' . $issue_info['issue_id'])
                    );
                    }

                    
                }
        }
        

        $totalcategories = $this->model_news_category->getTotalCategories();
        $data['total'] = $totalcategories;
        
        // Magazines
        $data['ed_magazines'] = $this->load->controller('magazines/ed_magazines');

        // Newsletter
        $data['newsletter'] = $this->load->controller('common/newsletter');   

        $data['text_welcome_desc'] = $this->language->get('text_welcome_desc');
        $data['text_latest_articles'] = $this->language->get('text_latest_articles');
        $data['text_most_read'] = $this->language->get('text_most_read');
        $data['text_latest_magazines'] = $this->language->get('text_latest_magazines');
        $data['text_welcome'] = $this->language->get('text_welcome');
        $data['text_ed'] = $this->language->get('text_ed');

        $data['direction'] = $this->language->get('direction');            

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        
        $data['artcle'] = $this->url->link('magazines/magazines/details');

        $this->response->setOutput($this->load->view('common/home', $data));
    }

}
