<?php

class ControllerInformationAbout extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('information/about');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_welcome_desc'] = $this->language->get('text_welcome_desc');
        $data['text_about'] = $this->language->get('text_about');
        $data['text_ed'] = $this->language->get('text_ed');

        $this->load->model('tool/image');

        if ($this->config->get('config_image')) {
            $data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
        } else {
            $data['image'] = false;
        }

        // Trustees
        $data['trustees'] = array();
        $this->load->model('information/about');
        $this->load->model('tool/image');
        $all_trustees = $this->model_information_about->getTrustees();
        foreach ($all_trustees as $trustee) {   
            if ($trustee['image']) {
                $image = $this->model_tool_image->resize($trustee['image'], 886, 886);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 886, 886);
            }                    

            $data['trustees'][] = array(
                'trustees_id' => $trustee['trustees_id'],
                'image' => $image,
                'name' => $trustee['name'],
                'description' => html_entity_decode($trustee['description'], ENT_QUOTES, 'UTF-8'),
                'href' => $this->url->link('information/activities/activity', 'trustee=' . $trustee['trustees_id'])
            );
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('information/about', $data));
    }

}
