<?php

class ControllerNewsNews extends Controller {

    private $error = array();

    public function index() {


        $this->load->language('news/news');

        $this->load->model('news/category');

        $this->load->model('news/news');

        $this->load->model('tool/image');


        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'n.sort_order';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int) $this->request->get['limit'];
        } else {
            $limit = $this->config->get($this->config->get('config_theme') . '_news_limit');
        }

        $limit = 1;
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        if (isset($this->request->get['news_path'])) {
            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $news_path = '';

            $parts = explode('_', (string) $this->request->get['news_path']);

            $category_id = (int) array_pop($parts);

            foreach ($parts as $path_id) {
                if (!$news_path) {
                    $news_path = (int) $path_id;
                } else {
                    $news_path .= '_' . (int) $path_id;
                }

                $category_info = $this->model_news_category->getCategory($path_id);

                if ($category_info) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        'href' => $this->url->link('news/news', 'news_path=' . $news_path . $url)
                    );
                }
            }
        } else {
            $category_id = 0;
        }

        $category_info = $this->model_news_category->getCategory($category_id);

        if ($category_info) {
            $this->document->setTitle($category_info['meta_title']);
            $this->document->setDescription($category_info['meta_description']);
            $this->document->setKeywords($category_info['meta_keyword']);

            $data['heading_title'] = $category_info['name'];

            $data['text_refine'] = $this->language->get('text_refine');
            $data['text_empty'] = $this->language->get('text_empty');
            $data['text_author'] = $this->language->get('text_author');
            $data['text_sort'] = $this->language->get('text_sort');
            $data['text_limit'] = $this->language->get('text_limit');

            // Set the last category breadcrumb
            $data['breadcrumbs'][] = array(
                'text' => $category_info['name'],
                'href' => $this->url->link('news/news', 'news_path=' . $this->request->get['news_path'])
            );

            if ($category_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get($this->config->get('config_theme') . '_image_news_category_width'), $this->config->get($this->config->get('config_theme') . '_image_news_category_height'));
            } else {
                $data['thumb'] = '';
            }

            $data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');

            $url = '';


            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['categories'] = array();

            $results = $this->model_news_category->getCategories($category_id);

            foreach ($results as $result) {
                $filter_data = array(
                    'filter_category_id' => $result['category_id'],
                    'filter_sub_category' => true
                );

                $data['categories'][] = array(
                    'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_news_news->getTotalNews($filter_data) . ')' : ''),
                    'href' => $this->url->link('news/news', 'news_path=' . $this->request->get['news_path'] . '_' . $result['category_id'] . $url)
                );
            }

            $data['news'] = array();

            $filter_data = array(
                'filter_category_id' => $category_id,
                'sort' => $sort,
                'order' => $order,
                'start' => ($page - 1) * $limit,
                'limit' => $limit
            );

            $news_total = $this->model_news_news->getTotalNews($filter_data);

            $results = $this->model_news_news->getAllNews($filter_data);

            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_news_width'), $this->config->get($this->config->get('config_theme') . '_image_news_height'));
                }



                $data['news'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'date' => $result['ago'],
                    'badge' => $result['badge'],
                    'author' => $result['author'],
                    'viewed' => $result['viewed'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_news_description_length')) . '..',
                    'href' => $this->url->link('news/news/details', 'news_path=' . $this->request->get['news_path'] . '&news_id=' . $result['news_id'] . $url)
                );
            }

            $url = '';



            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['sorts'] = array();

            $data['sorts'][] = array(
                'text' => $this->language->get('text_default'),
                'value' => 'n.sort_order-ASC',
                'href' => $this->url->link('news/news', 'news_path=' . $this->request->get['news_path'] . '&sort=n.sort_order&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_name_asc'),
                'value' => 'nd.name-ASC',
                'href' => $this->url->link('news/news', 'news_path=' . $this->request->get['news_path'] . '&sort=nd.name&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_name_desc'),
                'value' => 'nd.name-DESC',
                'href' => $this->url->link('news/news', 'news_path=' . $this->request->get['news_path'] . '&sort=nd.name&order=DESC' . $url)
            );



            $url = '';



            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            $data['limits'] = array();

            $limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_news_limit'), 25, 50, 75, 100));

            sort($limits);

            foreach ($limits as $value) {
                $data['limits'][] = array(
                    'text' => $value,
                    'value' => $value,
                    'href' => $this->url->link('news/news', 'news_path=' . $this->request->get['news_path'] . $url . '&limit=' . $value)
                );
            }

            $url = '';



            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $pagination = new Pagination();
            $pagination->total = $news_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('news/news', 'news_path=' . $this->request->get['news_path'] . $url . '&page={page}');

            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($news_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($news_total - $limit)) ? $news_total : ((($page - 1) * $limit) + $limit), $news_total, ceil($news_total / $limit));

            if ($page == 1) {
                $this->document->addLink($this->url->link('news/news', 'news_path=' . $category_info['category_id'], true), 'canonical');
            } elseif ($page == 2) {
                $this->document->addLink($this->url->link('news/news', 'news_path=' . $category_info['category_id'], true), 'prev');
            } else {
                $this->document->addLink($this->url->link('news/news', 'news_path=' . $category_info['category_id'] . '&page=' . ($page - 1), true), 'prev');
            }

            if ($limit && ceil($news_total / $limit) > $page) {
                $this->document->addLink($this->url->link('news/news', 'news_path=' . $category_info['category_id'] . '&page=' . ($page + 1), true), 'next');
            }

            $data['sort'] = $sort;
            $data['order'] = $order;
            $data['limit'] = $limit;



           


            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            
            
            $data['flash'] = $this->load->controller('common/flash');

            $this->response->setOutput($this->load->view('news/news_list', $data));
        } else {
            $url = '';

            if (isset($this->request->get['news_path'])) {
                $url .= '&path=' . $this->request->get['news_path'];
            }



            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('news/news', $url)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    public function details() {
        $this->load->language('news/news');
        $this->load->model('news/issue');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $this->load->model('news/category');

        if (isset($this->request->get['news_path'])) {
            $news_path = '';

            $parts = explode('_', (string) $this->request->get['news_path']);

            $category_id = (int) array_pop($parts);

            foreach ($parts as $path_id) {
                if (!$news_path) {
                    $news_path = $path_id;
                } else {
                    $news_path .= '_' . $path_id;
                }

                $category_info = $this->model_news_category->getCategory($path_id);

                if ($category_info) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        'href' => $this->url->link('news/news', 'news_path=' . $news_path)
                    );
                }
            }

            // Set the last category breadcrumb
            $category_info = $this->model_news_category->getCategory($category_id);

            if ($category_info) {
                $url = '';

                if (isset($this->request->get['sort'])) {
                    $url .= '&sort=' . $this->request->get['sort'];
                }

                if (isset($this->request->get['order'])) {
                    $url .= '&order=' . $this->request->get['order'];
                }

                if (isset($this->request->get['page'])) {
                    $url .= '&page=' . $this->request->get['page'];
                }

                if (isset($this->request->get['limit'])) {
                    $url .= '&limit=' . $this->request->get['limit'];
                }

                $data['breadcrumbs'][] = array(
                    'text' => $category_info['name'],
                    'href' => $this->url->link('news/news', 'news_path=' . $this->request->get['news_path'] . $url)
                );
            }
        }


        $this->load->model('news/author');

        if (isset($this->request->get['author_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_author'),
                'href' => $this->url->link('news/author')
            );

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $author_info = $this->model_news_author->getAuthor($this->request->get['author_id']);

            if ($author_info) {
                $data['breadcrumbs'][] = array(
                    'text' => $author_info['name'],
                    'href' => $this->url->link('news/author/info', 'author_id=' . $this->request->get['author_id'] . $url)
                );
            }
        }

        if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
            $url = '';

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }



            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_search'),
                'href' => $this->url->link('news/search', $url)
            );
        }

        if (isset($this->request->get['news_id'])) {
            $news_id = (int) $this->request->get['news_id'];
        } else {
            $news_id = 0;
        }

        $this->load->model('news/news');

        $news_info = $this->model_news_news->getNews($news_id);

        if ($news_info) {
            $url = '';

            if (isset($this->request->get['news_path'])) {
                $url .= '&news_path=' . $this->request->get['news_path'];
            }


            if (isset($this->request->get['author_id'])) {
                $url .= '&author_id=' . $this->request->get['author_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }



            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $news_info['name'],
                'href' => $this->url->link('news/news/details', $url . '&news_id=' . $this->request->get['news_id'])
            );

            $this->document->setTitle($news_info['name']);
            $this->document->addLink($this->url->link('news/news/details', 'news_id=' . $this->request->get['news_id']), 'canonical');
            $this->document->addScript('front/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
            $this->document->addStyle('front/view/javascript/jquery/magnific/magnific-popup.css');



            $data['heading_title'] = $news_info['name'];

            $data['text_select'] = $this->language->get('text_select');
            $data['text_author'] = $this->language->get('text_author');

            $data['text_write'] = $this->language->get('text_write');

            $data['text_tags'] = $this->language->get('text_tags');
            $data['text_related'] = $this->language->get('text_related');

            $data['text_loading'] = $this->language->get('text_loading');

            $data['tab_description'] = $this->language->get('tab_description');
            $data['tab_attribute'] = $this->language->get('tab_attribute');

            $data['text_further_read'] = $this->language->get('text_further_read');
            $data['text_latest_magazines'] = $this->language->get('text_latest_magazines');

            $data['news_id'] = (int) $this->request->get['news_id'];
            $data['author'] = $news_info['author'];
            $data['ago'] = $news_info['ago'];
            $data['badge'] = $news_info['badge'];
            $data['viewed'] = $news_info['viewed'];
            $data['authors'] = $this->url->link('news/author/info', 'author_id=' . $news_info['author_id']);


            $data['description'] = html_entity_decode($news_info['description'], ENT_QUOTES, 'UTF-8');

            //$data['arrangement'] = $news_info['arrangement'];

            $issue_info = $this->model_news_issue->getIssue($news_info['issue_id']);
            $data['issue'] = $issue_info['name'];



            $this->load->model('tool/image');

            if ($news_info['image']) {
                $data['popup'] = $this->model_tool_image->resize($news_info['image'], $this->config->get($this->config->get('config_theme') . '_image_news_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_news_popup_height'));
            } else {
                $data['popup'] = '';
            }

            if ($news_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($news_info['image'], $this->config->get($this->config->get('config_theme') . '_image_news_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_news_thumb_height'));
            } else {
                $data['thumb'] = '';
            }

            if ($news_info['image']) {
                $data['image'] = $this->model_tool_image->resize($news_info['image'], 900, 600);
            } else {
                $data['image'] = $this->model_tool_image->resize('placeholder.png', 900, 600);
            }

            $data['images'] = array();

            $results = $this->model_news_news->getNewsImages($this->request->get['news_id']);

            foreach ($results as $result) {
                $data['images'][] = array(
                    'popup' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_news_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_news_popup_height')),
                    'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_news_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_news_additional_height')),
                    'extra' => HTTP_SERVER . 'image/' . $result['image']
                );
            }


            $data['share'] = $this->url->link('news/news/details', 'news_id=' . (int) $this->request->get['news_id']);

            $data['attribute_groups'] = $this->model_news_news->getNewsAttributes($this->request->get['news_id']);

            $data['related_news'] = array();

            $results = $this->model_news_news->getNewsRelated($this->request->get['news_id']);

            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], 900, 600);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', 900, 600);
                }
                $issue_related = $this->model_news_issue->getIssue($result['issue_id']);


                $data['related_news'][] = array(
                    'news_id' => $result['news_id'],
                    'image' => $image,
                    'related_issue' => $issue_related['name'],
                    'name' => $result['name'],
                    'date' => date("j M, Y", strtotime($result['date_added'])),
                    'author' => $result['author'],
                    'href' => $this->url->link('news/news/details', 'news_id=' . $result['news_id'])
                );
            }

            // Photographers

            $data['photographers'] = array();

            $photographer_results = $this->model_news_news->getPhotographersByNewsId($this->request->get['news_id']);

            foreach ($photographer_results as $photographer_result) {

                $data['photographers'][] = array(
                    'name' => $photographer_result['name']
                );
            }

            $data['tags'] = array();

            if ($news_info['tag']) {
                $tags = explode(',', $news_info['tag']);

                foreach ($tags as $tag) {
                    $data['tags'][] = array(
                        'tag' => trim($tag),
                        'href' => $this->url->link('news/search', 'tag=' . trim($tag))
                    );
                }
            }



            $this->model_news_news->updateViewed($this->request->get['news_id']);

            // Magazines
            $data['ed_magazines'] = $this->load->controller('magazines/ed_magazines');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $data['single'] = $this->url->link('information/details');
            $data['artcle'] = $this->url->link('magazines/magazines/details');

            if($news_info['arrangement'] == 'left') {

                $this->response->setOutput($this->load->view('news/details_left', $data));

            } else if($news_info['arrangement'] == 'right') {

                $this->response->setOutput($this->load->view('news/details_right', $data));

            } else {
                $this->response->setOutput($this->load->view('news/details', $data));
            }

            
        } else {
            $url = '';

            if (isset($this->request->get['news_path'])) {
                $url .= '&news_path=' . $this->request->get['news_path'];
            }


            if (isset($this->request->get['author_id'])) {
                $url .= '&author_id=' . $this->request->get['author_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['newscategory_id'])) {
                $url .= '&newscategory_id=' . $this->request->get['newscategory_id'];
            }



            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('news/news/details', $url . '&news_id=' . $news_id)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

}
