<?php

class ControllerNewsFeatured extends Controller {

    public function index() {

        $this->load->language('news/featured');
        $this->load->model('news/category');

        $data['text_featured'] = $this->language->get('text_featured');
        
        $data['featurednews'] = array();

        $results = $this->model_news_news->getNewsFeatured();

        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], 900, 600);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', 900, 600);
                }
                $issue_featured = $this->model_news_issue->getIssue($result['issue_id']);
                $category = $this->model_news_category->getCategoryByNewsId($result['news_id']);

                $data['featurednews'][] = array(
                    'news_id' => $result['news_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'category' => $category['name'],
                    'featured_issue' => $issue_featured['name'],
                    'href' => $this->url->link('news/news/details', 'news_id=' . $result['news_id'])
                );
            }
        }

        return $this->load->view('news/featured', $data);
    }

}
