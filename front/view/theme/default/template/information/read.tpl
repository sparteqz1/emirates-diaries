<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333;">
            <div class="container">
                <div class="intro-text">
                    <h2><?php echo $banner_title; ?></h2>                  

                </div>
            </div>
        </header>

        
       <!-- Most Read -->
        <section class="most-read">
            <div class="container">
                
                <div class="col-lg-12">
                    <div class="row">
                        <?php
                        $count = 0;
                        foreach ($popularnews as $popular_key => $popular) { ?>
                        <div class="col-lg-3 col-sm-6 articles">
                            <div class="card h-100">
                                <a class="card-img" href="<?php echo $popular['href']; ?>">
                                    <img class="card-img-top" src="<?php echo $popular['thumb']; ?>" alt="<?php echo $popular['name']; ?>">
                                    <span>
                                        <?php echo $popular['category']; ?>
                                    </span>
                                </a>

                                <a href="<?php echo $popular['href']; ?>" class="card-body">
                                    <h4 class="card-title">
                                        <?php echo $popular['name']; ?> 
                                    </h4>  
                                    <h5 class="date"><?php echo $popular['date']; ?></h5>
                                    <span class="author">
                                        <?php echo $popular['author']; ?> 
                                    </span>
                                </a>
                            </div>
                        </div>
                        <?php
                        $count = $count+1;
                        if($count%4 == '0') {
                        echo '</div><div class="row">';
                    }
                    } ?>
                    </div>

                </div>
                <!-- /.row -->            

            </div>
        </section>

       <!-- newsletter -->

<?php echo $newsletter; ?>
       
<?php echo $footer; ?>

</body>

</html>
