<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333; ">
    <div class="container">
        <div class="intro-text">

            <h2><?php echo $text_contact_banner; ?></h2>

        </div>
    </div>
</header>
<section class="single-article">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <iframe src="https://snazzymaps.com/embed/15657" width="100%" height="350px" style="border:none;"></iframe>

            </div>
            <!--<div class="col-md-4 contact-address">
                <div class="address">
                    <h3><?php echo $text_reach_us; ?></h3>
                    <p>
                        Emirates Diaries Office Address <br/>
                        Street Name. Pb no. xxx<br/>
                        Dubai, United Arab Emirates.<br/>
                    </p>
                </div>

            </div>-->
            <div class="col-md-12 contact-mails">
                <h2><?php echo $text_get_in_touch; ?></h2>
                <ul>
                    <li>
                        <h5><?php echo $text_general_enquiries; ?></h5>
                        <a href="">hello@emiratesdiaries.com</a>
                    </li>
                    <li>
                        <h5><?php echo $text_advertise; ?></h5>
                        <a href="">hello@emiratesdiaries.com</a>
                    </li>
                    <li>
                        <h5><?php echo $text_want_write; ?> </h5>
                        <a href="">hello@emiratesdiaries.com</a>
                    </li>
                    <li>
                        <h5><?php echo $text_write_story; ?></h5>
                        <a href="">hello@emiratesdiaries.com</a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</section>

<?php echo $footer; ?>

</body>

</html>