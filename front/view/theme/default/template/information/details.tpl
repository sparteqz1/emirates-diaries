<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333; ">
            <div class="container">
                <div class="intro-text">


                    <h2>Gafla: The Arabic Jeweler</h2>
                    <h5>My Dubai Issue</h5>

                </div>
            </div>
        </header>
        <section class="single-article" style="background: #eee;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 article-details">
                        <h4>By Maryam Al Mansoori <span>3 days ago</span></h4>

                    </div>
                    <div class="col-md-6 article-image">
                        <img class="img-fluid" src="front/view/theme/default/images/article/GAFLA10.jpg" alt="">
                        <img class="img-fluid" src="front/view/theme/default/images/article/gafla1.jpg" alt="">
                        <img class="img-fluid" src="front/view/theme/default/images/article/gafla2.jpg" alt="">
                        <img class="img-fluid" src="front/view/theme/default/images/article/gafla3.jpg" alt="">
                        
                        
                    </div>
                    <div class="col-md-6">

                        <h5>1. What’s your story?</h5>
                        <p>
                        Gafla is the Emirati Arabic for ‘caravan’ or ‘qafila’ in classic Arabic, and it’s inspired by the nomads' journey through the deserts and dunes of Arabia.
                        </p>
                        <h5>2. Who’s behind it?</h5>
                        <p>
                        The people behind the brand are:Hamad Bin Shaiban Al Muhairi – Managing Director; Abdulla Beljafla – Creative Director; and BushraBintDarwish – Junior Designer.
                            </p>
                        <h5>3. What makes Gafla innovative and different?</h5>
                        <p>
                        At Gafla, we are creating a celebration of timeless jewels inspired by the heritage of the United Arab Emirates, traditions of the Gulfand the rich Arabic culture of the Middle East.
                        </p>
                        <h5>4. Where can one purchase Gafla pieces? </h5>
                        <p>
                        Gafla offers 2 options for the clients – online ordering or private viewings.
                        </p>
                        <h5>6. How do you pick names for your pieces?</h5>
                        <p>
                        Every collection is inspired by an element that tells a story from our Emirati tradition and heritage, hence why we choose Arabic names to represent our collections
                        </p>
                        <h5>7. How would you describe your pieces? </h5>
                        <p>
                        Our jewelry is inspired by the traditions and the heritage of the UAE as well as the Gulf, but we design them in a way to make them look modern and international. 
                        </p>
                        <h5>8. Tell us about the design process… </h5>
                        <p>
                        Our Creative Director, Abdulla Beljafla designs all the pieces with BushraBintDarwish, the Junior Designer. The process starts merely by an idea, and then we go through a research phase from which we move onto prototyping and finalizing the design. It then finds its way to our workshop for production. 
                        </p>
                        <h5>9. What inspires your collections?</h5>
                        <p>
                        We at Gaflaare inspired by our surroundings, from the nature around us to the elements we see or experience everyday with our families and friends. For example, our Jumah collection is inspired by Friday being a blessed day in our culture as Arabs and as Muslims – it’s such a joyous day where families gather and enjoy their time. 
                        </p>
                        <h5>10. What are some of your goals as jewelry designers?</h5>
                        <p>
                        Our goal is to showcase our Emirati heritage, the traditions of the Gulf and the Arabic culture to the world through our jewels.
                        </p>
                        <h5>11. How did your jewelry interest come about?</h5>
                        <p>
                        Each one of us was interested in jewelry in many different ways. Women in the Gulf absolutely love to wear jewelry, so we took the opportunity of offering them unique pieces inspired by the United Arab Emirates. We think it is the perfect representation of our history and culture. 
                        </p>
                        <h5>12. Were there any obstacles you encountered during the establishment ofGafla?</h5>
                        <p>
                        Every business goes through bumpy roads, so of course we have been through a few obstacles but alhamdulillah we manage to get through them as a team with hard work and persistence.
                        </p>
                        <h5>13. What advice would you give to those starting a journey in the jewelry world?</h5>
                        <p>
                        There are particularly two pieces of advice that we really believe everyone who is trying to be in any business should know…  Education is your key to success, and always being original in your brand and designs.
                        </p>


                    </div>
                </div>
                
            </div>
        </section>





       <!-- Most Read -->
        <section class="most-read">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="section-heading">Further READ</h2> 
                    </div>
                </div> 
                <div class="col-lg-12">
                    <div class="row">

                        <div class="col-lg-3 col-sm-6 articles">
                            <div class="card h-100">
                                <a class="card-img" href="<?php echo $single; ?>">
                                    <img class="card-img-top" src="front/view/theme/default/images/article/window.jpg" alt="">
                                    <span>
                                        Architecture and Design Issue
                                    </span>
                                </a>

                                <a href="<?php echo $single; ?>" class="card-body">
                                    <h4 class="card-title">
                                        Lorem ipsum dolor sit 
                                        amet, consectetur 
                                    </h4>  
                                    <h5 class="date">1 Sept, 2017</h5>
                                    <span class="author">
                                        First Name 
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 articles">
                            <div class="card h-100">
                                <a class="card-img" href="<?php echo $single; ?>">
                                    <img class="card-img-top" src="front/view/theme/default/images/article/mosque2.jpg" alt="">
                                    <span>
                                        Architecture and Design Issue
                                    </span>
                                </a>

                                <a href="<?php echo $single; ?>" class="card-body">
                                    <h4 class="card-title">
                                        Lorem ipsum dolor sit 
                                        amet, consectetur 
                                    </h4>
                                    <h5 class="date">1 Sept, 2017</h5>
                                    <span class="author">
                                        First Name
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 articles">
                            <div class="card h-100">
                                <a class="card-img" href="<?php echo $single; ?>">
                                    <img class="card-img-top" src="front/view/theme/default/images/article/pearla2.jpg" alt="">
                                    <span>
                                        Architecture and Design Issue
                                    </span>
                                </a>

                                <a href="<?php echo $single; ?>" class="card-body">
                                    <h4 class="card-title">
                                        Today marks the 4th year since we’ve started 
                                        Emirates Diaries’ journey
                                    </h4>
                                    <h5 class="date">1 Sept, 2017</h5>
                                    <span class="author">
                                        Author Name
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>



                </div>
                <!-- /.row -->            

            </div>
        </section>


        <!-- Latest Magazine -->
        <section class="magazines">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="section-heading">Latest Magazines</h2> 
                    </div>
                </div> 

                <div class="row magazine-slider">

                    <div class="col-lg-3 col-sm-6">
                        <div class="card h-100">
                            <a class="card-img" href="<?php echo $artcle; ?>">
                                <img class="card-img-top" src="front/view/theme/default/images/magazines/1.jpg" alt="">

                            </a>

                            <a href="<?php echo $artcle; ?>" class="card-body">
                                <h4>
                                    Issue #1
                                </h4>  
                                <h3 class="card-title">
                                    The Culture
                                </h3>                                      
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card h-100">
                            <a class="card-img" href="<?php echo $artcle; ?>">
                                <img class="card-img-top" src="front/view/theme/default/images/magazines/2.jpg" alt="">

                            </a>

                            <a href="<?php echo $artcle; ?>" class="card-body">
                                <h4>
                                    Issue #2
                                </h4> 
                                <h3 class="card-title">
                                    New Beginnings
                                </h3>                                      
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <div class="card h-100">
                            <a class="card-img" href="<?php echo $artcle; ?>">
                                <img class="card-img-top" src="front/view/theme/default/images/magazines/3.jpg" alt="">

                            </a>

                            <a href="<?php echo $artcle; ?>" class="card-body">
                                <h4>
                                    Issue #3
                                </h4> 
                                <h3 class="card-title">
                                    Leadership
                                </h3>                                      
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card h-100">
                            <a class="card-img" href="<?php echo $artcle; ?>">
                                <img class="card-img-top" src="front/view/theme/default/images/magazines/4.jpg" alt="">

                            </a>

                            <a href="<?php echo $artcle; ?>" class="card-body">
                                <h4>
                                    Issue #4
                                </h4> 
                                <h3 class="card-title">
                                    Avant-Grade
                                </h3>                                      
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <div class="card h-100">
                            <a class="card-img" href="<?php echo $artcle; ?>">
                                <img class="card-img-top" src="front/view/theme/default/images/magazines/5.jpg" alt="">

                            </a>

                            <a href="<?php echo $artcle; ?>" class="card-body">
                                <h4>
                                    Issue #5
                                </h4>  
                                <h3 class="card-title">
                                    My Dubai
                                </h3>                                      
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card h-100">
                            <a class="card-img" href="<?php echo $artcle; ?>">
                                <img class="card-img-top" src="front/view/theme/default/images/magazines/6.jpg" alt="">

                            </a>

                            <a href="<?php echo $artcle; ?>" class="card-body">
                                <h4>
                                    Issue #6
                                </h4> 
                                <h3 class="card-title">
                                    Innovation
                                </h3>                                      
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <div class="card h-100">
                            <a class="card-img" href="<?php echo $artcle; ?>">
                                <img class="card-img-top" src="front/view/theme/default/images/magazines/7.jpg" alt="">

                            </a>

                            <a href="<?php echo $artcle; ?>" class="card-body">
                                <h4>
                                    Issue #7
                                </h4> 
                                <h3 class="card-title">
                                    Reading
                                </h3>                                      
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card h-100">
                            <a class="card-img" href="<?php echo $artcle; ?>">
                                <img class="card-img-top" src="front/view/theme/default/images/magazines/8.jpg" alt="">

                            </a>

                            <a href="<?php echo $artcle; ?>" class="card-body">
                                <h4>
                                    Issue #8
                                </h4> 
                                <h3 class="card-title">
                                    Architecture and Design
                                </h3>                                      
                            </a>
                        </div>
                    </div>


                </div>




                <!-- /.row -->            

            </div>
        </section>

<?php echo $footer; ?>

</body>

</html>
