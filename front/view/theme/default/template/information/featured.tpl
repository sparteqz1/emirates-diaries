<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333;">
            <div class="container">
                <div class="intro-text">
                    <h2><?php echo $banner_title; ?></h2>                  

                </div>
            </div>
        </header>

        <!-- Featured Grid -->
        <section class="" >
            <div class="container">               
                <div class="row featured">
            <?php
            $count = 0;

            foreach($featurednews as $featuredkey => $featured) { ?>
                <div class="item <?php if($featuredkey == '0' || $featuredkey == '4') { echo 'col-lg-6'; } else { echo 'col-lg-3'; } ?>">
                    <a href="<?php echo $featured['href']; ?>" class="img-hieght" style="background: url('<?php echo $featured["thumb"]; ?>'); background-size: cover; background-position: center center;">
                        <div class="title">
                            <h4><?php echo $featured['category']; ?></h4>
                            <h2><?php echo $featured['name']; ?></h2>
                        </div>
                    </a>                        
                </div>
                <?php
                $count = $count+1;
                if($count==6){
                        break;
                }   
                if($count % 3 == 0) {
                echo '</div><div class="row featured">';
                } 
               
                   
            } ?>            
        </div>
        
            </div>
        </section>



        <!-- Category-article -->
        <section class="category-article">
            <div class="container">              

                <?php 
                    if(count($featurednews)>5){
                    for($i=6;$i<count($featurednews);$i++) { ?>
                        <div class="row item">
                            <div class="col-md-6 category-article-img">
                                <a href="<?php echo $featurednews[$i]['href']; ?>">
                                    <img class="img-fluid" src="<?php echo $featurednews[$i]['thumb']; ?>" alt="<?php echo $featurednews[$i]['name']; ?>">
                                </a>
                            </div>
                            <div class="col-md-6 category-article-content">
                                <h4><?php echo $featurednews[$i]['category']; ?></h4>
                                <h3><a href="<?php echo $featurednews[$i]['href']; ?>"><?php echo $featurednews[$i]['name']; ?></a> 
                                </h3>
                                <p><?php echo $featurednews[$i]['description']; ?></p>
                                <h5 class="author"><?php echo $featurednews[$i]['author']; ?> <span><?php echo $featurednews[$i]['date']; ?></span></h5>
                                <a class="btn btn-primary more" href="<?php echo $featurednews[$i]['href']; ?>"><?php echo $text_read_more; ?></a>
                            </div>
                        </div>
                <?php } } ?>
                
                
                





            </div>
        </section>

<!-- newsletter -->

<?php echo $newsletter; ?>

<?php echo $footer; ?>

</body>

</html>
