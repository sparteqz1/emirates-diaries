<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333; ">
            <div class="container">
                <div class="intro-text">


                    <h2><?php echo $heading_title; ?></h2>
                    <h5><?php echo $issue; ?></h5>

                </div>
            </div>
        </header>
        <section class="single-article" style="background: #eee;">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 article-details">
                        <h4><?php if(isset($author) && $author != "") echo 'By '.$author; ?><span><?php echo $ago; ?></span></h4>
                        <?php if(isset($photographers) && count($photographers) != 0) {
                        echo '<h6>Photo Courtesy : '; 
                            $resultstr = array();
                            foreach ($photographers as $result) {
                              $resultstr[] = $result['name'];
                            }
                            echo implode(", ",$resultstr);
                        echo '</h6>';
                        } ?>

                    </div>
                    <div class="col-md-6">

                        <?php echo $description; ?>


                    </div>
                    <div class="col-md-6 article-image">
                        <img class="img-fluid" src="<?php echo $image; ?>" alt="<?php echo $heading_title; ?>">
                        <div class="row">
                        <?php foreach ($images as $extraimages) { ?>
                        <div class="col-md-6">
                            <a class="img-hieght" style="background: url(<?php echo $extraimages['extra']; ?>); background-size: cover; background-position: center center;">
                            </a>
                        </div>
                        <?php } ?>                                                              
                        </div>                                                                    
                        
                    </div>
                    
                </div>
                
            </div>
        </section>





       <!-- Most Read -->
       <section class="most-read">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="section-heading"><?php echo $text_further_read; ?></h2> 
                    </div>
                </div> 
                <div class="col-lg-12">
                    <div class="row">
                        <?php foreach ($related_news as $relatedkey => $related) { ?>

                        <div class="<?php if($relatedkey =='2') echo 'col-lg-6 col-sm-6'; else echo 'col-lg-3 col-sm-6'; ?> articles">
                            <div class="card h-100">
                                <a class="card-img" href="<?php echo $related['href']; ?>">
                                    <img class="card-img-top" src="<?php echo $related['image']; ?>" alt="<?php echo $related['name']; ?>">
                                    <span>
                                        <?php echo $related['related_issue']; ?>
                                    </span>
                                </a>

                                <a href="<?php echo $related['href']; ?>" class="card-body">
                                    <h4 class="card-title">
                                        <?php echo $related['name']; ?> 
                                    </h4>  
                                    <h5 class="date"><?php echo $related['date']; ?></h5>
                                    <span class="author">
                                        <?php echo $related['author']; ?> 
                                    </span>
                                </a>
                            </div>
                        </div>


                        <?php } ?>
                        
                    </div>



                </div>
                <!-- /.row -->            

            </div>
        </section>
        
        <!-- Latest Magazine -->
        <section class="magazines">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="section-heading"><?php echo $text_latest_magazines; ?></h2> 
                    </div>
                </div> 

                <div class="row magazine-slider">

                    <?php echo $ed_magazines; ?>


                </div>




                <!-- /.row -->            

            </div>
        </section>

<?php echo $footer; ?>

</body>

</html>
