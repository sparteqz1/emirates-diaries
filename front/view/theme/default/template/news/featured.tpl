<section class="" >
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading"><?php echo $text_featured; ?></h2> 
            </div>
        </div> 
        <div class="row featured">
            <?php
            $featuredcount = '0';
            foreach($featurednews as $featuredkey => $featured) { ?>
            <div class="item <?php if($featuredkey == '0' || $featuredkey == '4') { echo 'col-lg-6'; } else { echo 'col-lg-3'; } ?>">
                <a href="<?php echo $featured['href']; ?>" class="img-hieght" style="background: url('<?php echo $featured["thumb"]; ?>'); background-size: cover; background-position: center center;">
                    <div class="title">
                        <h4><?php echo $featured['category']; ?></h4>
                        <h2><?php echo $featured['name']; ?></h2>
                    </div>
                </a>                        
            </div>
            <?php
            $featuredcount = $featuredcount+1;
            if($featuredcount % 3 == '0') {
            echo '</div><div class="row featured">';
        } } ?>            
        </div>
    </div>
</section>