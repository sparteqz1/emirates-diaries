<?php echo $header; ?>


<!-- Inner Bnner -->
<section class="banner-parallax overlay-dark" data-image-src="<?php echo $author_image; ?>" data-parallax="scroll"> 
    <div class="inner-banner">
        <h3><?php echo $heading_title; ?></h3>
        <ul class="tm-breadcrum">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
            <?php } ?>
        </ul>
    </div>
</section>
<!-- Inner Bnner -->
<div class="clearfix"></div>
<?php echo $flash; ?>
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-9 col-sm-8">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>



                    <!-- latest list posts -->
                    <div class="post-widget m-0">
                        <?php if ($news) { ?>
                        <!-- Post List -->

                        <div class="p-30 light-shadow white-bg">
                            <ul class="list-posts" id="catPage_listing">

                                <?php foreach ($news as $post) { ?>
                                <li>
                                    <div class="row">



                                        <!-- thumbnail -->
                                        <div class="col-sm-4 col-xs-5">
                                            <div class="post-thumb"> 
                                                <img src="<?php echo $post['thumb']; ?>" alt="<?php echo $post['name']; ?>">
                                                <div class="thumb-hover">
                                                    <div class="position-center-center">
                                                        <a href="<?php echo $post['href']; ?>" class="fa fa-link"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- thumbnail -->

                                        <!-- post detail -->
                                        <div class="col-sm-8 col-xs-7">

                                            <div class="post-content">
                                                <h4><a href="<?php echo $post['href']; ?>"><?php echo $post['name']; ?></a></h4>
                                                <ul class="post-meta">
                                                    <li><i class="fa fa-user"></i><?php echo $post['author']; ?></li>
                                                    <li><i class="fa fa-clock-o"></i><?php echo $post['date']; ?></li>
                                                    <li><i class="fa fa-eye"></i><?php echo $post['viewed']; ?></li>
                                                </ul>
                                                <p><?php echo $post['description']; ?> <a href="<?php echo $post['href']; ?>" class="read-more">read more...</a></p>
                                            </div>

                                        </div>
                                        <!-- post detail -->
                                    </div>
                                </li>
                                <?php } ?>

                            </ul>
                        </div>
                        <!-- Post List -->
                        <!-- Post List -->

                        <?php echo $pagination; ?>

                        <?php } else { ?>

                        <div class="p-30 light-shadow white-bg">
                            <p><?php echo $text_empty; ?></p>
                            <div class="buttons">
                                <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                            </div>
                             <div class="clearfix"></div>
                        </div>
                        <?php } ?>

                    </div>
                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->

                <!-- Sidebar -->
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <aside class="side-bar">
                        <?php echo $column_right; ?>



                        <!-- facebook widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">find us on facebook</h3>
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/newjoofficial/&tabs=timeline&width=300&height=220&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId=568059436732228" width="300" height="220" style="border:none;overflow:hidden"></iframe>
                            </div>
                        </div>
                        <!-- facebook widget -->

                        <?php echo $column_left; ?>


                    </aside>

                </div>
                <!-- Sidebar -->

            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>

<?php echo $footer; ?>