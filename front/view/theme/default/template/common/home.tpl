<?php echo $header; ?>

<!-- slider -->
<div class="slider">
    <?php
    $left_arrow = 0;
    foreach($issues as $key => $issue) { ?>
    <header class="masthead" style="background: url('<?php echo $issue["issue_slider"]; ?>'); background-size: cover; background-position: center center;">
                <div class="container">
                    <div class="intro-text">
                        <div class="into-text-box">
                            <span class="slide-prev"><?php if($key != 0) echo '#'.$left_arrow; ?></span>
                            <div class="intro-lead-in <?php if($key == '0') echo 'animated fadeInDown'; ?>"><?php if ($direction=='rtl') { echo 'العدد'; } else { echo 'Issue'; } ?> #<?php echo $issue['issue_order']; ?></div>
                            <div class="intro-heading <?php if($key == '0') echo 'animated fadeInUp'; ?>">
                               <?php echo $issue['name']; ?>
                            </div>
                            <span class="slide-next"><?php if($key+1 != count($issues)) echo '#'.$issues[$key+1]['issue_order']; ?></span>
                        </div>
                    </div>
                </div>
            </header>

<?php
$left_arrow = $issue['issue_order'];
}
    ?>
            
            

        </div>       

 <?php foreach($issues as $issuekey => $issue) { ?>

<section class="content <?php if($issuekey == '0') echo 'one'; else { echo 'two'; } ?>" data-id="<?php echo $issuekey+1; ?>"> 
    <div class="container">
        <div class="row slider-content"> 
            <?php foreach($issue['articles'] as $article) { ?>
            <div class="col-md-4 item">
                <a href="<?php echo $article['href']; ?>" class="img-hieght" style="background: url('<?php echo $article["thumb"]; ?>'); background-size: cover; background-position: top;">
                    <div class="title">
                        <!--<h4><?php echo $issue['name']; ?></h4>-->
                        <h4><?php echo $article['category']; ?></h4>
                        <h2><?php echo $article['name']; ?></h2>
                    </div>
                </a> 
            </div>
            <?php } ?>                               
        </div>
    </div>
</section>
<?php } ?>


<!-- Welcome -->
<section class="welcome">
    <div class="container">
        <div class="row">
            <div class="col-lg-1"> </div>
            <div class="col-lg-10">
                <h5><?php echo $text_welcome; ?></h5>
                <h2><?php echo $text_ed; ?></h2>
                <hr>
                <p><?php echo $text_welcome_desc; ?></p>
            </div>
        </div>               

    </div>
</section>

<!-- Featured Grid -->

<?php echo $featured; ?>


<!-- newsletter -->

<?php echo $newsletter; ?>

<!-- Latest Grid -->
<section class="latest">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading"><?php echo $text_latest_articles; ?></h2> 
            </div>
        </div> 
        <div class="col-lg-12">
            <div class="row">
                <?php foreach($latestnews as $latestkey => $latest) { ?>
                <div class="<?php if($latestkey =='0') echo 'col-lg-6 col-sm-6'; else echo 'col-lg-3 col-sm-6'; ?> articles">
                    <div class="card h-100">
                        <a class="card-img" href="<?php echo $latest['href']; ?>">
                            <img class="card-img-top" src="<?php echo $latest['thumb']; ?>" alt="<?php echo $latest['name']; ?>">
                            <span>
                                <!--<?php echo $latest['latest_issue']; ?>-->
                                <?php echo $latest['category']; ?>
                            </span>
                        </a>

                        <a href="<?php echo $latest['href']; ?>" class="card-body">
                            <h4 class="card-title">
                                <?php echo $latest['name']; ?>

                            </h4>  
                            <h5 class="date"><?php echo $latest['date']; ?></h5>
                            <span class="author">
                                <?php echo $latest['author']; ?> 
                            </span>
                        </a>
                    </div>
                </div>
                <?php } ?>

            </div>



        </div>
        <!-- /.row -->            

    </div>
</section>


<!-- Most Read -->
<section class="most-read">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading"><?php echo $text_most_read; ?></h2> 
            </div>
        </div> 
        <div class="col-lg-12">
            <div class="row">
                <?php foreach ($popularnews as $mr_key => $most_read) { ?>

                <div class="<?php if($mr_key =='2') echo 'col-lg-6 col-sm-6'; else echo 'col-lg-3 col-sm-6'; ?> articles">
                    <div class="card h-100">
                        <a class="card-img" href="<?php echo $most_read['href']; ?>">
                            <img class="card-img-top" src="<?php echo $most_read['thumb']; ?>" alt="<?php echo $most_read['name']; ?>">
                            <span>
                                <?php echo $most_read['category']; ?>
                            </span>
                        </a>

                        <a href="<?php echo $most_read['href']; ?>" class="card-body">
                            <h4 class="card-title">
                                <?php echo $most_read['name']; ?> 
                            </h4>  
                            <h5 class="date"><?php echo $most_read['date']; ?></h5>
                            <span class="author">
                                <?php echo $most_read['author']; ?> 
                            </span>
                        </a>
                    </div>
                </div>
                <?php } ?>
                
            </div>



        </div>
        <!-- /.row -->            

    </div>
</section>


<!-- Latest Magazine -->
<section class="magazines">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading"><?php echo $text_latest_magazines; ?></h2> 
            </div>
        </div> 

        <div class="row magazine-slider">

           <?php echo $ed_magazines; ?>

        </div>


        <!-- /.row -->            

    </div>
</section>

<?php echo $footer; ?>


</body>

</html>