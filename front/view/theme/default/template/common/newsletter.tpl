<section class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <h6><?php echo $text_subscribe; ?></h6>
                <h2 class="section-heading"><?php echo $text_newsletter; ?></h2> 
                <hr>
                <p><?php echo $text_signup; ?></p>

                <form method="post" name="subscribeform" id="subscribeform" action="javascript:void(0);" >
                    <div class="input-group">
                        <input type="text" id="subscribemail" name="subscribemail" class="form-control" placeholder="<?php echo $text_newsletter_placeholder; ?>" aria-label="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" id="subscribe-btn" type="button">→</button>
                        </span>
                    </div>
                </form>


            </div>

        </div> 


    </div>
</section>