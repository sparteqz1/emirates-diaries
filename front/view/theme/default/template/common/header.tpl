<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Designfort" />
    <title>
        <?php echo $title; ?>
    </title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
    <?php } ?>
    <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
       <![endif]-->
    <link href="front/view/theme/default/images/fav.ico" rel="icon" />
    <!-- Bootstrap core CSS -->
    <link href="front/view/javascript/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="front/view/javascript/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="front/view/javascript/vendor/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="front/view/javascript/vendor/slick/slick-theme.css" />
    <!-- Custom styles for this template -->
    <link href="front/view/theme/default/stylesheet/animate.css" rel="stylesheet">
    <link href="front/view/theme/default/stylesheet/ed.css" rel="stylesheet">
    <link href="front/view/theme/default/stylesheet/designfort.css" rel="stylesheet">
    <link href="front/view/theme/default/stylesheet/responsive.css" rel="stylesheet">
    <link href="front/view/theme/default/stylesheet/developer.css" rel="stylesheet">
    <?php if ($direction=='rtl') { ?>
    <link href="front/view/theme/default/stylesheet/style-rtl.css" rel="stylesheet">
    <?php } ?>
    <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
</head>

<body id="page-top">
    <div class="ed-loader"></div>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="<?php echo $home; ?>">
                    <img class="img-fluid logo-white" src="front/view/theme/default/images/LOGO-white.svg" alt="">
                    <img class="img-fluid logo-black" src="front/view/theme/default/images/LOGO-black.svg" alt=""> 
                    <img class="img-fluid logo-new" src="front/view/theme/default/images/header-logo.png" alt=""> 
                </a>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item for-mobile">
                        <a class="nav-link js-scroll-trigger back category-back" href="#"><span>←</span>Back</a>
                    </li>
                    <li class="nav-item hide-mobile">
                        <a class="nav-link js-scroll-trigger" href="<?php echo $home; ?>">
                            <?php echo $text_home; ?>
                        </a>
                    </li>
                    <li class="nav-item hide-mobile">
                        <a class="nav-link js-scroll-trigger" href="<?php echo $about; ?>">
                            <?php echo $menu_about; ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger hide-this" href="#">
                            <?php echo $text_magazine; ?>
                        </a>
                        <ul class="sub-menu">
                           <?php foreach($magazine_menu as $magazine) { ?>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="<?php echo $magazine['href']; ?>">
                                    <?php if ($direction=='rtl') { echo 'العدد'; } else { echo 'Issue'; } ?> #<?php echo $magazine['sort']; ?>
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                        
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger hide-this" href="#">
                            Categories
                        </a>
                        <ul class="sub-menu">
                           <?php foreach($menucategories as $menu) { ?>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="<?php echo $menu['href']; ?>">
                                    <?php echo $menu['name']; ?>
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                        
                    </li>
                    <li class="nav-item hide-mobile">
                        <a class="nav-link js-scroll-trigger" href="<?php echo $contact; ?>">
                            <?php echo $menu_contact; ?>
                        </a>
                    </li>
                    <!-- <?php foreach($menucategories as $menu) { ?>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="<?php echo $menu['href']; ?>">
                            <?php echo $menu['name']; ?>
                        </a>
                    </li>
                    <?php } ?> -->
                </ul>
                <div class="burger">
                    <div class="menuToggle">
                        <input class="menu-check" type="checkbox" />
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="lang">
                        <a href="">
                            <?php echo $language; ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- side menu -->
    <div class="sidemenu">
        <div class="burger">
            <div class="menuToggle">
                <input class="sidemenu-check" type="checkbox" />
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <ul>
            <li class="for-mobile">
                <a href="<?php echo $home; ?>">
                    <?php echo $text_home; ?>
                </a>
            </li>
            <li class="for-mobile">
                <a href="#" class="categories-button category-menu">Categories</a>
            </li>
            <li><a href="<?php echo $magazines; ?>"><?php echo $menu_all_magazines; ?></a></li>
            <li><a href="<?php echo $latest; ?>"><?php echo $menu_latest_articles; ?></a></li>
            <li><a href="<?php echo $featured; ?>"><?php echo $menu_featured_articles; ?></a></li>
            <!--<li><a href="<?php echo $top; ?>"><?php echo $menu_top_articles; ?></a></li>
            <li><a href="<?php echo $read; ?>"><?php echo $menu_most_read; ?></a></li>-->
            <li><a href="<?php echo $gallery; ?>"><?php echo $menu_photo_library; ?></a></li>
            <li><a href="<?php echo $videogallery; ?>">Video Gallery</a></li>
        </ul>
        <ul class="social-icons">
            <li>
                <a href="">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
            </li>
            <li>
                <a href="">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
            </li>
            <li>
                <a href="">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
            </li>
        </ul>
        <ul class="quicklinks">
            <li><a href="<?php echo $about; ?>"><?php echo $menu_about; ?></a></li>
            <li><a href="<?php echo $contact; ?>"><?php echo $menu_contact; ?></a></li>
            <!--<li><a href="#"><?php echo $menu_advertise; ?></a></li>-->
            <li><a href="<?php echo $terms; ?>"><?php echo $menu_privacy; ?></a></li>
            <li><a href="#"><?php echo $menu_career; ?></a></li>
        </ul>
    </div>