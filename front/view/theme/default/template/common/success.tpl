<?php echo $header; ?>

<div class="clearfix"></div>

<!-- Inner Bnner -->
<section class="banner-parallax overlay-dark"  data-image-src="front/view/theme/default/images/newjo-banner.png"  data-parallax="scroll"> 
    <div class="inner-banner">
        <h3><?php echo $heading_title; ?></h3>
        <ul class="tm-breadcrum">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
            <?php } ?>
        </ul>
    </div>
</section>

<!-- Main Content -->
<main class="main-wrap"> 
    <div class="theme-padding">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-sm-12">
                <?php echo $content_top; ?>
            </div>
        </div>
        <div class="clearfix"></div>


        <!-- error 404 -->
        <div class="error-holder font-roboto">
            <div class="error-detail error-2">
                
                <h3><?php echo $heading_title; ?></h3>
                <p><?php echo $text_message; ?></p>
                <a href="<?php echo $continue; ?>" class="btn red"><?php echo $button_continue; ?></a>
            </div>
        </div>
        <!-- error 404 -->
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-sm-12">
                <?php echo $content_bottom; ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>       
</main>
<!-- main content -->  
<div class="clearfix"></div>

<?php echo $footer; ?>