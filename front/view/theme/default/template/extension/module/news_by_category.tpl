
<div class="clearfix"></div>
<div class="post-widget m-0 white-bg">
    <div class="cate-tab-navs">
        <div class="container">
            <ul class="nav-justified">
                <?php foreach ($categories as $category) { ?>
                <li class="<?php echo $category['class']; ?>"><a data-target="#cat<?php echo $category['category_id']; ?>" data-toggle="tab"><?php echo $category['name']; ?></a></li>

                <?php } ?>


            </ul>
        </div>
    </div>
    <div class="cate-tab-content theme-padding">
        <div class="container">
            <div class="tab-content">
                <?php foreach ($categories as $category) { ?>


                <div class="tab-pane fade <?php echo $category['fade']; ?>" id="cat<?php echo $category['category_id']; ?>">
                    <div class="row slider-post">

                        <?php foreach ($category['news'] as $news) { ?>

                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="<?php echo $news['thumb']; ?>" alt="<?php echo $news['name']; ?>">
                                    <span class="post-badge"><?php echo $category['name']; ?></span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="<?php echo $news['href']; ?>" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="<?php echo $news['href']; ?>"><?php echo $news['name']; ?></a></h4>
                                    <p><?php echo $news['description']; ?></p>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i><?php echo $news['author']; ?></li>
                                        <li><i class="icon-clock"></i><?php echo $news['date']; ?></li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <?php } ?>

                    </div>
                </div>

                <?php } ?>
            </div>
        </div>
    </div>
</div>     
<div class="clearfix"></div>