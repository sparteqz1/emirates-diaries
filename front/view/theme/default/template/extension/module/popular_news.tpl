<h3><?php echo $heading_title; ?></h3>
<div class="row">
    <?php foreach ($news as $post) { ?>
    <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="product-thumb transition">
            <div class="image"><a href="<?php echo $post['href']; ?>"><img src="<?php echo $post['thumb']; ?>" alt="<?php echo $post['name']; ?>" title="<?php echo $post['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
                <h4><a href="<?php echo $post['href']; ?>"><?php echo $post['name']; ?></a></h4>
                <p><?php echo $post['description']; ?></p>
                
            </div>

        </div>
    </div>
    <?php } ?>
</div>
