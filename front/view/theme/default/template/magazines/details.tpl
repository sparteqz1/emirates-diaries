<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333;">
    <div class="container">
        <div class="intro-text">
            <h2><?php echo $heading_title; ?></h2>
        </div>
    </div>
</header>
<!-- Featured Grid -->
<?php if(count($issue_results) > 0 ) { 
foreach ($categories as $categorykey => $category) {
if(count($category['news']) > 0) { ?>
<section class="" >
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading"><?php echo $category['name']; ?></h2>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="row">
                <?php foreach ($category['news'] as $articlekey => $article) { ?>
                <div class="col-lg-3 col-sm-6 articles">
                    <div class="card h-100">
                        <a class="card-img" href="<?php echo $article['href']; ?>">
                            <img class="card-img-top" src="<?php echo $article['thumb']; ?>">
                            
                        </a>
                        <a href="<?php echo $article['href']; ?>" class="card-body">
                            <h4 class="card-title">
                                <?php echo $article['name']; ?>
                            </h4>
                            <h5 class="date"><?php echo $article['date']; ?></h5>
                            <span class="author">
                                <?php echo $article['author']; ?>
                            </span>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <!-- /.row -->
    </div>
</section>
<?php } } } else { ?>
<section class="newsletter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">                
                        <p>
                            NO ARTICLES FOUND WITH THIS ISSUE!
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <div class="clearfix"></div>
<?php } ?>

<?php echo $footer; ?>

</body>

</html>
