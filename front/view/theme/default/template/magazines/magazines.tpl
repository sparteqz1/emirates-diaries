<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333;">
    <div class="container">
        <div class="intro-text">

            <h2><?php echo $banner_title; ?></h2>

        </div>
    </div>
</header>

<!-- Latest Magazine -->
<section class="magazines">
    <div class="container">

        <div class="row">

            <?php echo $ed_magazines; ?>

        </div>         

    </div>
</section>

<!-- Featured Grid -->

<?php echo $featured; ?>

<!-- newsletter -->

<?php echo $newsletter; ?>

<?php echo $footer; ?>

</body>

</html>
