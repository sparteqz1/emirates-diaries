<?php echo $header; ?>


<!-- Inner Bnner -->
<section class="banner-parallax overlay-dark"  data-image-src="<?php echo $banner_logo; ?>" data-parallax="scroll"> 
    <div class="inner-banner">
        <h3><?php echo $heading_title; ?></h3>
        <ul class="tm-breadcrum">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
            <?php } ?>
        </ul>
    </div>
</section>
<!-- Inner Bnner -->
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-9 col-sm-8">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>





                    <!-- latest list posts -->
                    <div class="post-widget m-0" id="content">

                        <div class="row mb-20">
                            <div class="col-sm-12">
                                <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
                                <div class="row mb-10">
                                    <div class="col-sm-6">
                                        <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="category_id" class="form-control">
                                            <option value="0"><?php echo $text_category; ?></option>
                                            <?php foreach ($categories as $category_1) { ?>
                                            <?php if ($category_1['category_id'] == $category_id) { ?>
                                            <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
                                            <?php } ?>
                                            <?php foreach ($category_1['children'] as $category_2) { ?>
                                            <?php if ($category_2['category_id'] == $category_id) { ?>
                                            <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
                                            <?php } ?>
                                            <?php foreach ($category_2['children'] as $category_3) { ?>
                                            <?php if ($category_3['category_id'] == $category_id) { ?>
                                            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" name="description" value="1" id="description" />
                                        <input type="hidden" name="sub_category" value="1"  />
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />
                                    </div>

                                </div>



                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <?php if ($products) { ?>

                        <div class="row">


                            <div class="col-md-6 col-xs-6">
                                <div class="form-group input-group input-group-sm">
                                    <label class="input-group-addon" for="input-sort"><?php echo $text_sort; ?></label>
                                    <select id="input-sort" class="form-control" onchange="location = this.value;">
                                        <?php foreach ($sorts as $sorts) { ?>
                                        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <div class="form-group input-group input-group-sm">
                                    <label class="input-group-addon" for="input-limit"><?php echo $text_limit; ?></label>
                                    <select id="input-limit" class="form-control" onchange="location = this.value;">
                                        <?php foreach ($limits as $limits) { ?>
                                        <?php if ($limits['value'] == $limit) { ?>
                                        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="row">
                            <?php foreach ($products as $product) { ?>
                            <!-- product -->
                            <div class="col-md-4 col-sm-6 col-xs-4">

                                <!-- product holder -->
                                <div class="product-holder">
                                    <!-- product img -->
                                    <div class="product-thumb">
                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                                        <!-- product hover -->
                                        <div class="product-hover">
                                            <ul>
                                                <li><a href="javascript:;" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="fa fa-exchange"></a></li>
                                                <li><a href="<?php echo $product['href']; ?>" class="fa fa-eye" ></a></li>

                                            </ul>
                                        </div>
                                        <!-- product hover -->
                                    </div>
                                    <!-- product img -->

                                    <!-- product detail -->
                                    <div class="product-detail white-bg">
                                        <h5><?php echo $product['name']; ?></h5>
                                        <div class="price-review">

                                            <?php if ($product['price']) { ?>
                                            <span><?php echo $product['price']; ?></span>
                                            <?php } ?>

                                            <?php if ($product['rating']) { ?>
                                            <!-- reviews -->
                                            <ul class="reviews">
                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                <?php if ($product['rating'] < $i) { ?>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <?php } else { ?>
                                                <li><i class="fa fa-star"></i></li>
                                                <?php } ?>
                                                <?php } ?>
                                            </ul>
                                            <!-- reviews -->
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <!-- product detail -->

                                </div>
                                <!-- product holder -->

                            </div>
                            <!-- product -->

                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                        <?php echo $pagination; ?>
                        <div class="clearfix"></div>


                        <?php } else { ?>

                        <div class="p-30 light-shadow white-bg">
                            <p><?php echo $text_empty; ?></p>

                            <div class="clearfix"></div>
                        </div>
                        <?php } ?>

                    </div>
                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->

                <!-- Sidebar -->
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <aside class="side-bar">
                        <?php echo $column_right; ?>



                        <!-- facebook widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">find us on facebook</h3>
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/newjoofficial/&tabs=timeline&width=300&height=220&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId=568059436732228" width="300" height="220" style="border:none;overflow:hidden"></iframe>
                            </div>
                        </div>
                        <!-- facebook widget -->

                        <?php echo $column_left; ?>


                    </aside>

                </div>
                <!-- Sidebar -->

            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>
<script type="text/javascript">
    $('#button-search').bind('click', function () {
        url = 'index.php?route=product/search';
        var search = $('#content input[name=\'search\']').prop('value');
        if (search) {
            url += '&search=' + encodeURIComponent(search);
        }

        var category_id = $('#content select[name=\'category_id\']').prop('value');
        if (category_id > 0) {
            url += '&category_id=' + encodeURIComponent(category_id);
        }

        var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');
        if (sub_category) {
            url += '&sub_category=true';
        }

        var filter_description = $('#content input[name=\'description\']:checked').prop('value');
        if (filter_description) {
            url += '&description=true';
        }

        location = url;
    });
    $('#content input[name=\'search\']').bind('keydown', function (e) {
        if (e.keyCode == 13) {
            $('#button-search').trigger('click');
        }
    });
    $('select[name=\'category_id\']').on('change', function () {
        if (this.value == '0') {
            $('input[name=\'sub_category\']').prop('disabled', true);
        } else {
            $('input[name=\'sub_category\']').prop('disabled', false);
        }
    });
    $('select[name=\'category_id\']').trigger('change');
</script>
<?php echo $footer; ?>