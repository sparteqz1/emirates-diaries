<?php echo $header; ?>


<!-- Inner Bnner -->
<section class="banner-parallax overlay-dark"  data-image-src="<?php echo $banner_logo; ?>"  data-parallax="scroll"> 
    <div class="inner-banner">
        <h3><?php echo $heading_title; ?></h3>
        <ul class="tm-breadcrum">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
            <?php } ?>
        </ul>
    </div>
</section>
<!-- Inner Bnner -->
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-9 col-sm-8">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>



                    <!-- latest list posts -->
                    <div class="post-widget m-0">
                        <?php if ($categories) { ?>
                        <!-- Post List -->

                        <!-- Heading -->
                        <div class="primary-heading">
                            <h2><?php echo $heading_title; ?></h2>
                        </div>
                        <!-- Heading -->

                        <div class="p-30 light-shadow white-bg">
                            <div id="post-slider-2">
                                <?php foreach ($categories as $category) { ?>

                                <?php if ($category['manufacturer']) { ?>

                                <?php foreach ($category['manufacturer'] as $manufacturer) { ?>

                                <div class="post style-1">

                                    <!-- thumbnail -->
                                    <div class="post-thumb"> 
                                        <img src="<?php echo $manufacturer['thumb']; ?>" alt="">

                                        <!-- post thumb hover -->
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="<?php echo $manufacturer['href']; ?>" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                        <!-- post thumb hover -->

                                    </div>
                                    <!-- thumbnail -->
                                    <div class="post-content">

                                        <h5 class="m-0 text-center"><a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?> </a></h5>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php } ?>
                                <?php } ?>
                            </div>

                        </div>
                        <!-- Post List -->

                        <?php } else { ?>

                        <div class="p-30 light-shadow white-bg">
                            <p><?php echo $text_empty; ?></p>
                            <div class="buttons">
                                <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php } ?>

                    </div>
                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->

                <!-- Sidebar -->
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <aside class="side-bar">
                        <?php echo $column_right; ?>



                        <!-- facebook widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">find us on facebook</h3>
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/newjoofficial/&tabs=timeline&width=300&height=220&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId=568059436732228" width="300" height="220" style="border:none;overflow:hidden"></iframe>
                            </div>
                        </div>
                        <!-- facebook widget -->

                        <?php echo $column_left; ?>


                    </aside>

                </div>
                <!-- Sidebar -->

            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>

<?php echo $footer; ?>