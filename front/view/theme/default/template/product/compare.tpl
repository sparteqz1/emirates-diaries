<?php echo $header; ?>


<!-- Inner Bnner -->
<section class="banner-parallax overlay-dark"  data-image-src="<?php echo $banner_logo; ?>"  data-parallax="scroll"> 
    <div class="inner-banner">
        <h3><?php echo $heading_title; ?></h3>
        <ul class="tm-breadcrum">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
            <?php } ?>
        </ul>
    </div>
</section>
<!-- Inner Bnner -->
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-12 col-sm-12">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>



                    <!-- latest list posts -->
                    <div class="post-widget m-0">

                        <?php if ($success) { ?>
                        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <?php if ($products) { ?>
                        <!-- Post List -->

                        <!-- Heading -->
                        <div class="primary-heading">
                            <h2><?php echo $heading_title; ?></h2>
                        </div>
                        <!-- Heading -->

                        <div class="p-30 light-shadow white-bg">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $text_product; ?></strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $text_name; ?></td>
                                        <?php foreach ($products as $product) { ?>
                                        <td><a href="<?php echo $product['href']; ?>"><strong><?php echo $product['name']; ?></strong></a></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td><?php echo $text_image; ?></td>
                                        <?php foreach ($products as $product) { ?>
                                        <td class="text-center"><?php if ($product['thumb']) { ?>
                                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
                                            <?php } ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td><?php echo $text_price; ?></td>
                                        <?php foreach ($products as $product) { ?>
                                        <td><?php echo $product['price']; ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td><?php echo $text_model; ?></td>
                                        <?php foreach ($products as $product) { ?>
                                        <td><?php echo $product['model']; ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td><?php echo $text_manufacturer; ?></td>
                                        <?php foreach ($products as $product) { ?>
                                        <td><?php echo $product['manufacturer']; ?></td>
                                        <?php } ?>
                                    </tr>

                                    <?php if ($review_status) { ?>
                                    <tr>
                                        <td><?php echo $text_rating; ?></td>
                                        <?php foreach ($products as $product) { ?>
                                        <td class="rating"><?php for ($i = 1; $i <= 5; $i++) { ?>
                                            <?php if ($product['rating'] < $i) { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                            <?php } else { ?>
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                            <?php } ?>
                                            <?php } ?>
                                            <br />
                                            <?php echo $product['reviews']; ?></td>
                                        <?php } ?>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td><?php echo $text_summary; ?></td>
                                        <?php foreach ($products as $product) { ?>
                                        <td class="description"><?php echo $product['description']; ?></td>
                                        <?php } ?>
                                    </tr>

                                </tbody>
                                <?php foreach ($attribute_groups as $attribute_group) { ?>
                                <thead>
                                    <tr>
                                        <td colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                    </tr>
                                </thead>
                                <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                                <tbody>
                                    <tr>
                                        <td><?php echo $attribute['name']; ?></td>
                                        <?php foreach ($products as $product) { ?>
                                        <?php if (isset($product['attribute'][$key])) { ?>
                                        <td><?php echo $product['attribute'][$key]; ?></td>
                                        <?php } else { ?>
                                        <td></td>
                                        <?php } ?>
                                        <?php } ?>
                                    </tr>
                                </tbody>
                                <?php } ?>
                                <?php } ?>
                                <tr>
                                    <td></td>
                                    <?php foreach ($products as $product) { ?>
                                    <td><a href="<?php echo $product['remove']; ?>" class="btn btn-danger btn-block"><?php echo $button_remove; ?></a></td>
                                    <?php } ?>
                                </tr>
                            </table>

                        </div>
                        <!-- Post List -->

                        <?php } else { ?>

                        <div class="p-30 light-shadow white-bg">
                            <p><?php echo $text_empty; ?></p>
                            <div class="buttons">
                                <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php } ?>

                    </div>
                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->


            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>

<?php echo $footer; ?>