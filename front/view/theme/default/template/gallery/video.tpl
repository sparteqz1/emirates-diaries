<?php echo $header; ?>

<header class="masthead inner-page" style="background: #333; ">
    <div class="container">
        <div class="intro-text">


            <h2><?php echo $banner_title; ?></h2>


        </div>
    </div>
</header>

<!-- Featured Grid -->
<section class="" >
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-heading"><?php echo $text_featured; ?></h2> 
            </div>
        </div> 
        <div class="row featured">
            <?php foreach($galleries as $gallery) { ?>
            <div class="col-lg-4 gallery-box item">
                <div class="gallery-item">
                    
                    <a data-fancybox="video-gallery" href="https://www.youtube.com/watch?v=<?php echo $gallery['video']; ?>" >
                        <img class="" src="<?php echo getYouTubeThumbnailImage($gallery['video']); ?>" alt="<?php echo $gallery['name']; ?>">
                    </a>

                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php echo $footer; ?>

<?php 
function getYouTubeThumbnailImage($video_id) {
    return "http://i3.ytimg.com/vi/$video_id/hqdefault.jpg";
}
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" /> -->
</body>

</html>
