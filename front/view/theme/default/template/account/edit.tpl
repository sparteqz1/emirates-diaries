<?php echo $header; ?>

<!-- Inner Bnner -->
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-12">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>

                    <!-- contact map -->
                    <div class="post-widget">

                        <!-- Heading -->
                        <div class="primary-heading">
                            <h2><?php echo $heading_title; ?></h2>
                        </div>
                        <!-- Heading -->

                        <div class="clearfix"></div>


                        <?php if ($error_warning) { ?>
                        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                        <?php } ?>
                        <div class="clearfix"></div>


                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="col-sm-12">
                                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <fieldset>
                                        <legend><?php echo $text_your_details; ?></legend>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?> </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                                                <?php if ($error_firstname) { ?>
                                                <div class="text-danger"><?php echo $error_firstname; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                                                <?php if ($error_lastname) { ?>
                                                <div class="text-danger"><?php echo $error_lastname; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                                            <div class="col-sm-10">
                                                <input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                                                <?php if ($error_email) { ?>
                                                <div class="text-danger"><?php echo $error_email; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                                            <div class="col-sm-10">
                                                <input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                                                <?php if ($error_telephone) { ?>
                                                <div class="text-danger"><?php echo $error_telephone; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </fieldset>
                                    <div class="buttons clearfix">
                                        <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                                        <div class="pull-right">
                                            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->


            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>

<?php echo $footer; ?>