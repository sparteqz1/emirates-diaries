

<?php echo $header; ?>

<!-- Inner Bnner -->
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-12">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>

                    <!-- contact map -->
                    <div class="post-widget">

                        <!-- Heading -->
                        <div class="primary-heading">
                            <h2><?php echo $heading_title; ?></h2>
                        </div>
                        <!-- Heading -->

                        <div class="clearfix"></div>

                        <?php if ($error_warning) { ?>
                        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <p><?php echo $text_account_already; ?></p>
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <fieldset id="account">
                                <legend><?php echo $text_your_details; ?></legend>
                                <input type="hidden" name="customer_group_id" value="<?php echo $customer_group_id; ?>" />
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                                        <?php if ($error_firstname) { ?>
                                        <div class="text-danger"><?php echo $error_firstname; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                                        <?php if ($error_lastname) { ?>
                                        <div class="text-danger"><?php echo $error_lastname; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                                    <div class="col-sm-10">
                                        <input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                                        <?php if ($error_email) { ?>
                                        <div class="text-danger"><?php echo $error_email; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                                    <div class="col-sm-10">
                                        <input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                                        <?php if ($error_telephone) { ?>
                                        <div class="text-danger"><?php echo $error_telephone; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>

                            </fieldset>
                            <fieldset id="address">
                                <legend><?php echo $text_your_address; ?></legend>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-company"><?php echo $entry_company; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>" id="input-company" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-address-1"><?php echo $entry_address_1; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" class="form-control" />
                                        <?php if ($error_address_1) { ?>
                                        <div class="text-danger"><?php echo $error_address_1; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city" class="form-control" />
                                        <?php if ($error_city) { ?>
                                        <div class="text-danger"><?php echo $error_city; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-postcode"><?php echo $entry_postcode; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control" />
                                        <?php if ($error_postcode) { ?>
                                        <div class="text-danger"><?php echo $error_postcode; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
                                    <div class="col-sm-10">
                                        <select name="country_id" id="input-country" class="form-control">
                                            <option value=""><?php echo $text_select; ?></option>
                                            <?php foreach ($countries as $country) { ?>
                                            <?php if ($country['country_id'] == $country_id) { ?>
                                            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <?php if ($error_country) { ?>
                                        <div class="text-danger"><?php echo $error_country; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-zone"><?php echo $entry_zone; ?></label>
                                    <div class="col-sm-10">
                                        <select name="zone_id" id="input-zone" class="form-control">
                                        </select>
                                        <?php if ($error_zone) { ?>
                                        <div class="text-danger"><?php echo $error_zone; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>

                            </fieldset>
                            <fieldset>
                                <legend><?php echo $text_your_password; ?></legend>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
                                    <div class="col-sm-10">
                                        <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                                        <?php if ($error_password) { ?>
                                        <div class="text-danger"><?php echo $error_password; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                                    <div class="col-sm-10">
                                        <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
                                        <?php if ($error_confirm) { ?>
                                        <div class="text-danger"><?php echo $error_confirm; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend><?php echo $text_newsletter; ?></legend>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $entry_newsletter; ?></label>
                                    <div class="col-sm-10">
                                        <?php if ($newsletter) { ?>
                                        <label class="radio-inline">
                                            <input type="radio" name="newsletter" value="1" checked="checked" />
                                            <?php echo $text_yes; ?></label>
                                        <label class="radio-inline">
                                            <input type="radio" name="newsletter" value="0" />
                                            <?php echo $text_no; ?></label>
                                        <?php } else { ?>
                                        <label class="radio-inline">
                                            <input type="radio" name="newsletter" value="1" />
                                            <?php echo $text_yes; ?></label>
                                        <label class="radio-inline">
                                            <input type="radio" name="newsletter" value="0" checked="checked" />
                                            <?php echo $text_no; ?></label>
                                        <?php } ?>
                                    </div>
                                </div>
                            </fieldset>
                            <?php echo $captcha; ?>

                            <div class="buttons">
                                <div class="pull-right">
                                    <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                                </div>
                            </div>

                        </form>


                    </div>

                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->


            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>

<script type="text/javascript">
    $('select[name=\'country_id\']').on('change', function () {
        $.ajax({
            url: 'index.php?route=account/account/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                if (json['postcode_required'] == '1') {
                    $('input[name=\'postcode\']').parent().parent().addClass('required');
                } else {
                    $('input[name=\'postcode\']').parent().parent().removeClass('required');
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] && json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('select[name=\'zone_id\']').html(html);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'country_id\']').trigger('change');
</script>
<?php echo $footer; ?>