
<?php echo $header; ?>

<!-- Inner Bnner -->
<div class="clearfix"></div>

<!-- Main Content -->
<main class="wrapper"> 
    <div class="theme-padding">
        <div class="container">

            <!-- Main Content Row -->
            <div class="row">

                <div class="col-md-12">


                    <div class="clearfix"></div>
                    <?php echo $content_top; ?>
                    <div class="clearfix"></div>

                    <!-- contact map -->
                    <div class="post-widget">

                        <!-- Heading -->
                        <div class="primary-heading">
                            <h2><?php echo $heading_title; ?></h2>
                        </div>
                        <!-- Heading -->

                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="col-sm-12">
                                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?php echo $entry_newsletter; ?></label>
                                            <div class="col-sm-10">
                                                <?php if ($newsletter) { ?>
                                                <label class="radio-inline">
                                                    <input type="radio" name="newsletter" value="1" checked="checked" />
                                                    <?php echo $text_yes; ?> </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="newsletter" value="0" />
                                                    <?php echo $text_no; ?></label>
                                                <?php } else { ?>
                                                <label class="radio-inline">
                                                    <input type="radio" name="newsletter" value="1" />
                                                    <?php echo $text_yes; ?> </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="newsletter" value="0" checked="checked" />
                                                    <?php echo $text_no; ?></label>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="buttons clearfix">
                                        <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                                        <div class="pull-right">
                                            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <?php echo $content_bottom; ?>
                    <div class="clearfix"></div>

                </div>
                <!-- Content -->


            </div>   
            <!-- Main Content Row -->

        </div>
    </div>  



</main>

<div class="clearfix"></div>

<?php echo $footer; ?>