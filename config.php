<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/bitback/emirates-diaries/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/bitback/emirates-diaries/');

// DIR
define('DIR_APPLICATION', 'D:/xampp/htdocs/bitback/emirates-diaries/front/');
define('DIR_SYSTEM', 'D:/xampp/htdocs/bitback/emirates-diaries/system/');
define('DIR_IMAGE', 'D:/xampp/htdocs/bitback/emirates-diaries/image/');
define('DIR_LANGUAGE', 'D:/xampp/htdocs/bitback/emirates-diaries/front/language/');
define('DIR_TEMPLATE', 'D:/xampp/htdocs/bitback/emirates-diaries/front/view/theme/');
define('DIR_CONFIG', 'D:/xampp/htdocs/bitback/emirates-diaries/system/config/');
define('DIR_CACHE', 'D:/xampp/htdocs/bitback/emirates-diaries/system/storage/cache/');
define('DIR_DOWNLOAD', 'D:/xampp/htdocs/bitback/emirates-diaries/system/storage/download/');
define('DIR_LOGS', 'D:/xampp/htdocs/bitback/emirates-diaries/system/storage/logs/');
define('DIR_MODIFICATION', 'D:/xampp/htdocs/bitback/emirates-diaries/system/storage/modification/');
define('DIR_UPLOAD', 'D:/xampp/htdocs/bitback/emirates-diaries/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'emirates-diaries');
define('DB_PORT', '3306');
define('DB_PREFIX', 'sp_');
